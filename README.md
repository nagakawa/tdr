## TDR

Moved to [GitLab](https://gitlab.com/nagakawa/tdr).

TDR Dodges Rain.

### Installation

For other compilers / IDEs, you're unfortunately on your own. Note that your C++ compiler must support `#pragma once`, which it probably does.

#### CMake + Conan

    pip install conan
    conan remote add public-conan https://api.bintray.com/conan/bincrafters/public-conan
    cd build
    conan install .. --build fmt
    cd ..
    # add options as you please
    cmake .
    make

### Looking Around

The directory structure is as such:

    3rdparty/
      agc/
        (our dependencies)
      (other dependencies)
    include/
      AGL/
      FileUtil/
      TDR/
    src/
      AGL/
      FileUtil/
      TDR/
    AGLTest/
    TDRTest/

At the time of writing, TDR consists of three components:

* FileUtil: a library to settle encoding differences between Windows and other OSes
* AGL: a C++ wrapper around OpenGL, plus more extra goodies.
* TDR: the engine itself (WIP)

Plus a few test components:

* AGLTest: test program for AGL.
* TDRTest: test program for TDR.

### License

TDR itself is licensed under the MIT license; see `LICENSE` for details. As far as I know, I'm not frooping up with licensing. If I am frooping around with licenses, then ***let me know ASAP!***

See the `3rdparty/licences` directory for licences of third-party libraries.

### Resource Credits

Sample images (crate, awesome face), used in AGL test, by [Learn OpenGL](http://www.learnopengl.com), licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

[kardinal.ttf](http://conlinguistics.org/arka/e_data_atom_1.html) by Seren Arbazard; [alef.ttf](https://www9.atwiki.jp/hrain/pages/79.html) by Nias Avelantis.
