#pragma once

#include <memory>
#include <vector>

// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H

#include <AGL/text/XText.h>
#include <TDR/Game.h>
#include <TDR/particle/ParticleManager.h>

#include "TestMessage.h"
#include "TestSharedData.h"

class TDRTestView;

class TDRTestModel
    : public tdr::GameModel<
          TDRTestModel, TDRTestView, TestSharedData, TDRTestMsg> {
public:
  TDRTestModel(ModelOpts&& opts);
  void myTick() override;
  void onGrazeOnce() override;
  void onHit() override;
  void onShot() override;
  void onBomb() override;
  void onBossSceneStart(tdr::BossScene& scene) override;
  void onBossSceneAttackStart(
      tdr::BossScene& scene, const tdr::BossAttack& attack) override;
  void onBossSceneBreak(
      tdr::BossScene& scene, const tdr::BossAttack& attack,
      tdr::BossScene::PlayStat st) override;
  void onBossSceneEnd(tdr::BossScene& scene) override;
  void onSpawnErasureItem(size_t nBullets, const tdr::PV* positions) override;
  void onCollectItem(size_t nItems, tdr::ItemRec* items) override;

private:
};

class TDRTestView : public tdr::GameView<
                        TDRTestModel, TDRTestView, TestSharedData, TDRTestMsg> {
public:
  struct DisplayGauge {
    DisplayGauge() = default;
    DisplayGauge(const char* text, std::function<std::string()>&& generator) :
        text(text), generator(std::move(generator)) {}
    const char* text;
    std::function<std::string()> generator;
    size_t ri;
  };
  using tdr::GameView<
      TDRTestModel, TDRTestView, TestSharedData, TDRTestMsg>::GameView;
  void myinit() override;
  void myreset() override;
  void mainLoop() override;
  void readKeys();
  void myrender() override;
  void myrenderf() override;
  void myPostRender() override;
  void renderBossSceneFront(const tdr::BossScene& scene) override;
  void renderBossSceneBack(const tdr::BossScene& scene) override;
  void onEnd() override;
  void receive(const Message& m) override {
    std::visit([this](const auto& arg) { receive(arg); }, m);
  }
  std::unique_ptr<tdr::Stage<TDRTestModel>> getStage(size_t stageno) override;

private:
  std::vector<DisplayGauge> dgs;
  agl::XText hud;
  agl::XText bossSceneText;
  std::unique_ptr<agl::Sprite2D> bossbarSprite;
  agl::Texture shots, playerShots, enemy, items, particles, bossbar;
  agl::Texture backShotsheet;
  std::unique_ptr<tdr::ParticleManager> pm;
  void drawChar(tdr::ShotID id, glm::u8vec4 blend, tdr::PV position);
  void scorePopup(int64_t score, tdr::PV position, glm::u8vec4 blend);
  void receive(const Graze& g);
  void receive(Hit);
  void receive(End);
  void receive(const ScorePopup& p);
  void receive(SetBossSprite s);
  void receive(BeginCutin c);
  void receive(SpellBonus b);
};
