#pragma once

#include <memory>

// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H

#include <AGL/FBO.h>
#include <AGL/Sprite2D.h>
#include <AGL/text/Font.h>

class TestSharedData {
public:
  TestSharedData();
  FT_Library ftl;
  std::unique_ptr<agl::Font> alef, inje;
  std::unique_ptr<agl::Sprite2D> pauseScreen;
  agl::Texture fboTex;
  agl::Texture fboTexMS;
  agl::FBO fboMS;
  agl::FBO fboSS;
};
