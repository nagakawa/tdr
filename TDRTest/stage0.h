#pragma once

#include <AGL/Texture.h>
#include <TDR/Stage.h>

#include "TestSharedData.h"

class TDRTestModel;

class TDRTestStage0 : public tdr::Stage<TDRTestModel> {
public:
  void initialise() override;
  void mainLoop() override;

protected:
private:
  void startBoss();
};
