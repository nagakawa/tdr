#pragma once

#include <variant>

#include <TDR/bullet/defs.h>

struct Graze {
  tdr::PV position;
};

struct Hit {};

struct End {};

struct ScorePopup {
  int64_t value;
  tdr::PV position;
  glm::u8vec4 blend;
};

struct SetBossSprite {
  const char* path;
};

struct BeginCutin {
  uint32_t id;
};

struct SpellBonus {
  int64_t value;
};

using TDRTestMsg =
    std::variant<Graze, Hit, ScorePopup, SetBossSprite, BeginCutin, SpellBonus>;
