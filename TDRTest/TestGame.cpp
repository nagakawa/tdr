#include "TestGame.h"

#include <inttypes.h>

#include <charconv>
#include <iostream>
#include <optional>

#include <fmt/core.h>

#include <AGL/ScenedGLFWApplication.h>
#include <AGL/sound/Mixer.h>
#include <AGL/text/gaussianblur.h>
#include <TDR/item/defs.h>
#include <TDR/yield.h>

#include "TestPause.h"
#include "constants.h"
#include "stage0.h"

const char* sounds[] = {"graze", "death", "shoot", "stage", "tdr_pause_interj"};

// We have to declare these arrays out of line because C++ doesn't support
// compound literals unlike C99.
static const agl::UIRect16 pulsatingOrbRects[] = {
    {0, 80, 16, 96},
    {16, 80, 32, 96},
    {32, 80, 48, 96},
    {48, 80, 64, 96},
};
static const uint32_t pulsatingOrbTimes[] = {4, 8, 12, 16};

static const tdr::Graphic shotGraphics[] = {
    {
        /* .texCoords = */ {0, 0, 16, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 4,
    },
    {
        /* .texCoords = */ {16, 0, 32, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 4,
    },
    {
        /* .texCoords = */ {32, 0, 48, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 4,
    },
    {
        /* .texCoords = */ {48, 0, 64, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 4,
    },
    {
        /* .texCoords = */ {64, 0, 80, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 4,
    },
    {
        /* .texCoords = */ {0, 16, 16, 32},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
    },
    {
        /* .texCoords = */ {16, 16, 32, 32},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
    },
    {
        /* .texCoords = */ {32, 16, 48, 32},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
    },
    {
        /* .texCoords = */ {48, 16, 64, 32},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
    },
    {
        /* .texCoords = */ {64, 16, 80, 32},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
    },
    {
        /* .texCoords = */ {0, 32, 32, 64},
        /* .visualRadius = */ 16,
        /* .collisionRadius = */ 8,
    },
    {
        /* .texCoords = */ {0, 64, 16, 80},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
        /* .delayColour = */ {255, 255, 255},
        /* .alpha = */ 255,
        /* .renderMode = */ agl::BMIndex::alpha,
        /* .intrinsicAngularVelocity = */ kfp::frac32::fraction(1, 80),
    },
    {
        /* .texCoords = */ {16, 64, 32, 80},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
        /* .delayColour = */ {255, 255, 255},
        /* .alpha = */ 255,
        /* .renderMode = */ agl::BMIndex::alpha,
        /* .intrinsicAngularVelocity = */ kfp::frac32::fraction(1, 80),
    },
    {
        /* .texCoords = */ {32, 64, 48, 80},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
        /* .delayColour = */ {255, 255, 255},
        /* .alpha = */ 255,
        /* .renderMode = */ agl::BMIndex::alpha,
        /* .intrinsicAngularVelocity = */ kfp::frac32::fraction(1, 80),
    },
    {
        /* .texCoords = */ {48, 64, 64, 80},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
        /* .delayColour = */ {255, 255, 255},
        /* .alpha = */ 255,
        /* .renderMode = */ agl::BMIndex::alpha,
        /* .intrinsicAngularVelocity = */ kfp::frac32::fraction(1, 80),
    },
    {
        /* .texCoords = */ {64, 64, 80, 80},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
        /* .delayColour = */ {255, 255, 255},
        /* .alpha = */ 255,
        /* .renderMode = */ agl::BMIndex::alpha,
        /* .intrinsicAngularVelocity = */ kfp::frac32::fraction(1, 80),
    },
    {
        /* .texCoords = */ {4, pulsatingOrbRects, pulsatingOrbTimes},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 3,
        /* .delayColour = */ {90, 255, 255},
    },
};

static const tdr::EnemyGraphic enemyGraphics[] = {
    {
        // pink fairy
        /* .texCoords = */ {0, 0, 16, 31},
        /* .collisionRadiusToPlayer = */ 6,
        /* .collisionRadiusToShot = */ 12,
    },
    {
        // funny robot
        /* .texCoords = */ {64, 0, 96, 32},
        /* .collisionRadiusToPlayer = */ 12,
        /* .collisionRadiusToShot = */ 20,
    },
};

static const tdr::Graphic playerShotGraphics[] = {
    {
        // silver arrow
        /* .texCoords = */ {0, 0, 16, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 8,
        /* .delayColour = */ {224, 224, 224},
        /* .alpha = */ 140,
    },
    {
        // steel arrow
        /* .texCoords = */ {16, 0, 32, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 8,
        /* .delayColour = */ {144, 144, 144},
        /* .alpha = */ 140,
    },
    {
        // spiked seed
        /* .texCoords = */ {32, 0, 48, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 8,
        /* .delayColour = */ {128, 200, 128},
        /* .alpha = */ 140,
    },
    {
        // throwing needle
        /* .texCoords = */ {48, 0, 64, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 8,
        /* .delayColour = */ {255, 255, 192},
    },
    {
        // solar prominence
        /* .texCoords = */ {64, 0, 80, 16},
        /* .visualRadius = */ 8,
        /* .collisionRadius = */ 8,
        /* .delayColour = */ {255, 255, 192},
        /* .alpha = */ 80,
    },
    {
        // star – ideally we'd like to add colours to this, but blend colours
        // on bullets are nyi
        /* .texCoords = */ {128, 0, 160, 32},
        /* .visualRadius = */ 16,
        /* .collisionRadius = */ 24,
        /* .delayColour = */ {255, 255, 255},
    },
};

static const tdr::Graphic delayCloud = {
    /* .texCoords = */ {0, 992, 32, 1024},
    /* .visualRadius = */ 16,
    /* .collisionRadius = */ 16,
    /* .delayColour = */ {255, 255, 255},
    /* .alpha = */ 130,
};

static const tdr::ItemTraits itemTraits[] = {
    {
        // bullet cancel item
        /* .texCoords = */ {10, 10, 20, 20},
        /* .bmIndex = */ agl::BMIndex::alpha,
        /* .initialYVelocity = */ 0,
        /* .maxSpeed = */ 10,
        /* .movePattern = */ tdr::ItemMovePattern::moveToPlayer,
        /* .attractedOnPoc = */ true,
    },
};

#define DECL_DIGIT_GRAPHIC(n) \
  { \
    /* texcoords = */ {16 * n, 32, 16 + 16 * n, 48}, \
        /* .blend = */ {255, 255, 255, 255}, \
        /* .bmIndex = */ agl::BMIndex::alpha, \
  }
static const tdr::ParticleGraphic particleGraphics[] = {
    {
        /* .texcoords = */ {0, 0, 15, 15},
        /* .blend = */ {255, 255, 255, 255},
        /* .bmIndex = */ agl::BMIndex::alpha,
    },
    {
        /* texcoords = */ {0, 16, 8, 23},
        /* .blend = */ {255, 255, 255, 255},
        /* .bmIndex = */ agl::BMIndex::alpha,
    },
    {
        /* texcoords = */ {16, 0, 48, 32},
        /* .blend = */ {255, 255, 255, 255},
        /* .bmIndex = */ agl::BMIndex::alpha,
    },
    DECL_DIGIT_GRAPHIC(0),
    DECL_DIGIT_GRAPHIC(1),
    DECL_DIGIT_GRAPHIC(2),
    DECL_DIGIT_GRAPHIC(3),
    DECL_DIGIT_GRAPHIC(4),
    DECL_DIGIT_GRAPHIC(5),
    DECL_DIGIT_GRAPHIC(6),
    DECL_DIGIT_GRAPHIC(7),
    DECL_DIGIT_GRAPHIC(8),
    DECL_DIGIT_GRAPHIC(9),
    DECL_DIGIT_GRAPHIC(10),
};

struct SpellcardInfo {
  std::optional<std::string_view> name;
};

static const SpellcardInfo spellcardInfo[] = {
    {
        /* .name = */ std::nullopt,
    },
    {
        /* .name = */ "falm e yui \"yuiar e retl\"",
    },
    {
        /* .name = */ "maladi \"esta kaen teeze\"",
    },
};

static int64_t
getSpellBonusAt(int64_t bonus, uint32_t currentTime, uint32_t maxTime) {
  int64_t rawScore =
      (3 * bonus +
       7 * bonus * std::min(maxTime - 300, currentTime) / (maxTime - 300)) /
      10;
  return (rawScore / 10) * 10;
}

TDRTestModel::TDRTestModel(ModelOpts&& opts) :
    tdr::GameModel<TDRTestModel, TDRTestView, TestSharedData, TDRTestMsg>(
        std::move(opts)) {
  tdr::Shotsheet shotsheet;
  shotsheet.setRects(
      sizeof(shotGraphics) / sizeof(shotGraphics[0]), shotGraphics);
  this->setShotsheet(std::move(shotsheet));
  tdr::Shotsheet playerShotSheet;
  playerShotSheet.setRects(
      sizeof(playerShotGraphics) / sizeof(playerShotGraphics[0]),
      playerShotGraphics);
  this->setPlayerShotsheet(std::move(playerShotSheet));
  tdr::EnemyShotsheet enemySheet;
  enemySheet.setRects(
      sizeof(enemyGraphics) / sizeof(enemyGraphics[0]), enemyGraphics);
  this->setEnemyShotsheet(std::move(enemySheet));
  tdr::ItemShotsheet itemSheet;
  itemSheet.setRects(sizeof(itemTraits) / sizeof(itemTraits[0]), itemTraits);
  this->setItemShotsheet(std::move(itemSheet));
  // Spawn task to manage shots
  getManager().spawn([this]() {
    using namespace kfp::literals;
    tdr::BulletList& bl = getPlayerBulletList();
    while (true) {
      tdr::PV pc = this->getPlayer().getPosition();
      if (isShooting()) {
        for (int i = -2; i < 3; ++i) {
          tdr::BulletHandle h = bl.createShotA1(
              pc - tdr::PV(0, -10), 6, "0.75"_frac32 + "0.0083"_frac32 * i,
              player_shot::flare, 0);
          h.setDamage(15);
        }
        tdr::cwait(4);
      }
      kcr::yield();
    }
  });
}

void TDRTestModel::myTick() {}

void TDRTestModel::onGrazeOnce() {
  notify(Graze{getPlayer().getPosition()});
}

void TDRTestModel::onHit() {
  notify(Hit{});
}

void TDRTestModel::onShot() {}

void TDRTestModel::onBomb() {
  auto& gp = getPlayer();
  gp.setInvincibilityFrames(240);
  gp.setBombFrames(180);
  getManager().spawn([this, &gp]() {
    using namespace kfp::literals;
    tdr::BulletList& bl = getPlayerBulletList();
    for (int frame = 0; frame < 180; ++frame) {
      tdr::PV pc = gp.getPosition();
      // Shoot 3 bullets in random directions each frame,
      // and make earlier bullets faster than later ones
      kfp::s16_16 speed = "7"_s16_16 - "4"_s16_16 * frame / 180;
      for (int i = 0; i < 3; ++i) {
        kfp::frac32 direction = getRng().next<kfp::frac32>();
        tdr::BulletHandle s =
            bl.createShotA1(pc, speed, direction, player_shot::star, 0);
        s.setEraseShots(true).setDamage(25);
        s.setPlayerAttackDivision(tdr::PlayerAttackDivision::spell);
      }
      kcr::yield();
    }
  });
}

void TDRTestModel::onSpawnErasureItem(
    size_t nBullets, const tdr::PV* positions) {
  tdr::ItemList& il = getItemList();
  il.createItemsA1(nBullets, positions, item::cancelItem);
}

void TDRTestModel::onCollectItem(size_t nItems, tdr::ItemRec* items) {
  auto& gp = getPlayer();
  for (size_t i = 0; i < nItems; ++i) {
    const tdr::ItemRec& item = items[i];
    // item.position => location where item was collected
    // item.itemId => item ID
    switch (item.itemId) {
    case item::cancelItem: {
      int64_t value = 100;
      gp.cd.score += value;
      notify(ScorePopup{value, gp.getPosition(), {115, 225, 0, 255}});
      break;
    }
    default: {
      fmt::print("Unknown item {}\n", item.itemId);
    }
    }
  }
}

// TODO: particle effects and other eye candy

void TDRTestModel::onBossSceneStart(tdr::BossScene& scene) {
  (void) scene;
}

void TDRTestModel::onBossSceneAttackStart(
    tdr::BossScene& scene, const tdr::BossAttack& attack) {
  (void) scene;
  notify(BeginCutin{attack.id});
  tdr::EnemyHandle boss = scene.getBoss();
  boss.setDamageRate({0, 0});
  // Set incoming damage rate to 0 for 120 frames,
  // then over the next 180 frames, gradually increase it to 1.
  getManager().spawn([&scene]() {
    tdr::EnemyHandle boss = scene.getBoss();
    const tdr::BossAttack* at = scene.currentAttack();
    tdr::cwait(120);
    for (int i = 0; i < 180; ++i) {
      if (boss.isMarkedForDeletion() || scene.currentAttack() != at) return;
      kfp::s16_16 rate = kfp::s16_16(i) / 180;
      boss.setDamageRate({rate, rate});
    }
  });
}

void TDRTestModel::onBossSceneBreak(
    tdr::BossScene& scene, const tdr::BossAttack& attack,
    tdr::BossScene::PlayStat st) {
  (void) scene;
  if (attack.score != 0) {
    if (st.hasMissedOrBombed() || st.timeRemaining == 0) {
      notify(SpellBonus{0});
    } else {
      int64_t bonus =
          getSpellBonusAt(attack.score, st.timeRemaining, attack.startTime);
      notify(SpellBonus{bonus});
      getPlayer().cd.score += bonus;
    }
  }
  breakScreen();
}

void TDRTestModel::onBossSceneEnd(tdr::BossScene& scene) {
  scene.getBoss().markForDeletion();
  unregisterBossScene();
}

void TDRTestView::myinit() {
  std::cerr << "init begin\n";
  (void) agl::getShaders(2);
  this->setDestination(getSharedData().fboMS);
  /* local */ {
    // Set up HUD
    hud.setUp();
    hud.setApp(this->app);
    hud.setFont(this->getSharedData().alef.get());
    agl::TextStyle style;
    style.fill.setVerticalGradient();
    style.fill.col1 = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    style.fill.col2 = glm::vec4(0.75f, 0.75f, 0.75f, 1.0f);
    style.outline.setColour(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
    style.outlineThreshold = 0.2f;
    hud.getTextStyle() = style;
    dgs.emplace_back("galis", [this]() {
      return std::to_string(getModel().getBulletList().nBullets());
    });
    dgs.emplace_back(
        "lots", [this]() { return std::to_string(getModel().getVKFlags()); });
    dgs.emplace_back("ito", [this]() {
      return std::to_string(getModel().getPlayer().cd.score);
    });
    dgs.emplace_back("kitto", [this]() {
      return std::to_string(getModel().getPlayer().cd.lives);
    });
    dgs.emplace_back("arfa", [this]() {
      return std::to_string(getModel().getPlayer().cd.bombs);
    });
    dgs.emplace_back("zak", [this]() {
      return std::to_string(getModel().getPlayer().cd.graze);
    });
    dgs.emplace_back("retl", [this]() {
      return std::to_string(getModel().getEnemyList().nEnemies());
    });
    dgs.emplace_back(
        "moj", [this]() { return std::to_string(app->getRollingFPS()); });
    float y = 50;
    for (DisplayGauge& dg : dgs) {
      agl::LayoutInfo l1;
      l1.fontSize = 36;
      l1.margin = 4;
      l1.maxWidth = 350;
      hud.addText(std::string(dg.text), l1, glm::vec2(880, y));
      agl::LayoutInfo l2;
      l2.fontSize = 36;
      l2.margin = 4;
      l2.maxWidth = 350;
      l2.hAlign = agl::HAlign::right;
      size_t ri = hud.addText("", l2, glm::vec2(1230, y));
      dg.ri = ri;
      y += 80;
    }
  }
  /* local */ {
    // Set up boss scene text
    bossSceneText.setUp();
    bossSceneText.setApp(nullptr);
    bossSceneText.setFont(this->getSharedData().alef.get());
    agl::TextStyle style;
    style.fill.setVerticalGradient();
    style.fill.col1 = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    style.fill.col2 = glm::vec4(0.75f, 0.75f, 1.0f, 1.0f);
    style.outline.setColour(glm::vec4(0.0f, 0.0f, 0.2f, 1.0f));
    style.outlineThreshold = 0.2f;
    bossSceneText.getTextStyle() = style;
    agl::LayoutInfo nameLayout;
    nameLayout.fontSize = 20;
    nameLayout.margin = 4;
    nameLayout.maxWidth = 500;
    bossSceneText.addText("", nameLayout, glm::vec2(50, 30));
    agl::LayoutInfo healthLayout;
    healthLayout.fontSize = 36;
    healthLayout.margin = 4;
    healthLayout.maxWidth = 500;
    bossSceneText.addText("", healthLayout, glm::vec2(30, 70));
    agl::LayoutInfo timeLayout;
    timeLayout.fontSize = 40;
    timeLayout.margin = 4;
    timeLayout.maxWidth = 800;
    timeLayout.hAlign = agl::HAlign::right;
    bossSceneText.addText("", timeLayout, glm::vec2(700, 30));
  }
  // Load textures
  shots = agl::Texture("textures-tdr/shotsheet.png");
  playerShots = agl::Texture("textures-tdr/playershots.png");
  enemy = agl::Texture("textures-tdr/enemy.png");
  items = agl::Texture("textures-tdr/items.png");
  particles = agl::Texture("textures-tdr/particles.png");
  bossbar = agl::Texture("textures-tdr/healthbar.png");
  loadPlayerSprite(playerShots.weak());
  setPlayerSpriteRect({24, 464, 48, 498});
  bossbarSprite = std::make_unique<agl::Sprite2D>(bossbar.weak());
  // Load sounds
  agl::Mixer& m = app->getMixer();
  for (const char* sound : sounds) {
    m.addSoundFromFile(sound, std::string("sounds-tdr/") + sound + ".ogg");
  }
  // Load pause screen shader
  /* local */ {
    agl::Shader frag = agl::openShaderFromFile2(
        "shader-tdr/pause_vignette_fragment.glsl", GL_FRAGMENT_SHADER);
    auto& sd = this->getSharedData();
    sd.pauseScreen = std::make_unique<agl::Sprite2D>(sd.fboTex.weak(), &frag);
    sd.pauseScreen->addSprite({
        {0, 960, 1280, 0},
        {0, 0, 1280, 960},
    });
    sd.pauseScreen->setApp(this->app);
    sd.pauseScreen->update();
  }
  // Initialise shotsheets
  this->setShotSprites(shots.weak());
  this->setEnemySprites(enemy.weak());
  this->setPlayerShotSprites(playerShots.weak());
  this->getBulletListView().setDelayCloud(delayCloud);
  this->setItemSprites(items.weak());
  this->getBulletListView().setAngleOffset("0.25"_frac32);
  this->getPlayerBulletListView().setAngleOffset(-"0.25"_frac32);
  tdr::ParticleShotsheet particleSheet;
  particleSheet.setRects(
      sizeof(particleGraphics) / sizeof(particleGraphics[0]), particleGraphics);
  pm = std::make_unique<tdr::ParticleManager>(
      &getPlayfieldView(), std::move(particleSheet), particles.weak());
  m.playSound("stage", agl::PlayOptions{1.0f, 0.0f, true, 0, 6'727'680});
  std::cerr << "init end\n";
}

void TDRTestView::myreset() {
  agl::Mixer& m = app->getMixer();
  m.playSound("stage", agl::PlayOptions{1.0f, 0.0f, true, 0, 6'727'680});
  pm->reset();
}

void TDRTestView::mainLoop() {
  // Work
  readKeys();
  for (DisplayGauge& dg : dgs) {
    auto& re = hud.getElement(dg.ri);
    re.text = dg.generator();
  }
  using namespace kfp::literals;
  kfp::frac32 angle = rng.next<kfp::frac32>();
  kfp::s2_30 c, s;
  kfp::sincos(angle, c, s);
  kfp::s16_16 sp = "2.5"_s16_16;
  tdr::ParticleInfo& part = pm->spawn(
      particle::sparkleBig, tdr::PV(416 - 32, 150), tdr::PV(sp * c, sp * s),
      120);
  part.blendRate = {0, 0, 0, -128 * 255 / 120};
  part.tangentialAcceleration = "0.01"_s2_30;
  part.rotation = angle;
}

void TDRTestView::readKeys() {
  if (testKey2(GLFW_KEY_ESCAPE) == agl::KeyStatus::enter) {
    agl::setDefaultFBOAsActive();
    app->pushScene<TDRPause>();
  }
  // More to come...
}

void TDRTestView::myrender() {
  hud.render();
}

void TDRTestView::myrenderf() {
  pm->update();
  pm->render();
}

void TDRTestView::myPostRender() {
  auto& sd = this->getSharedData();
  sd.fboMS.blitTo(sd.fboSS, 1280, 960);
}

void TDRTestView::renderBossSceneFront(const tdr::BossScene& scene) {
  using Element = agl::XText::Element;
  Element& name = bossSceneText.getElement(0);
  name.text = "aaaaaaaaaa";
  Element& health = bossSceneText.getElement(1);
  health.text = fmt::format(
      "+{}   {}", scene.getRemainingSteps(), scene.getBoss().getHealth());
  Element& mel = bossSceneText.getElement(2);
  mel.text = fmt::format("{:.2f}", scene.getTimeRemaining() / 60.0);
  bossSceneText.render();
  /* local */ {
    assert(bossbarSprite != nullptr);
    float xright =
        19 + 730 * scene.getBoss().getHealth() / scene.currentAttack()->health;
    bossbarSprite->clear();
    bossbarSprite->addSprite({{0, 0, 768, 32}, {0, 0, 768, 32}});
    bossbarSprite->addSprite({{0, 32, xright, 64}, {0, 0, xright, 32}});
    size_t remainingSteps = scene.getRemainingSteps();
    for (size_t i = 0; i < remainingSteps; ++i) {
      float startX = (float) (24 * i);
      bossbarSprite->addSprite(
          {{0, 64, 24, 88}, {startX, 64, 24 + startX, 88}});
    }
    bossbarSprite->render();
  }
}

void TDRTestView::renderBossSceneBack(const tdr::BossScene& scene) {
  (void) scene; // todo
}


void TDRTestView::drawChar(
    tdr::ShotID id, glm::u8vec4 blend, tdr::PV position) {
  tdr::ParticleInfo& part =
      pm->spawn(id, position, tdr::PV(0, -"0.5"_s16_16), 60);
  part.setBlend(blend);
  part.blendRate = {0, 0, 0, -blend.a * 128 / 60};
}

// 8px for spaces, 16px for other chrs
constexpr int charWidth = 16;
constexpr int spaceWidth = 9;

void TDRTestView::scorePopup(
    int64_t score, tdr::PV position, glm::u8vec4 blend) {
  char buf[21]; // signed 64-bit int can't have more than 20 chars; I checked
  auto [end, err] = std::to_chars(buf, buf + 20, score, 10);
  *end = '\0';
  int nChars = end - buf;
  int nDigits = (score < 0) ? nChars - 1 : nChars;
  int nSpaces = (nDigits - 1) / 4;
  int width = spaceWidth * nSpaces + charWidth * nChars;
  tdr::PV p = position + tdr::PV{-width / 2 + charWidth / 2, -18};
  const char* s = buf;
  if (*s == '-') {
    // Draw the minus sign
    drawChar(particle::minusSign, blend, p);
    p.x += charWidth;
    ++s;
  }
  int num = 0;
  while (*s != '\0') {
    drawChar(particle::digit0 + (*s - '0'), blend, p);
    p.x += charWidth;
    ++num;
    if (num % 4 == 0) p.x += spaceWidth;
    ++s;
  }
}

void TDRTestView::receive(const Graze& g) {
  agl::Mixer& m = app->getMixer();
  m.playSound("graze");
  using namespace kfp::literals;
  kfp::frac32 angle = rng.next<kfp::frac32>();
  kfp::s2_30 c, s;
  kfp::sincos(angle, c, s);
  kfp::s16_16 sp = rng.next<kfp::s16_16>("2.5"_s16_16, "3.5"_s16_16);
  kfp::s16_16 rad = 10;
  tdr::ParticleInfo& part = pm->spawn(
      particle::sparkleSmall, g.position + tdr::PV(rad * c, rad * s),
      tdr::PV(sp * c, sp * s), 60);
  part.acceleration = {0, "0.1"_s16_16};
  part.blendRate = {0, 0, 0, -128 * 255 / 60};
  part.rotation = angle;
}

void TDRTestView::receive(Hit) {
  agl::Mixer& m = app->getMixer();
  m.playSound("death");
}

void TDRTestView::onEnd() {
  agl::setDefaultFBOAsActive();
  app->pushScene<TDRPause>(); // Same scene handles pause & game end
}

void TDRTestView::receive(const ScorePopup& p) {
  scorePopup(p.value, p.position, p.blend);
}

void TDRTestView::receive(SetBossSprite s) {
  backShotsheet = agl::Texture(s.path);
  getEnemyListView().setBackTexture(backShotsheet.weak());
}

void TDRTestView::receive(BeginCutin c) {
  fmt::print("Received cutin start for {}\n", c.id);
  // TODO: this won't render
  getManager().spawn([this, c]() {
    const tdr::BossScene& scene = *(getModel().getBossScene());
    const tdr::BossAttack* at = scene.currentAttack();
    uint32_t sid = c.id;
    const SpellcardInfo& info = spellcardInfo[sid];
    if (info.name.has_value()) {
      const auto name = *info.name;
      agl::XText cutin;
      cutin.setFont(getSharedData().inje.get());
      cutin.addText(
          std::string(name), agl::LayoutInfo{700, 32, 0, 0, agl::HAlign::right},
          glm::vec2{728, 270});
      agl::TextStyle style;
      style.fill.setVerticalGradient();
      style.fill.col1 = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
      style.fill.col2 = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
      style.outline.setColour(glm::vec4(1.0f, 1.0f, 1.0f, 0.0f));
      style.outlineThreshold = 0.10f;
      style.dropShadowStyle = {
          /* .dropShadowColour = */ {0, 0, 0, 0.8},
          /* .dropShadowOffset = */ {5, 5},
          /* .dropShadowRadius = */ 2,
      };
      cutin.getTextStyle() = style;
      for (size_t i = 0; i < 90; ++i) {
        cutin.render();
        cutin.setPos(glm::vec2{728, 268 - 2 * i});
        kcr::yield();
      }
      cutin.addText(
          "", agl::LayoutInfo{700, 20, 0, 0, agl::HAlign::right},
          glm::vec2{728, 130});
      while (getModel().getBossScene()->currentAttack() == at) {
        if (scene.getStat().hasMissedOrBombed()) {
          cutin.getElement(1).text = fmt::format("ito 0 / seta {}/{}", 0, 1);
        } else {
          int64_t bonus = getSpellBonusAt(
              at->score, scene.getTimeRemaining(), at->startTime);
          cutin.getElement(1).text =
              fmt::format("ito {} / seta {}/{}", bonus, 0, 1);
        }
        cutin.render();
        kcr::yield();
      }
    }
  });
}

void TDRTestView::receive(SpellBonus b) {
  fmt::print("bonus {}\n", b.value);
  getManager().spawn([this, b]() {
    if (b.value > 0) {
      agl::XText cutin, score;
      cutin.setFont(getSharedData().alef.get());
      cutin.setUp();
      cutin.addText(
          "ximik kardia!", agl::LayoutInfo{700, 48, 0, 0, agl::HAlign::centre},
          glm::vec2{384, 200});
      score.setFont(getSharedData().inje.get());
      score.setUp();
      score.addText(
          std::to_string(b.value),
          agl::LayoutInfo{700, 32, 0, 0, agl::HAlign::centre},
          glm::vec2{384, 280});
      /* local */ {
        agl::TextStyle style;
        style.fill.setVerticalGradient();
        style.fill.col1 = glm::vec4(1.0f, 1.0f, 0.6f, 1.0f);
        style.fill.col2 = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
        style.outline.setColour(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
        style.outlineThreshold = 0.25f;
        style.fillThreshold = 0.4f;
        cutin.getTextStyle() = style;
        style.fill.col1 = glm::vec4(0.6f, 1.0f, 0.6f, 1.0f);
        score.getTextStyle() = style;
      }
      for (size_t i = 0; i < 180; ++i) {
        cutin.render();
        score.render();
        kcr::yield();
      }
    } else {
      agl::XText cutin;
      cutin.setFont(getSharedData().alef.get());
      cutin.setUp();
      cutin.addText(
          "fesik kardia...",
          agl::LayoutInfo{700, 48, 0, 0, agl::HAlign::centre},
          glm::vec2{384, 200});
      /* local */ {
        agl::TextStyle style;
        style.fill.setVerticalGradient();
        style.fill.col1 = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
        style.fill.col2 = glm::vec4(0.9f, 0.9f, 0.9f, 1.0f);
        style.outline.setColour(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
        style.outlineThreshold = 0.25f;
        style.fillThreshold = 0.4f;
        cutin.getTextStyle() = style;
      }
      for (size_t i = 0; i < 180; ++i) {
        cutin.render();
        kcr::yield();
      }
    }
  });
}

std::unique_ptr<tdr::Stage<TDRTestModel>>
TDRTestView::getStage(size_t stageno) {
  switch (stageno) {
  case 0: return std::make_unique<TDRTestStage0>();
  default: return nullptr;
  }
}
