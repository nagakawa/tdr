#version 330 core

in vec2 texCoord;
out vec4 colour;
uniform sampler2D tex;

void main() {
  // Reduce to 65% brightness at centre and 10% at edges
  float radius = distance(texCoord, vec2(0.5f, 0.5f));
  float brightness = mix(0.65f, 0.1f, radius / sqrt(0.5f));
  colour = vec4(texture(tex, texCoord).rgb * brightness, 1.0f);
}
