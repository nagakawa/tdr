#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <stdexcept>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// SOIL
#include <SOIL/SOIL.h>
// json
#include <nlohmann/json.hpp>
using json = nlohmann::json;
// GLM
#include <AGL/EBO.h>
#include <AGL/GLFWApplication.h>
#include <AGL/ScenedGLFWApplication.h>
#include <AGL/Shader.h>
#include <AGL/ShaderProgram.h>
#include <AGL/Texture.h>
#include <AGL/VAO.h>
#include <AGL/VBO.h>
#include <AGL/sound/Mixer.h>
#include <AGL/text/XText.h>
#include <FileUtil/exe_location.h>
#include <FileUtil/get_fs_headers.h>
#include <TDR/Bullet.h>
#include <TDR/Game.h>
#include <TDR/Stage.h>
#include <TDR/tdrprofiling.h>
#include <TDR/yield.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <kozet_fixed_point/kfp_random.h>

#include "Test.h"
#include "TestGame.h"
#include "TestPause.h"
#include "TestSharedData.h"
#include "stage0.h"

void readConfig(const json& jcfg, agl::ApplicationOptions& opts) {
  opts.debug = jcfg.value("debug", true);
  opts.audioDevice = jcfg.value("audioDevice", -1);
}

void test(const char* replayName) {
  std::ifstream replayFh;
  if (replayName != nullptr) replayFh.open(replayName, std::ios::binary);
  std::cout << "Starting TDRTest...\n";
  agl::ApplicationOptions opts;
  opts.width = 1280;
  opts.height = 960;
  opts.glMajor = 4;
  opts.glMinor = 5;
  opts.debug = true;
  opts.title = u8"TDR Test App";
  opts.popts = &tdr::defaultProfilerOptions;
  {
    std::ifstream fh("config.json");
    if (!fh.fail()) {
      json jcfg;
      fh >> jcfg;
      readConfig(jcfg, opts);
    }
  }
  auto app = std::make_unique<agl::ScenedGLFWApplication<TestSharedData>>(opts);
  app->insertScene<TDRTestView>([replayName, &replayFh]() {
    agl::Texture stgFrame("textures-tdr/astgframe.png");
    return std::make_unique<TDRTestView>(
        std::move(stgFrame),
        // STG frame dimensions from Danmakufu defaults
        // (cf.
        // https://dmf.shrinemaiden.org/wiki/System_Functions#SetStgFrame),
        // multiplied by 2 to compensate for double resolution
        (416 - 32) * 2, (464 - 16) * 2, 32 * 2, 16 * 2, (416 - 32) * 2,
        (464 - 16) * 2, replayName != nullptr ? &replayFh : nullptr);
  });
  app->insertScene<TDRPause>([]() { return std::make_unique<TDRPause>(); });
  app->changeScene<TDRTestView>();
  app->start();
}

static void glfwError(int id, const char* description) {
  (void) id;
  std::cerr << description << std::endl;
}

int main(int argc, char** argv) {
  const char* fname = (argc >= 2) ? argv[1] : nullptr;
  /* local */ {
    auto exedir = futil::getExeLocation();
    if (!exedir.has_value()) {
      std::cerr << "Couldn't get location of executable\n";
      exit(-1);
    }
    auto p = fs::path(*exedir).parent_path();
    fs::current_path(p);
  }
  glfwSetErrorCallback(glfwError);
  glfwInit();
  try {
    test(fname);
  } catch (const std::exception& e) {
    std::cerr << u8"An error has Okuued!\n\n" << e.what() << u8"\n\n";
    return -1;
  } catch (char const* s) {
    std::cerr << u8"An error has Okuued!\n\n" << s << u8"\n\n";
    return -1;
  } catch (const std::string& s) {
    std::cerr << u8"An error has Okuued!\n\n" << s << u8"\n\n";
    return -1;
  }
  glfwTerminate();
  return 0;
}
