#pragma once

#include <TDR/bullet/defs.h>

namespace enemy {
  enum Enemy : tdr::ShotID {
    pinkFairy = 0,
    robot = 1,
  };
};

namespace player_shot {
  enum PlayerShot : tdr::ShotID {
    silverArrow = 0,
    steelArrow = 1,
    seed = 2,
    needle = 3,
    flare = 4,
    star = 5,
  };
};

namespace shot {
  enum Shot : tdr::ShotID {
    purpleBall = 0,
    greenBall = 1,
    redBall = 2,
    blueBall = 3,
    orangeBall = 4,
    purpleMissile = 5,
    greenMissile = 6,
    redMissile = 7,
    blueMissile = 8,
    orangeMissile = 9,
    orangeBubble = 10,
    purpleStar = 11,
    greenStar = 12,
    redStar = 13,
    blueStar = 14,
    orangeStar = 15,
    pulsatingOrb = 16,
  };
}

namespace item {
  enum Item : tdr::ShotID {
    cancelItem = 0,
  };
}

namespace particle {
  enum Particle : tdr::ShotID {
    sparkleBig = 0,
    sparkleSmall = 1,
    star = 2,
    digit0 = 3,
    digit1 = 4,
    digit2 = 5,
    digit3 = 6,
    digit4 = 7,
    digit5 = 8,
    digit6 = 9,
    digit7 = 10,
    digit8 = 11,
    digit9 = 12,
    minusSign = 13,
  };
}
