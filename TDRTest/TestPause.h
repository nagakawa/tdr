#pragma once

#include <memory>
#include <vector>

// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H

#include <AGL/Scene.h>
#include <AGL/text/XText.h>

#include "TestSharedData.h"

class TDRPause : public agl::Scene<TestSharedData> {
public:
  TDRPause() = default;
  void readKeys();
  void update() override;
  void render() override;
  void initialise() override;
  void onEnter() override;
  void onLeave() override;

private:
  agl::XText hud;
  int16_t currentOption;
  int16_t ticks;
  void updateLabels();
};
