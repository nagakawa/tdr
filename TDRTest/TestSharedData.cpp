#include "TestSharedData.h"

TestSharedData::TestSharedData() {
  if (FT_Init_FreeType(&ftl) != 0) throw "Failed to initialise FreeType.";
  alef = std::make_unique<agl::Font>(ftl, "textures-tdr/alef.ttf", 128);
  inje = std::make_unique<agl::Font>(ftl, "textures-tdr/inje_arnant.ttf", 128);
  agl::FBOTexMS ft = agl::makeFBOForMeMS(1280, 960);
  fboTex = std::move(ft.ss.texture);
  fboTexMS = std::move(ft.ms.texture);
  fboSS = std::move(ft.ss.fbo);
  fboMS = std::move(ft.ms.fbo);
  fboTex.addName("fboTex");
  fboTexMS.addName("fboTexMS");
}
