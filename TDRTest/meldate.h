#pragma once

#include <stdint.h>
#include <time.h>

struct MelDateTime {
  int32_t salt;
  uint8_t xelt;
  uint8_t sel;
  uint8_t miv, fei, jin;
  static MelDateTime fromTime(time_t t);
  int formatFname(char buf[20]);
};
