#include "TestPause.h"

#include <fstream>
#include <iostream>

#include <AGL/ScenedGLFWApplication.h>
#include <AGL/sound/Mixer.h>
#include <TDR/menuhelper.h>

#include "TestGame.h"
#include "meldate.h"

enum PauseOption : int16_t {
  pResume,
  pSaveReplay,
  pQuit,
  pRestart,
  pCount,
};

void TDRPause::initialise() {
  hud.setUp();
  hud.setApp(this->app);
  hud.setFont(this->getSharedData().alef.get());
  agl::TextStyle style;
  style.fill.setVerticalGradient();
  style.fill.col1 = glm::vec4(0.85f, 1.0f, 0.8f, 1.0f);
  style.fill.col2 = glm::vec4(0.5f, 1.0f, 0.4f, 1.0f);
  style.outline.setColour(glm::vec4(0.2f, 0.5f, 0.05f, 1.0f));
  style.outlineThreshold = 0.2f;
  hud.getTextStyle() = style;
  agl::LayoutInfo pauseLayout;
  pauseLayout.fontSize = 48;
  pauseLayout.margin = 4;
  pauseLayout.maxWidth = 350;
  for (int16_t i = 0; i < (int16_t) pCount; ++i) {
    [[maybe_unused]] size_t li = hud.addText("", pauseLayout);
    assert((size_t) i == li);
    auto& elem = hud.getElement(i);
    bool isCurrent = (i == 0);
    elem.position = glm::vec2(70 + 70 * i + (isCurrent ? 50 : 0), 440 + 50 * i);
  }
  agl::Mixer& m = app->getMixer();
  m.addContext("pause_scr");
}

void TDRPause::update() {
  readKeys();
}

static tdr::MenuOptions myMenuOptions = {
    /* .initialDelay = */ 20,
    /* .repeatDelay = */ 10,
};

void TDRPause::readKeys() {
  if (testKey2(GLFW_KEY_ESCAPE) == agl::KeyStatus::enter) { app->popScene(); }
  TDRTestView& g = *(this->app->getScene<TDRTestView>());
  const TDRTestModel& m = g.getModel();
  bool isOver = m.isGameOver();
  bool isFinished = m.isDone();
  uint16_t nContinues = m.getNumContinues();
  // Handle up/down
  agl::KeyStatus up = testKey2(GLFW_KEY_UP), down = testKey2(GLFW_KEY_DOWN);
  int newOption =
      tdr::menuHelper(currentOption, pCount, up, down, ticks, myMenuOptions);
  if (newOption != currentOption) {
    for (int16_t i = 0; i < pCount; ++i) {
      auto& elem = hud.getElement(i);
      bool isCurrent = (i == newOption);
      elem.position =
          glm::vec2(70 + 70 * i + (isCurrent ? 50 : 0), 440 + 50 * i);
    }
  }
  currentOption = newOption;
  if (testKey2(GLFW_KEY_Z) == agl::KeyStatus::enter) {
    switch (currentOption) {
    case pResume: {
      if (isFinished) break;
      if (isOver) g.useContinue();
      app->popScene();
      break;
    }
    case pQuit: {
      exit(0);
      break;
    }
    case pRestart: {
      TDRTestView* game = app->getScene<TDRTestView>();
      assert(game != nullptr);
      app->getMixer().switchContext("");
      game->reset();
      app->popScene();
      break;
    }
    case pSaveReplay: {
      if (nContinues != 0) break; // can't save replay after continue
      char fname[] = "tdrtest.xxxx.xx.xx.xx.xx.xx.msl";
      time_t t = time(nullptr);
      MelDateTime dt = MelDateTime::fromTime(t);
      dt.formatFname(fname + 8);
      fname[27] = '.';
      std::ofstream out(fname);
      TDRTestView* game = app->getScene<TDRTestView>();
      assert(game != nullptr);
      game->saveReplay(out, t, std::string("estsian"));
      break;
    }
    default: {
      std::cerr << "Option not yet implemented\n";
      abort();
    }
    }
  }
  // More to come...
}

void TDRPause::render() {
  glClearColor(0.9f, 0.5f, 0.6f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  this->getSharedData().pauseScreen->render();
  hud.render();
}

void TDRPause::onEnter() {
  updateLabels();
  currentOption = pResume;
  for (int16_t i = 0; i < pCount; ++i) {
    auto& elem = hud.getElement(i);
    bool isCurrent = (i == currentOption);
    elem.position = glm::vec2(70 + 70 * i + (isCurrent ? 50 : 0), 440 + 50 * i);
  }
  agl::Mixer& m = app->getMixer();
  m.switchContext("pause_scr");
  m.playSound("tdr_pause_interj");
}

void TDRPause::onLeave() {
  agl::Mixer& m = app->getMixer();
  m.switchContext("");
}

void TDRPause::updateLabels() {
  const TDRTestView& g = *(this->app->getScene<TDRTestView>());
  const TDRTestModel& m = g.getModel();
  bool isOver = m.isGameOver();
  bool isFinished = m.isDone();
  uint16_t nContinues = m.getNumContinues();
  hud.getElement(0).text =
      isFinished ? "(vaxtik liva)" : isOver ? "onk" : "mak";
  hud.getElement(1).text = (nContinues == 0) ? "vanz" : "(onkes liva)";
  hud.getElement(2).text = "took";
  hud.getElement(3).text = "yoos";
}
