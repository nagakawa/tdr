[requires]
freetype/2.9.1@bincrafters/stable
glm/0.9.9.4@g-truc/stable
vorbis/1.3.6@bincrafters/stable
boost/1.69.0@conan/stable
portaudio/v190600.20161030@bincrafters/stable
jsonformoderncpp/3.5.0@vthiery/stable
fmt/5.3.0@bincrafters/stable
libpng/1.6.36@bincrafters/stable
harfbuzz/2.4.0@bincrafters/stable

[generators]
cmake

[options]
harfbuzz:with_freetype=True
