#pragma once

#include <assert.h>
#include <stdint.h>

#include <algorithm>
#include <limits>
#include <numeric>
#include <vector>

namespace tdr {
  /**
   * \brief A class that allocates (generation, index) pairs.
   *
   * This class allocates and frees slots, such that
   *
   * - once a (generation, index) pair is returned from the `allocate` method,
   *   it will never be returned from it again (or at least not in a long time)
   *   unless the allocator is cleared
   * - no two handles allocated at the same time from the same allocator
   *   has the same index
   *
   * These rules allow the index fields of handles to be used as indices into
   * arrays.
   */
  template<typename Gen = uint64_t, typename Index = size_t>
  class GenerationalAllocator {
  public:
    struct Handle {
      Gen generation;
      Index index;
    };
    /// Return whether the handle is valid; i.e. it is within bounds of the
    /// generation array and its generation is current.
    bool isValid(Handle h) const noexcept {
      return h.index < generations.size() &&
             generations[h.index] == h.generation;
    }
    /// Return whether the handle is live – in other words, it was allocated
    /// but not freed.
    bool isLive(Handle h) const noexcept {
      return isValid(h) && !freeBits[h.index];
    }
    /// Return whether there is a live handle at a certain index.
    bool isLive(Index i) const noexcept { return !freeBits[i]; }
    /// Return the generation associated with the index.
    Gen getGeneration(Index i) const noexcept { return generations[i]; }
    /// Allocate a handle.
    Handle allocate(bool& appended) {
      if (freeList.empty()) {
        Index i = (Index) generations.size();
        assert(i != std::numeric_limits<Index>::max());
        generations.push_back(0);
        freeBits.push_back(false);
        appended = true;
        return Handle{0, i};
      }
      Index i = freeList.back();
      freeList.pop_back();
      Gen g = ++generations[i];
      freeBits[i] = false;
      appended = false;
      return Handle{g, i};
    }
    /**
     * \brief Allocates multiple handles from this allocator more efficiently
     * than calling `allocate` multiple times. I hope.
     *
     * After calling this function, the pointer `handles` will point to a block
     * of `count` elements, so that elements [0, existing) will be indices
     * that were not newly appended in the allocator (instead being recycled
     * from already-freed elements) and [existing, count) will consist of a
     * sequence of appended handles whose indices ascend consecutively.
     *
     * \param count The number of handles to allocate.
     * \param handles A block to `count` `Handle`s to store the new handles at.
     * \param existing The number of handles that are **not** appended.
     */
    void allocm(size_t count, Handle* handles, size_t& existing) {
      size_t nFree = freeList.size();
      std::copy_n(freeList.begin(), nFree, handles);
      freeList.clear();
      size_t nToAppend = count - nFree;
      generations.insert(generations.end(), nToAppend, 0);
      freeBits.insert(freeBits.end(), nToAppend, false);
      Index i = (Index) generations.size();
      std::generate(handles + nFree, handles + count, [&i]() { return i++; });
      existing = nFree;
    }
    /**
     * \brief Free a handle.
     *
     * The handle must be live.
     */
    void release(Handle h) {
      assert(isLive(h));
      freeBits[h.index] = true;
      freeList.push_back(h.index);
    }
    /// Free the handle associated with an index.
    void release(size_t i) {
      assert(isLive(i));
      freeBits[i] = true;
      freeList.push_back(i);
    }
    /**
     * \brief Clear the allocator.
     *
     * This invalidates all existing handles.
     */
    void clear() {
      std::fill(freeBits.begin(), freeBits.end(), true);
      freeList.resize(generations.size());
      std::iota(freeList.begin(), freeList.end(), 0);
    }
    /// Count the number of live handles.
    size_t countExtant() const noexcept {
      return std::count_if(
          freeBits.begin(), freeBits.end(), [](bool f) { return !f; });
    }

  private:
    std::vector<Index> freeList;
    std::vector<Gen> generations;
    std::vector<bool> freeBits;
  };
}
