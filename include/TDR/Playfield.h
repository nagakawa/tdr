#pragma once

#include <memory>

#include <AGL/FBO.h>
#include <AGL/rect.h>
#include <glm/glm.hpp>

namespace tdr {
  /**
   * \brief Represents the area in which a danmaku game occurs.
   */
  class Playfield {
  public:
    Playfield() = default;
    Playfield(int w, int h, int offw, int offh, int aw = 0, int ah = 0) :
        w(w), h(h), aw((aw == 0) ? w : aw), ah((ah == 0) ? h : ah), offw(offw),
        offh(offh) {
      aoffw = offw * w / this->aw;
      aoffh = offh * h / this->ah;
    }
    int getWidth() const { return w; }
    int getHeight() const { return h; }
    glm::ivec2 getDimensions() const { return glm::ivec2(w, h); }
    int getActualWidth() const { return aw; }
    int getActualHeight() const { return ah; }
    int getOffsetX() const { return offw; }
    int getOffsetY() const { return offh; }
    int getActualOffsetX() const { return aoffw; }
    int getActualOffsetY() const { return aoffh; }
    agl::Rect getActualBounds() const {
      return {(float) aoffw, (float) aoffh, (float) (aoffw + aw),
              (float) (aoffh + ah)};
    }
    agl::Rect getActualBoundsZero() const {
      return {0, 0, (float) aw, (float) ah};
    }
    agl::TRect<int> getActualBoundsZeroI() const { return {0, 0, aw, ah}; }
    agl::Rect getActualBoundsZeroInvertY() const {
      return {0, (float) ah, (float) aw, 0};
    }

  private:
    int w, h;
    int aw, ah;
    int offw, offh;
    int aoffw, aoffh;
  };
  static_assert(
      std::is_trivially_copyable_v<Playfield> &&
      std::is_trivially_destructible_v<Playfield>);
  class PlayfieldView : public Playfield {
  public:
    PlayfieldView(const Playfield& p) : Playfield(p) {
      agl::FBOTexMS ft =
          agl::makeFBOForMeMS(getActualWidth(), getActualHeight());
      fbo = std::move(ft.ss.fbo);
      tex = std::move(ft.ss.texture);
      fboMS = std::move(ft.ms.fbo);
      texMS = std::move(ft.ms.texture);
      tex.addName("Playfield::tex");
      texMS.addName("Playfield::texMS");
    }
    void blit() { fboMS.blitTo(fbo, getActualWidth(), getActualHeight()); }
    int getFBOID() const { return fbo.getID(); }
    agl::FBO& getFBO() { return fbo; }
    agl::FBO& getFBOMS() { return fboMS; }
    agl::Texture& getTexture() { return tex; }
    const agl::FBO& getFBO() const { return fbo; }
    const agl::Texture& getTexture() const { return tex; }

  private:
    agl::FBO fbo;
    agl::Texture tex; // Texture associated with FBO
    agl::FBO fboMS;
    agl::Texture texMS; // Texture associated with FBO
  };
}
