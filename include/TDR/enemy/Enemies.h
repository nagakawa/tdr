#pragma once

#include <assert.h>
#include <stdint.h>

#include <memory>
#include <vector>

#include <AGL/BlendMode.h>
#include <glm/glm.hpp>
#include <zekku/kfp_interop/timath.h>

#include "TDR/bullet/defs.h"
#include "TDR/enemy/Behaviour.h"
#include "TDR/enemy/EnemyHandle.h"
#include "TDR/enemy/defs.h"

namespace tdr {
  class Enemies {
  public:
    struct Physics {
      // TODO: which fields (if any) should be in their own array?
      PV position; // 8
      PV velocity; // 16
      kfp::s16_16 collisionRadiusToPlayer; // 20
      kfp::s16_16 collisionRadiusToShot; // 24
      DamageRate damageRate; // 32
      int32_t health; // 36
      bool autoDelete; // 37
      Circle hitboxToPlayer() const {
        return Circle{position, collisionRadiusToPlayer};
      }
      Circle hitboxToShot() const {
        return Circle{position, collisionRadiusToShot};
      }
    };
    struct Render {
      agl::UIRect16 texcoords;
      PV dimensions;
      bool takesBackSprite = false;
    };
    EnemyHandle spawn(
        PV position, kfp::s16_16 collisionRadiusToPlayer,
        kfp::s16_16 collisionRadiusToShot, int32_t health) noexcept;
    /// Update the positions of all enemies...
    void update() noexcept;
    /// Return true if the `i`th bullet collides with `c`.
    bool collidesWithPlayer(BulletIndex i, const Circle& c) const noexcept;
    bool collidesWithShot(BulletIndex i, const Circle& c) const noexcept;
    /// Check for collisions against a circle.
    template<typename F>
    void collideWithPlayer(const Circle& c, F&& callback) noexcept;
    template<typename F>
    void collideWithShot(const Circle& c, F&& callback) noexcept;
    bool isDeleted(BulletIndex i) const noexcept { return !ga.isLive(i); }
    void deleteEnemy(BulletIndex i) noexcept { return ga.release(i); }
    void deleteEnemy(GAH gh) noexcept { return ga.release(gh); }
    void spurt(std::vector<EnemyRenderInfo>* rinfo) const noexcept;
    size_t count() const noexcept { return ga.countExtant(); }
    /// Remove all enemies from this list.
    void clear() noexcept;
    template<typename F> void forEach(F&& f) {
      for (BulletIndex i = 0; i < physicses.size(); ++i) {
        if (isDeleted(i)) continue;
        f(EnemyHandle{this, {ga.getGeneration(i), i}});
      }
    }

  private:
    GA ga;
    std::vector<Physics> physicses;
    std::vector<Render> renders;
    std::vector<std::unique_ptr<EnemyBehaviour>> behaviours;
    friend class EnemyHandle;
  };
  template<typename F>
  void Enemies::collideWithPlayer(const Circle& c, F&& callback) noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      if (isDeleted(i)) continue;
      if (collidesWithPlayer(i, c)) {
        callback(EnemyHandle{this, GAH{ga.getGeneration(i), i}});
      }
    }
  }
  template<typename F>
  void Enemies::collideWithShot(const Circle& c, F&& callback) noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      if (isDeleted(i)) continue;
      if (collidesWithShot(i, c)) {
        callback(EnemyHandle{this, GAH{ga.getGeneration(i), i}});
      }
    }
  }
  inline EnemyHandle::EnemyHandle(Enemies* e, GAH gh) : e(e), gh(gh) {}
  inline bool EnemyHandle::isMarkedForDeletion() const noexcept {
    return !e->ga.isLive(gh);
  }
  inline PV& EnemyHandle::getPosition() noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].position;
  }
  inline const PV& EnemyHandle::getPosition() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].position;
  }
  inline PV& EnemyHandle::getVelocity() noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].velocity;
  }
  inline const PV& EnemyHandle::getVelocity() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].velocity;
  }
  inline kfp::s16_16 EnemyHandle::getCollisionRadiusToPlayer() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].collisionRadiusToPlayer;
  }
  inline EnemyHandle&
  EnemyHandle::setCollisionRadiusToPlayer(kfp::s16_16 r) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->physicses[gh.index].collisionRadiusToPlayer = r;
    return *this;
  }
  inline kfp::s16_16 EnemyHandle::getCollisionRadiusToShot() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].collisionRadiusToShot;
  }
  inline EnemyHandle&
  EnemyHandle::setCollisionRadiusToShot(kfp::s16_16 r) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->physicses[gh.index].collisionRadiusToShot = r;
    return *this;
  }
  inline DamageRate EnemyHandle::getDamageRate() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].damageRate;
  }
  inline EnemyHandle& EnemyHandle::setDamageRate(DamageRate rate) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->physicses[gh.index].damageRate = rate;
    return *this;
  }
  inline agl::UIRect16 EnemyHandle::getTexcoords() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->renders[gh.index].texcoords;
  }
  inline EnemyHandle& EnemyHandle::setTexcoords(agl::UIRect16 r) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->renders[gh.index].texcoords = r;
    e->renders[gh.index].dimensions = {r.right - r.left, r.bottom - r.top};
    return *this;
  }
  inline EnemyHandle& EnemyHandle::markForDeletion() noexcept {
    if (isMarkedForDeletion()) return *this;
    e->deleteEnemy(gh);
    return *this;
  }
  inline Circle EnemyHandle::getHitboxToShot() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    const Enemies::Physics& p = e->physicses[gh.index];
    return {p.position, p.collisionRadiusToShot};
  }
  inline int32_t EnemyHandle::getHealth() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].health;
  }
  inline EnemyHandle& EnemyHandle::setHealth(int32_t h) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->physicses[gh.index].health = h;
    return *this;
  }
  inline bool EnemyHandle::deletesOnDealth() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    return e->physicses[gh.index].autoDelete;
  }
  inline EnemyHandle& EnemyHandle::setDeleteOnDeath(bool d) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->physicses[gh.index].autoDelete = d;
    return *this;
  }
  inline EnemyHandle& EnemyHandle::setBacksheet(bool b) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted enemy");
    e->renders[gh.index].takesBackSprite = b;
    return *this;
  }
}
