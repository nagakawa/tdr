#pragma once

#include <AGL/ShaderProgram.h>
#include <AGL/Texture.h>
#include <AGL/VAO.h>
#include <AGL/VBO.h>

#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"
#include "TDR/enemy/Enemies.h"
#include "TDR/enemy/EnemyHandle.h"

namespace tdr {
  class PlayfieldView;
  class EnemyList {
  public:
    /**
     * \brief Construct an enemy list.
     *
     * \param p A pointer to the playfield to be used. Should not be null.
     * \param shotsheet The shotsheet to use for the enemies.
     */
    EnemyList(EnemyShotsheet&& shotsheet) : shotsheet(std::move(shotsheet)) {}
    /// Update the positions of the enemies over one frame.
    void updatePositions();
    /**
     * \brief Check for collisions against a circle.
     *
     * \param h The circle to check against.
     * \param callback A callable to call on the bullet for each collision.
     */
    template<typename F> void collideWithPlayer(const Circle& h, F&& callback) {
      enemies.collideWithPlayer(h, std::forward<F>(callback));
    }
    template<typename F> void collideWithShot(const Circle& h, F&& callback) {
      enemies.collideWithShot(h, std::forward<F>(callback));
    }
    /// Get the number of enemies.
    size_t nEnemies() const noexcept;
    void clear() noexcept;
    EnemyHandle
    createEnemyB1(PV position, PV velocity, ShotID id, int32_t health);
    EnemyHandle createBoss(
        PV position, kfp::s16_16 collisionRadiusToPlayer,
        kfp::s16_16 collisionRadiusToShot);
    template<typename F> void forEach(F&& f) {
      enemies.forEach(std::forward<F>(f));
    }

    const EnemyShotsheet& getShotsheet() const noexcept { return shotsheet; }
    void setShotsheet(EnemyShotsheet&& s) noexcept { this->shotsheet = s; }

  private:
    Enemies enemies;
    EnemyShotsheet shotsheet;
    friend class EnemyListView;
  };
  class EnemyListView {
  public:
    EnemyListView(const EnemyList* el, PlayfieldView* p) : el(el), p(p) {}
    /**
     * \brief Set up rendering for the enemy list.
     *
     * This should be called before any rendering.
     */
    void setUp();
    /// Render the enemies in the enemy list.
    void render();
    void setFrontTexture(agl::WeakTexture t) { this->frontTex = std::move(t); }
    void setBackTexture(agl::WeakTexture t) { this->backTex = std::move(t); }
    const EnemyList& getModel() const noexcept { return *el; }
    void setModel(const EnemyList* el) noexcept { this->el = el; }

  private:
    const EnemyList* el;
    agl::WeakTexture frontTex;
    agl::WeakTexture backTex;
    std::array<std::vector<EnemyRenderInfo>, agl::nPresetBlends> rinfo;
    std::vector<size_t> offsets;
    PlayfieldView* p;
    agl::VBO vbo;
    agl::VBO instanceVBO;
    agl::VAO vao;
    agl::ShaderProgram program;
    bool hasSetUniforms = false;
    bool hasInitialisedProgram = false;
    void setUniforms();
    void spurt();
    void update();
  };
}
