#pragma once

#include <stdint.h>

#include <functional>
#include <memory>
#include <vector>

#include "TDR/enemy/EnemyHandle.h"

namespace tdr {
  enum class [[nodiscard]] SceneResult{
      normal,
      change,
      end,
  };
  struct BossAttack {
    std::function<void()> handler;
    int64_t score;
    uint32_t startTime;
    int32_t health;
    uint32_t id;
    bool isTimeout;
  };
  struct BossStep {
    std::vector<BossAttack> attacks;
  };
  class BossScene {
  public:
    struct PlayStat {
      uint32_t nMiss, nBomb;
      uint32_t timeRemaining;
      bool hasMissedOrBombed() const { return nMiss > 0 || nBomb > 0; }
    };
    constexpr static size_t notStarted = -1;
    BossScene(EnemyHandle b) :
        boss(b), activeStep(notStarted), activeAttack(notStarted) {
      boss.setDeleteOnDeath(false);
    }
    ~BossScene() = default;
    BossScene(const BossScene&) = delete;
    BossScene(BossScene&&) = default;
    BossScene& operator=(const BossScene&) = delete;
    BossScene& operator=(BossScene&&) = default;
    /**
     * Start the next attack.
     *
     * \return `SceneResult::change` if succeeded; `SceneResult::end` if there
     * are no more attacks
     */
    SceneResult startNextAttack() noexcept;
    const BossAttack* currentAttack() const noexcept {
      if (activeStep >= steps.size()) return nullptr;
      return &(steps[activeStep].attacks[activeAttack]);
    }
    SceneResult tick() noexcept;
    void onMiss() noexcept { ++st.nMiss; }
    void onBomb() noexcept { ++st.nBomb; }
    bool isActive() const noexcept { return activeStep < steps.size(); }
    uint32_t getTimeRemaining() const noexcept { return st.timeRemaining; }
    size_t getMissCount() const noexcept { return st.nMiss; }
    size_t getBombCount() const noexcept { return st.nBomb; }
    PlayStat getStat() const noexcept { return st; }
    uint32_t getId() const noexcept { return id; }
    void setId(uint32_t id) noexcept { this->id = id; }
    void addBossAttack(size_t stepId, BossAttack&& at);
    EnemyHandle getBoss() const noexcept { return boss; }
    size_t getActiveStep() const noexcept { return activeStep; }
    size_t getActiveAttack() const noexcept { return activeAttack; }
    size_t getRemainingSteps() const noexcept {
      return steps.size() - activeStep - 1;
    }

  private:
    std::vector<BossStep> steps;
    EnemyHandle boss;
    size_t activeStep, activeAttack;
    PlayStat st;
    uint32_t id;
  };
}
