#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <zekku/kfp_interop/timath.h>
#include <zekku/timath.h>

namespace tdr {
  class EnemyHandle;
  class EnemyBehaviour {
  public:
    virtual ~EnemyBehaviour();
    virtual void tick(EnemyHandle& h);
    virtual void onDeath(const EnemyHandle& h);
  };
}
