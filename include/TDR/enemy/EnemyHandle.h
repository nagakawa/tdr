#pragma once

#include "TDR/bullet/defs.h"
#include "TDR/enemy/defs.h"

namespace tdr {
  class Enemies;
  class EnemyHandle {
  public:
    EnemyHandle(const EnemyHandle& h) = default;
    EnemyHandle(EnemyHandle&& h) = default;
    EnemyHandle& operator=(const EnemyHandle& h) = default;
    EnemyHandle& operator=(EnemyHandle&& h) = default;
    ~EnemyHandle() = default;
    /// Return true if this enemy is about to be deleted.
    bool isMarkedForDeletion() const noexcept;
    /// Get the position of the enemy.
    PV& getPosition() noexcept;
    /// Get the position of the enemy.
    const PV& getPosition() const noexcept;
    /// Set the position of the enemy.
    EnemyHandle& setPosition(const PV& x) noexcept {
      getPosition() = x;
      return *this;
    }
    /// Add an offset to the position of the enemy.
    EnemyHandle& addPosition(const PV& x) noexcept {
      getPosition() += x;
      return *this;
    }
    /// Get the velocity of the enemy.
    PV& getVelocity() noexcept;
    /// Get the velocity of the enemy.
    const PV& getVelocity() const noexcept;
    /// Set the velocity of the enemy.
    EnemyHandle& setVelocity(const PV& v) noexcept {
      getVelocity() = v;
      return *this;
    }
    /// Add an offset to the velocity of the enemy.
    EnemyHandle& addVelocity(const PV& v) noexcept {
      getVelocity() += v;
      return *this;
    }
    /// Get the health of the enemy.
    int32_t getHealth() const noexcept;
    /// Set the health of the enemy.
    EnemyHandle& setHealth(int32_t h) noexcept;
    /// Add health to the enemy.
    EnemyHandle& addHealth(int32_t h) noexcept {
      return setHealth(getHealth() + h);
    }
    /// Add health to the enemy, adjusted for its damage rate.
    EnemyHandle&
    addHealthAdjusted(int32_t h, PlayerAttackDivision div) noexcept {
      kfp::Fixed<int64_t, 32> rate = getDamageRate().getMultiplierFor(div);
      int32_t adjustedHealth = (int32_t)(rate * h).floor();
      return addHealth(adjustedHealth);
    }
    /// Get the collision radius of this enemy.
    kfp::s16_16 getCollisionRadiusToPlayer() const noexcept;
    /// Set the collision radius of this enemy.
    EnemyHandle& setCollisionRadiusToPlayer(kfp::s16_16 r) noexcept;
    /// Get the collision radius of this enemy.
    kfp::s16_16 getCollisionRadiusToShot() const noexcept;
    /// Set the collision radius of this enemy.
    EnemyHandle& setCollisionRadiusToShot(kfp::s16_16 r) noexcept;
    /// Get the damage rate of this enemy.
    DamageRate getDamageRate() const noexcept;
    /// Set the damage rate of this enemy.
    EnemyHandle& setDamageRate(DamageRate rate) noexcept;
    /// Get the texture coordinates on the shotsheet for the bullet's graphic.
    agl::UIRect16 getTexcoords() const noexcept;
    /// Set the texture coordinates on the shotsheet for the bullet's graphic.
    EnemyHandle& setTexcoords(const agl::UIRect16 r) noexcept;
    /// Mark an enemy for deletion.
    EnemyHandle& markForDeletion() noexcept;
    /// Get the hitbox of the enemy to shots.
    Circle getHitboxToShot() const noexcept;
    /// Return true if the enemy will be deleted when its HP is 0.
    bool deletesOnDealth() const noexcept;
    /// Set whether the enemy will be deleted when its HP is 0.
    EnemyHandle& setDeleteOnDeath(bool d) noexcept;
    /// Set whether the backsheet should be used for this enemy.
    EnemyHandle& setBacksheet(bool b) noexcept;

  private:
    EnemyHandle(Enemies* e, GAH gh);
    Enemies* e;
    GAH gh;
    friend class Enemies;
  };
}
