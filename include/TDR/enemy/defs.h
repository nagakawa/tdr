#pragma once

#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"

namespace tdr {
  /// Describes how incoming shot and bomb damage is multiplied for enemies.
  struct DamageRate {
    /// The factor to multiply incoming shot damage by.
    kfp::s16_16 fromShot = 1;
    /// The factor to multiply incoming spell damage by.
    kfp::s16_16 fromBomb = 1;
    constexpr kfp::s16_16 getMultiplierFor(PlayerAttackDivision div) const
        noexcept {
      switch (div) {
      case PlayerAttackDivision::shot: return fromShot;
      case PlayerAttackDivision::spell: return fromBomb;
      }
    }
  };
  struct EnemyRenderInfo {
    EnemyRenderInfo() = default;
    PV position, dimensions;
    agl::UIRect16 texcoords;
    kfp::frac32 rotation;
    bool takesBackSprite; // If true, use the back spritesheet to render.
  };
  /// Represents an enemy graphic from a shotsheet of enemies.
  struct EnemyGraphic {
    agl::UIRect16 texcoords;
    kfp::s16_16 collisionRadiusToPlayer;
    kfp::s16_16 collisionRadiusToShot;
  };
  using EnemyShotsheet = TShotsheet<EnemyGraphic>;
  static_assert(
      std::is_trivial_v<EnemyRenderInfo>, "EnemyRenderInfo should be trivial");
}
