#pragma once

#include <unordered_map>

#include <AGL/rect.h>
#include <kozet_fixed_point/kfp.h>
#include <zekku/geometry.h>

#include "TDR/GenerationalAllocator.h"

namespace tdr {
  // ===== TYPEDEFS ==========================================================
  using Circle = zekku::Circle<kfp::s16_16>;
  using Line = zekku::Line<kfp::s16_16>;
  using Laser = zekku::AABB<kfp::s16_16>;
  using PV = glm::tvec2<kfp::s16_16>;
  /// A property that determines what kind of player shot we have.
  enum class PlayerAttackDivision : uint8_t {
    shot, ///< Used for ordinary player shots.
    spell, ///< Used for bullets spawned by bombs.
  };
  /**
   * \brief A structure used for rendering bullets.
   */
  struct BulletRenderInfo {
    BulletRenderInfo() = default;
    PV position;
    agl::UIRect16 texcoords;
    kfp::frac32 visualAngle;
    kfp::s16_16 visualLength;
    kfp::s16_16 visualWidth;
    uint32_t isLaser;
    glm::u8vec4 blendColour;
  };
  static_assert(
      offsetof(BulletRenderInfo, visualAngle) + 4 ==
          offsetof(BulletRenderInfo, visualLength),
      "Offset of visualLength in BulletRenderInfo must be "
      "exactly 4 more than that of visualAngle");
  static_assert(
      offsetof(BulletRenderInfo, visualLength) + 4 ==
          offsetof(BulletRenderInfo, visualWidth),
      "Offset of visualWidth in BulletRenderInfo must be "
      "exactly 4 more than that of visualRadius");
  static_assert(
      std::is_trivial_v<BulletRenderInfo>,
      "BulletRenderInfo should be trivial");
  // ===== BULLETFLAGS =======================================================
  /// Flags used in \ref Bullets to indicate properties of a bullet.
  enum class BulletFlags : uint8_t {
    none = 0, ///< No options set.
    /// For player bullets only, erases any enemy shots that are not
    /// spell-resistant and come into contact with this one.
    erasesShots = 1,
    /// For enemy bullets only, will not be erased by bombs.
    spellResistant = 1,
    /// This bullet uses speed/angle parameters to move instead
    /// of horizontal and vertical velocities.
    useRadial = 2,
    /// The visual and movement angles for this bullet are not
    /// synchronised.
    detachVisualAndMovementAngles = 4,
    /// This bullet will be deleted once it is completely out of
    /// bounds.
    deleteWhenOutOfBounds = 8,
    /// This bullet has a hitbox and will hit the player if their
    /// hitboxes intersect.
    collides = 16,
    /// Some properties of this bullet need to be updated.
    dirty = 32,
    /// This bullet is a laser.
    isLaser = 64,
    /// This bullet will drop an item when cancelled.
    shouldDropItem = 128,
    /// Default properties of a bullet when it spawns.
    defo =
        useRadial | deleteWhenOutOfBounds | collides | dirty | shouldDropItem,
  };
  inline BulletFlags operator&(BulletFlags a, BulletFlags b) {
    return (BulletFlags)((unsigned) a & (unsigned) b);
  }
  inline BulletFlags operator|(BulletFlags a, BulletFlags b) {
    return (BulletFlags)((unsigned) a | (unsigned) b);
  }
  inline BulletFlags operator^(BulletFlags a, BulletFlags b) {
    return (BulletFlags)((unsigned) a ^ (unsigned) b);
  }
  inline BulletFlags& operator&=(BulletFlags& a, BulletFlags b) {
    return a = a & b;
  }
  inline BulletFlags& operator|=(BulletFlags& a, BulletFlags b) {
    return a = a | b;
  }
  inline BulletFlags& operator^=(BulletFlags& a, BulletFlags b) {
    return a = a ^ b;
  }
  inline BulletFlags operator~(BulletFlags a) {
    return (BulletFlags)(~(unsigned) a);
  }
  inline bool operator==(BulletFlags a, BulletFlags b) {
    return (unsigned) a == (unsigned) b;
  }
  inline bool operator!=(BulletFlags a, BulletFlags b) {
    return (unsigned) a != (unsigned) b;
  }
  inline bool operator==(BulletFlags a, unsigned b) {
    return (unsigned) a == b;
  }
  inline bool operator!=(BulletFlags a, unsigned b) {
    return (unsigned) a != b;
  }
  using BulletIndex = uint32_t;
  using GA = GenerationalAllocator<uint64_t, BulletIndex>;
  using GAH = GA::Handle;
  class Bullets;
}
