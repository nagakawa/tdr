#pragma once

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#include <algorithm>
#include <limits>
#include <memory>
#include <stack>
#include <unordered_set>

#include "zekku/BoxQuadTree.h"
#include "zekku/geometry.h"
#include "zekku/timath.h"

namespace tdr {
  template<
      typename F, // type to use for coördinates
      typename I, // index type
      typename P, // type to store as points
      typename GetBB, // callable to get bounding box
      typename GetCollision // callable to test collision
      >
  class IAABBTree {
  public:
    static_assert(std::numeric_limits<F>::is_specialized);
    static_assert(
        std::is_integral<I>::value, "Your I is not an integer, dum dum!");
    static_assert(
        std::is_unsigned<I>::value,
        "Don't use a signed int for sizes, dum dum!");
    static_assert(
        std::numeric_limits<F>::is_specialized,
        "Your F is not a number, dum dum!");
    static constexpr size_t initialCapacity = 16;
    template<typename... Args>
    IAABBTree(GetBB&& gbb, GetCollision&& gcol, F expansionLength) :
        gbb(std::move(gbb)), gcol(std::move(gcol)), nodes(initialCapacity),
        root(-1U), expansionLength(expansionLength) {
      setUpFreeList(0);
    }
    struct Handle {
      I index;
    };
    P deref(Handle h) const {
      assert(nodes[h.index].height == 0);
      return nodes[h.index].data.data;
    }
    Handle insert(P data) {
      I nid = getFreeNode();
      zekku::AABB<F> aabb = gbb(data).expand(expansionLength);
      /* local */ {
        Node& node = nodes[nid];
        if (root == -1U) { // No root!
          node.makeLeaf(-1, aabb, data);
          root = nid;
          return {nid};
        }
      }
      I sid = findBestSibling(aabb);
      I newpid = getFreeNode();
      Node& node = nodes[nid];
      Node& sibling = nodes[sid];
      Node& newParent = nodes[newpid];
      I oldpid = sibling.parentOrNext;
      newParent.makeBranch(1 + sibling.height, oldpid, aabb, nid, sid);
      sibling.parentOrNext = newpid;
      if (oldpid != -1U) {
        Node& oldParent = nodes[oldpid];
        if (oldParent.data.children.left == sid)
          oldParent.data.children.left = newpid;
        else if (oldParent.data.children.right == sid)
          oldParent.data.children.right = newpid;
        else
          assert(!"Old parent does not have found sibling as left or right child!");
      } else {
        root = newpid;
      }
      node.makeLeaf(newpid, aabb, data);
      syncHierarchy(nid);
      return {nid};
    }
    void remove(Handle h) {
      I id = h.index;
      Node& n = nodes[id];
      assert(n.isLeaf());
      I pid = n.parentOrNext;
      if (pid == -1U) {
        // This is the only node in the tree
        releaseFreeNode(id);
        root = -1U;
        return;
      }
      Node& p = nodes[pid];
      I sid = (id == p.data.children.left) ? p.data.children.right :
                                             p.data.children.left;
      Node& s = nodes[sid];
      I ppid = p.parentOrNext;
      Node& pp = nodes[ppid];
      // P(N, S) -> S; P(S, N) -> S
      s.parentOrNext = ppid;
      if (ppid != -1U) {
        if (pp.data.children.left == pid)
          pp.data.children.left = sid;
        else if (pp.data.children.right == pid)
          pp.data.children.right = sid;
        else
          assert(!"pp doesn't have p as child?");
      } else {
        root = sid;
      }
      releaseFreeNode(pid);
      releaseFreeNode(id);
      syncHierarchy(sid);
    }
    Handle update(Handle h) {
      I id = h.index;
      Node& n = nodes[id];
      // Still within the bounding box?
      auto bb = gbb(n.data.data);
      if (!bb.isWithin(n.bounding)) {
        // No, update
        remove(h);
        return insert(n.data.data);
      }
      return h;
    }
    template<typename Q = zekku::AABB<F>, typename C>
    bool query(const Q& shape, const C& callback) const {
      if (root == -1U) return true;
      std::stack<I, std::vector<I>> st(std::vector<I>{});
      st.push(root);
      while (!st.empty()) {
        I id = st.top();
        st.pop();
        const Node& n = nodes[id];
        if (shape.intersects(n.bounding)) {
          if (n.isLeaf()) {
            if (gcol(n.data.data, shape)) {
              bool stat = callback(n.data.data);
              if (!stat) return false;
            }
          } else {
            st.push(n.data.children.left);
            st.push(n.data.children.right);
          }
        }
      }
      return true;
    }
    template<typename Q = zekku::AABB<F>>
    std::vector<Handle> query(const Q& shape) const {
      std::vector<Handle> entries;
      query(shape, entries);
      return entries;
    }
    template<typename Q = zekku::AABB<F>>
    void query(const Q& shape, std::vector<Handle>& entries) const {
      /*(void) query(shape, [&entries](const T& t) {
        entries.push_back(&t);
        return true;
      });*/
      if (root == -1U) return;
      size_t oldSize = entries.size();
      std::stack<I, std::vector<I>> st(std::vector<I>{});
      st.push(root);
      while (!st.empty()) {
        I id = st.top();
        st.pop();
        const Node& n = nodes[id];
        if (shape.intersects(n.bounding)) {
          if (n.isLeaf()) {
            entries.push_back(Handle{id});
          } else {
            st.push(n.data.children.left);
            st.push(n.data.children.right);
          }
        }
      }
      auto newEnd = std::remove_if(
          entries.begin() + oldSize, entries.end(),
          [&shape, this](const Handle h) { return !gcol(deref(h), shape); });
      entries.erase(newEnd, entries.end());
    }
    template<typename C> void apply(const C& callback) {
      if (root == -1U) return;
      std::stack<I, std::vector<I>> st(std::vector<I>{});
      st.push(root);
      std::unordered_set<I> blacklist;
      std::cerr << "apply:\n";
      while (!st.empty()) {
        I id = st.top();
        st.pop();
        Node& n = nodes[id];
        std::cerr << "Popping " << id << " w/ height " << n.height << "\n";
        if (n.isFree()) {
          dump();
          abort();
        }
        if (n.isLeaf()) {
          if (blacklist.count(id) != 0) continue;
          bool shouldKeep = callback(n.data.data);
          if (shouldKeep) {
            // Update zekku::AABB and reïnsert if necessary
            // Is the actual hitbox still in the zekku::AABB?
            auto bb = gbb(n.data.data);
            if (!bb.isWithin(n.bounding)) {
              // No, update
              remove(Handle{id});
              blacklist.insert(insert(n.data.data).index);
              std::cerr << "UPDATE\n";
              // n.bounding = gbb(*n.data.data);
              // syncHierarchy(id);
            }
          } else {
            // Remove node from tree
            remove(Handle{id});
            std::cerr << "DELETE\n";
          }
        } else {
          st.push(n.data.children.left);
          st.push(n.data.children.right);
          std::cerr << "Pushing " << n.data.children.left << "\n";
          std::cerr << "Pushing " << n.data.children.right << "\n";
        }
      }
    }
    void dump() {
      std::unordered_set<I> visited;
      dump(root, -1U, 0, visited);
    }

  private:
    class Node {
    public:
      Node() = default;
      void makeFree(I next) {
        height = -1U;
        parentOrNext = next;
      }
      void makeLeaf(I parent, const zekku::AABB<F>& aabb, P data) {
        bounding = aabb;
        height = 0;
        parentOrNext = parent;
        this->data.data = data;
      }
      void makeBranch(
          I height, I parent, const zekku::AABB<F>& aabb, I left, I right) {
        assert(height != 0 && height != -1U);
        bounding = aabb;
        this->height = height;
        parentOrNext = parent;
        this->data.children.left = left;
        this->data.children.right = right;
      }
      bool isFree() const { return height == -1U; }
      bool isLeaf() const { return height == 0; }
      // Assuming 64-bit pointers and default type params...
      zekku::AABB<F> bounding; // 16B
      // 0 = child, -1 = free
      I height; // 20B
      I parentOrNext; // 24B
      union {
        P data;
        struct {
          I left, right;
        } children;
      } data; // 32B
      // This is why we used a raw union here and not a variant.
    };
    ZK_NOUNIQADDR GetBB gbb;
    ZK_NOUNIQADDR GetCollision gcol;
    std::vector<Node> nodes;
    I freeList;
    I root;
    F expansionLength;
    void setUpFreeList(size_t startIndex) {
      for (size_t i = startIndex; i < nodes.size() - 1; ++i) {
        nodes[i].parentOrNext = i + 1;
        nodes[i].height = -1;
      }
      nodes.back().parentOrNext = -1;
      nodes.back().height = -1;
      freeList = startIndex;
    }
    I getFreeNode() {
      if (freeList == -1U) {
        size_t oldSize = nodes.size();
        assert(oldSize < std::numeric_limits<I>::max() / 2);
        nodes.resize(oldSize * 2);
        setUpFreeList(oldSize);
      }
      I old = freeList;
      freeList = nodes[old].parentOrNext;
      return old;
    }
    void releaseFreeNode(I id) {
      Node& n = nodes[id];
      n.makeFree(freeList);
      freeList = id;
    }
    static auto nodecost(const Node& n, const zekku::AABB<F>& aabb) {
      assert(!n.isFree());
      auto un = n.bounding.unions(aabb);
      return un.area() - n.bounding.area();
    }
    I findBestSibling(const zekku::AABB<F>& aabb) const {
      const Node* curNode = &nodes[root];
      while (!curNode->isLeaf()) {
        const Node& l = nodes[curNode->data.children.left];
        const Node& r = nodes[curNode->data.children.right];
        // The heuristics were originally based on Box2D's implementation,
        // but they didn't work out too well, so they're slightly
        // here.
        auto area = curNode->bounding.area();
        auto cost = area;
        auto cost1 = nodecost(l, aabb);
        auto cost2 = nodecost(r, aabb);
        if (cost < cost1 && cost < cost2) break;
        curNode = &nodes
                      [(cost1 < cost2) ? curNode->data.children.left :
                                         curNode->data.children.right];
      }
      return curNode - &nodes[0];
    }
    void updateNodeHeight(Node& n) {
      const Node& l = nodes[n.data.children.left];
      const Node& r = nodes[n.data.children.right];
      n.height = 1 + std::max(l.height, r.height);
      n.bounding = l.bounding.unions(r.bounding);
    }
    void syncHierarchy(I id) {
      I curNode = nodes[id].parentOrNext;
      while (curNode != -1U) {
        Node& node = nodes[curNode];
        const Node& l = nodes[node.data.children.left];
        const Node& r = nodes[node.data.children.right];
        int hd = r.height - l.height;
        if (hd > 1 || hd < -1) {
          curNode = rebalance(curNode, hd);
          curNode = nodes[curNode].parentOrNext;
          continue;
        }
        node.height = 1 + std::max(l.height, r.height);
        node.bounding = l.bounding.unions(r.bounding);
        curNode = node.parentOrNext;
      }
    }
    I rebalance(I xid, int xhd) {
      I nrid = rebalanceAux(xid, xhd);
      Node& newRoot = nodes[nrid];
      if (newRoot.parentOrNext == -1U)
        root = nrid;
      else {
        Node& parent = nodes[newRoot.parentOrNext];
        if (parent.data.children.left == xid)
          parent.data.children.left = nrid;
        else if (parent.data.children.right == xid)
          parent.data.children.right = nrid;
        else
          assert(!"Parent somehow lost xid?");
      }
      return nrid;
    }
    I rebalanceAux(I xid, int xhd) {
      Node& x = nodes[xid];
      bool isZRightOfX = xhd > 0;
      I zid = isZRightOfX ? x.data.children.right : x.data.children.left;
      Node& z = nodes[zid];
      assert(z.height > 0);
      Node& zl = nodes[z.data.children.left];
      Node& zr = nodes[z.data.children.right];
      I pox = x.parentOrNext;
      int zhd = zr.height - zl.height;
      if (isZRightOfX && zhd >= 0) {
        // Right Right: X(t1, Z(t23, t4)) -> Z(X(t1, t23), t4)
        I t1 = x.data.children.left;
        I t23 = z.data.children.left;
        x.parentOrNext = zid;
        x.data.children.left = t1;
        x.data.children.right = t23;
        z.parentOrNext = pox;
        z.data.children.left = xid;
        nodes[t1].parentOrNext = xid;
        nodes[t23].parentOrNext = xid;
        updateNodeHeight(x);
        updateNodeHeight(z);
        return zid;
      } else if (!isZRightOfX && zhd <= 0) {
        // Left Left: X(Z(t1, t23), t4) -> Z(t1, X(t23, t4))
        I t23 = z.data.children.right;
        I t4 = x.data.children.right;
        x.parentOrNext = zid;
        x.data.children.left = t23;
        x.data.children.right = t4;
        z.parentOrNext = pox;
        z.data.children.right = xid;
        nodes[t23].parentOrNext = xid;
        nodes[t4].parentOrNext = xid;
        updateNodeHeight(x);
        updateNodeHeight(z);
        return zid;
      } else if (isZRightOfX && zhd < 0) {
        // Right Left: X(t1, Z(Y(t2, t3), t4)) -> Y(X(t1, t2), Z(t3, t4))
        // or          X(t1, Z(Y, t4)) -> Y(X(t1, t4), Z)
        I yid = z.data.children.left;
        Node& y = nodes[yid];
        // I t1 = x.data.children.left;
        I t4 = z.data.children.right;
        if (y.height == 0) {
          // special case
          x.data.children.right = t4;
          x.parentOrNext = yid;
          z.parentOrNext = yid;
          z.height = 0; // set to leaf
          y.parentOrNext = pox;
          y.data.children.left = xid;
          y.data.children.right = zid;
          nodes[t4].parentOrNext = xid;
          updateNodeHeight(x);
          updateNodeHeight(y);
        } else {
          I t2 = y.data.children.left;
          I t3 = y.data.children.right;
          x.data.children.right = t2;
          x.parentOrNext = yid;
          z.parentOrNext = yid;
          z.data.children.left = t3;
          y.parentOrNext = pox;
          y.data.children.left = xid;
          y.data.children.right = zid;
          nodes[t2].parentOrNext = xid;
          nodes[t3].parentOrNext = zid;
          updateNodeHeight(x);
          updateNodeHeight(z);
          updateNodeHeight(y);
        }
        return yid;
      } else if (!isZRightOfX && zhd > 0) {
        // Left Right: X(Z(t1, Y(t2, t3)), t4) -> Y(X(t1, t2), Z(t3, t4))
        // or          X(Z(t1, Y), t4) -> Y(X, Z(t1, t4))
        I yid = z.data.children.right;
        Node& y = nodes[yid];
        I t1 = z.data.children.left;
        I t4 = x.data.children.right;
        if (y.height == 0) {
          x.parentOrNext = yid;
          x.height = 0; // set to leaf
          z.parentOrNext = yid;
          z.data.children.right = t4;
          y.parentOrNext = pox;
          y.data.children.left = xid;
          y.data.children.right = zid;
          nodes[t4].parentOrNext = zid;
          updateNodeHeight(z);
          updateNodeHeight(y);
        } else {
          I t2 = y.data.children.left;
          I t3 = y.data.children.right;
          x.parentOrNext = yid;
          x.data.children.left = t1;
          x.data.children.right = t2;
          z.parentOrNext = yid;
          z.data.children.left = t3;
          z.data.children.right = t4;
          y.parentOrNext = pox;
          y.data.children.left = xid;
          y.data.children.right = zid;
          nodes[t1].parentOrNext = xid;
          nodes[t2].parentOrNext = xid;
          nodes[t3].parentOrNext = zid;
          nodes[t4].parentOrNext = zid;
          updateNodeHeight(x);
          updateNodeHeight(z);
          updateNodeHeight(y);
        }
        return yid;
      } else {
        assert(!"No cases matched!");
        return -1U;
      }
    }
    static void indent(size_t n) {
      for (size_t i = 0; i < n; ++i) std::cerr << ' ';
    }
    void dump(I id, I parent, size_t s, std::unordered_set<I>& visited) {
      Node& n = nodes[id];
      indent(s);
      if (s >= 20) {
        std::cerr << "We've gone deep enough.\n";
        return;
      }
      std::cerr << "Node #" << id;
      if (!n.isFree() && n.parentOrNext != parent)
        std::cerr << " (Warning: parent does not match)";
      if (visited.count(id)) {
        std::cerr << " was already visited; we're hyper fucked!\n";
        return;
        // abort();
      }
      visited.insert(id);
      if (n.isFree()) {
        std::cerr << " shouldn't be in the tree!\n";
        return;
        // abort();
      }
      if (n.isFree()) {
        std::cerr << " (WTF, this node is free?!!)\n";
      } else if (n.isLeaf()) {
        std::cerr << " (leaf with data " << n.data.data << ")\n";
      } else {
        std::cerr << " (stem)\n";
        dump(n.data.children.left, id, s + 1, visited);
        dump(n.data.children.right, id, s + 1, visited);
      }
    }
  };
}
