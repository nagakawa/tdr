#pragma once

#include <stdint.h>

#include <algorithm>
#include <bitset>
#include <limits>
#include <type_traits>

#include <glm/glm.hpp>
#include <zekku/BoxQuadTree.h>
#include <zekku/base.h>
#include <zekku/geometry.h>

namespace tdr {
  /**
   * \brief A quadtree that stores instances of an arbitrary trivially
   * copyable type and receives callables that define the bounding boxes and
   * collision behaviours associated therewith.
   *
   * \tparam F The type to use for the coördinates.
   * \tparam I The type to use for internal indices. This imposes a limit on
   * how many objects this quadtree can store.
   * \tparam P The type to store in the quadtree.
   * \tparam GetBB A callable with the following method:
   *   zekku::AABB<F> operator()(P p) const;
   * \tparam GetCollision A callable with the following method for `Q` =
   * `zekku::AABB<F>`, as well as any other shapes against which one wants
   * to query:
   *   bool operator()(P p, const Q& q) const;
   */
  template<
      typename F, // type to use for coördinates
      typename I, // index type
      typename P, // type to store as points
      typename GetBB, // callable to get bounding box
      typename GetCollision // callable to test collision
      >
  class IQuadTree {
  public:
    static constexpr I nc = zekku::QUADTREE_NODE_COUNT;
    static_assert(
        std::numeric_limits<uint8_t>::max() >= zekku::QUADTREE_NODE_COUNT);
    static_assert(std::numeric_limits<F>::is_specialized);
    static_assert(std::is_integral_v<I>);
    static_assert(std::is_trivially_copyable_v<P>);
    struct Handle {
      I nodeno;
      uint8_t index;
    };
    IQuadTree(const zekku::AABB<F>& box, GetBB&& gbox, GetCollision&& gcol) :
        box(box), root((I) nodes.allocate()), gbox(std::forward<GetBB>(gbox)),
        gcol(std::forward<GetCollision>(gcol)) {
      Node& n = nodes.get(root);
      n.parent = -1U;
      n.box = box;
    }
    IQuadTree(const IQuadTree<F, I, P, GetBB, GetCollision>&) = delete;
    Handle insert(P t) {
      zekku::AABB<F> bb = gbox(t);
      /*
        TODO: this chokes on anything that isn't inside the box.
        Should at least fall back in this case, or ideally expand
        the bounding box.
      */
      while (!bb.isWithin(box)) {
        // Expansion direction is a bit flaky...
        expandBox(bb.c.x > box.c.x, bb.c.y > box.c.y);
      }
      return insert(t, bb, root);
    }
    void remove(Handle h) {
      Node& n = nodes.get(h.nodeno);
      uint8_t index = h.index;
      n.occupied[index] = false;
      n.hash ^= zekku::BBHash<F>()(gbox(n.nodes[index]));
      collapseEmptyNodes(n);
    }
    Handle update(Handle h) {
      Node& n = nodes.get(h.nodeno);
      uint8_t index = h.index;
      P p = n.nodes[index];
      zekku::AABB<F> bb = gbox(p);
      if (bb.isWithin(n.box)) {
        if (n.stem && (bb.isWithin(n.box.nw()) || bb.isWithin(n.box.ne()) ||
                       bb.isWithin(n.box.sw()) || bb.isWithin(n.box.se()))) {
          remove(h);
          return insert(p, bb, h.nodeno);
        }
        return h;
      }
      // Need to change nodes
      remove(h);
      while (!bb.isWithin(box)) {
        // Expansion direction is a bit flaky...
        expandBox(bb.c.x > box.c.x, bb.c.y > box.c.y);
      }
      return insert(p, bb, root);
    }
    template<typename Q> void query(const Q& shape, std::vector<Handle>& out) {
      query(shape, out, root);
    }
    template<typename Q> std::vector<Handle> query(const Q& shape) {
      std::vector<Handle> out;
      query(shape, out);
      return out;
    }
    P deref(Handle h) const { return nodes.get(h.nodeno).nodes[h.index]; }

  private:
    struct Node {
      Node() : hash(0), link(false), stem(false) {}
      zekku::AABB<F> box;
      size_t hash;
      I children[4];
      I parent;
      bool link, stem;
      std::bitset<nc> occupied;
      P nodes[nc];
    };
    I createNode() {
      size_t i = nodes.allocate();
      return (I) i;
    }
    void freeNode(I i) { nodes.deallocate(i); }
    zekku::AABB<F> box;
    zekku::Pool<Node> nodes;
    I root;
    ZK_NOUNIQADDR GetBB gbox;
    ZK_NOUNIQADDR GetCollision gcol;
    Handle insertStem(P t, const zekku::AABB<F>& bb, I root) {
      Node* np = &nodes.get(root);
      unsigned count = 0, index = 4;
      if (bb.intersects(np->box.nw())) {
        ++count;
        index = 0;
      }
      if (bb.intersects(np->box.ne())) {
        ++count;
        index = 1;
      }
      if (bb.intersects(np->box.sw())) {
        ++count;
        index = 2;
      }
      if (bb.intersects(np->box.se())) {
        ++count;
        index = 3;
      }
      // By now, at least one element of intersect *should* be true,
      // but rounding errors can result in p intersecting with box
      // but not with any of its subboxes.
      // Intersects two or more quadrants?
      if (count >= 2) { return insert(t, bb, root, true); }
      // Otherwise...
      switch (index) {
      case 0: return insert(t, bb, np->children[0]);
      case 1: return insert(t, bb, np->children[1]);
      case 2: return insert(t, bb, np->children[2]);
      case 3: return insert(t, bb, np->children[3]);
      default: assert(!"impossible");
      }
    }
    Handle
    insert(P t, const zekku::AABB<F>& bb, I root, bool forceHere = false) {
      Node* np = &nodes.get(root);
      while (np->link) {
        root = np->children[0];
        np = &nodes.get(root);
      }
      if (np->stem && !forceHere) { // stem; forward to branch
        return insertStem(t, bb, root);
      }
      size_t freeSlot = 0;
      while (freeSlot < nc) {
        if (!np->occupied[freeSlot]) break;
        ++freeSlot;
      }
      if (freeSlot != nc) { // can fit into this node
        np->nodes[freeSlot] = t;
        np->hash ^= zekku::BBHash<F>()(bb);
        np->occupied[freeSlot] = true;
        return Handle{root, (uint8_t) freeSlot};
      } else if (np->hash != 0 && !np->link && !forceHere) {
        // Leaf is full; split into multiple trees
        I nw = createNode();
        I ne = createNode();
        I sw = createNode();
        I se = createNode();
        np = &nodes.get(root); // reference might be invalidated
        np->children[0] = nw;
        np->children[1] = ne;
        np->children[2] = sw;
        np->children[3] = se;
        nodes.get(nw).parent = nodes.get(ne).parent = nodes.get(sw).parent =
            nodes.get(se).parent = root;
        nodes.get(nw).box = np->box.nw();
        nodes.get(ne).box = np->box.ne();
        nodes.get(sw).box = np->box.sw();
        nodes.get(se).box = np->box.se();
        np->stem = true;
        return insertStem(t, bb, root);
      } else {
        // Leaf is full, and chances are:
        // Either all n points are the same, or
        // we have a false positive of the above,
        // or forceHere is true
        // (in which case isNowhere might be true as well)
        I nw = createNode(); // Create a node for overflow
        np = &nodes.get(root);
        Node& nwNode = nodes.get(nw);
        // Transfer children from *np to nw (if any)
        if (np->stem) {
          nwNode.stem = true;
          std::copy_n(np->children, 4, nwNode.children);
          for (size_t i = 0; i < 4; ++i) {
            nodes.get(nwNode.children[i]).parent = nw;
          }
        }
        np->children[0] = nw;
        np->link = true;
        np->stem = false;
        nwNode.parent = root;
        nwNode.box = np->box;
        return insert(t, bb, nw);
      }
    }
    bool isEmpty(const Node& n) {
      return n.occupied.none() && !n.link && !n.stem;
    }
    void collapseEmptyNodes(Node& n) {
      if (n.parent == -1U) return; // Doesn't have a parent
      if (!isEmpty(n)) return;
      // Collapse a node if all of its children are empty
      Node& parent = nodes.get(n.parent);
      if (parent.link) {
        // Parent is link node; child is only child
        freeNode(&n - nodes.data());
        parent.link = false;
      } else if (
          isEmpty(nodes.get(parent.children[0])) &&
          isEmpty(nodes.get(parent.children[1])) &&
          isEmpty(nodes.get(parent.children[2])) &&
          isEmpty(nodes.get(parent.children[3]))) {
        for (size_t i = 0; i < 4; ++i) freeNode(parent.children[i]);
        parent.stem = false;
      }
      collapseEmptyNodes(parent);
    }
    template<typename Q>
    void query(const Q& shape, std::vector<Handle>& out, I root) const {
      const Node* np = &(nodes.get(root));
      // Abort if the query shape doesn't intersect the box
      if (!shape.intersects(np->box)) return;
      while (np->link) {
        for (I i = 0; i < nc; ++i) {
          if (np->occupied[i] && gcol(np->nodes[i], shape)) {
            out.push_back(Handle{(P)(np - nodes.data()), (uint8_t) i});
          }
        }
        np = &(nodes.get(np->children[0]));
      }
      for (I i = 0; i < nc; ++i) {
        if (np->occupied[i] && gcol(np->nodes[i], shape)) {
          out.push_back(Handle{(P)(np - nodes.data()), (uint8_t) i});
        }
      }
      if (np->stem) {
        // Stem (and possibly a leaf)
        query(shape, out, np->children[0]);
        query(shape, out, np->children[1]);
        query(shape, out, np->children[2]);
        query(shape, out, np->children[3]);
      }
    }
    void expandBox(bool xplus, bool yplus) {
      int xsign = xplus ? 1 : -1;
      int ysign = yplus ? 1 : -1;
      zekku::AABB<F> expandedBox = {
          {box.c.x + box.s.x * xsign, box.c.y + box.s.y * ysign},
          {box.s.x * 2, box.s.y * 2}};
      size_t childno = 2 * xplus + yplus;
      I newRoot = nodes.allocate();
      I c0 = nodes.allocate();
      I c1 = nodes.allocate();
      I c2 = nodes.allocate();
      Node& newNode = nodes.get(newRoot);
      newNode.box = expandedBox;
      newNode.parent = (I) -1;
      newNode.stem = true;
      switch (childno) {
      case 0:
        newNode.children[0] = root;
        newNode.children[1] = c0;
        newNode.children[2] = c1;
        newNode.children[3] = c2;
        break;
      case 1:
        newNode.children[0] = c0;
        newNode.children[1] = root;
        newNode.children[2] = c1;
        newNode.children[3] = c2;
        break;
      case 2:
        newNode.children[0] = c0;
        newNode.children[1] = c1;
        newNode.children[2] = root;
        newNode.children[3] = c2;
        break;
      case 3:
        newNode.children[0] = c0;
        newNode.children[1] = c1;
        newNode.children[2] = c2;
        newNode.children[3] = root;
        break;
      default: assert(!"childno is out of range");
      }
      for (size_t i = 0; i < 4; ++i) {
        Node& n = nodes.get(newNode.children[i]);
        n.parent = newRoot;
        n.box = expandedBox.getSubboxByClass(i);
      }
      box = expandedBox;
      root = newRoot;
    }
    static void indent(size_t n) {
      for (size_t i = 0; i < n; ++i) std::cerr << ' ';
    }
    static void printAABB(const zekku::AABB<F>& box) {
      std::cerr << "[" << box.c[0] - box.s[0] << ", " << box.c[1] - box.s[1]
                << "; " << box.c[0] + box.s[0] << ", " << box.c[1] + box.s[1]
                << "] ";
    }
    void dump(I root, size_t s = 0) const {
      const Node* n = &nodes.get(root);
      const Node* end = n;
      while (end->link) end = &nodes.get(end->children[0]);
      if (end->stem) {
        std::cerr << "Stem (with overflow nodes) ";
        printAABB(n->box);
        std::cerr << ": ";
      } else {
        std::cerr << "Leaf ";
        printAABB(n->box);
        std::cerr << ": ";
      }
      while (n->link) {
        std::cerr << "«" << (n - nodes.data()) << "» ";
        for (size_t i = 0; i < nc; ++i) { std::cerr << n->nodes[i] << " "; }
        n = &nodes.get(n->children[0]);
      }
      std::cerr << "«" << (n - nodes.data()) << "» ";
      for (size_t i = 0; i < nc; ++i) {
        if (n->occupied[i]) std::cerr << n->nodes[i] << " ";
      }
      if (n->stem) {
        std::cerr << '\n';
        for (size_t i = 0; i < 4; ++i) {
          indent(s);
          std::cerr << ((i & 2) != 0 ? 'S' : 'N') << ((i & 1) != 0 ? 'E' : 'W')
                    << ' ';
          dump(n->children[i], s + 1);
        }
      }
      std::cerr << "\n";
      indent(s);
    }

  public:
    void dump() const { dump(root, 0); }
  };
}
