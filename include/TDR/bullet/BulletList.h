#pragma once

#include <AGL/FBO.h>
#include <AGL/GLFWApplication.h>
#include <AGL/Shader.h>
#include <AGL/ShaderProgram.h>
#include <AGL/Texture.h>
#include <AGL/VAO.h>
#include <AGL/VBO.h>

#include "TDR/bullet/BulletHandle.h"
#include "TDR/bullet/Bullets.h"
#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"

namespace tdr {
  class PlayfieldView;
  /**
   * \brief A list of bullets in a danmaku game.
   *
   * This class handles collision and updating, as well as batch-rendering
   * the bullets (unlike Danmakufu).
   */
  class BulletList {
  public:
    /**
     * \brief Construct a bullet list.
     *
     * \param p A pointer to the playfield to be used. Should not be null.
     * \param shotsheet The shotsheet to use for the bullets.
     */
    BulletList(Shotsheet&& shotsheet) : bullets(std::move(shotsheet)) {}
    /// Check if any bullets collide with the circle.
    // bool check(const Circle& h);
    // bool check(const Line& h);
    /// Update the positions of the bullets over one frame.
    void updatePositions(const agl::IRect16& bounds);
    /**
     * \brief Create a radial bullet.
     *
     * This method corresponds to Danmakufu's `CreateShotA1`.
     * \param position The position to spawn the bullet at (px)
     * \param speed The speed of the bullet (px / fr)
     * \param angle The angle of the bullet (turns)
     * \param graph The shot graphic to use for the bullet
     * \param delay The delay after which the bullet should spawn (fr)
     * \return A handle to the bullet.
     */
    BulletHandle createShotA1(
        PV position, kfp::s16_16 speed, kfp::frac32 angle, ShotID id,
        uint8_t delay);
    /**
     * \brief Create a radial laser that moves like a bullet.
     *
     * This method corresponds to Danmakufu's `CreateLooseLaser`.
     * \param position The position to spawn the bullet at (px)
     * \param speed The speed of the bullet (px / fr)
     * \param angle The angle of the bullet (turns)
     * \param length The length of the laser (px)
     * \param width The width of the laser (px)
     * \param graph The shot graphic to use for the bullet
     * \param delay The delay after which the bullet should spawn (fr)
     * \return A handle to the laser. The `position` supplied
     * corresponds to the tip of the laser.
     */
    BulletHandle createLooseLaser(
        PV position, kfp::s16_16 speed, kfp::frac32 angle, kfp::s16_16 length,
        kfp::s16_16 width, ShotID id, uint8_t delay);
    /**
     * \brief Check for collisions against a circle.
     *
     * \param h The circle to check against.
     * \param callback A callable to call on the bullet for each collision.
     */
    template<typename F> void collide(const Circle& h, F&& callback) {
      bullets.collide(h, std::forward<F>(callback));
    }
    /**
     * \brief Try to graze the bullets.
     *
     * \param h The circle to check against.
     * \param callback A callable to call on the bullet for each graze.
     */
    template<typename F> void graze(const Circle& h, F&& callback) {
      bullets.graze(h, std::forward<F>(callback));
    }
    /// Get the number of bullets.
    size_t nBullets() const;
    void clear();
    /**
     * \brief Set the default graze frequencies.
     *
     * \param bullet The default graze frequency for non-laser bullets.
     * Value | Behaviour of grazing
     * ------|---------------------
     *    -1 | Can't graze this bullet
     *     0 | Can graze only once
     * n > 0 | Can graze every n frames
     * \param laser The default graze frequency for lasers.
     */
    void setDefaultGrazeFrequencies(int8_t bullet, int8_t laser) {
      defaultBulletGrazeFrequency = bullet;
      defaultLaserGrazeFrequency = laser;
    }

    template<typename F> void forEach(F&& f) {
      bullets.forEach(std::forward<F>(f));
    }

    const Shotsheet& getShotsheet() const noexcept {
      return bullets.getShotsheet();
    }
    void setShotsheet(Shotsheet&& s) noexcept {
      bullets.setShotsheet(std::move(s));
    }

    std::vector<PV> getShotPositions() {
      std::vector<PV> positions;
      bullets.pushItemList(positions);
      return positions;
    }

    void setLaserErasureItemDistance(kfp::s16_16 value) {
      bullets.setLaserErasureItemDistance(value);
    }

  private:
    Bullets bullets;

  private:
    int8_t defaultBulletGrazeFrequency = 0;
    int8_t defaultLaserGrazeFrequency = 5;
    friend class BulletListView;
  };
  class BulletListView {
  public:
    BulletListView(const BulletList* bl, PlayfieldView* p) : bl(bl), p(p) {}
    /**
     * \brief Set up rendering for the bullet list.
     *
     * This should be called before any rendering.
     */
    void setUp();
    /// Set the texture used to render the bullets.
    void setTexture(agl::WeakTexture t) { this->t = std::move(t); }
    /// Render the bullets in the bullet list.
    void render();

    /// Set the delay cloud graphic.
    void setDelayCloud(Graphic gr) { delayCloud = gr; }

    const BulletList& getModel() const noexcept { return *bl; }
    void setModel(const BulletList* bl) noexcept { this->bl = bl; }

    void setAngleOffset(kfp::frac32 off) { angleOffset = off; }

  private:
    const BulletList* bl;
    PlayfieldView* p;
    agl::VBO vbo;
    agl::VBO instanceVBO;
    agl::VAO vao;
    agl::ShaderProgram program;
    agl::WeakTexture t;
    std::array<std::vector<BulletRenderInfo>, agl::nPresetBlends> rinfo;
    std::vector<size_t> offsets;
    Graphic delayCloud;
    kfp::frac32 angleOffset = 0;
    bool hasSetUniforms = false;
    bool hasInitialisedProgram = false;
    void setUniforms();
    void spurt();
    void update();
#if AGL_PROFILER_TDR_GOODIES
  public:
    double spurtTime;
#endif
  };
}
