#pragma once

#include "TDR/bullet/Shotsheet.h"

namespace tdr {
  struct Graphic;
  /**
   * \brief A handle to a bullet.
   *
   * The instance of \ref Bullets to which an instance of this class points
   * should live at least as long as the handle.
   *
   * Note that only \ref isMarkedForDeletion() and \ref markForDeletion()
   * can be called on deleted bullets. In debug builds, other methods will
   * assert that the bullet is not deleted.
   */
  class BulletHandle {
  public:
    BulletHandle(const BulletHandle& h) = default;
    BulletHandle(BulletHandle&& h) = default;
    BulletHandle& operator=(const BulletHandle& h) = default;
    BulletHandle& operator=(BulletHandle&& h) = default;
    ~BulletHandle() = default;
    // Just enough methods to make Bullet.cpp compile.
    // Eventually, everything from the old Bullet class will be
    // available here.
    /// Return true if this bullet is radial.
    bool isRadial() const noexcept;
    /// Return true if this bullet is about to be deleted.
    bool isMarkedForDeletion() const noexcept;
    /// Return true if this bullet is a laser.
    bool isLaserBullet() const noexcept;
    /// Return true if this bullet is spell-resistant.
    bool isSpellResistant() const noexcept;
    /// Return true if this bullet erases enemy shots.
    bool erasesShots() const noexcept;
    /**
     * \brief Return true if this bullet will be deleted when completely
     * outside the playfield.
     */
    bool willDeleteWhenOutOfBounds() const noexcept;
    /// Return true if this bullet can hit the player.
    bool willCollide() const noexcept;
    /// Return true if this bullet will drop an item when cancelled.
    bool willDropItem() const noexcept;
    /// Set whether this bullet is spell-resistant.
    BulletHandle& setSpellResistant(bool value) noexcept;
    /// Set whether this bullet erases enemy shots.
    BulletHandle& setEraseShots(bool value) noexcept;
    /// Set whether this bullet can hit the player.
    BulletHandle& setCollide(bool value) noexcept;
    /// Set whether this bullet will be deleted when completely
    /// outside the playfield.
    BulletHandle& setDeleteWhenOutOfBounds(bool value) noexcept;
    /// Set whether this bullet will drop an item when cancelled.
    BulletHandle& setDropItem(bool value) noexcept;
    /// Get the blend mode index.
    agl::BMIndex getBlendModeIndex() const noexcept;
    /**
     * \brief Get the true centre of the bullet.
     *
     * This is not necessarily equal to its *position* (returned by
     * `getPosition()`); in lasers, the position points to the end
     * of the laser instead of the centre.
     */
    glm::tvec2<kfp::s16_16> getTrueCentre() const noexcept;
    /// Get the position of the bullet.
    PV& getPosition() noexcept;
    /// Get the position of the bullet.
    const PV& getPosition() const noexcept;
    /// Set the position of the bullet.
    BulletHandle& setPosition(const PV& x) noexcept {
      getPosition() = x;
      return *this;
    }
    /// Add an offset to the position of the bullet.
    BulletHandle& addPosition(const PV& x) noexcept {
      getPosition() += x;
      return *this;
    }
    kfp::s2_30 getRefpoint() const noexcept;
    BulletHandle& setRefpoint(kfp::s2_30 r) noexcept;
    /// Get the velocity of the bullet.
    const PV& getVelocity() noexcept;
    /**
     * \brief Set the velocity of a bullet.
     *
     * The bullet must not be radial.
     */
    BulletHandle& setVelocity(const PV& v) noexcept;
    /**
     * \brief Add an offset to the velocity of a bullet.
     *
     * The bullet must not be radial.
     */
    BulletHandle& addVelocity(const PV& v) noexcept;
    /// Get the acceleration of the bullet.
    const PV& getAcceleration() noexcept;
    /**
     * \brief Set the acceleration of a bullet.
     *
     * The bullet must not be radial.
     */
    BulletHandle& setAcceleration(const PV& a) noexcept;
    /**
     * \brief Add an offset to the acceleration of a bullet.
     *
     * The bullet must not be radial.
     */
    BulletHandle& addAcceleration(const PV& a) noexcept;
    /// Get the speed of a bullet.
    kfp::s16_16 getSpeed() noexcept;
    /**
     * \brief Set the speed of a bullet.
     *
     * The bullet must be radial.
     */
    BulletHandle& setSpeed(kfp::s16_16 s) noexcept;
    /**
     * \brief Add a value to the speed of a bullet.
     *
     * The bullet must be radial.
     */
    BulletHandle& addSpeed(kfp::s16_16 s) noexcept;
    /// Get the movement angle of a bullet.
    kfp::frac32 getAngle() noexcept;
    /**
     * \brief Set the movement angle of a bullet.
     *
     * The bullet must be radial.
     */
    BulletHandle& setAngle(kfp::frac32 a) noexcept;
    /**
     * \brief Add a value to the movement angle of a bullet.
     *
     * The bullet must be radial.
     */
    BulletHandle& addAngle(kfp::frac32 a) noexcept;
    /// Get the angular velocity of a bullet.
    kfp::frac32 getAngularVelocity() noexcept;
    /**
     * \brief Set the angular velocity of a bullet.
     *
     * The bullet must be radial.
     */
    BulletHandle& setAngularVelocity(kfp::frac32 a) noexcept;
    /**
     * \brief Add a value to the angular velocity of a bullet.
     *
     * The bullet must be radial.
     */
    BulletHandle& addAngularVelocity(kfp::frac32 a) noexcept;
    /// Get the visual angle of a bullet.
    kfp::frac32 getVisualAngle() const noexcept;
    /// Set the visual angle of a bullet.
    BulletHandle& setVisualAngle(kfp::frac32 a) noexcept;
    /// Add a value to the visual angle of a bullet.
    BulletHandle& addVisualAngle(kfp::frac32 a) noexcept;
    /// Get the collision radius of this bullet.
    kfp::s16_16 getCollisionRadius() const noexcept;
    /// Set the collision radius of this bullet.
    BulletHandle& setCollisionRadius(kfp::s16_16 r) noexcept;
    /// Get the length of this laser.
    kfp::s16_16 getLaserLength() const noexcept;
    /// Set the length of this laser.
    BulletHandle& setLaserLength(kfp::s16_16 l) noexcept;

    /**
     * \brief Get the ``visual width'' of a bullet.
     *
     * Note that the method name is a misnomer; it returns half of the bullet
     * width.
     */
    kfp::s16_16 getVisualWidth() const noexcept;
    /**
     * \brief Get the ``visual width'' of a bullet.
     *
     * Note that the method name is a misnomer; it returns half of the bullet
     * width.
     */
    BulletHandle& setVisualWidth(kfp::s16_16 w) noexcept;
    /**
     * \brief Set the ``visual length'' of a bullet.
     *
     * Note that the method name is a misnomer; it returns half of the bullet
     * length.
     */
    kfp::s16_16 getVisualLength() const noexcept;
    /**
     * \brief Set the ``visual length'' of a bullet.
     *
     * Note that the method name is a misnomer; it returns half of the bullet
     * length.
     */
    BulletHandle& setVisualLength(kfp::s16_16 l) noexcept;
    /// Return the delay before this bullet spawns.
    uint8_t getDelay() const noexcept;
    /// Set the delay before this bullet appears.
    /// Note: NYI
    BulletHandle& setDelay(uint8_t d) noexcept;
    /// Get the amount of damage dealt by a bullet to an enemy.
    int16_t getDamage() const noexcept;
    /// Set the amount of damage dealt by a bullet to an enemy.
    BulletHandle& setDamage(int16_t d) noexcept;
    /**
     * \brief Get the graze frequency of this bullet.
     * \return
     * Value | Behaviour of grazing
     * ------|---------------------
     *    -1 | Can't graze this bullet
     *     0 | Can graze only once
     * n > 0 | Can graze every n frames
     */
    int8_t getGrazeFrequency() const noexcept;
    /**
     * \brief Set the graze frequency of this bullet.
     * \param f
     * Value | Behaviour of grazing
     * ------|---------------------
     *    -1 | Can't graze this bullet
     *     0 | Can graze only once
     * n > 0 | Can graze every n frames
     */
    BulletHandle& setGrazeFrequency(int8_t f) noexcept;
    /// Mark a bullet for deletion.
    BulletHandle& markForDeletion() noexcept;
    /// Get the hitbox of the bullet.
    Circle getHitbox() const noexcept;
    /// Return true if the hitbox of this bullet intersects with the given
    /// circle.
    bool intersectsWith(const Circle& c) const noexcept;
    BulletHandle& setAlpha(uint8_t alpha) noexcept;
    BulletHandle& setBlendColour(glm::u8vec4 col) noexcept;
    BulletHandle& setBlendMode(agl::BMIndex bm) noexcept;
    BulletHandle& setGraphic(ShotID id) noexcept;
    /// Get the attack division of a bullet.
    PlayerAttackDivision getPlayerAttackDivision() const noexcept;
    /// Set the attack division of a bullet.
    BulletHandle& setPlayerAttackDivision(PlayerAttackDivision div) noexcept;
    void pushPosition(std::vector<tdr::PV>& positions) const noexcept;

  private:
    BulletHandle(Bullets* b, GAH gh);
    void refreshParameters() noexcept;
    void makeDirty() noexcept;
    bool getFlag(BulletFlags f) const noexcept;
    BulletHandle& setFlag(BulletFlags f, bool b) noexcept;
    Bullets* b;
    GAH gh;
    friend class Bullets;
  };
}
