#pragma once

#include <vector>

#include <boost/align/aligned_allocator.hpp>

#include <AGL/BlendMode.h>
#include <glm/glm.hpp>
#include <zekku/kfp_interop/timath.h>

#include "TDR/bullet/BulletHandle.h"
#include "TDR/bullet/Shotsheet.h"

namespace tdr {
  constexpr kfp::s16_16 defaultLaserErasureItemDistance = 6;
  template<typename T, typename U>
  inline zekku::AABB<U> rectToAABB(const agl::TRect<T>& r) {
    glm::tvec2<U> c((U)(r.left + r.right) / 2, (U)(r.top + r.bottom) / 2);
    glm::tvec2<U> s((U)(r.right - r.left) / 2, (U)(r.bottom - r.top) / 2);
    return zekku::AABB<U>{c, s};
  }
  class Bullets {
  public:
    /// A type that encodes a hitbox.
    struct alignas(32) Hitbox {
      /// The position and radius of the bullet.
      Circle circle;
      /// Half the length of the laser, in addition to the radius.
      /// 0 denotes a regular bullet.
      kfp::s16_16 len;
      /// Where on the laser z.c refers to.
      /// 0: centre; 1: tip; -1: end of trail
      kfp::s2_30 refpoint;
      // TODO: for lasers, the cosine and sine of this value are calculated
      // in multiple places; this is an expensive operation, so it would
      // be nicer to calculate it less often
      /// The visual angle of the bullet. For lasers, this is the direction
      /// of the laser.
      kfp::frac32 visualAngle;
    };
    static_assert(sizeof(Hitbox) <= 32);
    struct alignas(32) Physics {
      PV velocity, acceleration; // 16
      kfp::s16_16 speed; // 20
      kfp::frac32 angle, angularVelocity; // 28
    };
    static_assert(sizeof(Physics) <= 32);
    struct alignas(32) Render {
      kfp::s16_16 visualWidth, visualLength; // 8
      glm::u8vec4 blend = {255, 255, 255, 255}; // 12
      ShotID shotId; // 14
      agl::BMIndex bmIndex = agl::BMIndex::alpha; // 15
      PlayerAttackDivision div = PlayerAttackDivision::shot; // 16
      kfp::frac32 currentAngleOffset = 0; // 20
      uint32_t frame = 0; // 24
      Render(ShotID id, const Graphic& gr);
    };
    static_assert(sizeof(Render) <= 32);
    struct Graze {
      int8_t grazeFrequency = 0;
      int8_t timeToNextGraze = 0;
    };
    struct Delay {
      uint8_t current, max;
    };
    Bullets(Shotsheet&& shotsheet) : shotsheet(std::move(shotsheet)) {}
    /// Remove all bullets from this list.
    void clear() noexcept;
    /// Spawn a bullet with default properties.
    BulletHandle spawn(Hitbox h, bool isLaser, ShotID id) noexcept;
    /// Update the positions of all bullets, performing movement calculations
    /// and removing out-of-bounds bullets.
    void update(const agl::IRect16& bounds) noexcept;
    /// Check for collisions against a circle.
    template<typename F> void collide(const Circle& c, F&& callback) noexcept;
    /// Check for grazes against a circle.
    template<typename F> void graze(const Circle& c, F&& callback) noexcept;
    /// Return true if the `i`th bullet collides with `c`.
    bool collidesWith(BulletIndex i, const Circle& c) const noexcept;
    /// Return true if the `i`th bullet collides with `c`.
    bool collidesWith(BulletIndex i, const zekku::AABB<kfp::s16_16>& box) const
        noexcept;
    glm::tvec2<kfp::s16_16> getTrueCentre(BulletIndex i) const noexcept;
    glm::tvec2<kfp::s16_16> getTrueCentreAssumingLaser(BulletIndex i) const
        noexcept;
    bool outOfBounds(BulletIndex i, const zekku::AABB<kfp::s16_16>& box) const
        noexcept;
    bool isDeleted(BulletIndex i) const noexcept { return !ga.isLive(i); }
    void deleteBullet(BulletIndex i) noexcept { return ga.release(i); }
    void deleteBullet(GAH gh) noexcept { return ga.release(gh); }
    bool collidesWith(GAH gh, const Circle& c) const noexcept {
      assert(ga.isLive(gh));
      return collidesWith(gh.index, c);
    }
    void pushItemList(BulletIndex i, std::vector<tdr::PV>& positions) const
        noexcept;
    void pushItemList(std::vector<tdr::PV>& positions) const noexcept;
    size_t count() const noexcept;
    template<typename F> void forEach(F&& f) {
      for (BulletIndex i = 0; i < physicses.size(); ++i) {
        if (isDeleted(i)) continue;
        f(BulletHandle{this, {ga.getGeneration(i), i}});
      }
    }

    const Shotsheet& getShotsheet() const noexcept { return shotsheet; }
    void setShotsheet(Shotsheet&& s) noexcept { this->shotsheet = s; }
    void setLaserErasureItemDistance(kfp::s16_16 value) {
      laserErasureItemDistance = value;
    }

#define TDR_GENERATE_ACCESSOR(Type, name, which) \
  Type& name(const BulletHandle& h) noexcept { \
    assert(this == h.b); \
    assert(ga.isLive(h.gh)); \
    return which[h.gh.index]; \
  } \
  const Type& name(const BulletHandle& h) const noexcept { \
    assert(this == h.b); \
    assert(ga.isLive(h.gh)); \
    return which[h.gh.index]; \
  } \
  Type& name(const GAH& h) noexcept { \
    assert(ga.isLive(h)); \
    return which[h.index]; \
  } \
  const Type& name(const GAH& h) const noexcept { \
    assert(ga.isLive(h)); \
    return which[h.index]; \
  }
    TDR_GENERATE_ACCESSOR(BulletFlags, getBulletFlags, bulletFlags)
    TDR_GENERATE_ACCESSOR(Hitbox, getHitbox, hitboxes)
    TDR_GENERATE_ACCESSOR(Physics, getPhysics, physicses)
    TDR_GENERATE_ACCESSOR(Render, getRender, renders)
    TDR_GENERATE_ACCESSOR(Graze, getGraze, grazes)
    TDR_GENERATE_ACCESSOR(uint16_t, getDamage, damages)
    TDR_GENERATE_ACCESSOR(Delay, getDelay, delays)
#undef TDR_GENERATE_ACCESSOR
    void
    spurt(std::vector<BulletRenderInfo>* rinfo, const Graphic& delayCloud) const
        noexcept;

  private:
    struct MyGetBB {
      Bullets* b;
      zekku::AABB<kfp::s16_16> operator()(BulletIndex p) const noexcept;
    };
    struct MyGetCollision {
      Bullets* b;
      bool operator()(BulletIndex p, const Circle& q) const {
        return b->collidesWith(p, q);
      }
      bool operator()(BulletIndex p, const zekku::AABB<kfp::s16_16>& q) const {
        return b->collidesWith(p, q);
      }
    };
    uint8_t getDelayOf(BulletIndex i) const noexcept {
      return delays[i].current;
    }
    agl::UIRect16 getRectOf(const Render& r) const noexcept;
    glm::u8vec3 getDelayColourOf(const Render& r) const noexcept;
    const Graphic& getGraphicOf(const Render& r) const noexcept;
    GA ga;
    std::vector<BulletFlags> bulletFlags;
    std::vector<Hitbox, boost::alignment::aligned_allocator<Hitbox, 32>>
        hitboxes;
    std::vector<Physics, boost::alignment::aligned_allocator<Physics, 32>>
        physicses;
    std::vector<Render, boost::alignment::aligned_allocator<Render, 32>>
        renders;
    std::vector<Graze> grazes;
    std::vector<uint16_t> damages;
    std::vector<Delay> delays;
    Shotsheet shotsheet;
    kfp::s16_16 laserErasureItemDistance = defaultLaserErasureItemDistance;
    void refreshParametersWithoutCleaning(BulletIndex i) noexcept;
    void refreshParameters(BulletIndex i) noexcept;
    void refreshParameters() noexcept;
    void refreshGraze() noexcept;
    friend class BulletHandle;
  };
  template<typename F>
  void Bullets::collide(const Circle& c, F&& callback) noexcept {
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      if (isDeleted(i)) continue;
      if (collidesWith(i, c)) {
        callback(BulletHandle{this, GAH{ga.getGeneration(i), i}});
      }
    }
  }
  template<typename F>
  void Bullets::graze(const Circle& c, F&& callback) noexcept {
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      if (isDeleted(i)) continue;
      if (collidesWith(i, c)) {
        Graze& g = grazes[i];
        if (g.timeToNextGraze != 0 || g.grazeFrequency == -1) continue;
        g.timeToNextGraze = g.grazeFrequency == 0 ? -1 : g.grazeFrequency - 1;
        callback(BulletHandle{this, GAH{ga.getGeneration(i), i}});
      }
    }
  }
  // ===== BULLETHANDLE (PART 2) =============================================
  inline BulletHandle::BulletHandle(Bullets* b, GAH gh) : b(b), gh(gh) {}
  inline bool BulletHandle::getFlag(BulletFlags f) const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return (b->bulletFlags[gh.index] & f) != 0;
  }
  inline BulletHandle&
  BulletHandle::setFlag(BulletFlags f, bool value) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    if (value)
      b->bulletFlags[gh.index] |= f;
    else
      b->bulletFlags[gh.index] &= ~f;
    return *this;
  }
  inline bool BulletHandle::isRadial() const noexcept {
    return getFlag(BulletFlags::useRadial);
  }
  inline bool BulletHandle::isMarkedForDeletion() const noexcept {
    return !b->ga.isLive(gh);
  }
  inline bool BulletHandle::isLaserBullet() const noexcept {
    return getFlag(BulletFlags::isLaser);
  }
  inline bool BulletHandle::willDeleteWhenOutOfBounds() const noexcept {
    return getFlag(BulletFlags::deleteWhenOutOfBounds);
  }
  inline bool BulletHandle::willCollide() const noexcept {
    return getFlag(BulletFlags::collides);
  }
  inline bool BulletHandle::willDropItem() const noexcept {
    return getFlag(BulletFlags::shouldDropItem);
  }
  inline bool BulletHandle::isSpellResistant() const noexcept {
    return getFlag(BulletFlags::spellResistant);
  }
  inline bool BulletHandle::erasesShots() const noexcept {
    return getFlag(BulletFlags::erasesShots);
  }
  inline BulletHandle& BulletHandle::setSpellResistant(bool value) noexcept {
    return setFlag(BulletFlags::spellResistant, value);
  }
  inline BulletHandle& BulletHandle::setEraseShots(bool value) noexcept {
    return setFlag(BulletFlags::erasesShots, value);
  }
  inline BulletHandle& BulletHandle::setCollide(bool value) noexcept {
    return setFlag(BulletFlags::collides, value);
  }
  inline BulletHandle& BulletHandle::setDropItem(bool value) noexcept {
    return setFlag(BulletFlags::shouldDropItem, value);
  }
  inline BulletHandle&
  BulletHandle::setDeleteWhenOutOfBounds(bool value) noexcept {
    return setFlag(BulletFlags::deleteWhenOutOfBounds, value);
  }
  inline agl::BMIndex BulletHandle::getBlendModeIndex() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->renders[gh.index].bmIndex;
  }
  inline glm::tvec2<kfp::s16_16> BulletHandle::getTrueCentre() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->getTrueCentre(gh.index);
  }
  inline PV& BulletHandle::getPosition() noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->hitboxes[gh.index].circle.c;
  }
  inline const PV& BulletHandle::getPosition() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->hitboxes[gh.index].circle.c;
  }
  inline void BulletHandle::refreshParameters() noexcept {
    b->refreshParameters(gh.index);
  }
  inline const PV& BulletHandle::getVelocity() noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    if (isRadial()) refreshParameters();
    return b->physicses[gh.index].velocity;
  }
  inline BulletHandle& BulletHandle::setVelocity(const PV& v) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(!isRadial() && "setVelocity requires non-radial bullet");
    makeDirty();
    b->physicses[gh.index].velocity = v;
    return *this;
  }
  inline BulletHandle& BulletHandle::addVelocity(const PV& v) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(!isRadial() && "addVelocity requires non-radial bullet");
    makeDirty();
    b->physicses[gh.index].velocity += v;
    return *this;
  }
  inline const PV& BulletHandle::getAcceleration() noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    if (isRadial()) refreshParameters();
    return b->physicses[gh.index].acceleration;
  }
  inline BulletHandle& BulletHandle::setAcceleration(const PV& a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(!isRadial() && "setAcceleration requires non-radial bullet");
    b->physicses[gh.index].acceleration = a;
    return *this;
  }
  inline BulletHandle& BulletHandle::addAcceleration(const PV& a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(!isRadial() && "addAcceleration requires non-radial bullet");
    b->physicses[gh.index].acceleration += a;
    return *this;
  }
  inline kfp::s16_16 BulletHandle::getSpeed() noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    if (!isRadial()) refreshParameters();
    return b->physicses[gh.index].speed;
  }
  inline BulletHandle& BulletHandle::setSpeed(kfp::s16_16 s) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isRadial() && "setSpeed requires non-radial bullet");
    makeDirty();
    b->physicses[gh.index].speed = s;
    return *this;
  }
  inline BulletHandle& BulletHandle::addSpeed(kfp::s16_16 s) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isRadial() && "addSpeed requires non-radial bullet");
    makeDirty();
    b->physicses[gh.index].speed += s;
    return *this;
  }
  inline kfp::frac32 BulletHandle::getAngle() noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    if (!isRadial()) refreshParameters();
    return b->physicses[gh.index].angle;
  }
  inline BulletHandle& BulletHandle::setAngle(kfp::frac32 a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isRadial() && "setAngle requires non-radial bullet");
    makeDirty();
    b->physicses[gh.index].angle = a;
    return *this;
  }
  inline BulletHandle& BulletHandle::addAngle(kfp::frac32 a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isRadial() && "addAngle requires non-radial bullet");
    makeDirty();
    b->physicses[gh.index].angle += a;
    return *this;
  }
  inline kfp::frac32 BulletHandle::getAngularVelocity() noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    if (!isRadial()) refreshParameters();
    return b->physicses[gh.index].angularVelocity;
  }
  inline BulletHandle&
  BulletHandle::setAngularVelocity(kfp::frac32 a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isRadial() && "setAngularVelocity requires non-radial bullet");
    b->physicses[gh.index].angularVelocity = a;
    return *this;
  }
  inline BulletHandle&
  BulletHandle::addAngularVelocity(kfp::frac32 a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isRadial() && "addAngularVelocity requires non-radial bullet");
    b->physicses[gh.index].angularVelocity += a;
    return *this;
  }
  inline kfp::frac32 BulletHandle::getVisualAngle() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->hitboxes[gh.index].visualAngle;
  }
  inline BulletHandle& BulletHandle::setVisualAngle(kfp::frac32 a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->hitboxes[gh.index].visualAngle = a;
    return *this;
  }
  inline BulletHandle& BulletHandle::addVisualAngle(kfp::frac32 a) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->hitboxes[gh.index].visualAngle += a;
    return *this;
  }
  inline kfp::s16_16 BulletHandle::getVisualWidth() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->renders[gh.index].visualWidth;
  }
  inline BulletHandle& BulletHandle::setVisualWidth(kfp::s16_16 w) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->renders[gh.index].visualWidth = w;
    return *this;
  }
  inline kfp::s16_16 BulletHandle::getVisualLength() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->renders[gh.index].visualLength;
  }
  inline BulletHandle& BulletHandle::setVisualLength(kfp::s16_16 l) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->renders[gh.index].visualLength = l;
    return *this;
  }
  inline uint8_t BulletHandle::getDelay() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->delays[gh.index].current;
  }
  inline BulletHandle& BulletHandle::setDelay(uint8_t d) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->delays[gh.index].current = d;
    b->delays[gh.index].max = d;
    return *this;
  }
  inline int8_t BulletHandle::getGrazeFrequency() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->grazes[gh.index].grazeFrequency;
  }
  inline BulletHandle& BulletHandle::setGrazeFrequency(int8_t f) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->grazes[gh.index].grazeFrequency = f;
    return *this;
  }
  inline BulletHandle& BulletHandle::markForDeletion() noexcept {
    if (isMarkedForDeletion()) return *this;
    b->deleteBullet(gh);
    return *this;
  }
  inline void BulletHandle::makeDirty() noexcept {
    b->bulletFlags[gh.index] |= BulletFlags::dirty;
  }
  inline kfp::s16_16 BulletHandle::getCollisionRadius() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->hitboxes[gh.index].circle.r;
  }
  inline BulletHandle&
  BulletHandle::setCollisionRadius(kfp::s16_16 r) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->hitboxes[gh.index].circle.r = r;
    return *this;
  }
  inline kfp::s16_16 BulletHandle::getLaserLength() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isLaserBullet());
    return b->hitboxes[gh.index].len;
  }
  inline BulletHandle& BulletHandle::setLaserLength(kfp::s16_16 l) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isLaserBullet());
    b->hitboxes[gh.index].len = l;
    return *this;
  }
  inline kfp::s2_30 BulletHandle::getRefpoint() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isLaserBullet());
    return b->hitboxes[gh.index].refpoint;
  }
  inline BulletHandle& BulletHandle::setRefpoint(kfp::s2_30 r) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    assert(isLaserBullet());
    b->hitboxes[gh.index].refpoint = r;
    return *this;
  }
  inline int16_t BulletHandle::getDamage() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->damages[gh.index];
  }
  inline BulletHandle& BulletHandle::setDamage(int16_t d) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->damages[gh.index] = d;
    return *this;
  }
  inline Circle BulletHandle::getHitbox() const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->hitboxes[gh.index].circle;
  }
  inline bool BulletHandle::intersectsWith(const Circle& c) const noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->collidesWith(gh, c);
  }
  inline BulletHandle& BulletHandle::setAlpha(uint8_t alpha) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->renders[gh.index].blend.a = alpha;
    return *this;
  }
  inline BulletHandle& BulletHandle::setBlendColour(glm::u8vec4 col) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->renders[gh.index].blend = col;
    return *this;
  }
  inline BulletHandle& BulletHandle::setBlendMode(agl::BMIndex bm) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->renders[gh.index].bmIndex = bm;
    return *this;
  }
  inline BulletHandle& BulletHandle::setGraphic(ShotID id) noexcept {
    const Graphic& gr = b->shotsheet.getRectByID(id);
    b->renders[gh.index].shotId = id;
    return setVisualLength(gr.visualRadius)
        .setVisualWidth(gr.visualRadius)
        .setCollisionRadius(gr.collisionRadius)
        .setBlendMode(gr.renderMode);
  }
  inline PlayerAttackDivision BulletHandle::getPlayerAttackDivision() const
      noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    return b->renders[gh.index].div;
  }
  inline BulletHandle&
  BulletHandle::setPlayerAttackDivision(PlayerAttackDivision div) noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->renders[gh.index].div = div;
    return *this;
  }
  inline void BulletHandle::pushPosition(std::vector<tdr::PV>& positions) const
      noexcept {
    assert(
        !isMarkedForDeletion() &&
        "Cannot call this method on a deleted bullet");
    b->pushItemList(gh.index, positions);
  }

}
