#pragma once

#include <assert.h>
#include <stdint.h>

#include <algorithm>
#include <vector>

#include <AGL/BlendMode.h>
#include <FileUtil/attributes.h>

#include "TDR/bullet/defs.h"

namespace tdr {
  /**
   * \brief A class describing the texture coordinate rectangles used to
   * render a shot.
   *
   * This class is optimised for the common case when the shot is not animated.
   */
  class AnimatedRect {
  public:
    constexpr AnimatedRect() : end(nullptr), one{} {}
    /// Construct an `AnimatedRect` from a single rectangle.
    /* implicit */ constexpr AnimatedRect(agl::UIRect16 texcoords) :
        end(nullptr), one(texcoords) {}
    /// Construct an `AnimatedRect` from a single rectangle.
    /* implicit */ constexpr AnimatedRect(
        uint16_t left, uint16_t top, uint16_t right, uint16_t bottom) :
        end(nullptr),
        one{left, top, right, bottom} {}
    /**
     * \brief Construct an `AnimatedRect` describing an animated shot.
     *
     * \param count The number of animation frames. Expected to be greater
     * than 1.
     * \param texcoords A pointer to a block of `count` rectangles.
     * \param frames A pointer to a block of `count` monotonically increasing
     *   values, such that `texcoords[i]` is used for frames in the interval
     *   `[count[i - 1], count[i])` (where we take `count[-1]` to be 0 for
     *   exposition purposes).
     */
    CTH_NONNULL(3, 4)
    constexpr AnimatedRect(
        uint32_t count, const agl::UIRect16* texcoords,
        const uint32_t* frames) :
        end(frames + count),
        many{texcoords, frames} {
      assert(count > 1);
    }
    agl::UIRect16 at(uint32_t frame) const noexcept {
      if (end == nullptr) return one;
      // If animated, then return the last frame with its value no less than
      // `frame`
      auto it = std::upper_bound(many.frames, end, frame);
      assert(it < end);
      return many.texcoords[it - many.frames];
    }
    uint32_t period() const noexcept {
      if (end == nullptr) return 1;
      return end[-1];
    }

  private:
    // Nasty layout hackery to make sure we can initialise this class at compile
    // time.
    const uint32_t* end;
    // This type has to be declared out-of-line because of silly C++ rules
    struct Many {
      const agl::UIRect16* texcoords; // array of `count` rects
      // array of `count` values increasing monotonically
      const uint32_t* frames;
    };
    union {
      agl::UIRect16 one;
      Many many;
    };
  };
  /// Represents a shot graphic from a shotsheet.
  struct Graphic {
    AnimatedRect texcoords;
    kfp::s16_16 visualRadius;
    kfp::s16_16 collisionRadius;
    glm::u8vec3 delayColour = {255, 255, 255};
    uint8_t alpha = 255;
    agl::BMIndex renderMode = agl::BMIndex::alpha;
    kfp::frac32 intrinsicAngularVelocity = 0;
  };
  using ShotID = uint16_t;
  /**
   * \brief A shotsheet for bullets.
   *
   * This class holds mappings from integers to graphics.
   *
   * \tparam T the graphic class to use
   */
  template<typename T> class TShotsheet {
  public:
    TShotsheet() = default;
    /**
     * \brief Get a \ref Graphic object by its ID.
     */
    const T& getRectByID(ShotID id) const {
      assert(id < graphics.size());
      return graphics[id];
    }
    /**
     * \brief Set rect data for this shotsheet.
     * \param count The number of shot IDs to set.
     * \param graphics A pointer to `count` graphics.
     */
    void setRects(size_t count, const T* graphics) {
      this->graphics.resize(count);
      std::copy_n(graphics, count, this->graphics.begin());
    }

  private:
    std::vector<T> graphics;
  };
  using Shotsheet = TShotsheet<Graphic>;
}
