#pragma once

#include <stdint.h>

#include <AGL/GLFWApplication.h>

namespace tdr {
  struct MenuOptions {
    int initialDelay, repeatDelay;
  };
  /**
   * \brief Helper for menus.
   * \param option The previous option selected.
   * \param count The total number of options.
   * \param up An \ref agl::KeyStatus value returned when testing for
   *   the up key.
   * \param down An \ref agl::KeyStatus value returned when testing for
   *   the down key.
   * \param ticks A reference to the number of ticks left. Will be modified.
   * \param opts A reference to your menu options.
   */
  int menuHelper(
      int option, int count, agl::KeyStatus up, agl::KeyStatus down,
      int16_t& ticks, const MenuOptions& opts);
}
