#pragma once

#include "TDR/Game.h"
#include "TDR/yield.h"

namespace tdr {
  auto alwaysTrue = []() { return true; };
  /**
   * \brief Move a movable object toward a point at a given speed.
   *
   * This function must be called inside a coroutine.
   *
   * \tparam T The type of the thing to move. Must have `getPosition()`,
   * `setPosition(PV p)` and `setVelocity(PV v)` methods at the very least.
   * \tparam P A predicate. If this evaluates to false, then movement will
   * be aborted.
   */
  template<typename T, typename P>
  void moveTowardAtSpeed(
      T movable, tdr::PV destination, kfp::s16_16 speed,
      P&& condition = alwaysTrue) {
    tdr::PV source = movable.getPosition();
    tdr::PV delta = destination - source;
    kfp::s16_16 scale = kfp::hypot<int32_t, 16>(delta.x, delta.y) / speed;
    if (scale >= 1) {
      tdr::PV dir = delta / scale;
      movable.setVelocity(dir);
      tdr::cwait((int) scale);
      if (movable.isMarkedForDeletion() || !condition()) return;
    }
    movable.setVelocity(tdr::PV{0, 0});
    movable.setPosition(destination);
  }
}
