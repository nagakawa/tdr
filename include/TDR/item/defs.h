#pragma once

#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"
#include "TDR/enemy/defs.h"

namespace tdr {
  using ItemRenderInfo = EnemyRenderInfo;
  enum class ItemMovePattern {
    moveDown,     ///< No special behaviour
    moveToPlayer, ///< When item starts to move down, fly toward player
  };
  struct ItemTraits {
    agl::UIRect16 texcoords;
    agl::BMIndex bmIndex;
    // Should be negative to move up
    kfp::s16_16 initialYVelocity;
    kfp::s16_16 maxSpeed;
    ItemMovePattern movePattern;
    // Does the item fly toward the player when they move above the PoC?
    bool attractedOnPoc;
  };
  using ItemShotsheet = TShotsheet<ItemTraits>;
  static_assert(
      std::is_trivial_v<ItemTraits>, "ItemTraits should be trivial");
  struct ItemRec {
    PV position;
    ShotID itemId;
  };
}
