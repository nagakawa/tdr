#pragma once

#include <AGL/ShaderProgram.h>
#include <AGL/Texture.h>
#include <AGL/VAO.h>
#include <AGL/VBO.h>

#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"
#include "TDR/item/ItemHandle.h"
#include "TDR/item/Items.h"

namespace tdr {
  class Playfield;
  class PlayfieldView;
  class ItemList {
  public:
    /**
     * \brief Construct an item list.
     *
     * \param p A pointer to the playfield to be used. Should not be null.
     * \param shotsheet The shotsheet to use for the items.
     */
    ItemList(ItemShotsheet&& shotsheet) : shotsheet(std::move(shotsheet)) {}
    /**
     * \brief Update the positions of the items over one frame.
     *
     * \param playerPosition The current position of the player.
     */
    void updatePositions(PV playerPosition, bool alive, const Playfield& p);
    /**
     * \brief Check for collisions against a circle.
     *
     * \param h The circle to check against.
     * \param callback A callable to call on the bullet for each collision.
     */
    template<typename F> void forAllCollected(F&& callback) {
      items.forAllCollected(std::forward<F>(callback));
    }
    /// Get the number of enemies.
    size_t nItems() const noexcept;
    void clear() noexcept;
    // EnemyHandle createEnemyB1(
    //    PV position, PV velocity, const EnemyGraphic& graph, int32_t health);
    template<typename F> void forEach(F&& f) {
      items.forEach(std::forward<F>(f));
    }
    void nullifyAttraction() noexcept { items.nullifyAttraction(); }
    kfp::s16_16 getGravity() const noexcept { return items.getGravity(); }
    void setGravity(kfp::s16_16 g) noexcept { items.setGravity(g); }
    kfp::s16_16 getCollectRadius() const noexcept {
      return items.getCollectRadius();
    }
    void setCollectRadius(kfp::s16_16 g) noexcept { items.setCollectRadius(g); }
    kfp::s16_16 getPoc() const noexcept { return items.getPoc(); }
    void setPoc(kfp::s16_16 g) noexcept { items.setPoc(g); }
    ItemHandle createItemA1(PV position, ShotID type);
    void createItemsA1(size_t count, const PV* position, ShotID type);

    const ItemShotsheet& getShotsheet() const noexcept { return shotsheet; }
    void setShotsheet(ItemShotsheet&& s) noexcept { this->shotsheet = s; }

  private:
    Items items;
    ItemShotsheet shotsheet;
    friend class ItemListView;
  };
  class ItemListView {
  public:
    ItemListView(const ItemList* il, PlayfieldView* p) : il(il), p(p) {}
    /**
     * \brief Set up rendering for the item list.
     *
     * This should be called before any rendering.
     */
    void setUp();
    /// Render the items in the item list.
    void render();
    void setTexture(agl::WeakTexture t) { this->t = std::move(t); }
    const ItemList& getModel() const noexcept { return *il; }
    void setModel(const ItemList* il) noexcept { this->il = il; }

  private:
    const ItemList* il;
    std::array<std::vector<ItemRenderInfo>, agl::nPresetBlends> rinfo;
    std::vector<size_t> offsets;
    PlayfieldView* p;
    agl::VBO vbo;
    agl::VBO instanceVBO;
    agl::VAO vao;
    agl::ShaderProgram program;
    agl::WeakTexture t;
    bool hasSetUniforms = false;
    bool hasInitialisedProgram = false;
    void setUniforms();
    void spurt();
    void update();
  };
}
