#pragma once

#include <assert.h>
#include <stdint.h>

#include <memory>
#include <vector>

#include <AGL/BlendMode.h>
#include <glm/glm.hpp>
#include <zekku/kfp_interop/timath.h>

#include "TDR/bullet/defs.h"
#include "TDR/item/ItemHandle.h"
#include "TDR/item/defs.h"

namespace tdr {
  using namespace kfp::literals;
  class Items {
  public:
    constexpr static kfp::s16_16 defaultGravity = "0.1"_s16_16;
    constexpr static kfp::s16_16 defaultCollectRadius = "30"_s16_16;
    struct Physics {
      PV position;
      PV velocity;
      kfp::s16_16 maxSpeed;
      kfp::s16_16 collisionRadius;
      ItemMovePattern movePattern;
      ShotID itemId;
      bool attractedOnPoc;
      bool flyingTowardPlayer;
      bool collected;
    };
    struct Render {
      agl::UIRect16 texcoords;
      PV dimensions;
      agl::BMIndex bmIndex;
    };
    ItemHandle
    spawn(PV position, ShotID itemId, const ItemTraits& traits) noexcept;
    /// Update the positions of all items...
    void update(PV playerPosition, bool alive, kfp::s16_16 lowerY) noexcept;
    template<typename F> void forAllCollected(F&& callback) noexcept;
    bool isDeleted(BulletIndex i) const noexcept { return !ga.isLive(i); }
    void deleteItem(BulletIndex i) noexcept { return ga.release(i); }
    void deleteItem(GAH gh) noexcept { return ga.release(gh); }
    void spurt(std::vector<ItemRenderInfo>* rinfo) const noexcept;
    size_t count() const noexcept { return ga.countExtant(); }
    /// Remove all items from this list.
    void clear() noexcept;
    template<typename F> void forEach(F&& f) {
      for (BulletIndex i = 0; i < physicses.size(); ++i) {
        if (isDeleted(i)) continue;
        f(ItemHandle{this, {ga.getGeneration(i), i}});
      }
    }
    void nullifyAttraction() noexcept;
    kfp::s16_16 getGravity() const noexcept { return gravity; }
    void setGravity(kfp::s16_16 g) noexcept { gravity = g; }
    kfp::s16_16 getCollectRadius() const noexcept { return collectRadius; }
    void setCollectRadius(kfp::s16_16 g) noexcept { collectRadius = g; }
    kfp::s16_16 getPoc() const noexcept { return poc; }
    void setPoc(kfp::s16_16 g) noexcept { poc = g; }

  private:
    bool shouldFlyToPlayer(
        PV playerPosition, bool alive, const Physics& p,
        kfp::Fixed<int64_t, 32> l2) const noexcept;
    GA ga;
    std::vector<Physics> physicses;
    std::vector<Render> renders;
    kfp::s16_16 gravity = defaultGravity; // pixels per frame
    kfp::s16_16 collectRadius = defaultCollectRadius;
    kfp::s16_16 poc = -1;
    friend class ItemHandle;
  };
  template<typename F> void Items::forAllCollected(F&& callback) noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      if (isDeleted(i)) continue;
      if (physicses[i].collected) {
        callback(ItemHandle{this, GAH{ga.getGeneration(i), i}});
      }
    }
  }
  inline ItemHandle::ItemHandle(Items* e, GAH gh) : e(e), gh(gh) {}
  inline bool ItemHandle::isMarkedForDeletion() const noexcept {
    return !e->ga.isLive(gh);
  }
  inline PV& ItemHandle::getPosition() noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].position;
  }
  inline const PV& ItemHandle::getPosition() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].position;
  }
  inline PV& ItemHandle::getVelocity() noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].velocity;
  }
  inline const PV& ItemHandle::getVelocity() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].velocity;
  }
  inline ShotID ItemHandle::getId() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].itemId;
  }
  inline ItemHandle& ItemHandle::markForDeletion() noexcept {
    if (isMarkedForDeletion()) return *this;
    e->deleteItem(gh);
    return *this;
  }
  /*
  inline kfp::s16_16 ItemHandle::getCollisionRadiusToPlayer() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].collisionRadiusToPlayer;
  }
  inline ItemHandle&
  ItemHandle::setCollisionRadiusToPlayer(kfp::s16_16 r) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    e->physicses[gh.index].collisionRadiusToPlayer = r;
    return *this;
  }
  inline kfp::s16_16 ItemHandle::getCollisionRadiusToShot() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].collisionRadiusToShot;
  }
  inline ItemHandle&
  ItemHandle::setCollisionRadiusToShot(kfp::s16_16 r) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    e->physicses[gh.index].collisionRadiusToShot = r;
    return *this;
  }
  inline agl::UIRect16 ItemHandle::getTexcoords() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->renders[gh.index].texcoords;
  }
  inline ItemHandle& ItemHandle::setTexcoords(agl::UIRect16 r) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    e->renders[gh.index].texcoords = r;
    e->renders[gh.index].dimensions = { r.right - r.left, r.bottom - r.top };
    return *this;
  }
  inline Circle ItemHandle::getHitboxToShot() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    const Items::Physics& p = e->physicses[gh.index];
    return {p.position, p.collisionRadiusToShot};
  }
  /// Get the health of the enemy.
  inline int32_t ItemHandle::getHealth() const noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    return e->physicses[gh.index].health;
  }
  /// Set the health of the enemy.
  inline ItemHandle& ItemHandle::setHealth(int32_t h) noexcept {
    assert(
        !isMarkedForDeletion() && "Cannot call this method on a deleted item");
    e->physicses[gh.index].health = h;
    return *this;
  }
  */
}
