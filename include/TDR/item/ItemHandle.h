#pragma once

#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"

namespace tdr {
  class Items;
  class ItemHandle {
  public:
    ItemHandle(const ItemHandle& h) = default;
    ItemHandle(ItemHandle&& h) = default;
    ItemHandle& operator=(const ItemHandle& h) = default;
    ItemHandle& operator=(ItemHandle&& h) = default;
    ~ItemHandle() = default;
    /// Return true if this enemy is about to be deleted.
    bool isMarkedForDeletion() const noexcept;
    /// Get the position of the enemy.
    PV& getPosition() noexcept;
    /// Get the position of the enemy.
    const PV& getPosition() const noexcept;
    /// Set the position of the enemy.
    ItemHandle& setPosition(const PV& x) noexcept {
      getPosition() = x;
      return *this;
    }
    /// Add an offset to the position of the enemy.
    ItemHandle& addPosition(const PV& x) noexcept {
      getPosition() += x;
      return *this;
    }
    /// Get the velocity of the enemy.
    PV& getVelocity() noexcept;
    /// Get the velocity of the enemy.
    const PV& getVelocity() const noexcept;
    /// Set the velocity of the enemy.
    ItemHandle& setVelocity(const PV& v) noexcept {
      getVelocity() = v;
      return *this;
    }
    /// Add an offset to the velocity of the enemy.
    ItemHandle& addVelocity(const PV& v) noexcept {
      getVelocity() += v;
      return *this;
    }
    ShotID getId() const noexcept;
    /// Mark an item for deletion.
    ItemHandle& markForDeletion() noexcept;

  private:
    ItemHandle(Items* e, GAH gh);
    Items* e;
    GAH gh;
    friend class Items;
  };
}
