#pragma once

#include <vector>

#include <AGL/BlendMode.h>
#include <AGL/ShaderProgram.h>
#include <AGL/Texture.h>
#include <AGL/VAO.h>
#include <AGL/VBO.h>

#include "TDR/Playfield.h"
#include "TDR/particle/defs.h"

namespace tdr {
  inline constexpr glm::i16vec4 scaleBlend(glm::u8vec4 blend) {
    return (int16_t) 128 * (glm::i16vec4) blend;
  }
  inline constexpr glm::u8vec4 unscaleBlend(glm::i16vec4 blend) {
    return glm::clamp(blend, (int16_t) 0, (int16_t)(255 * 128)) / (int16_t) 128;
  }
  struct ParticleInfo {
    ParticleInfo(
        ParticleGraphic graph, PV position, PV velocity,
        uint32_t lifespan) noexcept :
        texcoords(graph.texcoords),
        blend(scaleBlend(graph.blend)), blendRate{0, 0, 0, 0},
        position(position), velocity(velocity), acceleration{0, 0},
        tangentialAcceleration{0}, rotation{0}, rotationRate{0},
        lifespan(lifespan), bmIndex(agl::BMIndex::alpha) {}
    void setBlend(glm::u8vec4 blend) { this->blend = scaleBlend(blend); }
    agl::UIRect16 texcoords;
    // The two fields below are scaled by 128.
    glm::i16vec4 blend;
    glm::i16vec4 blendRate;
    PV position, velocity, acceleration;
    kfp::s2_30 tangentialAcceleration; // positive values go ACW
    kfp::frac32 rotation, rotationRate;
    uint32_t lifespan;
    agl::BMIndex bmIndex;
  };
  static_assert(
      std::is_standard_layout_v<ParticleInfo>,
      "ParticleInfo should be trivial");
  /**
   * A dead simple particle system. Suited mostly for eye candy rather than
   * for usual particle system applications.
   */
  class ParticleManager {
  public:
    ParticleManager(
        PlayfieldView* p, ParticleShotsheet&& ss, agl::WeakTexture t) :
        p(p),
        shotsheet(std::move(ss)), t(std::move(t)) {
      setUp();
    }
    void update() noexcept;
    ParticleInfo&
    spawn(ParticleGraphic graph, PV position, PV velocity, uint32_t lifespan) {
      return particles.emplace_back(graph, position, velocity, lifespan);
    }
    ParticleInfo&
    spawn(ShotID graph, PV position, PV velocity, uint32_t lifespan) {
      return spawn(shotsheet.getRectByID(graph), position, velocity, lifespan);
    }
    void render();
    void reset() noexcept { particles.clear(); }

  private:
    PlayfieldView* p;
    ParticleShotsheet shotsheet;
    std::array<std::vector<ParticleRenderInfo>, agl::nPresetBlends> rinfo;
    std::vector<size_t> offsets;
    agl::VBO vbo;
    agl::VBO instanceVBO;
    agl::VAO vao;
    agl::ShaderProgram program;
    agl::WeakTexture t;
    std::vector<ParticleInfo> particles;
    bool hasSetUniforms = false;
    void setUp();
    void spurt();
    void renderUpdate();
    void setUniforms();
  };
}
