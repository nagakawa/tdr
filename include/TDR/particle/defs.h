#pragma once

#include "TDR/bullet/Shotsheet.h"
#include "TDR/bullet/defs.h"

namespace tdr {
  struct ParticleRenderInfo {
    ParticleRenderInfo() = default;
    PV position, dimensions;
    agl::UIRect16 texcoords;
    kfp::frac32 rotation;
    glm::u8vec4 blend;
  };
  /// Represents an particle graphic from a shotsheet of particles.
  struct ParticleGraphic {
    agl::UIRect16 texcoords;
    glm::u8vec4 blend;
    agl::BMIndex bmIndex;
  };
  using ParticleShotsheet = TShotsheet<ParticleGraphic>;
  static_assert(
      std::is_trivial_v<ParticleRenderInfo>,
      "ParticleRenderInfo should be trivial");
}
