#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include <AGL/profiling.h>

namespace tdr {
  namespace track_names {
    enum Tracks {
      frameTime,
      tickTime,
      totalUpdateTime,
      bulletUpdateTime,
      collisionCheckTime,
      renderTime,
      bulletCount,
      userUpdateTime,
      bulletListSpurtTime,
    };
  }
  extern agl::ProfilerOptions defaultProfilerOptions;
}
