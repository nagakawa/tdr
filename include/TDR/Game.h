#pragma once

#include <stdint.h>
#include <time.h>

#include <functional>
#include <iostream>
#include <memory>
#include <random>

#include <fmt/core.h>

#include <AGL/Scene.h>
#include <AGL/ScenedGLFWApplication.h>
#include <AGL/Sprite2D.h>
#include <AGL/Texture.h>
#include <kozet_coroutine/kcr.h>
#include <kozet_fixed_point/kfp.h>
#include <kozet_fixed_point/random/kfp_wrapper.h>

#include "TDR/Bullet.h"
#include "TDR/Player.h"
#include "TDR/Playfield.h"
#include "TDR/Replay.h"
#include "TDR/Stage.h"
#include "TDR/VKMap.h"
#include "TDR/enemy/BossScene.h"
#include "TDR/enemy/EnemyList.h"
#include "TDR/gamemacros.h"
#include "TDR/item/ItemList.h"

namespace tdr {
  template<TPARAMS> struct GameModelOptions {
    TYPEDECLS;
    /// A common data object to pass in, or `std::nullopt` to pass in an
    /// empty one.
    std::optional<CommonData> cd;
    /// A pointer to the view.
    View* v;
    /// A playfield object to use for the model.
    Playfield p;
    /// A pointer to the player traits that the player should adopt.
    const PlayerTraits* traits;
    /// A unique pointer to an instance of `Stage` for the stage you want
    /// to play.
    std::unique_ptr<Stage> stage;
    /// The seed that should be used to initialise the random number generator.
    uint64_t seed;
    /// The stage number (e.g. from the replay file).
    unsigned stageIndex;
  };
  template<TPARAMS> GameModelOptions<TPM> getDefaultOptions() {
    return GameModelOptions<TPM>{
        std::nullopt, nullptr, Playfield{}, &defaultPlayerTraits,
        nullptr,      0,       -1u,
    };
  }
  /**
   * \brief A model for a danmaku game.
   *
   * You should subclass this class and \ref GameView to form a pair of a
   * model and view.
   *
   * This is where gameplay-related code goes. Anything cosmetic belongs
   * to the corresponding subclass of \ref GameView.
   *
   * A \ref GameModel can send messages to a \ref GameView to prompt it
   * to do such things as display something or play a sound. We recommend
   * using a `std::variant` of different message types for this purpose.
   *
   * Subclasses of \ref GameView have only const access to the underlying
   * model in order to provide some enforcement of determinism.
   *
   * ### Notes about reproducibility
   *
   * In order for replays to synchronise across different builds,
   * the game logic must be reproducible. In other words, game logic
   * must **not**:
   *
   * - Use any sort of global state (such as `rand()`)
   * - Rely on timing (e.g.\ the delta between two frames)
   * - Use a nondeterministic RNG `std::random_device`
   * - Use random number distributions -- these are not guaranteed to
   *   be reproducible
   * - Use floating-point arithmetic (to be extra safe)
   *
   * Of course, anything that's strictly rendering-related doesn't have
   * such requirements.
   *
   * \tparam GM Your subclass of \ref GameModel
   * \tparam GV Your subclass of \ref GameView
   * \tparam SD The shared data for the \ref GameView
   * \tparam Msg The message type for communicating from `GM` to `GV`
   */
  template<TPARAMS> class GameModel {
  public:
    TYPEDECLS;
    static_assert(
        std::is_move_constructible_v<SD> && std::is_move_assignable_v<SD> &&
        std::is_move_constructible_v<Message> &&
        std::is_move_assignable_v<Message>);
    GameModel() = default;
    /**
     * \brief Construct a game model.
     *
     * \param opts Options for the model.
     */
    GameModel(GameModelOptions<TPM>&& opts);
    GameModel(GameModel<TPM>&& a) = default;
    GameModel<TPM>& operator=(GameModel<TPM>&& a) = default;
    virtual ~GameModel() {}
    /// Simulate one tick of the game.
    void tick();
    /// Get the bullet list.
    BulletList& getBulletList() { return bullets; }
    /// Get the player bullet list.
    BulletList& getPlayerBulletList() { return playerBullets; }
    /// Get the enemy list.
    EnemyList& getEnemyList() { return enemies; }
    /// Get the item list.
    ItemList& getItemList() { return items; }
    /// Get the bullet list.
    const BulletList& getBulletList() const { return bullets; }
    /// Get the player bullet list.
    const BulletList& getPlayerBulletList() const { return playerBullets; }
    /// Get the enemy list.
    const EnemyList& getEnemyList() const { return enemies; }
    /// Get the item list.
    const ItemList& getItemList() const { return items; }
    /// Change the shotsheet.
    void setShotsheet(Shotsheet&& shotsheet) {
      bullets.setShotsheet(std::move(shotsheet));
    }
    /// Change the shotsheet for player bullets.
    void setPlayerShotsheet(Shotsheet&& shotsheet) {
      playerBullets.setShotsheet(std::move(shotsheet));
    }
    /// Change the shotsheet for enemies.
    void setEnemyShotsheet(EnemyShotsheet&& ss) {
      enemies.setShotsheet(std::move(ss));
    }
    /// Change the shotsheet for items.
    void setItemShotsheet(ItemShotsheet&& ss) {
      items.setShotsheet(std::move(ss));
    }
    /// Get the current player.
    const Player& getPlayer() const { return gp; }
    /// Get the current player.
    Player& getPlayer() { return gp; }
    /// Return true if the player ran out of lives.
    bool isGameOver() const { return gp.isGameOver(); }
    /// Get the playfied.
    const Playfield& getPlayfield() const { return p; }
    /// Get the playfied.
    Playfield& getPlayfield() { return p; }
    /**
     * \brief Continue the game after it is over.
     *
     * At this time, only continue-from-where-you-left-off (as in most Touhou
     * games) is supported. Continue-from-start-of-stage (as in MoF, SA and
     * UFO) is NYI.
     */
    void useContinue();
    /// Get the number of continues used.
    uint16_t getNumContinues() const { return nContinues; }
    /// Get the index of the current stage.
    unsigned currentStage() const { return stageIndex; }
    /// Get the dimensions of the playfield.
    glm::tvec2<int> getDimensions() const {
      return {p.getWidth(), p.getHeight()};
    }
    /// Return true if the given virtual key is pressed.
    bool testVirtualKey(int code) const noexcept;
    /// Return a @ref KeyStatus for the given virtual key.
    agl::KeyStatus testVirtualKey2(int code) const noexcept;
    /// Return true if the player is shooting.
    bool isShooting() const noexcept {
      return gp.canMove() && testVirtualKey(vk::shot);
    }
    /**
     * \brief Return the virtual key flags.
     *
     * \return An integer where bit #i is set if and only if virtual key
     * #i is pressed.
     */
    uint64_t getVKFlags() const { return vkFlags; }
    /**
     * \brief Update the virtual key flags.
     *
     * The current flags are stored as the flags of last frame, and then the
     * current VK flags are updated.
     */
    void updateVKFlags(uint64_t fl) {
      vkFlagsPrev = vkFlags;
      vkFlags = fl;
    }
    /**
     * \brief Create a boss scene.
     *
     * \param b A handle to the enemy to turn into a boss
     * \return A reference to the \ref BossScene object
     */
    BossScene& registerBossScene(EnemyHandle b);
    /// Delete the current boss scene.
    void unregisterBossScene() { bossScene = std::nullopt; }
    /// Get the current boss scene, if any.
    const std::optional<BossScene>& getBossScene() const { return bossScene; }
    /**
     * Delete all bullets on the screen and call \ref onSpawnErasureItem on
     * all of them.
     */
    void breakScreen();
    /// Get the current random number generator.
    kfp::Wrapper<std::mt19937_64>& getRng() { return rng; }
    /// Return true if this model is done.
    bool isDone() const noexcept { return done; }
    void finishStage() noexcept { done = true; }

  protected:
    /// Send a message to the view.
    virtual void notify(const Message& m);
    /// Perform any user actions to be done every frame.
    virtual void myTick() = 0;
    /// Move the player. If you override this method, make sure to call
    /// `movePlayer()` on the parent class as well.
    virtual void movePlayer();
    /// Perform any user actions to be done when the player is hit.
    virtual void onHit() {}
    /// Perform any user actions to be done when the player loses a life.
    virtual void onDown() {}
    /// Perform any user actions to be done when the player shoots.
    virtual void onShot() {}
    /// Perform any user actions to be done when the player uses a spell.
    virtual void onBomb() {}
    /**
     * \brief Perform any user actions to be done when collecting an item.
     *
     * This is usually the way to handle item effects.
     */
    virtual void onCollectItem(size_t /*nItems*/, ItemRec* /*items*/) {}
    /**
     * \brief Perform any user actions to be done when a bullet is erased
     * and an item should be spawned.
     *
     * The callback works on a block of coördinates to minimise the number
     * of virtual function calls needed when erasing a large number of
     * bullets at once.
     *
     * @param nBullets the number of bullets erased
     * @param positions a pointer to a block of `nBullets` positions
     * where the bullets were erased
     */
    virtual void onSpawnErasureItem(size_t nBullets, const PV* positions) {
      (void) nBullets;
      (void) positions;
    }
    /**
     * \brief Perform any user actions to be done when the game is continued
     * (after running out of lives).
     *
     * Note that \ref onRespawn() is also called after this method.
     */
    virtual void onContinue() {
      gp.cd.score = std::min<int>(nContinues, 9); // Do it like Touhou
    }
    /// Perform any user actions to be done when the player respawns.
    virtual void onRespawn() {}
    /// Perform any user actions to be done when the player grazes a bullet.
    virtual void onGraze(BulletHandle& /*b*/) {}
    /// Perform any user actions to be done on the frames when the player
    /// grazes at least one bullet.
    virtual void onGrazeOnce() {}
    /*
     * THINGS TO DO WITH BOSSES:
     */
    /// Perform any user actions to be done when a boss scene starts.
    virtual void onBossSceneStart(BossScene& /*scene*/) {}
    /// Perform any user actions to be done when a new attack starts in a boss
    /// scene.
    virtual void
    onBossSceneAttackStart(BossScene& /*scene*/, const BossAttack& /*attack*/) {
    }
    /// Perform any user actions to be done when a boss scene transitions to
    /// the next attack.
    virtual void onBossSceneBreak(
        BossScene& /*scene*/, const BossAttack& /*attack*/,
        BossScene::PlayStat /*st*/) {}
    /**
     * \brief Perform any user actions to be done when a boss scene ends.
     *
     * You should at least delete the boss and unregister the scene in this
     * handler, at least eventually.
     */
    virtual void onBossSceneEnd(BossScene& /*scene*/) {}
    /// Get the coroutine manager for this model.
    kcr::Manager& getManager() { return man; }
    void onEnd() { view->onEnd(); }

  private:
    View* view;
    Playfield p;
    kfp::Wrapper<std::mt19937_64> rng;
    Player gp;
    std::unique_ptr<Stage> curStage;
    BulletList bullets;
    BulletList playerBullets;
    EnemyList enemies;
    ItemList items;
    std::optional<BossScene> bossScene;
    kcr::Manager man;
    bool done = false;
    uint64_t vkFlagsPrev;
    uint64_t vkFlags;
    unsigned stageIndex;
    uint16_t nContinues = 0;
    void checkForCollision();
    void checkForCollisionEnemy();
    void checkForCollisionItem();
    void checkShotErasure();
    void down();
    friend Stage;
    friend Player;
  };

  /**
   * \brief A \ref Scene that represents a danmaku game.
   *
   * You should subclass this class and \ref GameModel to form a pair
   * of a model and view.
   *
   * This is the class responsible for handling input and output.
   *
   * For more details, see \ref GameModel.
   */
  template<TPARAMS> class GameView : public agl::Scene<SD> {
  public:
    TYPEDECLS;
    GameView(
        agl::Texture&& stgFrame, int w, int h, int offw, int offh, int aw,
        int ah, std::istream* rfile,
        const PlayerTraits* traits = &defaultPlayerTraits);
    /// Get read-only access to the model owned by the view.
    const Model& getModel() const {
      assert(model.has_value());
      return *model;
    }
    /// Initialise the game.
    void initialise() override final;
    /// Perform actions to be done every frame.
    void update() override final;
    /// Perform actions that respond to key input.
    void render() override final;
    /// Receive a message from the model.
    virtual void receive(const Message& m) = 0;
    /// Get the bullet list view.
    BulletListView& getBulletListView() { return bulletView; }
    /// Get the player bullet list view.
    BulletListView& getPlayerBulletListView() { return playerBulletView; }
    /// Get the enemy list view.
    EnemyListView& getEnemyListView() { return enemyView; }
    /// Get the item list view.
    ItemListView& getItemListView() { return itemView; }
    /// Change the sprites used for bullets.
    void setShotSprites(agl::WeakTexture t) {
      bulletView.setTexture(std::move(t));
    }
    /// Change the sprites used for player bullets.
    void setPlayerShotSprites(agl::WeakTexture t) {
      playerBulletView.setTexture(std::move(t));
    }
    /// Change the sprites used for enemies.
    void setEnemySprites(agl::WeakTexture t) {
      enemyView.setFrontTexture(std::move(t));
    }
    /// Change the sprites used for items.
    void setItemSprites(agl::WeakTexture t) {
      itemView.setTexture(std::move(t));
    }
    /// Return true if replaying from a file.
    bool isReplay() const { return reading; }
    /// Get the playfield view.
    const PlayfieldView& getPlayfieldView() const { return p; }
    /// Get the playfield view.
    PlayfieldView& getPlayfieldView() { return p; }
    /**
     * \brief Load the player sprite.
     *
     * @ref setPlayerSpriteRect should be called afterwards to set the
     * source rectangle on the texture.
     * \param t The texture to use for the sprite.
     */
    void loadPlayerSprite(agl::WeakTexture t);
    /**
     * \brief Set the source rectangle for the player sprite.
     *
     * @ref loadPlayerSprite should be called before this method.
     *
     * This method can be called multiple times to animate the player sprite.
     * \param r The source rectangle on the texture to use, in pixels.
     */
    void setPlayerSpriteRect(agl::Rect r);
    /// Reset the game -- recreate the model as if it were a new game.
    void reset();
    /// Start the stage'th stage of the game.
    bool startStage(unsigned stage);
    /// Try to go to stage `nextStage`.
    bool tryAdvanceStage(unsigned nextStage);
    /// Set the FBO to which the stage will be drawn.
    void setDestination(agl::FBO& fbo) { destination = &fbo; }
    /**
     * \brief Save a replay.
     *
     * \param out The stream to write the data to.
     * \param timestamp A timestamp to save to the replay.
     * \param name A name to save to the replay.
     */
    void saveReplay(std::ostream& out, time_t timestamp, std::string&& name) {
      r.setTimestamp(timestamp);
      r.setName(std::move(name));
      r.save(out);
    }
    /// Tell the model to continue the game after the player has lost their
    /// last life.
    void useContinue() {
      assert(model.has_value());
      model->useContinue();
    }
    uint16_t getReplayFps() const { return fps; }
    bool isDone() const { return model->isDone(); }

  protected:
    /// Get the `stageno`th stage, or `nullptr` if none.
    /// You should override this method to return a stage for appropriate
    /// `stageno`s.
    virtual std::unique_ptr<Stage> getStage(size_t stageno) = 0;
    /// Perform any user actions to be done every frame.
    virtual void mainLoop() = 0;
    /// Perform any user initialisation.
    virtual void myinit() {}
    /// Perform anything to be done on reset.
    virtual void myreset() {}
    /// Perform any user rendering inside the playfield.
    virtual void myrenderf() {}
    /// Perform any user rendering outside the playfield.
    virtual void myrender() {}
    /// Perform any tasks that needs to be done at the end of the frame.
    virtual void myPostRender() {}
    /// Perform any user rendering inside the playfield when a boss scene
    /// is active.
    virtual void renderBossSceneFront(const BossScene& /*scene*/) {}
    /// Perform any user rendering outside the playfield when a boss scene
    /// is active.
    virtual void renderBossSceneBack(const BossScene& /*scene*/) {}
    /// Perform any user actions to be done when the game ends
    /// (either ran out of lives or finished the last stage).
    virtual void onEnd() {}
    /// A random number generator for use by the \ref GameView.
    kfp::Wrapper<std::mt19937_64> rng;
    kcr::Manager& getManager() { return man; }

  private:
    kcr::Manager man;
    std::optional<Model> model;
    PlayfieldView p;
    agl::FBO* destination = nullptr;
    BulletListView bulletView;
    BulletListView playerBulletView;
    EnemyListView enemyView;
    ItemListView itemView;
    agl::Sprite2D pfSprite;
    agl::Texture stgFrame;
    agl::Sprite2D stgFrameSprite;
    std::unique_ptr<agl::Sprite2D> playerSprite;
    const PlayerTraits* traits = &defaultPlayerTraits;
    float psw = 0.0f, psh = 0.0f;
    Replay r;
    uint16_t fps; // divide by 100 to get real value
    bool reading;
    bool playerSpriteLoaded = false;
    VKMap vkMap;

    ModelOpts
    getModelOpts(unsigned stage, std::unique_ptr<Stage> stagePtr, size_t fseed);
  };

  template<TPARAMS>
  GameModel<TPM>::GameModel(GameModelOptions<TPM>&& opts) :
      view(opts.v), p(opts.p),
      gp(PV(kfp::s16_16(p.getWidth()) / 2, kfp::s16_16(p.getHeight()) * 3 / 4),
         opts.traits, std::move(opts.cd)),
      curStage(std::move(opts.stage)), bullets(Shotsheet{}),
      playerBullets(Shotsheet{}), enemies(EnemyShotsheet{}),
      items(ItemShotsheet{}), stageIndex(opts.stageIndex) {
    gp.setParent((Model*) this);
    gp.setBounds(
        agl::TRect<kfp::s16_16>{3, 3, p.getWidth() - 3, p.getHeight() - 3});
    man.setAdvanceOnExit(false);
    man.spawn([]() {
      while (true) {
        kcr::yield();
        kcr::exit();
      }
    });
    rng.seed(opts.seed);
    if (curStage == nullptr) return;
    curStage->setParent((Model*) this);
    curStage->initialise();
  }

  template<TPARAMS> void GameModel<TPM>::notify(const Message& m) {
    view->receive(std::move(m));
  }

  template<TPARAMS> void GameModel<TPM>::tick() {
    // Check for collision
    checkForCollision();
    checkForCollisionEnemy();
    checkForCollisionItem();
    checkShotErasure();
    movePlayer();
    // Execute user-defined tasks
    man.enter();
    curStage->mainLoop();
    // Update positions of entities
    bullets.updatePositions(
        {0, 0, (int16_t) p.getWidth(), (int16_t) p.getHeight()});
    playerBullets.updatePositions(
        {0, 0, (int16_t) p.getWidth(), (int16_t) p.getHeight()});
    enemies.updatePositions();
    items.updatePositions(gp.getPosition(), gp.canMove(), p);
    if (bossScene.has_value()) {
      // Boss scene is currently active...
      const BossAttack* at = bossScene->currentAttack();
      BossScene::PlayStat st = bossScene->getStat();
      SceneResult r = bossScene->tick();
      if (r == SceneResult::change || r == SceneResult::end) {
        if (at != nullptr)
          this->onBossSceneBreak(*bossScene, *at, st);
        else
          this->onBossSceneStart(*bossScene);
        if (bossScene->currentAttack() != nullptr)
          this->onBossSceneAttackStart(*bossScene, *bossScene->currentAttack());
      }
      if (r == SceneResult::end) { this->onBossSceneEnd(*bossScene); }
    }
    gp.evolveState();
    myTick();
  }

  template<TPARAMS>
  bool GameModel<TPM>::testVirtualKey(int code) const noexcept {
    return (vkFlags & (1 << code)) != 0;
  }

  template<TPARAMS>
  agl::KeyStatus GameModel<TPM>::testVirtualKey2(int code) const noexcept {
    bool now = (vkFlags >> code) & 1;
    bool then = (vkFlagsPrev >> code) & 1;
    return agl::KeyStatus(2 * then + now);
  }

  template<TPARAMS> void GameModel<TPM>::movePlayer() {
    if (gp.canMove()) {
      bool focused = testVirtualKey(vk::focus);
      kfp::s16_16 speed = gp.getPlayerSpeed(focused);
      glm::tvec2<kfp::s16_16> offset(0, 0);
      if (testVirtualKey(vk::left)) { offset.x -= speed; }
      if (testVirtualKey(vk::right)) { offset.x += speed; }
      if (testVirtualKey(vk::up)) { offset.y -= speed; }
      if (testVirtualKey(vk::down)) { offset.y += speed; }
      gp.move(offset);
    }
    if (isShooting()) onShot();
    if (gp.canBomb() && testVirtualKey(vk::bomb)) {
      --gp.cd.bombs;
      if (bossScene.has_value()) { bossScene->onBomb(); }
      onBomb();
    }
  }

  template<TPARAMS> void GameModel<TPM>::checkForCollision() {
    if (!gp.canMove()) return;
    bool grazed = false;
    bullets.graze(gp.getGrazebox(), [this, &grazed](BulletHandle b) {
      ++gp.cd.graze;
      onGraze(b);
      grazed = true;
    });
    if (grazed) onGrazeOnce();
    if (gp.isInvincible()) return;
    bool isHit = false;
    bullets.collide(gp.getHitbox(), [this, &isHit](BulletHandle b) {
      gp.getHit(b);
      isHit = true;
    });
    if (!isHit) {
      enemies.collideWithPlayer(gp.getHitbox(), [this, &isHit](EnemyHandle e) {
        gp.getHit(e);
        isHit = true;
      });
    }
    if (isHit) {
      items.nullifyAttraction();
      onHit();
    }
  }

  template<TPARAMS> void GameModel<TPM>::checkForCollisionEnemy() {
    enemies.forEach([this](EnemyHandle eh) {
      playerBullets.collide(eh.getHitboxToShot(), [&](BulletHandle bh) {
        eh.addHealthAdjusted(-bh.getDamage(), bh.getPlayerAttackDivision());
        bh.markForDeletion();
      });
    });
  }

  template<TPARAMS> void GameModel<TPM>::checkForCollisionItem() {
    std::vector<ItemRec> recs;
    items.forAllCollected([&recs](ItemHandle i) {
      recs.push_back(ItemRec{i.getPosition(), i.getId()});
      i.markForDeletion();
    });
    if (!recs.empty()) onCollectItem(recs.size(), recs.data());
  }

  template<TPARAMS> void GameModel<TPM>::checkShotErasure() {
    std::vector<PV> positions;
    playerBullets.forEach([&positions, this](BulletHandle pbh) {
      if (pbh.erasesShots()) {
        bullets.forEach([pbh, &positions](BulletHandle bh) {
          // TODO: this doesn't work properly if the player shot is a laser
          if (!bh.isSpellResistant() && bh.intersectsWith(pbh.getHitbox())) {
            bh.pushPosition(positions);
            bh.markForDeletion(); // TODO: add a callback
          }
        });
      }
    });
    onSpawnErasureItem(positions.size(), positions.data());
  }

  template<TPARAMS> void GameModel<TPM>::useContinue() {
    assert(isGameOver());
    if (nContinues < UINT16_MAX) ++nContinues;
    gp.getState().flag = PlayerStateFlag::normal;
    gp.reset();
    // Don't reset manager state or replay
    onContinue();
    onRespawn();
  }

  template<TPARAMS> void GameModel<TPM>::down() {
    if (bossScene.has_value()) { bossScene->onMiss(); }
    onDown();
  }

  template<TPARAMS>
  BossScene& GameModel<TPM>::registerBossScene(EnemyHandle b) {
    bossScene.emplace(b);
    return *bossScene;
  }

  template<TPARAMS> void GameModel<TPM>::breakScreen() {
    std::vector<PV> positions = bullets.getShotPositions();
    onSpawnErasureItem(positions.size(), positions.data());
    bullets.clear();
  }

  /*
   * Note that due to the odd behaviour of calling C++ virtual functions
   * in constructors, we have to use two-phase initialisation to construct
   * a subclass of GameView. This is suboptimal, because this forces
   * having our game model field be able to hold an empty state (here
   * by using `std::optional`).
   */
  template<TPARAMS>
  GameView<TPM>::GameView(
      agl::Texture&& stgFrame, int w, int h, int offw, int offh, int aw, int ah,
      std::istream* rfile, const PlayerTraits* traits) :
      p(Playfield{w, h, offw, offh, aw, ah}),
      bulletView(nullptr, &p), playerBulletView(nullptr, &p),
      enemyView(nullptr, &p), itemView(nullptr, &p),
      pfSprite(p.getTexture().weak()), stgFrame(std::move(stgFrame)),
      stgFrameSprite(this->stgFrame.weak()), traits(traits),
      reading(rfile != nullptr), vkMap(getDefaultVKMap()) {
    pfSprite.addSprite(agl::Sprite2DInfo{
        p.getActualBoundsZeroInvertY(),
        p.getActualBounds(),
    });
    this->stgFrame.addName("Game::stgFrame");
    if (rfile != nullptr) {
      auto opt = Replay::load(*rfile);
      if (!opt.has_value()) {
        std::cerr << "warn: replay failed to load\n";
        reading = false;
      } else {
        r = std::move(*opt);
      }
    }
    man.setAdvanceOnExit(false);
    man.spawn([]() {
      while (true) {
        kcr::yield();
        kcr::exit();
      }
    });
  }

  template<TPARAMS> void GameView<TPM>::initialise() {
    auto* app = this->app;
    pfSprite.setApp(app);
    stgFrameSprite.addSprite(agl::Sprite2DInfo{
        {0, 0, (float) app->getWidth(), (float) app->getHeight()},
        {0, 0, (float) app->getWidth(), (float) app->getHeight()}});
    stgFrameSprite.setApp(app);
    myinit();
    myreset();
    // We have to start the stage from here instead of from GameModel's
    // ctor; otherwise, the version of the method called will be of
    // GameModel instead of the user's derived class.
    // (see https://en.cppreference.com/w/cpp/language/virtual)
    [[maybe_unused]] bool b = startStage(0);
    assert(b);
  }

  // TODO(kozet): refactor to work with multiple stages
  template<TPARAMS> void GameView<TPM>::update() {
    assert(model.has_value());
    // Read virtual keypresses
    ReplaySection& sec = r.currentSection();
    if (reading) {
      uint64_t fl;
      bool success = sec.read(fl, fps);
      if (!success) {
        // If not last stage, move to next stage
        unsigned nextStage = r.getStageAfter(r.getCurrentStage());
        if (!tryAdvanceStage(nextStage)) {
          onEnd();
          return;
        }
      }
      // XXX: what to do if the read didn't succeed?
      // Need to test with multiple stages.
      model->updateVKFlags(fl);
    } else {
      if (model->isDone()) {
        // If not last stage, move to next stage
        unsigned nextStage = r.getCurrentStage() + 1;
        if (!tryAdvanceStage(nextStage)) {
          onEnd();
          return;
        }
      }
      uint64_t fl = 0;
      for (size_t i = 0; i < vkMap.size(); ++i) {
        if (agl::Scene<SD>::testKey(vkMap[i])) fl |= (1 << i);
      }
      model->updateVKFlags(fl);
      fps = (uint16_t)(this->app->getFPS() * 100);
      sec.write(fl, fps);
    }
    model->tick();
    mainLoop();
  }

  template<TPARAMS> void GameView<TPM>::render() {
    const Model& m = getModel();
    const Player& gp = m.getPlayer();
    const auto& bossScene = m.getBossScene();
    glClearColor(0.5f, 0.7f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    p.getFBOMS().setActive();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (playerSpriteLoaded && gp.shouldRenderSprite()) {
      auto pos = gp.getPos();
      float fpx = pos.x.toDouble(), fpy = pos.y.toDouble();
      playerSprite->get(0).dest = {
          fpx - 0.5f * psw,
          fpy - 0.5f * psh,
          fpx + 0.5f * psw,
          fpy + 0.5f * psh,
      };
      playerSprite->update();
      playerSprite->render();
    }
    // NOTE: items should be rendered first to avoid obscuring
    // bullets or enemies
    itemView.render();
    playerBulletView.render();
    enemyView.render();
    bulletView.render();
    myrenderf();
    if (bossScene.has_value()) { renderBossSceneFront(*bossScene); }
    man.enter();
    p.blit();
    if (destination == nullptr || destination->getID() == 0)
      agl::setDefaultFBOAsActive();
    else
      destination->setActive();
    pfSprite.render();
    stgFrameSprite.render();
    myrender();
    if (bossScene.has_value()) { renderBossSceneBack(*bossScene); }
    myPostRender();
    if (destination != nullptr && destination->getID() != 0) {
      agl::FBO def;
      destination->blitTo(def, this->app->getWidth(), this->app->getHeight());
    }
  }

  template<TPARAMS>
  typename GameView<TPM>::ModelOpts GameView<TPM>::getModelOpts(
      unsigned stage, std::unique_ptr<Stage> stagePtr, size_t fseed) {
    // Set common data
    ReplaySection& sec = r.currentSection();
    auto nextCd = ([&]() -> std::optional<CommonData> {
      if (stage == 0) return std::nullopt;
      assert(model.has_value());
      return reading ? sec.getCommonData() : model->getPlayer().cd;
    })();
    ModelOpts opts;
    opts.cd = std::move(nextCd);
    opts.v = (View*) this;
    opts.p = (const Playfield&) p;
    opts.traits = traits;
    opts.stage = std::move(stagePtr);
    opts.seed = fseed;
    opts.stageIndex = stage;
    return opts;
  }

  template<TPARAMS> bool GameView<TPM>::startStage(unsigned stage) {
    fmt::print(stderr, "slax {} et kit\n", stage);
    std::unique_ptr<Stage> stagePtr = getStage(stage);
    if (stagePtr == nullptr) return false;
    // Set current stage
    r.setCurrentStage(stage);
    // Set seed
    ReplaySection& sec = r.currentSection();
    sec.rewind();
    uint64_t fseed;
    if (reading) {
      fseed = sec.getRNGSeed();
    } else {
      std::random_device d;
      fseed = d();
      sec.setRNGSeed(fseed);
    }
    bool hasSetup = model.has_value();
    ModelOpts opts = getModelOpts(stage, std::move(stagePtr), fseed);
    model.emplace(std::move(opts));
    bulletView.setModel(&model->getBulletList());
    playerBulletView.setModel(&model->getPlayerBulletList());
    enemyView.setModel(&model->getEnemyList());
    itemView.setModel(&model->getItemList());
    if (!hasSetup) {
      bulletView.setUp();
      playerBulletView.setUp();
      enemyView.setUp();
      itemView.setUp();
    }
    return true;
  }

  template<TPARAMS> bool GameView<TPM>::tryAdvanceStage(unsigned nextStage) {
    if (nextStage == -1U) {
      return false;
    } else {
      bool started = startStage(nextStage);
      if (!started) return false;
    }
    return true;
  }


  template<TPARAMS> void GameView<TPM>::loadPlayerSprite(agl::WeakTexture t) {
    t.addName("Game::playerTexture");
    playerSprite = std::make_unique<agl::Sprite2D>(t);
    // Don't explicitly set the app -- Sprite2D:s don't need it explicitly set
    // and we want it to default to the viewport from when it renders to the
    // inside-STG-frame FBO.
    playerSprite->clear();
    // Waiting for C++20 designated initialisers...
    // Anyway, leave the rects unspecified; we'll worry about it later.
    playerSprite->addSprite({
        /* .source = */ {0, 0, 0, 0},
        /* .dest   = */ {0, 0, 0, 0},
    });
    playerSpriteLoaded = true;
  }

  template<TPARAMS> void GameView<TPM>::setPlayerSpriteRect(agl::Rect r) {
    assert(playerSpriteLoaded);
    psw = r.right - r.left;
    psh = r.bottom - r.top;
    playerSprite->get(0).source = r;
  }

  template<TPARAMS> void GameView<TPM>::reset() {
    std::cerr << "reset\n";
    if (!reading) r.clear();
    this->app->getMixer().clear();
    man = kcr::Manager();
    man.setAdvanceOnExit(false);
    man.spawn([]() {
      while (true) {
        kcr::yield();
        kcr::exit();
      }
    });
    myreset();
    std::cerr << "about to start stage\n";
    [[maybe_unused]] bool b = startStage(0);
    assert(b);
  }
}
