#pragma once

#include <stdint.h>

#include <iosfwd>
#include <string>
#include <unordered_map>

#include "TDR/Bullet.h"
#include "TDR/defset.h"

namespace tdr {
  /**
   * \brief Stores statistics such as score, lives, bombs and graze,
   * as well as any user fields.
   */
  class CommonData {
  public:
    CommonData() : score(0), lives(2), bombs(3), graze(0) {}
    // yay more abuse of preprocessor macros
    DEF_SET(Int, uint64_t, integers)
    // DEF_SET(Double, double, doubles)
    DEF_SET(String, std::string, strings)
    void reset();

  private:
    CommonData(std::istream& fh, bool& succ);
    void save(std::ostream& fh) const;
    static std::optional<CommonData> load(std::istream& fh);
    std::unordered_map<std::string, uint64_t> integers;
    // std::unordered_map<std::string, double> doubles;
    std::unordered_map<std::string, std::string> strings;

  public:
    uint64_t score, lives, bombs, graze;
    Circle hitbox;

    friend class ReplaySection;
  };
}
