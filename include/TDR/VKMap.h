#pragma once

#include <vector>

namespace tdr {
  namespace vk {
    /// Symbolic constants for virtual keys.
    enum {
      left, ///< The key to move the player left.
      right, ///< The key to move the player right.
      up, ///< The key to move the player up.
      down, ///< The key to move the player down.
      shot, ///< The key to make the player shoot.
      bomb, ///< The key to trigger a bomb from the player.
      focus, ///< The key to slow down the player's movement (focus).
      user, ///< Any keys for user actions.
    };
  }
  extern int defaultVKMap[];
  /// A mapping from physical to virtual keys.
  using VKMap = std::vector<int>;
  /// Get the default virtual key map.
  VKMap getDefaultVKMap();
}
