#pragma once

#define TPARAMS typename GM, typename GV, typename SD, typename Msg
#define TPM GM, GV, SD, Msg
#define TYPEDECLS \
  using Model = GM; \
  using View = GV; \
  using Message = Msg; \
  using Stage = Stage<Model>; \
  using Player = Player<Model>; \
  using ModelOpts = GameModelOptions<TPM>
