#pragma once

#include <kozet_coroutine/kcr.h>

namespace tdr {
  /**
   * \brief Yield a certain number of times.
   *
   * If this is called inside a coroutine manager that enters every frame,
   * then this has the effect of suspending execution for `ticks` frames.
   *
   * \param ticks The number of times to yield.
   */
  inline void cwait(size_t ticks) {
    for (size_t i = 0; i < ticks; ++i) kcr::yield();
  }
}
