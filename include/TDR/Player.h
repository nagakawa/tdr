#pragma once

#include <memory>

#include <kozet_fixed_point/kfp.h>

#include "TDR/Bullet.h"
#include "TDR/CommonData.h"
#include "TDR/enemy/EnemyList.h"

using namespace kfp::literals;

namespace tdr {
  /**
   * Describes characteristics of a player.
   */
  struct PlayerTraits {
    /**
     * \var unfocusedSpeed
     * \brief Unfocused speed, in pixels per frame.
     *
     * \var focusedSpeed
     * \brief Focused speed, in pixels per frame.
     *
     * \var startingBombs
     * \brief Number of bombs given at start, as well as when the player
     * loses a life.
     *
     * \var startingLives
     * \brief Number of lives given at the start of the game.
     *
     * \var deathbombTime
     * \brief Time between when the player is hit and when they lose a life,
     * in frames. The player may bomb in that interval to avoid losing a life.
     *
     * \var respawnTime
     * \brief Time between when the player loses a life and they respawn,
     * in frames.
     *
     * \var respawnInvincibilityTime
     * \brief Time between when the player respawns and they lose
     * invincibility, in frames.
     *
     * \var hitRadius
     * \brief Hitbox radius of player, in pixels.
     *
     * \var grazeRadius
     * \brief Grazebox radius of player, in pixels.
     */
    kfp::s16_16 unfocusedSpeed, focusedSpeed;
    unsigned startingBombs;
    unsigned startingLives;
    int deathbombTime;
    int respawnTime;
    int respawnInvincibilityTime;
    kfp::s16_16 hitRadius;
    kfp::s16_16 grazeRadius;
  };
  /**
   * \brief Describes basic player state.
   */
  enum class PlayerStateFlag {
    normal, ///< Player is alive.
    hit, ///< Player has been hit, but can still bomb to save their life.
    down, ///< Player has lost a life.
    end, ///< Player has lost their last life.
  };
  /**
   * \brief Describes complete player state.
   */
  struct PlayerState {
    /// Basic player state.
    PlayerStateFlag flag;
    /**
     * \brief The number of frames before the next state transition.
     *
     * flag   | timeLeft
     * -------|---------
     * normal | Frames left of invincibility.
     * hit    | Frames left to bomb before losing a life.
     * down   | Frames left before respawning.
     * end    | Unspecified.
     */
    int timeLeft;
    PlayerState() : flag(PlayerStateFlag::normal), timeLeft(0) {}
    /**
     * \brief Advance the state by one frame.
     *
     * \param traits The player traits to refer to for certain parameters.
     */
    void evolve(const PlayerTraits& traits) noexcept;
  };
  /// The default player traits used for @ref Player.
  extern PlayerTraits defaultPlayerTraits;
  /**
   * \brief Represents a player in a danmaku game.
   */
  template<typename GM> class Player {
  public:
    using Model = GM;
    /**
     * \brief Construct a new player.
     *
     * \param x The x-coordinate of the player.
     * \param y The y-coordinate of the player.
     * \param traits The pointer to the player traits to use.
     */
    Player(
        PV pos, const PlayerTraits* traits = &defaultPlayerTraits,
        std::optional<CommonData> cd = std::nullopt);
    /// Set the player stats to their starting state.
    void reset();
    /// Return true if player collides with circle.
    bool check(const Circle& h) { return h.intersects(cd.hitbox); }
    /// Return true if player collides with line.
    bool check(const Line& h) { return h.intersects(cd.hitbox); }
    /// Return the hitbox of this player.
    const Circle& getHitbox() const { return cd.hitbox; }
    /// Return the position of this player.
    const PV& getPosition() const { return cd.hitbox.c; }
    /// Get the current state of the player.
    PlayerState& getState() { return state; }
    /// Get the position of the player.
    glm::tvec2<kfp::s16_16> getPos() const { return cd.hitbox.c; }
    /// Get the grazebox of the player.
    const Circle getGrazebox() const { return {getPos(), traits->grazeRadius}; }
    /// Set the position of the player.
    void setPos(const glm::tvec2<kfp::s16_16>& p) { cd.hitbox.c = p; }
    /// Add an offset to the position of the player.
    void move(const glm::tvec2<kfp::s16_16>& off);
    /// Set the bounds outside of which the player cannot move.
    void setBounds(const agl::TRect<kfp::s16_16>& bounds) {
      this->bounds = bounds;
    }
    /// Return true if the player is invincible.
    bool isInvincible() const noexcept;
    /// Simulate a hit against a bullet.
    void getHit(BulletHandle& b);
    /// Simulate a hit against an enemy.
    void getHit(EnemyHandle& e);
    /// Advance the state by one frame.
    void evolveState() noexcept;
    /// Return true if the player can move.
    bool canMove() const noexcept {
      return state.flag == PlayerStateFlag::normal;
    }
    /// Return true if the player can bomb.
    bool canBomb() const noexcept;
    /// Return true if the game has ended.
    bool isGameOver() const noexcept {
      return state.flag == PlayerStateFlag::end;
    }
    /// Return true if the player is alive.
    bool isAlive() const noexcept;
    /// Return true if the player's sprite should be displayed.
    bool shouldRenderSprite() const noexcept { return isAlive(); }
    /// Get the number of frames before the player's bomb expires.
    int getBombFrames() const noexcept { return bombFrames; }
    /// Set the number of frames before the player's bomb expires.
    void setBombFrames(int f) noexcept { bombFrames = f; }
    /**
     * \brief Set the number of invincibility frames left for the player.
     *
     * Note that the player must be alive.
     */
    void setInvincibilityFrames(int f) noexcept;
    /// Set the parent game for this player.
    void setParent(Model* g) { this->g = g; }
    /// Set the traits of this player.
    void setPlayerTraits(PlayerTraits* t);
    /**
     * \brief Get the player's unfocused or focused speed.
     *
     * \param focused Whether the player is focused
     */
    kfp::s16_16 getPlayerSpeed(bool focused) {
      return focused ? traits->focusedSpeed : traits->unfocusedSpeed;
    }

    /// The @ref CommonData associated with the player.
    CommonData cd;

  private:
    const PlayerTraits* traits = &defaultPlayerTraits;
    PlayerState state;
    agl::TRect<kfp::s16_16> bounds;
    int bombFrames = 0;
    Model* g = nullptr;
  };
  template<typename GM> inline bool Player<GM>::isInvincible() const noexcept {
    return state.flag != PlayerStateFlag::normal || state.timeLeft > 0;
  }

  template<typename GM> inline bool Player<GM>::canBomb() const noexcept {
    return isAlive() && bombFrames <= 0 && cd.bombs > 0;
  }

  template<typename GM> inline bool Player<GM>::isAlive() const noexcept {
    return state.flag == PlayerStateFlag::normal ||
           state.flag == PlayerStateFlag::hit;
  }

  template<typename GM>
  inline void Player<GM>::setInvincibilityFrames(int f) noexcept {
    assert(isAlive());
    state.flag = PlayerStateFlag::normal;
    state.timeLeft = f;
  }

  template<typename GM> void Player<GM>::reset() {
    cd.reset();
    // cd.hitbox = {{x, y}, traits->hitRadius};
    cd.lives = traits->startingLives;
    cd.bombs = traits->startingBombs;
    bombFrames = 0;
  }

  template<typename GM> void Player<GM>::setPlayerTraits(PlayerTraits* t) {
    t = traits;
    cd.hitbox.r = t->hitRadius;
  }

  template<typename GM>
  Player<GM>::Player(
      PV pos, const PlayerTraits* traits, std::optional<CommonData> cd) :
      traits(traits) {
    if (cd.has_value()) {
      this->cd = std::move(*cd);
    } else {
      this->cd.hitbox = {pos, traits->hitRadius};
      this->cd.lives = traits->startingLives;
      this->cd.bombs = traits->startingBombs;
    }
  }

  template<typename GM>
  void Player<GM>::move(const glm::tvec2<kfp::s16_16>& off) {
    auto& pos = cd.hitbox.c;
    pos += off;
    pos.x = std::max(bounds.left, std::min(bounds.right, pos.x));
    pos.y = std::max(bounds.top, std::min(bounds.bottom, pos.y));
  }

  template<typename GM> void Player<GM>::getHit(BulletHandle& b) {
    if (!b.isSpellResistant()) b.markForDeletion();
    state.flag = PlayerStateFlag::hit;
    state.timeLeft = traits->deathbombTime;
  }

  template<typename GM> void Player<GM>::getHit(EnemyHandle& e) {
    e.addHealth(-100); // TOOD: maybe allow configuring this value?
    state.flag = PlayerStateFlag::hit;
    state.timeLeft = traits->deathbombTime;
  }

  template<typename GM> void Player<GM>::evolveState() noexcept {
    if (state.timeLeft == 0) {
      // About to transition from one state to another?
      switch (state.flag) {
      case PlayerStateFlag::hit: // Hit -> Down
        if (cd.lives == 0) { // It's game over, man!
          state.flag = PlayerStateFlag::end;
          g->onEnd();
          return;
        }
        --cd.lives;
        g->down();
        break;
      case PlayerStateFlag::down: // Down -> Normal
        cd.bombs = traits->startingBombs;
        g->onRespawn();
        break;
      default: {
      }
      }
    }
    state.evolve(*traits);
    if (state.flag != PlayerStateFlag::normal)
      bombFrames = 0;
    else if (bombFrames > 0)
      --bombFrames;
  }
}
