#pragma once

#include <stdint.h>
#include <time.h>

#include <iosfwd>
#include <map>
#include <vector>

#include <kozet_fixed_point/kfp.h>

#include "TDR/CommonData.h"

namespace tdr {
  /**
   * \brief A single stage of a replay.
   */
  class ReplaySection {
  public:
    ReplaySection() = default;
    /**
     * \brief Read one frame of a replay.
     *
     * This advances the replay cursor by one frame.
     *
     * \param keys The reference to where the keypress will be stored.
     * \param fps The reference to where the framerate will be stored
     * (in units of 0.01 fps).
     * \return True if the read succeeded, and false otherwise.
     */
    bool read(uint64_t& keys, uint16_t& fps);
    /**
     * \brief Write one frame of a replay.
     *
     * This advances the replay cursor by one frame.
     *
     * \param keys The keypresses to write.
     * \param fps The framerate to write (in units of 0.01 fps).
     */
    void write(uint64_t keys, uint16_t fps);
    /// Return the position of the replay cursor.
    size_t tell() const { return idx; }
    /// Set the position of the replay cursor.
    void seek(size_t i) { idx = i; }
    /// Rewind the position of the replay cursor to the start.
    void rewind() { seek(0); }
    /// Set the seed for the random number generator.
    void setRNGSeed(uint64_t seed) { this->seed = seed; }
    /// Get the seed for the random number generator.
    uint64_t getRNGSeed() { return seed; }
    /// Get the common data for this section.
    const CommonData& getCommonData() const { return cd; }
    /**
     * \brief Get the common data for this section.
     *
     * This common data represents the state of the game at the start of
     * the stage. For instance, the common data of a replay section
     * representing the first stage of a game will hold a score of 0,
     * not the score at the end of that stage.
     */
    CommonData& getCommonData() { return cd; }
    /// Set the common data for this section.
    void setCommonData(CommonData&& cd) { this->cd = std::move(cd); }

  private:
    ReplaySection(std::istream& fh, bool& succ);
    void save(std::ostream& fh) const;
    static std::optional<ReplaySection> load(std::istream& fh);
    CommonData cd;
    std::vector<uint64_t> keypresses;
    std::vector<uint16_t> fpses;
    uint64_t seed;
    size_t idx = 0;
    friend class Replay;
  };
  /**
   * \brief A replay file.
   *
   * A replay file stores the inputs needed to reproduce gameplay. A player
   * can save a replay file of their play after finishing it and play it back
   * later.
   *
   * A replay file, aside from metadata, has some number of *sections*. Each
   * section holds the data needed to play back a stage of the game,
   * independently from the other stages.
   *
   * In order to allow starting a replay from any stage, any attributes that
   * persist across stages is stored in a *common data area*. This area stores
   * the score, life count, bomb count, graze count and position of the player
   * by default, and more fields can be tracked.
   *
   * We outline the replay format here. All integers are stored little-endian
   * unless otherwise specified.
   *
   * Header:
   *
   * - A signature of 8 bytes: hex 08 00 03 0B 80 0D FF 0A
   * - A 64-bit timestamp
   * - The name, with a 1-byte prefix for length
   * - A comment, with a 1-byte prefix for length
   * - The number of replay sections (one for each stage), as a 32-bit unsigned
   *   integer
   *
   * Data for each section of the replay:
   *
   * - The stage number (unsigned 32-bit integer)
   * - The random seed used for this stage (unsigned 64-bit integer)
   * - The common data:
   *   - The score, lives, bombs and graze at the beginning of the stage
   *     (signed 64-bit integers)
   *   - The X- and Y-coordinates of the player and the radius of the hitbox
   *     at the beginning of the stage (signed 16.16 fixed-point numbers)
   *   - The number of custom integer fields in the common data (unsigned 32-bit
   *     integer)
   *   - For each field, its name (2-byte prefix for length) and value (signed
   *     64-bit integer)
   *   - The number of custom string fields in the common data (unsigned 32-bit
   *     integer)
   *   - For each field, its name (2-byte prefix for length) and value (string
   *     with 4-byte prefix for length)
   * - The number of frames (unsigned 64-bit integer)
   * - Number of bytes used per keypress (8-bit integer; either 1, 2, 4 or 8)
   * - The keypresses themselves (number of frames × bytes per keypress)
   * - The framerate values for each frame (16-bit integer representing
   *   100 × fps)
   */
  class Replay {
  public:
    Replay() : timestamp(0), currentStage(-1) {}
    /// Get the current stage being read from or written to.
    unsigned getCurrentStage() const { return currentStage; }
    /// Get the stage after stage number `stage`, or -1 if this is the last.
    unsigned getStageAfter(unsigned stage) const;
    /// Set the current stage.
    void setCurrentStage(unsigned stage) { currentStage = stage; }
    /// Get the current replay section being read from or written to.
    ReplaySection& currentSection() {
      assert(currentStage != -1u);
      return sections[currentStage];
    }
    /// Get the timestamp of the replay.
    time_t getTimestamp() { return timestamp; }
    /// Set the timestamp of the replay.
    void setTimestamp(time_t t) { timestamp = t; }
    std::string_view getName() const { return name; }
    void setName(std::string&& c) { name = std::move(c); }
    std::string_view getComment() const { return comment; }
    void setComment(std::string&& c) { comment = std::move(c); }
    /// Clear all data from the replay.
    void clear();
    /**
     * \brief Save this replay to a file.
     *
     * \param fh The stream to write the replay data to.
     */
    void save(std::ostream& fh) const;
    /**
     * \brief Load a replay from a file.
     *
     * \param fh The stream to read the replay data from.
     * \return The replay, or `std::nullopt` if reading failed.
     */
    static std::optional<Replay> load(std::istream& fh);

  private:
    Replay(std::istream& fh, bool& succ);
    std::string name;
    std::string comment;
    time_t timestamp;
    std::map<unsigned, ReplaySection> sections;
    unsigned currentStage;
  };
}
