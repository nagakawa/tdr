#pragma once

#include <kozet_coroutine/kcr.h>

#include "TDR/gamemacros.h"

namespace tdr {
  /**
   * \brief Represents a stage in a danmaku game.
   *
   * User code is expected to subclass this class.
   */
  template<class GM> class Stage {
  public:
    using Model = GM;
    virtual ~Stage() {}
    /*
      XXX:
      We use two-phase initialisation here instead of letting the stage do
      everything in the constructor because many things need to be done
      after the game model is initialised, while the stage object is created
      before then. Looking for a better way to do this.
    */
    /// Initialise the stage.
    virtual void initialise() {}
    /// Perform any user actions to be done every frame.
    virtual void mainLoop() = 0;
    void setParent(Model* g) { this->g = g; }

  protected:
    /**
     * \brief A pointer to the parent @ref Game.
     *
     * This is set only when the stage is started.
     */
    Model* g;
    // Right now, stages share coroutine managers with the game.
    // Debating whether to keep this as is or give each stage its own manager.
    /**
     * \brief Get the coroutine manager for this stage.
     *
     * Right now, the manager is shared with the parent game, but this might
     * change in a future update.
     */
    kcr::Manager& getManager() { return g->getManager(); }
    template<typename T> void notify(const T& m) { g->notify(m); }
  };
}
