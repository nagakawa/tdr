#pragma once

#if __has_include(<filesystem>)
#  include <filesystem>
namespace fs = std::filesystem;
#elif __has_include(<boost/filesystem.hpp>)
#  include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
#else
#  error "No filesystem library detected!"
#endif
