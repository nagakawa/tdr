#pragma once

#ifndef __cplusplus
#  error "I need C++!"
#endif

#include <limits.h>
#include <stdint.h>

#include <iostream>
#include <limits>
#include <optional>
#include <string>
#include <string_view>

#if CHAR_BIT != 8
#  error "Sorry, I can only handle 8-bit chars!"
#endif

namespace /* resistance is */ futil {
  void write16u(FILE* fh, uint16_t n);
  void write32u(FILE* fh, uint32_t n);
  void write64u(FILE* fh, uint64_t n);
  void write16s(FILE* fh, int16_t n);
  void write32s(FILE* fh, int32_t n);
  void write64s(FILE* fh, int64_t n);
  uint16_t read16u(FILE* fh);
  uint32_t read32u(FILE* fh);
  uint64_t read64u(FILE* fh);
  int16_t read16s(FILE* fh);
  int32_t read32s(FILE* fh);
  int64_t read64s(FILE* fh);
  void write16u(std::ostream& fh, uint16_t n);
  void write32u(std::ostream& fh, uint32_t n);
  void write64u(std::ostream& fh, uint64_t n);
  void write16s(std::ostream& fh, int16_t n);
  void write32s(std::ostream& fh, int32_t n);
  void write64s(std::ostream& fh, int64_t n);
  uint16_t read16u(std::istream& fh);
  uint32_t read32u(std::istream& fh);
  uint64_t read64u(std::istream& fh);
  int16_t read16s(std::istream& fh);
  int32_t read32s(std::istream& fh);
  int64_t read64s(std::istream& fh);
  template<typename T> bool writeStr(std::ostream& fh, std::string_view s) {
    static_assert(
        std::is_integral_v<T> && std::is_unsigned_v<T>,
        "T must be integral and unsigned");
    if (s.length() > std::numeric_limits<T>::max()) return false;
    T n = s.length();
    char buf[sizeof(T)];
    for (size_t i = 0; i < sizeof(T); ++i) {
      buf[i] = (char) (n & 0xFF);
      if constexpr (sizeof(T) > 1) // avoid shift overflow warning
        n >>= 8;
    }
    fh.write(buf, sizeof(T));
    fh.write(s.data(), s.length());
    return true;
  }
  template<typename T> std::optional<std::string> readStr(std::istream& fh) {
    char buf[sizeof(T)];
    fh.read(buf, sizeof(T));
    T n = 0;
    for (size_t i = 0; i < sizeof(T); ++i) {
      if constexpr (sizeof(T) > 1) // avoid shift overflow warning
        n <<= 8;
      n |= (uint8_t) buf[sizeof(T) - 1 - i];
    }
    if (!fh.good()) return std::nullopt;
    std::string s(n, '\0');
    fh.read(s.data(), n);
    return s;
  }
  template<typename T> bool writeStr(FILE* fh, std::string_view s) {
    static_assert(
        std::is_integral_v<T> && std::is_unsigned_v<T>,
        "T must be integral and unsigned");
    if (s.length() > std::numeric_limits<T>::max()) return false;
    T n = s.length();
    char buf[sizeof(T)];
    for (size_t i = 0; i < sizeof(T); ++i) {
      buf[i] = (char) (n & 0xFF);
      n >>= 8;
    }
    fwrite(buf, sizeof(T), 1, fh);
    fwrite(s.data(), s.length(), 1, fh);
    return true;
  }
  template<typename T> std::optional<std::string> readStr(FILE* fh) {
    char buf[sizeof(T)];
    size_t read = fread(buf, sizeof(T), 1, fh);
    T n = 0;
    for (size_t i = 0; i < sizeof(T); ++i) {
      n <<= 8;
      n |= (uint8_t) buf[sizeof(T) - 1 - i];
    }
    if (read < 1) return std::nullopt;
    std::string s(n, '\0');
    fread(s.data(), n, 1, fh);
    return s;
  }
}
