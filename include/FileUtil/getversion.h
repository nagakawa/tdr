#pragma once

#if __cplusplus >= 201103L
#  define CTH_CXX11 1
#endif

#if __cplusplus >= 201402L
#  define CTH_CXX14 1
#endif

#if __cplusplus >= 201703L
#  define CTH_CXX17 1
#endif

// There is not yet a defined value for __cplusplus for C++20;
// replace this to check against the actual value when we switch.
#if __cplusplus > 201703L
#  define CTH_CXX20 1
#endif
