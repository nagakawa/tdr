#pragma once

#include "FileUtil/getversion.h"

#if defined(__GNUC__)
#  define CTH_NONNULL(...) __attribute__((__nonnull__(__VA_ARGS__)))
#  define CTH_RETURNS_NONNULL __attribute__((__returns_nonnull__))
#  define CTH_ASSUME(x) __builtin_assume(x)
#else
#  define CTH_NONNULL(...)
#  define CTH_RETURNS_NONNULL
#  define CTH_ASSUME(x)
#endif

#ifdef __GNUC__
#  define CTH_RESTRICT __restrict__
#elif _MSC_VER
#  define CTH_RESTRICT __restrict
#else
#  define CTH_RESTRICT
#endif

// Replace CP_NOUNIQADDR with [[no_unique_address]] when we're ready to switch
// to C++20
#ifdef CTH_CPP20
#  define CTH_NOUNIQADDR [[no_unique_address]]
#else
#  define CTH_NOUNIQADDR
#endif

#ifdef __clang__
#  define CTH_TRIVIAL_RELOCATE __attribute__((__trivial_abi__))
#else
#  define CTH_TRIVIAL_RELOCATE
#endif
