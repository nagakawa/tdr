#pragma once

#include <optional>
#include <string>
#include <string_view>

namespace futil {
  std::optional<std::string> getExeLocation();
}
