#pragma once

#include "FileUtil/getversion.h"

#if CTH_CXX20 // use C++20 attributes (nyi in g++)
#  define IF_LIKELY(c) \
    if (c) [[likely]]
#  define IF_UNLIKELY(c) \
    if (c) [[unlikely]]
#elif defined(__GNUC__)
#  define IF_LIKELY(c) if (__builtin_expect(static_cast<bool>(c), 1))
#  define IF_UNLIKELY(c) if (__builtin_expect(static_cast<bool>(c), 0))
#else
#  define IF_LIKELY(c) if (c)
#  define IF_UNLIKELY(c) if (c)
#endif
