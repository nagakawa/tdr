#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#ifdef __cplusplus
extern "C" {
#endif

int openFile(FILE** streamptr, const char* filename, const char* mode);

#ifdef __cplusplus
}
#endif
