#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <memory>
#include <optional>
#include <stack>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

#include <parallel_hashmap/phmap.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "AGL/GLFWApplication.h"
#include "AGL/Message.h"
#include "AGL/Scene.h"

namespace agl {
  struct SceneOptions {
    bool isPersistent = false;
  };
  /**
   * \brief A subclass of @ref GLFWApplication that works with scenes.
   *
   * This class can store @ref Scene objects by name, as well as switch
   * to another scene.
   *
   * @tparam SharedData type of shared data to create
   *
   * TODO: ability to 'screenshot' a scene?
   * TODO: add ability to store thunks for scenes?
   */
  template<typename SharedData>
  class ScenedGLFWApplication : public GLFWApplication {
  public:
    using GLFWApplication::GLFWApplication;
    ScenedGLFWApplication(SharedData&& sd, const ApplicationOptions& opts) :
        GLFWApplication(opts), sharedData(std::move(sd)) {}
    void initialise() override;
    void tick() override;
    void readKeys() override;
    void onMouse(double xpos, double ypos) override;
    /**
     * \brief Insert a scene to this object.
     *
     * \tparam S The type of the scene to insert.
     * \tparam F The type of the callback passed in; usually not necessary to
     *   list this explicitly.
     * \param getSc The callback that returns a unique_ptr to a scene of type
     * `S`. \return A pointer to the scene.
     */
    template<typename S, typename F>
    std::enable_if_t<
        std::is_base_of_v<Scene<SharedData>, S> &&
            std::is_invocable_r_v<std::unique_ptr<S>, F>,
        void>
    insertScene(F&& getSc, SceneOptions opts = SceneOptions{});
    /**
     * \brief Switch to another scene.
     *
     * The actual scene switch will be deferred until the end of the frame
     * in which the method is called. That is, if this method is called in
     * a scene's `update()` method, then the `render()` method from the
     * same scene will be called before any methods of the new scene.
     *
     * \tparam S The type of the scene to get. Must be a subclass of S.
     */
    template<typename S>
    std::enable_if_t<std::is_base_of_v<Scene<SharedData>, S>, void>
    changeScene();
    /**
     * \brief Switch to another scene, saving the name of the current one.
     *
     * \tparam S The type of the scene to get. Must be a subclass of S.
     *
     * See `changeScene()` for caveats.
     */
    template<typename S>
    std::enable_if_t<std::is_base_of_v<Scene<SharedData>, S>, void> pushScene();
    /**
     * \brief Switch to the previous scene before the last `pushScene()` call.
     *
     * See `changeScene()` for caveats.
     * Aborts if the stack of scene names is empty.
     */
    void popScene();
    /**
     * \brief Get the scene of a certain type.
     *
     * \tparam S The type of the scene to get. Must be a subclass of S.
     * \return The pointer to the scene, or nullptr if none.
     */
    template<typename S>
    std::enable_if_t<std::is_base_of_v<Scene<SharedData>, S>, S*> getScene();
    SharedData& getSharedData() { return sharedData; }
    const SharedData& getSharedData() const { return sharedData; }

  private:
    struct Entry {
      std::function<std::unique_ptr<Scene<SharedData>>()> thunk;
      size_t references;
      SceneOptions options;
    };
    void changeScene(std::type_index k);
    MessageReturn sendToScene(const std::string& receiver, Message&& message);
    // std::unordered_map<std::type_index, std::unique_ptr<Scene<SharedData>>>
    //    scenes;
    phmap::flat_hash_map<std::type_index, std::unique_ptr<Scene<SharedData>>>
        sceneStore;
    phmap::flat_hash_map<std::type_index, Entry> sceneThunks;
    std::optional<std::type_index> currentSceneName;
    std::optional<std::type_index> nextSceneName;
    std::stack<std::type_index> scstack;
    SharedData sharedData;
    friend class Scene<SharedData>;
  };

  template<typename SharedData>
  template<typename S, typename F>
  std::enable_if_t<
      std::is_base_of_v<Scene<SharedData>, S> &&
          std::is_invocable_r_v<std::unique_ptr<S>, F>,
      void>
  ScenedGLFWApplication<SharedData>::insertScene(F&& getSc, SceneOptions opts) {
    sceneThunks[typeid(S)] = Entry{
        /* .thunk = */ std::forward<F>(getSc),
        /* .references = */ 0,
        /* .options = */ opts,
    };
  }

  template<typename SharedData>
  void ScenedGLFWApplication<SharedData>::changeScene(std::type_index k) {
    auto scene = sceneThunks.find(k);
    if (scene == sceneThunks.end()) {
      fprintf(stderr, "Scene '%s' does not exist!\n", k.name());
      abort();
    }
    nextSceneName = k;
  }

  template<typename SharedData>
  template<typename S>
  std::enable_if_t<std::is_base_of_v<Scene<SharedData>, S>, void>
  ScenedGLFWApplication<SharedData>::changeScene() {
    changeScene(typeid(S));
  }

  template<typename SharedData>
  template<typename S>
  std::enable_if_t<std::is_base_of_v<Scene<SharedData>, S>, void>
  ScenedGLFWApplication<SharedData>::pushScene() {
    assert(currentSceneName.has_value());
    scstack.push(*currentSceneName);
    auto it = sceneThunks.find(*currentSceneName);
    assert(it != sceneThunks.end());
    ++it->second.references;
    changeScene<S>();
  }

  template<typename SharedData>
  void ScenedGLFWApplication<SharedData>::popScene() {
    assert(!scstack.empty());
    changeScene(std::move(scstack.top()));
    auto it = sceneThunks.find(*currentSceneName);
    assert(it != sceneThunks.end());
    --it->second.references;
    scstack.pop();
  }

  template<typename SharedData>
  template<typename S>
  std::enable_if_t<std::is_base_of_v<Scene<SharedData>, S>, S*>
  ScenedGLFWApplication<SharedData>::getScene() {
    auto it = sceneStore.find(typeid(S));
    if (it == sceneStore.end()) return nullptr;
    return (S*) it->second.get();
  }

  template<typename SharedData>
  void ScenedGLFWApplication<SharedData>::initialise() {
    GLFWApplication::initialise();
  }

  template<typename SharedData> void ScenedGLFWApplication<SharedData>::tick() {
    GLFWApplication::tick();
    if (nextSceneName.has_value()) {
      if (currentSceneName.has_value()) {
        auto it = sceneStore.find(*currentSceneName);
        if (it != sceneStore.end()) it->second->onLeave();
        auto it2 = sceneThunks.find(*currentSceneName);
        assert(it2 != sceneThunks.end());
        const Entry& e = it2->second;
        if (e.references == 0 && !e.options.isPersistent) {
          sceneStore.erase(it);
        }
      }
      currentSceneName = std::move(*nextSceneName);
      nextSceneName = std::nullopt;
      auto it = sceneStore.find(*currentSceneName);
      if (it != sceneStore.end()) {
        Scene<SharedData>* sc = it->second.get();
        sc->onEnter();
      } else {
        auto it = sceneThunks.find(*currentSceneName);
        assert(it != sceneThunks.end());
        const Entry& e = it->second;
        auto sc = e.thunk();
        sc->setApp(this);
        sc->initialise();
        sc->setDimensions(
            getWidth(), getHeight(), getActualWidth(), getActualHeight());
        sc->onEnter();
        sceneStore.try_emplace(*currentSceneName, std::move(sc));
      }
    }
    auto it = sceneStore.find(*currentSceneName);
    assert(it != sceneStore.end());
    Scene<SharedData>* sc = it->second.get();
    sc->update();
    sc->render();
  }

  template<typename SharedData>
  void ScenedGLFWApplication<SharedData>::readKeys() {
    GLFWApplication::readKeys();
  }

  template<typename SharedData>
  void ScenedGLFWApplication<SharedData>::onMouse(double xpos, double ypos) {
    GLFWApplication::onMouse(xpos, ypos);
  }

  template<typename SharedData>
  MessageReturn ScenedGLFWApplication<SharedData>::sendToScene(
      const std::string& receiver, Message&& message) {
    (void) receiver;
    assert(currentSceneName.has_value());
    auto sc = sceneStore.find(*currentSceneName);
    assert(sc != sceneStore.end());
    return sc->second->onMessage(std::move(message));
  }
}