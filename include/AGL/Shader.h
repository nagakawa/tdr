#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <iosfwd>
#include <string_view>

#define GLEW_STATIC
#include <GL/glew.h>

namespace agl {
  /**
   * \brief A handle to a shader object.
   */
  class Shader {
  public:
    /// Create an empty shader object.
    Shader() : id(0), shaderType(0) {}
    /**
     * \brief Create a shader from a source.
     *
     * Will throw if shader fails to compile.
     * \param source The null-terminated source of shader.
     * \param type The type of shader to create.
     */
    Shader(const char* source, GLenum type);
    /**
     * \brief Create a shader from a source.
     *
     * Will throw if shader fails to compile.
     * \param source The source of shader, not necessarily null-terminated.
     * \param len The length of the source.
     * \param type The type of shader to create.
     */
    Shader(const char* source, size_t len, GLenum type);
    /**
     * \brief Create a shader from a SPIR-V binary.
     *
     * Will throw if shader fails to compile.
     * \param binary The SPIR-V binary of shader, not necessarily
     *   null-terminated.
     * \param len The length of the binary.
     * \param type The type of shader to create.
     */
    static Shader fromSpirV(const char* binary, size_t len, GLenum type);
    /**
     * \brief Create a shader from a source.
     *
     * Will throw if shader fails to compile.
     * \param source The null-terminated source of shader.
     * \param type The type of shader to create.
     */
    Shader(std::string_view source, GLenum type);
    /**
     * \brief Create a shader from a source file.
     *
     * Will throw if shader fails to compile.
     * \param f The file handle to the source.
     * \param type The type of shader to create.
     */
    Shader(FILE* f, GLenum type);
    /**
     * \brief Create a shader from a source file.
     *
     * Will throw if shader fails to compile.
     * \param f The stream to the source.
     * \param type The type of shader to create.
     */
    Shader(std::istream& f, GLenum type);
    ~Shader();
    Shader(const Shader& s) = delete;
    Shader& operator=(const Shader& s) = delete;
    Shader(Shader&& s) noexcept : id(s.id), shaderType(s.shaderType) {
      s.id = 0;
      s.shaderType = 0;
    }
    Shader& operator=(Shader&& s) noexcept {
      id = s.id;
      s.id = 0;
      shaderType = s.shaderType;
      s.shaderType = 0;
      return *this;
    }
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }

  private:
    /// The ID used by OpenGL for this object.
    GLuint id;
    /// The type of shader: usually GL_VERTEX_SHADER or GL_FRAGMENT_SHADER.
    GLenum shaderType;

  private:
    void init(const char* source, size_t len, GLenum type);
    void initBinary(
        const char* binary, size_t len, GLenum type,
        GLenum binType = GL_SHADER_BINARY_FORMAT_SPIR_V);
  };

  /**
   * \brief Create a shader from file.
   * \param fname The path to the source file.
   * \param type The type of shader to create.
   * \return A pointer to the shader; this should later be deleted with
   * 	 `delete`.
   * \deprecated Raw pointer use.
   */
  [[deprecated("Raw pointer use")]] Shader*
  openShaderFromFile(const char* fname, GLenum type);
  /**
   * \brief Create a shader from file.
   * \param fname The path to the source file.
   * \param type The type of shader to create.
   * \return The shader.
   */
  Shader openShaderFromFile2(const char* fname, GLenum type);
}