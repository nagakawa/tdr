#pragma once

#include <stdint.h>

#if AGL_PROFILER
#  include <stdio.h>
#endif
#include <memory>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "AGL/profiling.h"

namespace agl {
  class Mixer;
  constexpr int defaultWidth = 800;
  constexpr int defaultHeight = 600;
  constexpr const char* defaultTitle = "GLFWApplication";
  constexpr double fpsUpdatePeriod = 0.5;

  /**
   * \brief Key status values returned by @ref GLFWApplication::testKey2.
   */
  enum class KeyStatus {
    away = 0, ///< Key was not and is still not down.
    enter = 1, ///< Key was pressed this frame.
    leave = 2, ///< Key was released this frame.
    hold = 3, ///< Key was and is still down.
  };

  /**
   * \brief Options passed to \ref GLFWApplication.
   */
  struct ApplicationOptions {
    ProfilerOptions* popts = &defaultProfilerOptions;
    int width = defaultWidth; ///< The virtual width of the window.
    int height = defaultHeight; ///< The virtual height of the window.
    /// The actual width of the window, or 0 to be equal to `width`.
    int actualWidth = 0;
    /// The actual height of the window, or 0 to be equal to `height`.
    int actualHeight = 0;
    /// The title of the window.
    const char* title = defaultTitle;
    /// The major version of OpenGL to use.
    int glMajor = 3;
    /// The minor version of OpenGL to use.
    int glMinor = 3;
    /// If true, a debug context will be created.
    bool debug = false;
    /// Framerate will be capped to this value.
    int maxFPS = 60;
    /// The audio device to use for PortAudio. -1 to pick the default device.
    int audioDevice = -1;
  };

  /**
   * \brief An application.
   *
   * Users should subclass from this class.
   */
  class GLFWApplication {
  public:
    /**
     * \brief Create an application.
     *
     * \param width The virtual width of the window.
     * \param height The virtual height of the window.
     * \param actualWidth The actual width of the window,
     *   or 0 to be equal to `width`.
     * \param actualHeight The actual height of the window,
     *   or 0 to be equal to `height`.
     * \param title The title of the window.
     * \param glMajor The major version of OpenGL to use.
     * \param glMinor The minor version of OpenGL to use.
     * \param debug If true, a debug context will be created.
     * \param maxFPS Framerate will be capped to this value.
     */
    GLFWApplication(const ApplicationOptions& opts);
    GLFWApplication(const GLFWApplication&) = delete;
    GLFWApplication& operator=(const GLFWApplication&) = delete;
    /// Start the application.
    void start();
    virtual ~GLFWApplication();
    /// Perform any user initialisation.
    virtual void initialise();
    /// Perform any actions to be done every frame.
    virtual void tick();
    /// Perform any actions that respond to key input.
    virtual void readKeys();
    /**
     * \brief User handler for mouse events.
     */
    virtual void onMouse(double xpos, double ypos);
    /**
     * \brief Set a key as pressed.
     *
     * This method is not meant to be used by subclasses, but it is public
     * so @ref keyCallback can use them.
     */
    void setKey(int code);
    /**
     * \brief Set a key as not pressed.
     *
     * This method is not meant to be used by subclasses, but it is public
     * so @ref keyCallback can use them.
     */
    void resetKey(int code);
    /**
     * \brief Get the current framerate.
     *
     * This updates every frame. If you want to get a user-facing value, then
     * use @ref getRollingFPS() instead.
     */
    double getFPS() { return fps; }
    /// Get the time from the last frame to the current one, in seconds.
    double getDelta() { return delta; }
    /**
     * \brief Get the current framerate.
     *
     * This updates more slowly than @ref getFPS() and is more suitable for
     * displaying to users.
     */
    double getRollingFPS() { return rollingFPS; }
    int getWidth() const { return w; }
    int getHeight() const { return h; }
    int getActualWidth() const { return aw; }
    int getActualHeight() const { return ah; }
    int getFBOID() const { return 0; }
    /// Get the pointer to the GLFWwindow object held by the application.
    GLFWwindow* underlying() { return window; }
    void stashCurrentKeys();
    /// Get the mixer for this application.
    Mixer& getMixer();
    /**
     * \brief Writes a profiling track value.
     *
     * No effect if `AGL_PROFILER` is undefined or false.
     */
    void writeProfile(
        [[maybe_unused]] size_t trackno, [[maybe_unused]] double value) {
#if AGL_PROFILER
      trackValues[trackno] = value;
#endif
    }

  protected:
    bool testKey(int code);
    KeyStatus testKey2(int code);
    /// Enable or disable vertical synchronisation.
    void setVSyncEnable(bool enable);

  private:
    uint64_t keys[16];
    uint64_t keysPrev[16];
    GLFWwindow* window;
    GLdouble fps;
    GLdouble rollingFPS;
    GLdouble delta;
    GLdouble currentTime;
    GLdouble cumulDelta;
    GLint currInRF;
    GLint w;
    GLint h;
    GLint aw;
    GLint ah;
    GLint mfps;
    std::unique_ptr<Mixer> mixer;
#if AGL_PROFILER
    const ProfilerOptions* popts;
    std::vector<double> trackValues;
    FILE* outfh;
#endif
  };

  extern GLFWApplication* currentApp;

  void
  keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
  void mouseCallback(GLFWwindow* window, double xpos, double ypos);
}
