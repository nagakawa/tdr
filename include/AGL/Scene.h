#pragma once

#include <stdint.h>

#include "AGL/GLFWApplication.h"
#include "AGL/Message.h"

namespace agl {
  template<typename SharedData> class ScenedGLFWApplication;
  /**
   * \brief A scene that is managed by @ref ScenedGLFWApplication.
   *
   * This class is meant to be subclassed by user code.
   *
   * @tparam SharedData The type of shared data to expect from
   * @ref ScenedGLFWApplication.
   */
  template<typename SharedData> class Scene {
  public:
    /**
     * \brief Set the dimensions for this scene.
     *
     * This is meant to be called by @ref ScenedGLFWApplication.
     */
    void
    setDimensions(int width, int height, int actualWidth, int actualHeight);
    /**
     * \brief Set the parent application for this scene.
     *
     * This is meant to be called by @ref ScenedGLFWApplication.
     */
    void setApp(ScenedGLFWApplication<SharedData>* a) { app = a; }
    /// Perform any user initialisation.
    virtual void initialise() = 0;
    /// Perform any actions to be done every frame.
    virtual void update() = 0;
    /// Render to the screen.
    virtual void render() = 0;
    /// Called when this scene is entered.
    virtual void onEnter() {}
    /// Called when this scene is left.
    virtual void onLeave() {}
    /// Respond to a message sent from another scene.
    virtual MessageReturn onMessage(Message&& /*message*/) { return 0; }
    virtual ~Scene() = 0;
    /// Send a message to another scene.
    MessageReturn sendMessage(const std::string& receiver, Message&& message);
    SharedData& getSharedData() { return app->getSharedData(); }
    const SharedData& getSharedData() const { return app->getSharedData(); }

  protected:
    int w, h, aw, ah;
    /// Return true if the key is pressed.
    bool testKey(int code);
    /// Return a @ref KeyStatus describing the state of the key.
    KeyStatus testKey2(int code);
    /// Enable or disable vertical synchronisation.
    void setVSyncEnable(bool enable);
    /// The pointer to the parent application.
    ScenedGLFWApplication<SharedData>* app;
  };

  template<typename SharedData>
  void Scene<SharedData>::setDimensions(
      int width, int height, int actualWidth, int actualHeight) {
    w = width;
    h = height;
    aw = actualWidth;
    ah = actualHeight;
  }

  template<typename SharedData> bool Scene<SharedData>::testKey(int code) {
    return app->testKey(code);
  }

  template<typename SharedData>
  KeyStatus Scene<SharedData>::testKey2(int code) {
    return app->testKey2(code);
  }

  template<typename SharedData>
  void Scene<SharedData>::setVSyncEnable(bool enable) {
    app->setVSyncEnable(enable);
  }

  template<typename SharedData>
  MessageReturn Scene<SharedData>::sendMessage(
      const std::string& receiver, Message&& message) {
    return app->sendToScene(receiver, std::move(message));
  }

  template<typename SharedData> Scene<SharedData>::~Scene() {}
}