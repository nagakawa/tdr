#pragma once

#include <type_traits>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <parallel_hashmap/phmap.h>

#include "AGL/Shader.h"

// Glory to abusing
// Preprocessor directives!

/**
 * \brief Set a uniform of this shader program.
 * \param shader The shader program.
 * \param type The type of the uniform; e.g.\ `1f` for a float.
 * \param uname The name of the uniform.
 * \param value The value to set the uniform to.
 */
#define SETUNSP(shader, type, uname, value) \
  glProgramUniform##type( \
      (shader).getID(), glGetUniformLocation((shader).getID(), uname), value)

/**
 * \brief Set a uniform of this shader program as a vector of two elements.
 * \param shader The shader program.
 * \param type The type of the uniform; e.g.\ `2f` for a vector of 2 floats.
 * \param uname The name of the uniform.
 * \param value1 `value.x`, where `value` is the value to set the uniform to.
 * \param value2 `value.y`, where `value` is the value to set the uniform to.
 */
#define SETUNSP2(shader, type, uname, value1, value2) \
  glProgramUniform##type( \
      (shader).getID(), glGetUniformLocation((shader).getID(), uname), value1, \
      value2)

/**
 * \brief Set a uniform of this shader program from a pointer to a vector.
 * \param shader The shader program.
 * \param type The type of the uniform; e.g.\ `4fv` for a vector of 4 floats.
 * \param uname The name of the uniform.
 * \param value The pointer to the value to set the uniform to.
 */
#define SETUNSPV(shader, type, uname, value) \
  glProgramUniform##type( \
      (shader).getID(), glGetUniformLocation((shader).getID(), uname), 1, \
      value)

/**
 * \brief Set a uniform of this shader program from a pointer to an
 * 	 array of vectors.
 * \param shader The shader program.
 * \param type The type of the uniform; e.g.\ `4fv` for a vector of 4 floats.
 * \param uname The name of the uniform.
 * \param count The number of elements to pass.
 * \param value The pointer to the value to set the uniform to.
 */
#define SETUNSPVN(shader, type, uname, count, value) \
  glProgramUniform##type( \
      (shader).getID(), glGetUniformLocation((shader).getID(), uname), count, \
      value)

/**
 * \brief Set a uniform of this shader program from a pointer to a matrix.
 * \param shader The shader program.
 * \param type The type of the uniform; e.g.\ `4fv` for a 4x4 matrix of floats.
 * \param uname The name of the uniform.
 * \param value The pointer to the value to set the uniform to.
 */
#define SETUNSPM(shader, type, uname, value) \
  glProgramUniformMatrix##type( \
      (shader).getID(), glGetUniformLocation((shader).getID(), uname), 1, \
      GL_FALSE, value)

/**
 * \brief Set a uniform of this shader program from a pointer to an array of
 *   matrices.
 * \param shader The shader program.
 * \param type The type of the uniform; e.g.\ `4fv` for a 4x4 matrix of floats.
 * \param uname The name of the uniform.
 * \param count The number of elements to pass.
 * \param value The pointer to the value to set the uniform to.
 */
#define SETUNSPMCT(shader, type, uname, value, count, transpose) \
  glProgramUniformMatrix##type( \
      (shader).getID(), glGetUniformLocation((shader).getID(), uname), count, \
      transpose, value)

namespace agl {
  /**
   * \brief A handle to a shader program.
   *
   * Note that in addition to the methods listed, there are the following
   * methods:
   *
   * * `uniform*f`, `uniform*i`, `uniform*ui` for `*` = 1 to 4
   * * `uniformMatrix*f` for `*` = 2 to 4
   *
   * These work like their respective `glProgramUniform*` functions, except
   * that they take GLM types. They are overloaded to take either a shader
   * location as a `GLint` or a shader name.
   */
  class ShaderProgram {
  public:
    ShaderProgram();
    ~ShaderProgram();
    /// Attach a shader to the program.
    void attach(Shader& shader);
    /**
     * \brief Link the shader program.
     *
     * Throws if linking fails.
     */
    void link();
    /// Use the shader program.
    void use();
    /**
     * \brief Get the location to a shader uniform.
     *
     * \param name The name of the uniform.
     * \return An integer naming the location of the uniform.
     */
    GLuint getUniformLocation(const GLchar* name);
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }
    ShaderProgram(const ShaderProgram& s) = delete;
    ShaderProgram& operator=(const ShaderProgram& s) = delete;
    ShaderProgram(ShaderProgram&& s) noexcept : id(s.id) { s.id = 0; }
    ShaderProgram& operator=(ShaderProgram&& s) noexcept {
      id = s.id;
      s.id = 0;
      return *this;
    }

    // ======== METHODS TO SET UNIFORMS ========
#define COMPOSE_LOCATION_VARIANTS \
  X(const char*, getUniformLocation(l)) \
  X(GLint, l)

#define CONCAT(a, b) CONCAT2(a, b)
#define CONCAT2(a, b) a##b
#define CONCAT3(a, b, c) CONCAT(CONCAT(a, b), c)

#define X

#define spLetter f
#define Type float
#include "./shaderProgramUniformMethodImpl.h"

#define spLetter i
#define Type GLint
#include "./shaderProgramUniformMethodImpl.h"

#define spLetter ui
#define Type GLuint
#include "./shaderProgramUniformMethodImpl.h"


#undef X
#define X(Location, location) \
  void uniformMatrix2f(Location l, glm::mat2 value) { \
    glProgramUniformMatrix2fv(id, location, 1, false, glm::value_ptr(value)); \
  }
    COMPOSE_LOCATION_VARIANTS
#undef X
#define X(Location, location) \
  void uniformMatrix3f(Location l, glm::mat3 value) { \
    glProgramUniformMatrix3fv(id, location, 1, false, glm::value_ptr(value)); \
  }
    COMPOSE_LOCATION_VARIANTS
#undef X
#define X(Location, location) \
  void uniformMatrix4f(Location l, glm::mat4 value) { \
    glProgramUniformMatrix4fv(id, location, 1, false, glm::value_ptr(value)); \
  }
    COMPOSE_LOCATION_VARIANTS

#undef X
#undef COMPOSE_LOCATION_VARIANTS
#undef CONCAT
#undef CONCAT2
#undef CONCAT3

  private:
    /// The ID used by OpenGL for this object.
    GLuint id;
    phmap::flat_hash_map<std::string, GLint> locationCache;
    // Check that hetrogeneous lookups on locationCache works on const char*
    static_assert(std::is_same_v<
                  decltype(locationCache)::key_arg<const char*>, const char*>);
  };
}
