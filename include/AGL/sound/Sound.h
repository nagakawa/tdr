#pragma once

#include <stddef.h>
#include <stdint.h>

#include <iostream>
#include <vector>

#include <vorbis/vorbisfile.h>

namespace agl {
  /// The type used for samples of a sound.
  using SType = int16_t;
  /// Holds a sound resource.
  class Sound {
  public:
    Sound() : sampleCount(0), samplesPerSecond(0), channels(1) {}
    /**
     * \brief Create a new sound with the given sample count and rate, but
     * with unspecified samples.
     */
    Sound(size_t sampleCount, size_t samplesPerSecond, unsigned channels = 1) :
        samples(sampleCount * channels), sampleCount(sampleCount),
        samplesPerSecond(samplesPerSecond), channels(channels) {}
    Sound(Sound&& sound) = default;
    Sound& operator=(Sound&& sound) = default;
    /**
     * \brief Create a sound from Ogg Vorbis data in memory.
     * \param buffer The pointer to the data.
     * \param length The length of the data, in bytes.
     */
    Sound(void* buffer, size_t length, unsigned channels = 0);
    /**
     * \brief Create a sound from an Ogg Vorbis file.
     * \param fname The path to the sound file.
     */
    Sound(const char* fname, unsigned channels = 0);
    void initialiseFromOVFile(OggVorbis_File& vf, unsigned channels);
    /**
     * \brief Resample the sound at the specified rate.
     *
     * The \ref Mixer class calls this method on any sounds added to it to
     * change the sample rate to its native rate. This saves on having to
     * resample sounds on the fly.
     *
     * This method uses linear interpolation, but it can easily be switched
     * to use a higher-quality algorithm without any loss of performance
     * when playing back the sounds.
     *
     * \param newSampleRate The new sample rate.
     */
    void resample(size_t newSampleRate);
    ~Sound() = default;
    std::vector<SType> samples;
    size_t sampleCount;
    /// The sample rate, in samples per second.
    size_t samplesPerSecond;
    /// How many channels to use: either 1 or 2
    unsigned channels;
  };
}
