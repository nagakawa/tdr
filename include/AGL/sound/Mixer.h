#pragma once

#include <stddef.h>

#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

#include <parallel_hashmap/phmap.h>
#include <portaudio.h>

#include "AGL/sound/Sound.h"

namespace agl {
  class MixerException : public std::exception {
  public:
    explicit MixerException(PaError e) : e(e) {}
    const char* what() const noexcept override { return Pa_GetErrorText(e); }

  private:
    PaError e;
  };
  /// Describes options for playing a sound.
  struct PlayOptions {
    float volume = 1.0f; ///< The volume of the sound.
    /// The panning of the sound:
    /// `0.0f` is centred, negative values are to the left and positive values
    /// are to the right. A value of `-1.0f` pans fully to the left, and that of
    /// `+1.0f` pans fully to the right.
    float pan = 0.0f;
    bool loop = false; ///< The sound will loop if true.
    size_t loopStart = 0; ///< The start of the loop (in mixer samples).
    size_t loopEnd = 0; ///< The end of the loop (in mixer samples).
  };
  constexpr PlayOptions defaultPlayOptions = {
      1.0f, 0.0f, false, 0, 0,
  };
  /// Describes a sound currently being played in the mixer.
  struct MixerEntry {
    size_t key; ///< The key for the sound, loaded in Mixer.
    size_t curr; ///< The integer part of the cursor (in samples).
    PlayOptions options; ///< The options passed to \ref Mixer::playSound.
  };
  class Mixer;
  class MixerContext {
  public:
    int playSound(size_t index, const PlayOptions& options);
    void advance(unsigned long n, float* output, const Mixer& m);
    void restartSound(int id);
    void clear() { entries.clear(); }

  private:
    /// A map from handles to mixer entries.
    phmap::flat_hash_map<int, MixerEntry> entries;
    int handleCount = 0;
  };
  class Mixer {
  public:
    Mixer(int audioDevice = -1);
    ~Mixer();
    Mixer(const Mixer& m) = delete;
    Mixer& operator=(const Mixer& m) = delete;
    Mixer(Mixer&& m) = delete;
    Mixer& operator=(Mixer&& m) = delete;
    /**
     * \brief Add a sound to the mixer so it can later be played.
     * \param s The sound to add.
     * \return The key for the sound that was added.
     */
    size_t addSound(Sound&& s);
    /**
     * \brief Add a sound to the mixer so it can later be played.
     * \param fname The path to the sound file.
     * \return The key for the sound that was added.
     */
    size_t addSoundFromFile(const char* fname);
    /**
     * \brief Add a sound to the mixer so it can later be played.
     * \param fname The path to the sound file.
     * \return The key for the sound that was added.
     */
    size_t addSoundFromFile(const std::string& fname) {
      return addSoundFromFile(fname.c_str());
    }
    /**
     * \brief Add a sound to the mixer so it can later be played.
     *
     * If a sound with the same name is already present, then it is replaced.
     * \param index The named key to use to refer to this sound.
     * \param s The sound to add.
     * \return The key for the sound that was added.
     */
    size_t addSound(std::string&& index, Sound&& s);
    /**
     * \brief Add a sound to the mixer so it can later be played.
     *
     * If a sound with the same name is already present, then it is replaced.
     * \param index The named key to use to refer to this sound.
     * \param fname The path to the sound file.
     * \return The key for the sound that was added.
     */
    size_t addSoundFromFile(std::string&& index, const char* fname);
    /**
     * \brief Add a sound to the mixer so it can later be played.
     *
     * If a sound with the same name is already present, then it is replaced.
     * \param index The named key to use to refer to this sound.
     * \param fname The path to the sound file.
     * \return The key for the sound that was added.
     */
    size_t addSoundFromFile(std::string&& index, const std::string& fname) {
      return addSoundFromFile(std::move(index), fname.c_str());
    }
    /**
     * \brief Play the sound referred to by a key.
     *
     * \param index The key of the sound to play.
     * \param volume The volume at which to play the sound.
     * \return A handle to the @ref MixerEntry object for the sound.
     */
    int
    playSound(size_t index, const PlayOptions& options = defaultPlayOptions);
    /**
     * \brief Play the sound referred to by a name.
     *
     * \param index The name of the sound to play.
     * \param volume The volume at which to play the sound.
     * \return A handle to the @ref MixerEntry object for the sound.
     */
    int playSound(
        const std::string& index,
        const PlayOptions& options = defaultPlayOptions);
    /**
     * \brief Start a sound from the beginning.
     *
     * \param id A handle to a sound that was previously returned from \ref
     * playSound.
     */
    void restartSound(int id);
    void advance(unsigned long n, float* output);
    void clear();
    void stop();
    /**
     * \brief Get the current mixer context.
     *
     * A mixer can have one or more contexts, which store data about which
     * sounds are playing therein. Only the current context is advanced by
     * the mixer.
     */
    MixerContext& getCurrentContext() { return *currentContext; }
    /**
     * \brief Add a new context.
     *
     * \param name The name of the context to add.
     * \return True if successfully added; false if a context with this name
     * already exists.
     */
    bool addContext(std::string&& name);
    /**
     * \brief Switch to another context.
     *
     * \param name The name of the context to switch to.
     * \return True if successfully switched; false if no context with this name
     * exists.
     */
    bool switchContext(std::string&& name);
    /**
     * \brief Remove an existing context.
     *
     * \param name The name of the context to add.
     * \return True if successfully removed; false if a context with this name
     * does not exist, or if the name is an empty string.
     */
    bool removeContext(const std::string& name);

    float getGlobalVolume() const noexcept { return globalVolume; }
    void setGlobalVolume(float vol) noexcept { globalVolume = vol; }
    size_t getOutputRate() const noexcept { return outputRate; }

  private:
    /// A map from handles to mixer entries.
    phmap::flat_hash_map<std::string, size_t> soundIDsByName;
    std::vector<Sound> sounds;
    PaStream* stream;
    std::unordered_map<std::string, MixerContext> contexts;
    std::string contextName;
    MixerContext* currentContext;
    /// The global volume; the volumes of sounds played will be multiplied
    /// by this value.
    float globalVolume = 1.0f;
    size_t outputRate;
    friend class MixerContext;
  };
  int mixerCallback(
      const void* input, void* output, unsigned long frameCount,
      const PaStreamCallbackTimeInfo* timeInfo,
      PaStreamCallbackFlags statusFlags, void* userData);
}