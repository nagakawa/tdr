#pragma once

#include <stdint.h>

#include <string>
#include <variant>

namespace agl {
  /**
   * \brief A message type used to identify the functions of messages.
   */
  struct MessageType {
    // A struct is used here instead of an `enum class` since users might
    // want to define their own instances of the type.
    MessageType() = default;
    constexpr explicit MessageType(int id) : id(id) {}
    int id;
    operator int() const { return id; }
  };
  inline bool operator==(MessageType a, MessageType b) { return a.id == b.id; }
  inline bool operator!=(MessageType a, MessageType b) { return a.id != b.id; }
  /// Symbolic constants for common messages.
  namespace msgtype {
    constexpr MessageType reboot{4444};
  }
  using MessageReturn = int;
  /**
   * \brief A message to be sent to another scene.
   */
  struct Message {
    MessageType type;
    std::variant<int64_t, std::string> data;
    template<typename T> const T& getRef() const { return std::get<T>(data); }
    template<typename T> T& getRef() { return std::get<T>(data); }
    template<typename T> T&& getMove() { return std::get<T>(data); }
    /// Return true if the message contents are of the given type.
    template<typename T> bool is() { return std::holds_alternative<T>(data); }
  };
}
