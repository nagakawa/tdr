#pragma once

#include <any>
#include <optional>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

namespace agl {
  /// A template type function that returns the argument.
  /// This is identitcal to std::type_identity_t from C++20.
  template<typename A> using TypeIdentity = A;
  using ErasedPtr = std::unique_ptr<void, void (*)(void*)>;
  /**
   * \brief A class that maps types to instances to those types.
   *
   * \tparam F A template type function that should be applied to the map
   *   values.
   */
  template<template<typename A> typename F = TypeIdentity> class AnyMap {
  public:
    void* get(std::type_index k) {
      auto it = underlying.find(k);
      if (it == underlying.end()) return nullptr;
      return it->second.get();
    }
    const void* get(std::type_index k) const {
      auto it = underlying.find(k);
      if (it == underlying.end()) return nullptr;
      return it->second.get();
    }
    template<typename T> std::optional<std::reference_wrapper<F<T>>> get() {
      auto it = underlying.find(typeid(T));
      if (it == underlying.end()) return std::nullopt;
      return *reinterpret_cast<F<T>*>(it->second.get());
    }
    template<typename T>
    std::optional<std::reference_wrapper<const F<T>>> get() const {
      auto it = underlying.find(typeid(T));
      if (it == underlying.end()) return std::nullopt;
      return *reinterpret_cast<const F<T>*>(it->second.get());
    }
    template<typename T, typename... A>
    F<T>& insert(std::unique_ptr<F<T>>&& ptr) {
      void* raw = ptr.get();
      ErasedPtr erased(ptr.release(), [](void* data) { delete (F<T>*) data; });
      underlying.insert(
          std::make_pair(std::type_index(typeid(T)), std::move(erased)));
      return *reinterpret_cast<F<T>*>(raw);
    }
    template<typename T, typename... A> F<T>& emplace(A&&... args) {
      auto ptr = std::make_unique<F<T>>(std::forward<A>(args)...);
      return insert(std::move(ptr));
    }
    template<typename T, typename... A> void erase() {
      underlying.erase(typeid(T));
    }

  private:
    std::unordered_map<std::type_index, ErasedPtr> underlying;
  };
}
