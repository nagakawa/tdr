#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

namespace agl {
  /**
   * \brief Symbolic constants for blend mode indices.
   * Symbolic constants for blend mode indices. These are used by some classes
   * in AGL and TDR. Values from this enum are translated into @ref BlendMode
   * objects using the @ref blendModes array.
   */
  enum class BMIndex : uint8_t {
    alpha = 0,
    add,
    subtract,
    multiply,
    screen,
    shadow,
    // Add new blend mode indices before the following line
    /// The number of meaningful @ref BMIndex values. This value itself should
    /// not be used when a BMIndex is expected.
    count,
  };
  constexpr size_t nPresetBlends = (size_t) BMIndex::count;
  /**
   * \brief A structure that describes a blend mode.
   * See [the OpenGL wiki page](https://www.khronos.org/opengl/wiki/Blending)
   * for what these fields mean.
   */
  struct BlendMode {
    /// Equation used for RGB, passed to glBlendEquationSeparate.
    GLenum eqRGB;
    /// Equation used for alpha, passed to glBlendEquationSeparate.
    GLenum eqAlpha;
    /// Source function used for RGB, passed to glBlendFunctionSeparate.
    GLenum srcFuncRGB;
    /// Destination function used for RGB, passed to glBlendFunctionSeparate.
    GLenum destFuncRGB;
    /// Source function used for alpha, passed to glBlendFunctionSeparate.
    GLenum srcFuncAlpha;
    /// Destination function used for alpha, passed to glBlendFunctionSeparate.
    GLenum destFuncAlpha;
    /// Tell OpenGL to switch to this blend mode.
    void use() const;
    /// Alpha blend mode.
    static const BlendMode alpha;
    /// Add blend mode.
    static const BlendMode add;
    /// Subtract blend mode.
    static const BlendMode subtract;
    /// Multiply blend mode.
    static const BlendMode multiply;
    /// Screen blend mode.
    static const BlendMode screen;
    /// Shadow blend mode.
    static const BlendMode shadow;
    static inline BlendMode fromIndex(BMIndex idx);
  };
  /// Array that translates from BMIndex values to @ref BlendMode objects.
  extern const BlendMode blendModes[];
  BlendMode BlendMode::fromIndex(BMIndex idx) { return blendModes[(int) idx]; }
}
