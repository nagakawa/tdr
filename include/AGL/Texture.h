#pragma once

#include <stdint.h>
#define GLEW_STATIC
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "FileUtil/attributes.h"

namespace agl {
  /**
   * \brief Options for texture initialisation.
   * This is used by some constructors of @ref Texture, as well as the method
   * @ref Texture::changeTexture.
   */
  struct TexInitInfo {
    /// Internal format.
    GLenum internalFormat;
    /// Texture format.
    GLenum texFormat;
    /// Pixel type.
    GLenum pixelType;
    /// True if you want to check for null data.
    bool checkForNullData;
    /// True if you want to generate mipmaps for this texture.
    bool genMipMap;
    /// True if you want to generate a multisample texture.
    bool multisample;
  };
  extern const TexInitInfo DEFAULT_TEX_INIT;
  extern const TexInitInfo TEX_INIT_FOR_FBO;
  extern const TexInitInfo TEX_INIT_FOR_FBO_MS;
  /**
   * \brief A base class for a handle to an OpenGL texture.
   */
  class TTexture {
  public:
    TTexture(const TTexture& t) = default;
    TTexture& operator=(const TTexture& t) = default;
    /// Bind this texture to the OpenGL context.
    void bind() const;
    /**
     * \brief Bind this texture to the OpenGL context, at a specific
     *   texture unit.
     * \param slot The offset from `GL_TEXTURE0` of the texture unit.
     *   For instance, if `slot` is 2, then the texture will be bound to
     *   the texture unit `GL_TEXTURE2`.
     */
    void bindTo(GLint slot) const;
    /// Return the width of the texture.
    GLint getWidth() const noexcept { return width; }
    /// Return the height of the texture.
    GLint getHeight() const noexcept { return height; }
    glm::ivec2 getDimensions() const noexcept { return {width, height}; }
    glm::vec2 getDimensionsF() const noexcept {
      return {(float) width, (float) height};
    }
    /**
     * \brief Change the texture held by this object.
     * \param w The new width of the texture.
     * \param h The new height of the texture.
     * \param data The pointer to the new raw image data.
     * \param info Options for this texture.
     * \param genNew If true, will generate a new texture.
     *   This parameter is used by the constructors of Texture;
     *   user code should leave it to false.
     */
    void changeTexture(
        int w, int h, const unsigned char* data,
        const TexInitInfo& info = DEFAULT_TEX_INIT, bool genNew = false);
    /**
     * \brief Add a name for use in debugging.
     *
     * No effect when `NDEBUG` is defined.
     */
    void addName([[maybe_unused]] const char* s)
#ifdef NDEBUG
    {
    }
#else
        ;
#endif
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }

  protected:
    TTexture() noexcept : id(0) {}
    /// The ID used by OpenGL for this object.
    GLuint id;
    GLint width;
    GLint height;
    bool ms = false;
    void setTexture(
        int w, int h, const unsigned char* data,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
    friend class WeakTexture;
  };
  /**
   * \brief A non-owning (weak) handle for an OpenGL texture.
   */
  class WeakTexture : public TTexture {
  public:
    WeakTexture() : TTexture() {}
    WeakTexture(const TTexture& t) : TTexture(t) {}
    WeakTexture& operator=(const TTexture& t) {
      *(TTexture*) this = t;
      return *this;
    }
    ~WeakTexture() {}
  };
  /**
   * \brief An owning handle for an OpenGL texture.
   */
  class CTH_TRIVIAL_RELOCATE Texture : public TTexture {
  public:
    Texture() noexcept : TTexture() {}
    Texture(const TTexture& t) = delete;
    Texture(Texture&& t) noexcept : TTexture(t) { t.id = 0; }
    Texture& operator=(const Texture& t) = delete;
    Texture& operator=(Texture&& t) noexcept {
      id = t.id;
      width = t.width;
      height = t.height;
      ms = t.ms;
      t.id = 0;
      return *this;
    }
    /**
     * \brief Load a texture from file.
     * \param fname The path to the file.
     * \param info The options to pass to loading.
     */
    Texture(const char* fname, const TexInitInfo& info = DEFAULT_TEX_INIT);
    /**
     * \brief Load a texture from formatted image data (e.g.\ a PNG file).
     *
     * Note: The `bufferLength` parameter is an `int` instead of a `size_t`
     * because SOIL uses that type in `SOIL_load_image_from_memory`.
     * \param buffer The pointer to the image data.
     * \param bufferLength The size of the image data in bytes.
     * \param info The options to pass to loading.
     */
    Texture(
        const uint8_t* buffer, int bufferLength,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
    /**
     * \brief Load a texture from raw image data.
     * \param w The width of the image.
     * \param h The height of the image.
     * \param data The pointer to the image data.
     * \param info The options to pass to loading.
     */
    Texture(
        int w, int h, const unsigned char* data,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
    ~Texture();
    WeakTexture weak() { return WeakTexture(*this); }
  };
  /**
   * \brief A handle for a 3D OpenGL texture.
   * This class mostly follows the API of @ref Texture.
   */
  class Texture3 {
  public:
    Texture3(
        const char* fname, int divide,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
    // Using int instead of size_t for the buffer size here, because SOIL uses
    // that type in SOIL_load_image_from_memory. Uugghh.
    Texture3(
        const uint8_t* buffer, int divide, int bufferLength,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
    Texture3(
        int w, int h, int d, const unsigned char* data,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
    // Texture(const Texture& t);
    Texture3(Texture3&& t);
    Texture3();
    ~Texture3();
    void bind();
    void bindTo(GLint slot);
    GLint getWidth() const { return width; }
    GLint getHeight() const { return height; }
    GLint getDepth() const { return depth; }
    bool isMultisample() const { return ms; }
    void changeTexture(
        int w, int h, int d, const unsigned char* data,
        const TexInitInfo& info = DEFAULT_TEX_INIT, bool genNew = false);
    GLuint getID() const noexcept { return id; }

  private:
    GLuint id;

  private:
    GLint width;
    GLint height;
    GLint depth;
    bool ms = false;
    void setTexture(
        int w, int h, int d, const unsigned char* data,
        const TexInitInfo& info = DEFAULT_TEX_INIT);
  };
  extern Texture whiteTexture;
  void initWhiteTexture();
}
