#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

namespace agl {
  /// Unbind the current @ref EBO object.
  void resetEBO();
  /**
   * \brief A handle for an OpenGL element buffer object.
   */
  class EBO {
  public:
    /// Create an empty EBO.
    EBO();
    ~EBO();
    EBO(const EBO& ebo) = delete;
    EBO& operator=(const EBO& ebo) = delete;
    EBO(EBO&& ebo) noexcept : id(ebo.id) { ebo.id = 0; }
    EBO& operator=(EBO&& ebo) noexcept {
      id = ebo.id;
      ebo.id = 0;
      return *this;
    }
    /// Bind this EBO to the OpenGL context.
    void setActive();
    GLuint getID() const noexcept { return id; }
    /**
     * \brief Supply data to this EBO.
     * See [this page](https://www.khronos.org/opengl/wiki/GLAPI/glBufferData)
     * for more information.
     * \param size The number of bytes to feed.
     * \param data The pointer to this data.
     * \param usage One of GL_STATIC_DRAW, GL_DYNAMIC_DRAW or GL_STREAM_DRAW.
     */
    void feedData(GLint size, void* data, GLenum usage);
    /// The ID used by OpenGL for this object.
    GLuint id;
  };
}
