#pragma once

#define GLEW_STATIC
#include <stdlib.h>

#include <memory>

#include <GL/glew.h>

#include "AGL/Texture.h"

namespace agl {
  /**
   * \brief A handle for an OpenGL render buffer object.
   */
  class RBO {
  public:
    /// Create an empty RBO.
    RBO();
    RBO(RBO&& that) noexcept;
    ~RBO();
    RBO& operator=(RBO&& that) noexcept;
    /// Bind this RBO to the OpenGL context.
    void setActive();
    /**
     * \brief Allocate storage for this object.
     * See [this
     * page](https://www.khronos.org/opengl/wiki/GLAPI/glRenderbufferStorage)
     * for more information.
     * \param format The format to use for the image, equivalent to the
     *   `internalformat` parameter of `glRenderbufferStorage`.
     * \param width The width of the image.
     * \param height The height of the image.
     * \param multisample If true, use `glRenderbufferStorageMultisample`
     *   instead of `glRenderbufferStorage`.
     */
    void allocateStorage(
        GLenum format, GLint width, GLint height, bool multisample = false);
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }

  private:
    /// The ID used by OpenGL for this object.
    GLuint id = 0;
    friend class FBO;
  };
  /**
   * \brief A handle for an OpenGL frame buffer object.
   */
  class FBO {
  public:
    /**
     * \brief Create an FBO.
     * \param def If true, create an empty object (with an ID of 0).
     *   If false, generate an FBO.
     */
    FBO(bool def = true);
    // FBO(bool def, GLint i);
    FBO(FBO&& that) noexcept {
      id = that.id;
      that.id = 0;
      width = that.width;
      height = that.height;
    }
    ~FBO();
    FBO& operator=(FBO&& that) noexcept {
      std::swap(id, that.id);
      width = that.width;
      height = that.height;
      return *this;
    }
    /**
     * \brief Bind this FBO to the OpenGL context, and update the viewport to
     * its size.
     */
    void setActive();
    /**
     * \brief Bind this FBO to the OpenGL context, without updating the
     * viewport.
     */
    void setActiveNoViewport();
    /**
     * \brief Return true if this framebuffer is complete.
     */
    bool isComplete();
    /**
     * \brief Attach a texture to this FBO.
     * See [this
     * page](https://www.khronos.org/opengl/wiki/GLAPI/glFramebufferTexture) for
     * more information. \param attachment The attachment point of the
     * framebuffer. This would be the `attachment` parameter you'd pass to
     * `glFramebufferTexture`. \param texture The texture to attach. \param
     * texTarget The type of texture.
     */
    void attachTexture(
        GLenum attachment, TTexture& texture, GLenum texTarget = GL_TEXTURE_2D);
    /**
     * \brief Attach an RBO to this FBO.
     * See [this
     * page](https://www.khronos.org/opengl/wiki/GLAPI/glFramebufferRenderbuffer)
     * for more information.
     * \param attachment The attachment point of the framebuffer. This would
     *   be the `attachment` parameter you'd pass to `glRenderbufferTexture`.
     * \param rbo The RBO to attach.
     */
    void attachRBO(GLenum attachment, RBO&& rbo);
    /**
     * \brief Blit the contents of this FBO to another.
     * See [this
     * page](https://www.khronos.org/opengl/wiki/GLAPI/glBlitFramebuffer) for
     * more information. \param other The FBO to blit to. \param width The width
     * of the area to copy. \param width The height of the area to copy.
     */
    void blitTo(FBO& other, int width, int height);
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }

  private:
    /// The ID used by OpenGL for this object.
    GLuint id = 0;
    /// The width and height of the FBO.
    int width = 0, height = 0;
  };
  /**
   * A structure containing an FBO and the texture where output to it will
   * be stored.
   */
  struct FBOTex {
    FBO fbo;
    Texture texture;
    FBOTex(FBO&& fbo, Texture&& texture) :
        fbo(std::move(fbo)), texture(std::move(texture)) {}
    FBOTex(FBOTex&& that) :
        fbo(std::move(that.fbo)), texture(std::move(that.texture)) {}
  };
  /**
   * A structure containing an FBO and the texture where output to it will
   * be stored, both for multi-sample and single-sample flavours.
   */
  struct FBOTexMS {
    FBOTex ms;
    FBOTex ss;
    FBOTexMS(FBOTex&& ms, FBOTex&& ss) : ms(std::move(ms)), ss(std::move(ss)) {}
    FBOTexMS(FBOTexMS&& that) :
        ms(std::move(that.ms)), ss(std::move(that.ss)) {}
  };
  // FBO* getActiveFBO();
  // GLuint getActiveFBOID();
  /// Return an @ref FBOTex with sensible attachments.
  FBOTex makeFBOForMe(GLint width, GLint height);
  /// Return an @ref FBOTexMS with sensible attachments.
  FBOTexMS makeFBOForMeMS(GLint width, GLint height);
  /// Unbind the current @ref FBO object.
  void setDefaultFBOAsActive();
  /// Bind the FBO with the id `id` and update the current FBO field used by
  /// AGL.
  void bindFbo(GLuint id) noexcept;
  GLuint getCurrentFboId() noexcept;
}
