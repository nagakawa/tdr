#pragma once

#include <stdint.h>

#include <type_traits>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "AGL/BlendMode.h"
#include "AGL/EBO.h"
#include "AGL/GLFWApplication.h"
#include "AGL/Shader.h"
#include "AGL/ShaderProgram.h"
#include "AGL/Texture.h"
#include "AGL/VAO.h"
#include "AGL/VBO.h"

namespace agl {
  /**
   * Helper class to conditionally include a field based on a template
   * value.
   */
  template<bool useElems> struct ElementArray;
  template<> struct ElementArray<true> {
  protected:
    std::vector<uint32_t> elements;
    EBO ebo;
  };
  template<> struct ElementArray<false> {};
  /**
   * \brief A primitive object, akin to Danmakufu's `ObjPrim`.
   */
  template<bool useElems = false>
  class Primitive : public ElementArray<useElems> {
  public:
    /// A vertex of a primitive object.
    struct Vertex {
      glm::vec4 colour; ///< The colour and alpha of the vertex.
      /// The position of the vertex, in pixels. The z-coordinate of this
      /// vector is reserved for future use.
      glm::vec3 pos;
      glm::vec2 texcoords; ///< The texture coordinates in [0, 1].
    };
    Primitive(WeakTexture t = WeakTexture{}) :
        texture(t), bm(BlendMode::alpha), mode(0) {}
    /**
     * \brief Set the parent object for this sprite.
     *
     * This used to be required in older versions of AGL, but as of now,
     * Sprite2D will deduce the viewport dimensions if not supplied
     * a dimensional object.
     */
    void setApp(GLFWApplication* a) { app = a; }
    /**
     * \brief Set up this object for rendering.
     */
    void setUp();
    /**
     * \brief Render this object.
     */
    void render();
    /// Get the number of vertices.
    size_t getVertexCount() const noexcept { return vertices.size(); }
    /// Set the number of vertices.
    Primitive<useElems>& setVertexCount(size_t c) noexcept {
      vertices.resize(c);
      return *this;
    }
    /// Get the number of elements.
    template<bool u = useElems>
    std::enable_if_t<u, size_t> getElementCount() const noexcept {
      return this->elements.size();
    }
    /// Set the number of elements.
    template<bool u = useElems>
    std::enable_if_t<u, Primitive<u>&> setElementCount(size_t c) noexcept {
      this->elements.resize(c);
      return *this;
    }
    /// Get a vertex by index.
    const Vertex& operator[](size_t i) const noexcept { return vertices[i]; }
    /// Get a vertex by index.
    Vertex& operator[](size_t i) noexcept { return vertices[i]; }
    /// Get an element by index.
    template<bool u = useElems>
    std::enable_if_t<u, uint32_t> getElement(size_t i) const noexcept {
      return this->elements[i];
    }
    /// Set an element by index.
    template<bool u = useElems>
    std::enable_if_t<u, Primitive<u>&> setElement(size_t i, size_t e) noexcept {
      this->elements[i] = e;
      return *this;
    }
    /// Get the current primitive mode.
    GLenum getMode() const noexcept { return mode; }
    /// Set the current primitive mode.
    Primitive<useElems>& setMode(GLenum m) noexcept {
      mode = m;
      return *this;
    }

  private:
    void update();
    void setTexture(WeakTexture tex);
    std::vector<Vertex> vertices;
    WeakTexture texture;
    GLFWApplication* app = nullptr;
    BlendMode bm;
    GLint vp[4];
    GLenum mode;
    ShaderProgram program;
    VBO vbo;
    VAO vao;
    bool hasSetUniforms = false;
    bool hasInitialisedProgram = false;
    bool hasDeducedScreenDimensions = false;
  };
}
