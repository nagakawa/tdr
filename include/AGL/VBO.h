#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

namespace agl {
  /// Unbind the current @ref VBO object.
  void resetVBO();
  /**
   * \brief A handle for an OpenGL vertex buffer object.
   */
  class VBO {
  public:
    /// Create an empty VBO.
    VBO();
    ~VBO();
    VBO(const VBO& vbo) = delete;
    VBO& operator=(const VBO& vbo) = delete;
    VBO(VBO&& vbo) noexcept : id(vbo.id) { vbo.id = 0; }
    VBO& operator=(VBO&& vbo) noexcept {
      id = vbo.id;
      vbo.id = 0;
      return *this;
    }
    /// Bind this VBO to the OpenGL context.
    void setActive();
    /**
     * \brief Supply data to this VBO.
     * See [this page](https://www.khronos.org/opengl/wiki/GLAPI/glBufferData)
     * for more information.
     * \param size The number of bytes to feed.
     * \param data The pointer to this data. Can be nullptr, in which case
     *   you should use feedSubdata to supply the actual data.
     * \param usage One of GL_STATIC_DRAW, GL_DYNAMIC_DRAW or GL_STREAM_DRAW.
     */
    void feedData(GLint size, const void* data, GLenum usage);
    /**
     * \brief Update a section of data to this VBO.
     * See [this
     * page](https://www.khronos.org/opengl/wiki/GLAPI/glBufferSubData) for more
     * information. \param offset The offset at which to place the data \param
     * size The number of bytes to feed. \param data The pointer to this data.
     */
    void feedSubdata(GLintptr offset, GLint size, void* data);
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }

  private:
    /// The ID used by OpenGL for this object.
    GLuint id;
  };
}
