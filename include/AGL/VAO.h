#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

namespace agl {
  /// Unbind the current @ref VAO object.
  void resetVAO();
  /**
   * \brief A handle for an OpenGL vertex array object.
   */
  class VAO {
  public:
    /// Create an empty VAO.
    VAO();
    ~VAO();
    VAO(VAO&& vao) noexcept : id(vao.id) { vao.id = 0; }
    VAO& operator=(VAO&& vao) noexcept {
      id = vao.id;
      vao.id = 0;
      return *this;
    }
    /// Bind this VAO to the OpenGL context.
    void setActive();
    /// Return the ID used by OpenGL for this object.
    GLuint getID() const noexcept { return id; }

  private:
    /// The ID used by OpenGL for this object.
    GLuint id;
  };
}
