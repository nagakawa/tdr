#pragma once

#include <stdexcept>
#include <vector>

#include "AGL/Texture.h"
#include "AGL/rect.h"

struct FT_LibraryRec_;
struct FT_FaceRec_;
struct hb_font_t;

namespace agl {
  /// The default width and height of the font atlas.
  constexpr size_t ATLAS_SIZE = 1024;
  /// Convert a FreeType error code into a string.
  const char* ftErrorToString(int code);
  /// Wrapper around a FreeType error.
  class FTException : public std::exception {
  public:
    FTException(int code) : code(code) {}
    const char* what() const noexcept override { return ftErrorToString(code); }

  private:
    int code;
  };
  /**
   * \brief A font resource.
   *
   * This is passed to @ref XText.
   *
   * Internally, Font uses an atlas of multi-signed distance field textures.
   * This allows for scaling and outlines, as well as sharp corners.
   */
  class Font {
  public:
    /// Describes the location of a glyph.
    struct GlyphInfo {
      size_t texid; ///< The index of the texture in @ref texs.
      UIRect16 box; ///< The bounding box of the glyph within the texture.
      // This is WITHOUT the margins.
      int32_t w, h; ///< The width and height of the glyph, excluding margins.
      int32_t xoffset, yoffset; ///< The offset of the glyph from the origin.
    };
    /**
     * Load a font from file.
     *
     * \param ftl An FT_Library object obtained by `FT_Init_FreeType` or such.
     * \param filename The path to the font file.
     * \param s The size to use for the glyphs, expressed as the number of
     *   pixels from the baseline to the ascender.
     */
    Font(FT_LibraryRec_* ftl, const char* filename, size_t s);
    Font(const Font& f) = delete;
    Font(Font&& f);
    Font& operator=(const Font& f) = delete;
    Font& operator=(Font&& f);
    ~Font();
    /// Return the underlying `FT_Face`.
    FT_FaceRec_* getFont() { return face; }
    /// Return the underlying `hb_font_t*`.
    hb_font_t* getHBFont() { return facehb; }
    /// Get the size passed to the constructor.
    size_t getSize() { return size; }
    /// Get the width of the margin, in pixels.
    size_t margin() { return size / 8; }
    /// Get the @ref Font::GlyphInfo struct corresponding to a glyph ID.
    GlyphInfo& getInfo(uint32_t glyphID) { return rectsByGlyphID[glyphID]; }

  private:
    std::vector<Texture> texs;
    std::vector<GlyphInfo> rectsByGlyphID;
    FT_FaceRec_* face;
    hb_font_t* facehb;
    size_t size;
    friend class XText;
  };
}
