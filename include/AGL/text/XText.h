#pragma once

#include <stddef.h>

#include <memory>
#include <string>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "AGL/BlendMode.h"
#include "AGL/EBO.h"
#include "AGL/FBO.h"
#include "AGL/GLFWApplication.h"
#include "AGL/Shader.h"
#include "AGL/ShaderProgram.h"
#include "AGL/Sprite2D.h"
#include "AGL/Texture.h"
#include "AGL/VAO.h"
#include "AGL/VBO.h"
#include "AGL/rect.h"
#include "AGL/text/Font.h"
#include "AGL/text/GlyphDrawList.h"
#include "AGL/text/TextStyle.h"

namespace agl {
  /**
   * \brief An object that renders text.
   *
   * Instances of this object can render zero or more runs of text,
   * each at a different starting location and with different layout
   * parameters, but all runs share the same font and text style.
   * \ref setText, \ref getText, \ref setPos and \ref getPos assume
   * there is exactly one run of text, while \ref clearText,
   * \ref addText, \ref getNumElements and \ref getElement are intended
   * for use with multiple runs of text.
   */
  class XText {
  public:
    /**
     * \brief A structure describing a run, or a block of text at a certain
     * location.
     */
    struct Element {
      Element() = default;
      Element(const LayoutInfo& l) : layout(l) {}
      Element(std::string&& s, const LayoutInfo& l) :
          text(std::move(s)), layout(l) {}
      Element(std::string&& s, const LayoutInfo& l, glm::vec2 p) :
          position(p), text(std::move(s)), layout(l) {}
      /**
       * \brief The position to render the run.
       *
       * This is expressed in pixel coordinates where (0, 0) is the top-left
       * corner and the y-axis points down.
       */
      glm::vec2 position = {0, 0};
      /// The text to render for this run.
      std::string text;
      /// The layout to use for this run.
      LayoutInfo layout;
      /// Filled in when the text is laid out with diagnostics.
      LayoutDiagnostics diagnostics;
    };

    XText() = default;
    /// Construct a text object with one run.
    XText(const LayoutInfo& l) : elems{Element(l)} {}
    /// Set the parent object for this object.
    void setApp(GLFWApplication* a) { app = a; }
    /// Set up the text object.
    void setUp();
    /// Render text to the screen.
    void render();
    // Begin single-comp methods
    /// Set the text for this object to render.
    void setText(std::string&& s);
    /// Get the text for this object to render.
    const std::string& getText() const { return elems[0].text; }
    /// Set the position where the text should be drawn.
    void setPos(glm::vec2 p);
    /// Get the position where the text should be drawn.
    glm::vec2 getPos() const { return elems[0].position; }
    // End single-comp methods
    // Begin multi-comp methods
    /// Remove all runs from this object.
    void clearText() { elems.clear(); }
    /**
     * \brief Add a run of text.
     * \param s The string to assign to the run.
     * \param l The layout info to assign to the run.
     * \return An index that can be passed to \ref getElement as long as the
     * run is not removed.
     */
    size_t addText(std::string&& s, const LayoutInfo& l, glm::vec2 p = {0, 0});
    /// Get the number of runs of text.
    size_t getNumElements() const { return elems.size(); }
    /**
     * \brief Get a run of text by an index.
     * \param i The index. Must be less than `getNumElements()`, or your
     * program will vomit. Such a value can be obtained from \ref addText.
     */
    const Element& getElement(size_t i) const { return elems[i]; }
    /**
     * \brief Get a run of text by an index.
     * \param i The index. Must be less than `getNumElements()`, or your
     * program will vomit. Such a value can be obtained from \ref addText.
     */
    Element& getElement(size_t i) {
      isDirty = true;
      return elems[i];
    }
    // End multi-comp methods
    /// Get the current text style.
    TextStyle& getTextStyle() { return style; }
    /// Get the layout options for this object.
    LayoutInfo& getLayout() {
      if (elems.empty()) elems.emplace_back();
      return elems[0].layout;
    }
    /// Set the font used for this text object.
    void setFont(Font* f) { font = f; }
    /// Get the font used for this text object.
    Font* getFont() { return font; }

  private:
    struct RInfo {
      UIRect16 bounds;
      glm::vec2 pos;
      glm::vec2 glyphSize;
      GLint runIndex;
    };
    // Wrap this in a struct so we can std::optional it and not use it when
    // we don't have a drop shadow.
    struct DropShadowState {
      /*
        pipeline:
        render text in b1 -> horizontal blur from b1 to b2 ->
        vertical blur from b2 to b1
      */
      FBO b1, b2;
      Texture t1, t2;
      Sprite2D spr;
      std::optional<Sprite2D> hblur, vblur;
      ShaderProgram init;
    };
    void spurt(std::vector<GlyphInfo>& gis, Element& e);
    void updateOffsets();
    void update();
    void setUniforms1();
    void setUniforms2(ShaderProgram& p, size_t page, bool useStyle = true);
    void setTextRunUniforms(ShaderProgram& p);
    void initShadows();
    void renderShadows();
    void getViewportDimensions();
    bool hasShadow() const noexcept;
    void relayout();
    glm::vec2 positionAsNDC(const Element& e) const;

    std::vector<std::vector<RInfo>> rinfo;
    std::vector<size_t> offsets;
    std::vector<Element> elems;
    std::optional<DropShadowState> dropShadowState;
    TextStyle style;
    Font* font = nullptr;
    GLFWApplication* app = nullptr;
    GLint vp[4];
    VBO vbo;
    VBO instanceVBO;
    VAO vao;
    ShaderProgram program;
    bool hasInitialisedProgram = false;
    bool hasSetUniforms = false;
    bool isDirty = false;
    bool shadowsDone = false;
  };
}
