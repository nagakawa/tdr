#pragma once

#include <string_view>
#include <utility>

#include <AGL/Shader.h>

namespace agl {
  /**
   * \brief Get horizontal and vertical shaders for blurring a certain
   * radius.
   *
   * \param radius The blur radius (must be positive).
   */
  std::pair<Shader*, Shader*> getShaders(int radius);
}
