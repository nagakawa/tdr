#pragma once

#include <stdint.h>

#include <vector>

namespace agl {
  /// Describes the location and dimensions of a glyph in layouted text.
  struct GlyphInfo {
    /**
     * \var index
     * \brief The ID of the glyph.
     *
     * \var x
     * \brief The x-coordinate of the glyph, in 1/64ths of a pixel.
     *
     * \var y
     * \brief The y-coordinate of the glyph, in 1/64ths of a pixel.
     *
     * \var w
     * \brief The width of the glyph, in pixels.
     *
     * \var h
     * \brief The height of the glyph, in pixels.
     */
    uint32_t index; ///< The ID of the glyph.
    int32_t x, y; // 6 fractional bits
    int32_t w, h;
  };
  /**
   * \brief Describes a run of glyphs on the same line with the same
   * writing direction.
   */
  struct Run {
    /**
     * \var glyphs
     * \brief The list of glyphs and their locations.
     *
     * \var start
     * \brief The index from the string at which the run starts,
     *   in UTF-16 code units.
     *
     * \var len
     * \brief The length of the run, in UTF-16 code units.
     *
     * \var width
     * \brief The width of the run, in 1/64ths of a pixel.
     *
     * \var height
     * \brief The height of the run, in 1/64ths of a pixel.
     *
     * \var offx
     * \brief The horizontal offset of the run from the top-left corner,
     *   in 1/64ths of a pixel.
     *
     * \var offy
     * \brief The vertical offset of the run from the top-left corner,
     *   in 1/64ths of a pixel.
     */
    std::vector<GlyphInfo> glyphs;
    int32_t start, len;
    int32_t width, height;
    int32_t offx, offy;
  };
  class Font;
  /**
   * \brief Alignment -- determines how the x-coordinate of an @ref XText
   * object is treated.
   */
  enum class HAlign {
    left, ///< Specifies that x is at the left edge.
    centre, ///< Specifies that x is at the central axis.
    right, ///< Specifies that x is at the right edge.
  };
  /**
   * \brief Alignment -- determines how the y-coordinate of an @ref XText
   * object is treated.
   *
   * Note: this is currently unused.
   */
  enum class VAlign {
    top, ///< Specifies that y is at the top edge.
    middle, ///< Specifies that y is at the central axis.
    bottom, ///< Specifies that y is at the bottom edge.
  };
  /// Options to pass to @ref layoutText.
  struct LayoutInfo {
    size_t maxWidth = SIZE_MAX; ///< The maximum width of the text, in pixels.
    size_t fontSize = 16; ///< The size of the text, in pixels.
    size_t margin = 0; ///< How much margin to leave around the text, in pixels.
    size_t lineSkip = 0; ///< How much space to place between lines, in pixels.
    HAlign hAlign = HAlign::left; ///< The horizontal alignment of the text.
  };
  /// Diagnostics returned by @ref layoutText.
  struct LayoutDiagnostics {
    float lineHeight; ///< The line height, in pixels.
  };
  /**
   * \brief Lays out some text.
   *
   * \param utf8 Text to lay out, in UTF-8.
   * \param f Reference to a font.
   * \param layout Options to pass to the layout engine.
   * \param diagnostics Diagnostics returned by the layout engine.
   * \return A vector of @ref GlyphInfo objects showing the glyphs to place
   *   and where to place them.
   */
  std::vector<GlyphInfo> layoutText(
      const char* utf8, Font& f, const LayoutInfo& layout,
      LayoutDiagnostics& diagnostics);
}