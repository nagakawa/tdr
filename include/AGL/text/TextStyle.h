#pragma once

#ifdef _WIN32
#  warning "Do not build on Windows. Build on another OS instead."
#endif

#include <glm/glm.hpp>

#include "AGL/Texture.h"

namespace agl {
  class ShaderProgram;
  /// Style used by outlines and fills.
  struct FillStyle {
    glm::vec2 centre; ///< The centre of the gradient.
    glm::vec2 gradDir; ///< The direction of the gradient.
    glm::vec4 col1, col2; ///< The colours of the gradient.
    /// A pointer to the texture to use. If it is null, then an entirely white
    /// texture is used.
    WeakTexture t;
    FillStyle();
    FillStyle(const glm::vec4& colour);
    /// Sets both col1 and col2 to col.
    void setColour(const glm::vec4 col);
    /// Sets the gradient to be vertical.
    void setVerticalGradient();
    void passToShader(ShaderProgram& sp, const char* vname, GLint tIndex) const;
  };
  struct DropShadowStyle {
    /// The colour to use for the drop shadow.
    glm::vec4 dropShadowColour = {0.0f, 0.0f, 0.0f, 0.0f};
    /// The offset for the shadow.
    glm::vec2 dropShadowOffset = {0.0f, 0.0f};
    /// The blur radius. Because of how drop shadows are implemented, this
    /// cannot be changed after the first time the text object is rendered with
    /// a drop shadow.
    float dropShadowRadius = 0.0f;
    bool isActive() const noexcept { return dropShadowColour.a > 0; }
  };
  /// Style for text.
  struct TextStyle {
    /**
     * \var outline
     * \brief The @ref FillStyle for the outline.
     *
     * \var fill
     * \brief The @ref FillStyle for the inside of the text.
     *
     * \var dropShadowStyle
     * \brief The \ref DropShadowStyle for the text.
     *
     * \var outlineThreshold
     * \brief How far out the outline should reach. The default is 0.5;
     *   higher values are farther inside and lower values are outside.
     *
     * \var fillThreshold
     * \brief How far out the fill should reach. The default is 0.5;
     *   higher values are farther inside and lower values are outside.
     */
    FillStyle outline, fill;
    DropShadowStyle dropShadowStyle;
    // 0.5 = default; higher values are farther inside
    float outlineThreshold, fillThreshold;
    TextStyle();
    void passToShader(ShaderProgram& sp, const char* vname) const;
    void prepareRender() const;
  };
}
