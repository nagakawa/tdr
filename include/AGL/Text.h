#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// Pango
extern "C" {
#include <pango/pangocairo.h>
}

#include <stddef.h>

#include <memory>
#include <string>
#include <vector>

#include "AGL/BlendMode.h"
#include "AGL/EBO.h"
#include "AGL/GLFWApplication.h"
#include "AGL/Shader.h"
#include "AGL/ShaderProgram.h"
#include "AGL/Texture.h"
#include "AGL/VAO.h"
#include "AGL/VBO.h"
#include "AGL/cairoutil.h"
#include "AGL/rect.h"

namespace agl {
  extern const char* TXT_VERTEX_SOURCE;
  extern const char* TXT_FRAGMENT_SOURCE;
  class Text {
  public:
    Text();
    ~Text();
    std::string getText() { return text; }
    void relayout();
    void setText(std::string txt, bool rl = true);
    std::string getFont() { return font; }
    void setFont(std::string fnt) { font = fnt; }
    unsigned int getWidth() { return width; }
    unsigned int getHeight() { return height; }
    unsigned int getMargin() { return margin; }
    void setMargin(unsigned int m) { margin = m; }
    double getSize() { return size; }
    void setSize(double s) { size = s; }
    void render();
    void update();
    glm::vec4 getTopColour() { return topColour; }
    void setTopColour(glm::vec4 col) { topColour = col; }
    glm::vec4 getBottomColour() { return bottomColour; }
    void setBottomColour(glm::vec4 col) { bottomColour = col; }
    void setColour(glm::vec4 col) {
      topColour = col;
      bottomColour = col;
    }
    glm::vec2 getPosition() { return position; }
    bool isRich() { return rich; }
    void setRich(bool r) { rich = r; }
    void setPosition(glm::vec2 pos) { position = pos; }
    void setApp(GLFWApplication* a) { app = a; }

  private:
    void setUp();
    std::string text;
    std::string font;
    glm::vec4 topColour;
    glm::vec4 bottomColour;
    glm::vec2 position;
    glm::vec2 vp;
    Texture* texture;
    GLFWApplication* app;
    unsigned char* buffer;
    double size;
    VBO vbo;
    VAO vao;
    ShaderProgram program;
    unsigned int width, height;
    unsigned int margin;
    bool hasInitialisedProgram;
    bool rich;
    // bool remark;
  };
}
