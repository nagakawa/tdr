// No #pragma once!

// Pass in `spLetter` and `Type`

#undef X
#define X(Location, location) \
  void CONCAT(uniform1, spLetter)(Location l, Type value) { \
    CONCAT(glProgramUniform1, spLetter)(id, location, value); \
  }
COMPOSE_LOCATION_VARIANTS

#undef X
#define X(Location, location) \
  void CONCAT(uniform2, spLetter)(Location l, glm::tvec2<Type> value) { \
    CONCAT3(glProgramUniform2, spLetter, v) \
    (id, location, 1, glm::value_ptr(value)); \
  }
COMPOSE_LOCATION_VARIANTS

#undef X
#define X(Location, location) \
  void CONCAT(uniform3, spLetter)(Location l, glm::tvec3<Type> value) { \
    CONCAT3(glProgramUniform3, spLetter, v) \
    (id, location, 1, glm::value_ptr(value)); \
  }
COMPOSE_LOCATION_VARIANTS

#undef X
#define X(Location, location) \
  void CONCAT(uniform4, spLetter)(Location l, glm::tvec4<Type> value) { \
    CONCAT3(glProgramUniform4, spLetter, v) \
    (id, location, 1, glm::value_ptr(value)); \
  }
COMPOSE_LOCATION_VARIANTS

#undef spLetter
#undef Type
