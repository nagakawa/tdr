#pragma once

/**
 * \brief Embed a shader source.
 *
 * This requires some linking voodoo to get working. See AGL/CMakeLists.txt
 * to see how to do this (at least on Linux).
 *
 * \param sourceName The name of the file with the GLSL source of the shader,
 *   without any extension.
 * \param name The name you want to give from C++.
 * \param STATIC Prefix for the variable declarations. Usually, this will be
 *   either empty or `static`.
 * \return Nothing, but `nameSource` and `nameSourceSize` are defined.
 */
#define EMBED_SHADER(sourceName, name, STATIC) \
  extern "C" const char _binary_##sourceName##_glsl_start[]; \
  extern "C" const char _binary_##sourceName##_glsl_end[]; \
  STATIC size_t name##SourceSize = \
      _binary_##sourceName##_glsl_end - _binary_##sourceName##_glsl_start; \
  STATIC const char* name##Source = _binary_##sourceName##_glsl_start;
