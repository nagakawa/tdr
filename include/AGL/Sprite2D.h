#pragma once

#include <stddef.h>

#include <memory>
#include <optional>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "AGL/BlendMode.h"
#include "AGL/EBO.h"
#include "AGL/GLFWApplication.h"
#include "AGL/Shader.h"
#include "AGL/ShaderProgram.h"
#include "AGL/Texture.h"
#include "AGL/VAO.h"
#include "AGL/VBO.h"
#include "AGL/rect.h"

namespace agl {
  /**
   * \brief A pair of source and destination rectangles.
   *
   * This is used by @ref Sprite2D.
   */
  struct Sprite2DInfo {
    Rect source; ///< Where on the texture to get the sprite from, in pixels.
    Rect dest; ///< Where on the screen to draw it, in pixels.
  };
  /**
   * \brief A 2D sprite object.
   *
   * This class takes a texture and draws subrectangles of it onto the screen.
   * Thus, it can draw multiple sprites at a time.
   */
  class Sprite2D {
  public:
    /**
     * \brief Create a Sprite2D object holding the specified texture.
     * \param t The pointer to the texture to use. The caller is responsible
     * 	 for keeping the texture pointed to by `t` alive for the lifetime of
     *   this object.
     * \param customFragment A pointer to a custom fragment shader. If null
     *   (default), then the default fragment shader will be used. The
     *   following uniforms are given to the shader:
     *
     * Name     | Type      | Value
     * ---------|-----------|-------
     * texCoord | vec2      | Texture coordinates
     * tex      | sampler2D | The texture
     *
     * The shader should output a value of type `vec4` with the output colour.
     * \see src/AGL/shaders/s2d_fragment_source.glsl
     */
    Sprite2D(WeakTexture t, Shader* customFragment = nullptr);
    Sprite2D(Sprite2D&& other) = default;
    Sprite2D& operator=(Sprite2D&& other) = default;
    ~Sprite2D() = default;
    /**
     * \brief Render this object to the screen.
     */
    void render();
    /**
     * \brief Update the objects managed by this one about where to draw the
     * sprites.
     *
     * This method should be manually called whenever the rectangle data
     * itself is modified. @ref addSprite() will call this method
     * automatically.
     */
    void update();
    /**
     * \brief Add a sprite to this object.
     * \param loc The @ref Sprite2DInfo object that describes this sprite.
     * \return A handle to this sprite.
     */
    int addSprite(Sprite2DInfo loc);
    /// Set the texture for this sprite.
    void setTexture(WeakTexture tex);
    /**
     * \brief Set the parent object for this sprite.
     *
     * This used to be required in older versions of AGL, but as of now,
     * Sprite2D will deduce the viewport dimensions if not supplied
     * a dimensional object.
     */
    void setApp(GLFWApplication* a) { app = a; }
    /// Return the @ref Sprite2DInfo object associated with a handle.
    Sprite2DInfo& get(int index) { return sprites[index]; }
    /// Return the @ref Sprite2DInfo object associated with a handle.
    const Sprite2DInfo& get(int index) const { return sprites[index]; }
    /// Return a pointer to the @ref Sprite2DInfo object associated with a
    /// handle.
    Sprite2DInfo* getLoc(int index) { return sprites.data() + index; }
    /**
     * \brief Clear all sprites from this object.
     *
     * This invalidates all handles to sprites to this object.
     */
    void clear() { sprites.clear(); }

  private:
    void setUp(Shader* customFragment);
    std::vector<Sprite2DInfo> sprites;
    glm::vec2 vp;
    GLFWApplication* app = nullptr;
    WeakTexture texture;
    VBO vbo;
    VBO instanceVBO;
    VAO vao;
    ShaderProgram program;
    bool hasSetUniforms;
    bool hasDeducedScreenDimensions = false;
  };
  glm::vec2 inferViewport(const GLFWApplication* app);
}
