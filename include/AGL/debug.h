#pragma once

#include <stdio.h>

/**
 * \brief Print OpenGL error result.
 * A debugging macro to print the current error for OpenGL.
 * This macro is not recommended; instead, consider using a debugger or
 * using a debugging context.
 */
#define TRGLE \
  printf("Error code is 0x%x, at %s:%d.\n", glGetError(), __FILE__, __LINE__)
