#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

namespace agl {
  enum class ProfilerTrackType : uint8_t {
    duration,
    mode,
    count,
  };
  namespace track_names {
    enum Tracks {
      frameTime,
      tickTime,
    };
  }
  struct ProfilerTrack {
    const char* name;
    ProfilerTrackType type;
  };
  struct ProfilerOptions {
    const char* applicationName;
    size_t nTracks;
    const ProfilerTrack* tracks; // Waiting for std::span
    // Don't really need a read method here.
    void writeToFile(FILE* fh) const;
  };
  extern ProfilerOptions defaultProfilerOptions;
}
