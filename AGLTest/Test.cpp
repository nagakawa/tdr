// Standard library
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// SOIL
#include <SOIL/SOIL.h>
// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// PA
#include <portaudio.h>
// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H

#include "AGL/EBO.h"
#include "AGL/FBO.h"
#include "AGL/GLFWApplication.h"
#include "AGL/Primitive.h"
#include "AGL/Shader.h"
#include "AGL/ShaderProgram.h"
#include "AGL/Sprite2D.h"
#include "AGL/Text.h"
#include "AGL/Texture.h"
#include "AGL/VAO.h"
#include "AGL/VBO.h"
#include "AGL/debug.h"
#include "AGL/sound/Mixer.h"
#include "AGL/sound/Reader.h"
#include "AGL/sound/Sound.h"
#include "AGL/text/XText.h"
#include "FileUtil/exe_location.h"
#include "FileUtil/get_fs_headers.h"
#include "Test.h"

class AGLTest;

class Boxes {
public:
  Boxes(AGLTest* a);
  void tearDown() {}
  void render();
  void update() {}

private:
  void setUp();
  std::unique_ptr<agl::Texture> container;
  std::unique_ptr<agl::Texture> awesome;
  std::unique_ptr<agl::VBO> vbo;
  std::unique_ptr<agl::EBO> ebo;
  std::unique_ptr<agl::VAO> vao;
  std::unique_ptr<agl::ShaderProgram> program;
  AGLTest* app;
};

#define DIGIT_WIDTH 14.8f
#define DIGIT_HEIGHT 18
#define DIGIT_BASEX 16
#define DIGIT_BASEY 230

#define NUM_WORDS 7
const char* fnames[NUM_WORDS] = {
    "sounds/I.ogg",    "sounds/will.ogg", "sounds/shank.ogg", "sounds/your.ogg",
    "sounds/fuck.ogg", "sounds/king.ogg", "sounds/mom.ogg"};

class AGLTest : public agl::GLFWApplication {
public:
  using agl::GLFWApplication::GLFWApplication;
  void initialise() override {
    glfwSetInputMode(underlying(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    agl::FBOTexMS ft = agl::makeFBOForMeMS(800, 600);
    fboTex = std::move(ft.ss.texture);
    fboTexMS = std::move(ft.ms.texture);
    fboSS = std::move(ft.ss.fbo);
    fboMS = std::move(ft.ms.fbo);
    boxes = new Boxes(this);
    stex = agl::Texture("textures/fuckyou.png");
    sprites = new agl::Sprite2D(stex.weak());
    sprites->setApp(this);
    sprites->addSprite({{0, 448, 512, 512}, {0, 0, 512, 64}});
    sprites->addSprite(
        {{DIGIT_BASEX, DIGIT_BASEY, DIGIT_BASEX + 4 * DIGIT_WIDTH,
          DIGIT_BASEY + DIGIT_HEIGHT},
         {50, 500, 50 + 4 * DIGIT_WIDTH, 500 + DIGIT_HEIGHT}});
    sprites->addSprite(
        {{DIGIT_BASEX + 14 * DIGIT_WIDTH, DIGIT_BASEY,
          DIGIT_BASEX + 15 * DIGIT_WIDTH, DIGIT_BASEY + DIGIT_HEIGHT},
         {50 + 7 * DIGIT_WIDTH, 500, 50 + 8 * DIGIT_WIDTH,
          500 + DIGIT_HEIGHT}});
    for (int i = 0; i < 5; ++i) {
      GLfloat x = 50 + (4 + i + (i >= 3)) * DIGIT_WIDTH;
      sprites->addSprite({{0, DIGIT_BASEY, 0, DIGIT_BASEY + DIGIT_HEIGHT},
                          {x, 500, x + DIGIT_WIDTH, 500 + DIGIT_HEIGHT}});
    }
    fy = std::make_unique<agl::Text>();
    fy->setApp(this);
    fy->setMargin(240);
    fy->setFont("Segoe UI");
    fy->setSize(0.7);
    fy->setTopColour(glm::vec4(0.7f, 0.0f, 0.3f, 1.0f));
    fy->setBottomColour(glm::vec4(0.3f, 0.7f, 0.0f, 0.8f));
    fy->setRich(true);
    // fy->setText("Fuck you!");
    fy->setText(
        u8"<i>Bad</i> translation\n<u>Κακή μετάφραση</u>\nMala "
        u8"traducción\nплохой перевод\n下手な翻訳\n잘못된 번역\nתרגום "
        u8"גרוע\nترجمة سيئة\nD́ȉa͟c̈r̆ȉt̂ics\nThe Touhou Project (東方Project Tōhō "
        u8"Purojekuto, lit. Eastern Project), also known as Toho Project or "
        u8"Project Shrine Maiden, is a series of Japanese bullet hell shooter "
        u8"video games developed by the single-person Team Shanghai Alice. "
        u8"Team Shanghai Alice's sole member, <b>ZUN</b>, independently "
        u8"produces the games' graphics, music, and "
        u8"programming."
        u8"\n東方Project（とうほうプロジェクト）とは、同人サークルの上海アリス"
        u8"幻樂団によって製作されている著作物である。弾幕系シューティングを中心"
        u8"としたゲーム、書籍、音楽CDなどから成る。東方Projectの作品を一括して"
        u8"東方、東方Projectシリーズなどと称することもある。狭義には、上海アリ"
        u8"ス幻樂団のメンバー「<b>ZUN</"
        u8"b>」が制作している同人作品の一連の作品をあらわす。");
    fy->setPosition(glm::vec2(530, 20));
    vtex = std::move(fboTex);
    view = new agl::Sprite2D(vtex.weak());
    view->addSprite({{0, 600, 800, 0}, {0, 0, 800, 600}});
    view->addSprite({{0, 600, 800, 0}, {640, 0, 800, 120}});
    if (FT_Init_FreeType(&ftl) != 0) throw "Failed to initialise FreeType.";
    agl::LayoutInfo l;
    afont = std::make_unique<agl::Font>(ftl, "textures/kardinal.ttf", 64);
    l.fontSize = 24;
    l.margin = 4;
    l.maxWidth = 400;
    l.lineSkip = 20;
    xt = std::make_unique<agl::XText>(l);
    xt->setFont(afont.get());
    xt->setApp(this);
    xt->setUp();
    xt->setText("tu et arka.\nh\nasayarala\nsoonoyun!");
    xt->setPos(glm::vec2(50, 90));
    auto& style = xt->getTextStyle();
    style.fill.setVerticalGradient();
    style.fill.col1 = glm::vec4(0.85f, 1.0f, 0.8f, 1.0f);
    style.fill.col2 = glm::vec4(0.5f, 1.0f, 0.4f, 1.0f);
    style.outline.setColour(glm::vec4(0.2f, 0.5f, 0.05f, 1.0f));
    style.outlineThreshold = 0.2f;
    kas.setMode(GL_TRIANGLES).setVertexCount(3);
    kas[0] = {
        /* .colour = */ glm::vec4(1.0f, 0.0f, 0.0f, 1.0f),
        /* .pos = */ glm::vec3(126, 78, 1),
        /* .texcoords = */ glm::vec2(0, 0),
    };
    kas[1] = {
        /* .colour = */ glm::vec4(0.0f, 1.0f, 0.0f, 1.0f),
        /* .pos = */ glm::vec3(94, 52, 1),
        /* .texcoords = */ glm::vec2(0, 0),
    };
    kas[2] = {
        /* .colour = */ glm::vec4(0.0f, 0.0f, 1.0f, 1.0f),
        /* .pos = */ glm::vec3(67, 89, 1),
        /* .texcoords = */ glm::vec2(0, 0),
    };
    kas.setUp();
    auto& mixer = getMixer();
    for (int i = 0; i < NUM_WORDS; ++i) {
      soundIDs[i] = mixer.addSoundFromFile(fnames[i]);
      wasPressed[i] = false;
    }
    // setVSyncEnable(false);
  }
  bool ff = true;
  void tick() override {
    fboMS.setActive();
    glClearColor(0.5f, 0.7f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ++frame;
    int which = (frame >> 6) & 3;
    sprites->getLoc(0)->source.top = (GLfloat)(448 - (which << 6));
    sprites->getLoc(0)->source.bottom = (GLfloat)(512 - (which << 6));
    int rfps = (int) (getRollingFPS() * 100);
    for (int i = 4; i >= 0; --i) {
      setDigit(i, rfps % 10);
      rfps /= 10;
    }
    sprites->update();
    boxes->render();
    sprites->render();
    kas.render();
    if (relayoutText) fy->relayout();
    fy->render();
    std::string diag;
    diag += "t = ";
    diag += std::to_string(cameraPos.x);
    diag += "\n";
    diag += "k = ";
    diag += std::to_string(cameraPos.y);
    diag += "\n";
    diag += "x = ";
    diag += std::to_string(cameraPos.z);
    diag += "\nsoonoyun, fia!";
    xt->setText(std::move(diag));
    xt->render();
    fboMS.blitTo(fboSS, 800, 600);
    agl::setDefaultFBOAsActive();
    glClearColor(1.0f, 0.5f, 0.7f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    view->render();
    char ctitle[256];
    snprintf(
        ctitle, 255, "TestApp @ GL %s | FPS: %lf | per-frame relayout %s",
        glGetString(GL_VERSION), getRollingFPS(),
        (relayoutText ? "on" : "off"));
    glfwSetWindowTitle(underlying(), ctitle);
  }
  void readKeys() override {
    // Test if window should close
    if (testKey(GLFW_KEY_ESCAPE))
      glfwSetWindowShouldClose(underlying(), GL_TRUE);
    // Camera controls
    GLfloat cameraSpeed = 5.0f * (GLfloat) getDelta();
    if (testKey(GLFW_KEY_W)) cameraPos += cameraSpeed * cameraFront;
    if (testKey(GLFW_KEY_S)) cameraPos -= cameraSpeed * cameraFront;
    if (testKey(GLFW_KEY_A))
      cameraPos -=
          glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (testKey(GLFW_KEY_D))
      cameraPos +=
          glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    // Slider controls
    if (testKey(GLFW_KEY_UP)) mix += 0.005f;
    if (testKey(GLFW_KEY_DOWN)) mix -= 0.005f;
    auto& mixer = getMixer();
    for (int i = 0; i < NUM_WORDS; ++i) {
      bool t = testKey(GLFW_KEY_1 + i);
      if (t && !wasPressed[i]) {
        mixer.playSound(soundIDs[i]);
        wasPressed[i] = true;
      } else if (!t)
        wasPressed[i] = false;
    }
    if (testKey2(GLFW_KEY_R) == agl::KeyStatus::enter) {
      relayoutText = !relayoutText;
    }
  }
  void onMouse(double xpos, double ypos) override {
    if (firstMouse) {
      lastX = (GLfloat) xpos;
      lastY = (GLfloat) ypos;
      firstMouse = false;
    }
    GLfloat xoffset = (GLfloat) xpos - lastX;
    GLfloat yoffset = lastY - (GLfloat) ypos;
    lastX = (GLfloat) xpos;
    lastY = (GLfloat) ypos;
    GLfloat sensitivity = 0.10f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;
    yaw += xoffset;
    pitch += yoffset;
    if (pitch > 89.0f) pitch = 89.0f;
    if (pitch < -89.0f) pitch = -89.0f;
    glm::vec3 front;
    front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    cameraFront = glm::normalize(front);
  }
  void start() { GLFWApplication::start(); }
  ~AGLTest() {
    delete boxes;
    delete sprites;
    delete view;
    FT_Done_FreeType(ftl);
    Pa_Terminate();
  }
  GLfloat getMix() { return mix; }
  agl::Primitive<> kas;
  std::unique_ptr<agl::XText> xt;
  std::unique_ptr<agl::Font> afont;
  std::unique_ptr<agl::Text> fy;
  Boxes* boxes;
  agl::Sprite2D* sprites;
  agl::Sprite2D* view;
  size_t soundIDs[NUM_WORDS];
  FT_Library ftl;
  agl::Texture stex;
  agl::Texture vtex;
  agl::FBO fboMS;
  agl::FBO fboSS;
  agl::Texture fboTex;
  agl::Texture fboTexMS;
  glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
  glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
  glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
  GLfloat mix = 0.0f;
  GLfloat deltaTime = 0.0f;
  GLfloat lastFrame = 0.0f;
  GLfloat lastX = 400, lastY = 300;
  GLfloat yaw = 90, pitch = 0;
  GLfloat fov = 45.0f;
  int frame = 0;
  bool wasPressed[NUM_WORDS];
  bool firstMouse = true;
  bool relayoutText = false;
  void setDigit(int i, int v) {
    agl::Sprite2DInfo* spr = sprites->getLoc(3 + i);
    spr->source.left = DIGIT_BASEX + (4 + v) * DIGIT_WIDTH;
    spr->source.right = DIGIT_BASEX + (5 + v) * DIGIT_WIDTH;
  }
};

Boxes::Boxes(AGLTest* a) {
  app = a;
  setUp();
}
void Boxes::setUp() {
  container = std::make_unique<agl::Texture>("textures/container.jpg");
  awesome = std::make_unique<agl::Texture>("textures/awesomeface.png");
  vbo = std::make_unique<agl::VBO>();
  ebo = std::make_unique<agl::EBO>();
  agl::Shader vertexShader =
      agl::openShaderFromFile2("shader/vertex.glsl", GL_VERTEX_SHADER);
  agl::Shader fragmentShader =
      agl::openShaderFromFile2("shader/fragment.glsl", GL_FRAGMENT_SHADER);
  program = std::make_unique<agl::ShaderProgram>();
  program->attach(vertexShader);
  program->attach(fragmentShader);
  program->link();
  program->use();
  vao = std::make_unique<agl::VAO>();
  vao->setActive();
  vbo->feedData(sizeof(vertices), vertices, GL_STATIC_DRAW);
  ebo->feedData(sizeof(indices), indices, GL_STATIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
  glVertexAttribPointer(
      0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(
      1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
      (GLvoid*) (3 * sizeof(GLfloat)));
#pragma GCC diagnostic pop
  glEnableVertexAttribArray(1);
}
void Boxes::render() {
  glDisable(GL_BLEND);
  vao->setActive();
  program->use();
  glEnable(GL_DEPTH_TEST);
  container->bindTo(0);
  SETUNSP(*program, 1i, "ourTexture", 0);
  awesome->bindTo(1);
  SETUNSP(*program, 1i, "ourTexture2", 1);
  SETUNSP(*program, 1f, "m", app->mix);
  // GLfloat radius = 10.0f;
  // GLfloat camX = (GLfloat) sin(glfwGetTime()) * radius;
  // GLfloat camZ = (GLfloat) cos(glfwGetTime()) * radius;
  glm::mat4 view;
  view = glm::lookAt(
      app->cameraPos, // position
      app->cameraPos + app->cameraFront, // target
      app->cameraUp); // up
  GLfloat aspect = ((GLfloat) app->getWidth()) / app->getHeight();
  glm::mat4 projection;
  projection = glm::perspective(glm::radians(app->fov), aspect, 0.1f, 100.0f);
  for (int i = 0; i < 10; ++i) {
    glm::mat4 model;
    model = glm::translate(model, cubePositions[i]);
    model = glm::rotate(
        model,
        glm::radians(
            (GLfloat)((i % 3 == 0 ? glfwGetTime() * 50 : 0) + i * 20.0)),
        glm::vec3(0.5f, 1.0f, 0.0f));
    SETUNSPM(*program, 4fv, "model", glm::value_ptr(model));
    SETUNSPM(*program, 4fv, "view", glm::value_ptr(view));
    SETUNSPM(*program, 4fv, "projection", glm::value_ptr(projection));
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
  }
  agl::resetVAO();
}

static void glfwError(int id, const char* description) {
  (void) id;
  std::cerr << description << std::endl;
}

int main(int argc, char** argv) {
  (void) argc;
  (void) argv;
  /* local */ {
    auto exedir = futil::getExeLocation();
    if (!exedir.has_value()) {
      std::cerr << "Couldn't get location of executable\n";
      exit(-1);
    }
    auto p = fs::path(*exedir).parent_path();
    fs::current_path(p);
  }
  glfwSetErrorCallback(glfwError);
  glfwInit();
  try {
    agl::ApplicationOptions opts;
    opts.width = 800;
    opts.height = 600;
    opts.glMajor = 4;
    opts.glMinor = 5;
    opts.debug = true;
    opts.title = u8"AGL Test App";
    // Create a test window
    AGLTest* a = new AGLTest(opts);
    a->start();
  } catch (char const* s) {
    std::cerr << u8"An error has Okuued!\n\n" << s << u8"\n\n";
  } catch (const std::string& s) {
    std::cerr << u8"An error has Okuued!\n\n" << s << u8"\n\n";
  } catch (std::exception& e) {
    std::cerr << u8"An error has Okuued!\n\n" << e.what() << u8"\n\n";
  }
  glfwTerminate();
}
