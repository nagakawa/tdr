#include "TDR/Replay.h"

#include <algorithm>
#include <iostream>

#include <FileUtil/rwtype.h>

namespace tdr {
  bool ReplaySection::read(uint64_t& keys, uint16_t& fps) {
    if (idx >= keypresses.size()) {
      ++idx;
      return false;
    }
    keys = keypresses[idx];
    if (idx < fpses.size())
      fps = fpses[idx];
    else
      fps = 65535;
    ++idx;
    return true;
  }

  void ReplaySection::write(uint64_t keys, uint16_t fps) {
    if (keypresses.size() <= idx + 1) keypresses.resize(idx + 1);
    if (fpses.size() <= idx + 1) fpses.resize(idx + 1);
    keypresses[idx] = keys;
    fpses[idx] = fps;
    ++idx;
  }

  void Replay::clear() {
    name.clear();
    comment.clear();
    timestamp = 0;
    sections.clear();
    currentStage = -1;
  }

  static const char replayMagic[8] = {
      (char) 0x08, (char) 0x00, (char) 0x03, (char) 0x0B,
      (char) 0x80, (char) 0x0D, (char) 0xFF, (char) 0x0A,
  };

  void Replay::save(std::ostream& fh) const {
    fh.write(replayMagic, 8);
    futil::write64u(fh, timestamp);
    [[maybe_unused]] bool res = futil::writeStr<uint8_t>(fh, name);
    assert(res);
    res = futil::writeStr<uint8_t>(fh, comment);
    assert(res);
    futil::write32u(fh, sections.size());
    for (const auto& [stageNumber, section] : sections) {
      futil::write32u(fh, stageNumber);
      section.save(fh);
    }
  }

  // Note: does not save the stage number
  void ReplaySection::save(std::ostream& fh) const {
    futil::write64u(fh, seed);
    cd.save(fh);
    futil::write64u(fh, keypresses.size());
    uint64_t highestKey =
        *std::max_element(keypresses.begin(), keypresses.end());
    if (highestKey <= 0xFFULL) {
      // 1 byte per press
      fh.put(1);
      for (uint64_t k : keypresses) { fh.put((char) k); }
    } else if (highestKey <= 0xFFFFULL) {
      // 2 bytes per press
      fh.put(2);
      for (uint64_t k : keypresses) { futil::write16u(fh, (uint16_t) k); }
    } else if (highestKey <= 0xFFFF'FFFFULL) {
      // 4 bytes per press
      fh.put(4);
      for (uint64_t k : keypresses) { futil::write32u(fh, (uint32_t) k); }
    } else {
      // 8 bytes per press
      fh.put(8);
      for (uint64_t k : keypresses) { futil::write64u(fh, k); }
    }
    for (uint16_t fps : fpses) { futil::write16u(fh, fps); }
  }

  ReplaySection::ReplaySection(std::istream& fh, bool& succ) {
    succ = false;
    seed = futil::read64u(fh);
    if (!fh.good()) return;
    auto cd = CommonData::load(fh);
    if (!cd.has_value()) return;
    this->cd = std::move(*cd);
    size_t nPresses = futil::read64u(fh);
    uint8_t bytesPerPress = fh.get();
    keypresses.resize(nPresses);
    if (!fh.good()) return;
    switch (bytesPerPress) {
    case 1: {
      for (auto& k : keypresses) k = fh.get();
      break;
    }
    case 2: {
      for (auto& k : keypresses) k = futil::read16u(fh);
      break;
    }
    case 4: {
      for (auto& k : keypresses) k = futil::read32u(fh);
      break;
    }
    case 8: {
      for (auto& k : keypresses) k = futil::read64u(fh);
      break;
    }
    default: return;
    }
    if (!fh.good()) return;
    fpses.resize(nPresses);
    for (auto& fps : fpses) fps = futil::read16u(fh);
    succ = true;
  }

  std::optional<ReplaySection> ReplaySection::load(std::istream& fh) {
    bool b;
    ReplaySection cd(fh, b);
    if (!b) return std::nullopt;
    return cd;
  }

  Replay::Replay(std::istream& fh, bool& succ) {
    succ = false;
    char magic[8];
    fh.read(magic, 8);
    if (!fh.good()) return;
    if (!std::equal(
            std::begin(magic), std::end(magic), std::begin(replayMagic)))
      return;
    timestamp = futil::read64u(fh);
    if (!fh.good()) return;
    auto name = futil::readStr<uint8_t>(fh);
    if (!name.has_value()) return;
    this->name = std::move(*name);
    auto comment = futil::readStr<uint8_t>(fh);
    if (!comment.has_value()) return;
    this->comment = std::move(*comment);
    size_t nSecs = futil::read32u(fh);
    if (!fh.good()) return;
    for (size_t i = 0; i < nSecs; ++i) {
      unsigned stage = futil::read32u(fh);
      if (!fh.good()) return;
      auto sec = ReplaySection::load(fh);
      if (!sec.has_value()) return;
      sections.insert(std::make_pair(stage, std::move(*sec)));
    }
    succ = true;
  }

  std::optional<Replay> Replay::load(std::istream& fh) {
    bool b;
    Replay cd(fh, b);
    if (!b) return std::nullopt;
    return cd;
  }

  unsigned Replay::getStageAfter(unsigned stage) const {
    auto it = sections.upper_bound(stage);
    if (it == sections.end()) return -1U;
    return it->first;
  }
}
