#include "TDR/particle/ParticleManager.h"

#include <AGL/Shader.h>
#include <AGL/embedShader.h>

namespace tdr {
  EMBED_SHADER(pm_vertex_source, vertex, static)
  EMBED_SHADER(pm_fragment_source, fragment, static)

  static thread_local std::optional<agl::Shader> pmVertex, pmFragment;

  static void initPMShaders() {
    if (pmVertex.has_value()) return;
    pmVertex.emplace(vertexSource, vertexSourceSize - 1, GL_VERTEX_SHADER);
    pmFragment.emplace(
        fragmentSource, fragmentSourceSize - 1, GL_FRAGMENT_SHADER);
  }
  static void updateParticle(ParticleInfo& p) noexcept {
    --p.lifespan;
    p.blend += p.blendRate;
    p.position += p.velocity;
    p.velocity += p.acceleration;
    PV tangent{-p.velocity.y, p.velocity.x};
    p.velocity.x += tangent.x * p.tangentialAcceleration;
    p.velocity.y += tangent.y * p.tangentialAcceleration;
    p.rotation += p.rotationRate;
  }
  void ParticleManager::update() noexcept {
    for (ParticleInfo& p : particles) updateParticle(p);
    auto newEnd = std::remove_if(
        particles.begin(), particles.end(),
        [](const ParticleInfo& p) { return p.lifespan == 0; });
    particles.erase(newEnd, particles.end());
  }
  void ParticleManager::setUp() {
    assert(p != nullptr);
    initPMShaders();
    program.attach(*pmVertex);
    program.attach(*pmFragment);
    program.link();
    vao.setActive();
    // Vertex data
    vbo.feedData(
        sizeof(agl::rectangleVertices), (void*) agl::rectangleVertices,
        GL_DYNAMIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*) 0);
    // InstanceData
    renderUpdate();
    instanceVBO.setActive();
    glEnableVertexAttribArray(1);
    glVertexAttribIPointer(
        1, 2, GL_INT, sizeof(ParticleRenderInfo),
        (GLvoid*) (offsetof(ParticleRenderInfo, position)));
    glVertexAttribDivisor(1, 1);
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(
        2, 2, GL_INT, sizeof(ParticleRenderInfo),
        (GLvoid*) (offsetof(ParticleRenderInfo, dimensions)));
    glVertexAttribDivisor(2, 1);
    glEnableVertexAttribArray(3);
    glVertexAttribIPointer(
        3, 4, GL_UNSIGNED_SHORT, sizeof(ParticleRenderInfo),
        (GLvoid*) (offsetof(ParticleRenderInfo, texcoords)));
    glVertexAttribDivisor(3, 1);
    glEnableVertexAttribArray(4);
    glVertexAttribIPointer(
        4, 1, GL_UNSIGNED_INT, sizeof(ParticleRenderInfo),
        (GLvoid*) (offsetof(ParticleRenderInfo, rotation)));
    glVertexAttribDivisor(4, 1);
    glEnableVertexAttribArray(5);
    glVertexAttribIPointer(
        5, 4, GL_UNSIGNED_BYTE, sizeof(ParticleRenderInfo),
        (GLvoid*) (offsetof(ParticleRenderInfo, blend)));
    glVertexAttribDivisor(5, 1);
#pragma GCC diagnostic pop
    agl::resetVBO();
    agl::resetVAO();
  }

  void ParticleManager::renderUpdate() {
    spurt();
    instanceVBO.feedData(
        offsets.back() * sizeof(ParticleRenderInfo), nullptr, GL_STATIC_DRAW);
    for (size_t i = 0; i < rinfo.size(); ++i) {
      instanceVBO.feedSubdata(
          offsets[i] * sizeof(ParticleRenderInfo),
          (offsets[i + 1] - offsets[i]) * sizeof(ParticleRenderInfo),
          rinfo[i].data());
    }
  }

  void ParticleManager::spurt() {
    for (auto& ri : rinfo) ri.clear();
    for (const ParticleInfo& p : particles) {
      auto& toPush = rinfo[(size_t) p.bmIndex];
      toPush.emplace_back();
      ParticleRenderInfo& entry = toPush.back();
      entry.position = p.position;
      entry.rotation = p.rotation;
      entry.dimensions = {p.texcoords.right - p.texcoords.left,
                          p.texcoords.bottom - p.texcoords.top};
      entry.texcoords = p.texcoords;
      entry.blend = unscaleBlend(p.blend);
    }
    // Update offsets vector
    offsets.resize(rinfo.size() + 1);
    offsets[0] = 0;
    for (size_t i = 0; i < rinfo.size(); ++i)
      offsets[1 + i] = offsets[i] + rinfo[i].size();
  }

  void ParticleManager::setUniforms() {
    vao.setActive();
    program.use();
    t.bindTo(0);
    program.uniform1i("tex", 0);
    if (hasSetUniforms) return;
    program.uniform2f("texDimensions", t.getDimensionsF());
    program.uniform2f("screenDimensions", p->getDimensions());
    hasSetUniforms = true;
  }

  void ParticleManager::render() {
    vao.setActive();
    renderUpdate();
    p->getFBOMS().setActive();
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    setUniforms();
    for (size_t i = 0; i < rinfo.size(); ++i) {
      agl::blendModes[i].use();
      // XXX: 4.2-ism here, will have to fix later
      glDrawArraysInstancedBaseInstance(
          GL_TRIANGLE_STRIP, 0, 4, offsets[i + 1] - offsets[i], offsets[i]);
    }
    agl::resetVAO();
  }
}
