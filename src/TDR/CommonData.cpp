#include "TDR/CommonData.h"

#include <iostream>

#include <FileUtil/rwtype.h>

using namespace tdr;

namespace tdr {
  void CommonData::reset() {
    score = 0;
    lives = 2;
    bombs = 3;
    graze = 0;
    integers.clear();
    strings.clear();
  }

  void CommonData::save(std::ostream& fh) const {
    futil::write64u(fh, score);
    futil::write64u(fh, bombs);
    futil::write64u(fh, lives);
    futil::write64u(fh, graze);
    futil::write32s(fh, hitbox.c.x.underlying);
    futil::write32s(fh, hitbox.c.y.underlying);
    futil::write32s(fh, hitbox.r.underlying);
    futil::write32u(fh, integers.size());
    for (const auto& [key, value] : integers) {
      futil::writeStr<uint16_t>(fh, key);
      futil::write64u(fh, value);
    }
    futil::write32u(fh, strings.size());
    for (const auto& [key, value] : strings) {
      futil::writeStr<uint16_t>(fh, key);
      futil::writeStr<uint32_t>(fh, value);
    }
  }

  CommonData::CommonData(std::istream& fh, bool& succ) {
    succ = false;
    score = futil::read64u(fh);
    bombs = futil::read64u(fh);
    lives = futil::read64u(fh);
    graze = futil::read64u(fh);
    hitbox.c.x.underlying = futil::read32s(fh);
    hitbox.c.y.underlying = futil::read32s(fh);
    hitbox.r.underlying = futil::read32s(fh);
    size_t nInts = futil::read32u(fh);
    if (!fh.good()) return;
    for (size_t i = 0; i < nInts; ++i) {
      auto key = futil::readStr<uint16_t>(fh);
      if (!key.has_value()) return;
      uint64_t val = futil::read64u(fh);
      if (!fh.good()) return;
      integers.insert(std::pair(std::move(*key), val));
    }
    size_t nStrs = futil::read32u(fh);
    if (!fh.good()) return;
    for (size_t i = 0; i < nStrs; ++i) {
      auto key = futil::readStr<uint16_t>(fh);
      if (!key.has_value()) return;
      auto val = futil::readStr<uint16_t>(fh);
      if (!val.has_value()) return;
      strings.insert(std::pair(std::move(*key), std::move(*val)));
    }
    succ = true;
  }

  std::optional<CommonData> CommonData::load(std::istream& fh) {
    bool b;
    CommonData cd(fh, b);
    if (!b) return std::nullopt;
    return cd;
  }
}
