#include "TDR/item/ItemList.h"

#include <assert.h>

#include <iostream>
#include <optional>

#include <AGL/Shader.h>
#include <AGL/embedShader.h>

#include "TDR/Playfield.h"

namespace tdr {
  EMBED_SHADER(il_vertex_source, vertex, static)
  EMBED_SHADER(il_fragment_source, fragment, static)

  static thread_local std::optional<agl::Shader> ilVertex, ilFragment;

  static void initILShaders() {
    if (ilVertex.has_value()) return;
    ilVertex.emplace(vertexSource, vertexSourceSize - 1, GL_VERTEX_SHADER);
    ilFragment.emplace(
        fragmentSource, fragmentSourceSize - 1, GL_FRAGMENT_SHADER);
  }

  void
  ItemList::updatePositions(PV playerPosition, bool alive, const Playfield& p) {
    kfp::s16_16 lowerY = p.getHeight() + 16;
    items.update(playerPosition, alive, lowerY);
  }

  void ItemList::clear() noexcept { items.clear(); }

  size_t ItemList::nItems() const noexcept { return items.count(); }

  ItemHandle ItemList::createItemA1(PV position, ShotID type) {
    return items.spawn(position, type, shotsheet.getRectByID(type));
  }

  void ItemList::createItemsA1(size_t count, const PV* position, ShotID type) {
    const ItemTraits& traits = shotsheet.getRectByID(type);
    for (size_t i = 0; i < count; ++i) {
      items.spawn(position[i], type, traits);
    }
  }

  void ItemListView::setUp() {
    assert(p != nullptr);
    initILShaders();
    program.attach(*ilVertex);
    program.attach(*ilFragment);
    program.link();
    hasInitialisedProgram = true;
    vao.setActive();
    // Vertex data
    vbo.feedData(
        sizeof(agl::rectangleVertices), (void*) agl::rectangleVertices,
        GL_DYNAMIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*) 0);
    // InstanceData
    update();
    instanceVBO.setActive();
    glEnableVertexAttribArray(1);
    glVertexAttribIPointer(
        1, 2, GL_INT, sizeof(ItemRenderInfo),
        (GLvoid*) (offsetof(ItemRenderInfo, position)));
    glVertexAttribDivisor(1, 1);
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(
        2, 2, GL_INT, sizeof(ItemRenderInfo),
        (GLvoid*) (offsetof(ItemRenderInfo, dimensions)));
    glVertexAttribDivisor(2, 1);
    glEnableVertexAttribArray(3);
    glVertexAttribIPointer(
        3, 4, GL_SHORT, sizeof(ItemRenderInfo),
        (GLvoid*) (offsetof(ItemRenderInfo, texcoords)));
    glVertexAttribDivisor(3, 1);
    glEnableVertexAttribArray(4);
    glVertexAttribIPointer(
        4, 1, GL_INT, sizeof(ItemRenderInfo),
        (GLvoid*) (offsetof(ItemRenderInfo, rotation)));
    glVertexAttribDivisor(4, 1);
#pragma GCC diagnostic pop
    agl::resetVBO();
    agl::resetVAO();
  }

  void ItemListView::setUniforms() {
    vao.setActive();
    program.use();
    t.bindTo(0);
    program.uniform1i("tex", 0);
    if (hasSetUniforms) return;
    program.uniform2f("texDimensions", t.getDimensionsF());
    program.uniform2f("screenDimensions", p->getDimensions());
    hasSetUniforms = true;
  }

  void ItemListView::update() {
    spurt();
    instanceVBO.feedData(
        offsets.back() * sizeof(EnemyRenderInfo), nullptr, GL_STATIC_DRAW);
    for (size_t i = 0; i < rinfo.size(); ++i) {
      instanceVBO.feedSubdata(
          offsets[i] * sizeof(EnemyRenderInfo),
          (offsets[i + 1] - offsets[i]) * sizeof(EnemyRenderInfo),
          rinfo[i].data());
    }
  }

  void ItemListView::spurt() {
    for (auto& ri : rinfo) ri.clear();
    il->items.spurt(rinfo.data());
    // Update offsets vector
    offsets.resize(rinfo.size() + 1);
    offsets[0] = 0;
    for (size_t i = 0; i < rinfo.size(); ++i)
      offsets[1 + i] = offsets[i] + rinfo[i].size();
  }

  void ItemListView::render() {
    if (!hasInitialisedProgram) setUp();
    vao.setActive();
    update();
    p->getFBOMS().setActive();
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    setUniforms();
    for (size_t i = 0; i < rinfo.size(); ++i) {
      agl::blendModes[i].use();
      // XXX: 4.2-ism here, will have to fix later
      glDrawArraysInstancedBaseInstance(
          GL_TRIANGLE_STRIP, 0, 4, offsets[i + 1] - offsets[i], offsets[i]);
    }
    agl::resetVAO();
  }
}
