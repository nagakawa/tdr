#include "TDR/item/Items.h"

#include <iostream>

#include <kozet_fixed_point/kfp_extra.h>

#include "TDR/item/ItemHandle.h"

namespace tdr {
  ItemHandle
  Items::spawn(PV position, ShotID itemId, const ItemTraits& traits) noexcept {
    bool appended;
    auto gh = ga.allocate(appended);
    agl::UIRect16 tc = traits.texcoords;
    Physics p = {
        position,
        PV{0, traits.initialYVelocity},
        traits.maxSpeed,
        (tc.bottom - tc.top) * "1.2"_s16_16,
        traits.movePattern,
        itemId,
        traits.attractedOnPoc,
        false,
        false,
    };
    Render r = {
        tc,
        {tc.right - tc.left, tc.bottom - tc.top},
        traits.bmIndex,
    };
    if (appended) {
      physicses.push_back(p);
      renders.push_back(r);
    } else {
      physicses[gh.index] = p;
      renders[gh.index] = r;
    }
    return ItemHandle(this, gh);
  }
  void Items::spurt(std::vector<ItemRenderInfo>* rinfo) const noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      if (isDeleted(i)) continue;
      const Physics& p = physicses[i];
      const Render& r = renders[i];
      auto& toPush = rinfo[0];
      toPush.emplace_back();
      EnemyRenderInfo& entry = toPush.back();
      entry.position = p.position;
      entry.rotation = 0;
      entry.dimensions = r.dimensions;
      entry.texcoords = r.texcoords;
    }
  }
  bool Items::shouldFlyToPlayer(
      PV playerPosition, bool alive, const Physics& p,
      kfp::Fixed<int64_t, 32> l2) const noexcept {
    // Never fly to the player if they're dead.
    if (!alive) return false;
    // Items that are automatically attracted toward the player should do so
    // once they start moving down.
    if (p.movePattern == ItemMovePattern::moveToPlayer && p.velocity.y >= 0)
      return true;
    // Items should be attracted to the player if they are above the PoC line.
    if (p.attractedOnPoc && playerPosition.y < poc) return true;
    // Items that are close enough to the player should be attracted.
    auto th2 = kfp::longMultiply(collectRadius, collectRadius);
    if (l2 < th2) return true;
    return false;
  }
  void
  Items::update(PV playerPosition, bool alive, kfp::s16_16 lowerY) noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      Physics& p = physicses[i];
      if (p.position.y > lowerY + p.collisionRadius && !isDeleted(i)) {
        deleteItem(i);
      }
      if (isDeleted(i)) continue;
      PV d = p.position - playerPosition;
      auto l2 = kfp::longMultiply(d.x, d.x) + kfp::longMultiply(d.y, d.y);
      if (l2 <= kfp::longMultiply(p.maxSpeed + 1, p.maxSpeed + 1)) {
        p.collected = true;
      }
      if (shouldFlyToPlayer(playerPosition, alive, p, l2)) {
        p.flyingTowardPlayer = true;
      }
      if (p.flyingTowardPlayer) {
        // Blecch, have to do normalisation ourselves...
        auto l = kfp::sqrt(l2);
        if (l == 0)
          p.velocity = {0, 0};
        else
          p.velocity = -d / (kfp::s16_16) l * p.maxSpeed;
      } else {
        p.velocity.y = std::min(p.velocity.y + gravity, p.maxSpeed);
      }
      p.position += p.velocity;
    }
  }
  void Items::nullifyAttraction() noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      physicses[i].flyingTowardPlayer = false;
      physicses[i].velocity = PV(0, 0);
    }
  }
  void Items::clear() noexcept { ga.clear(); }
}
