#include "TDR/enemy/EnemyList.h"

#include <assert.h>

#include <iostream>
#include <optional>

#include <AGL/Shader.h>
#include <AGL/embedShader.h>

#include "TDR/Playfield.h"
#include "TDR/enemy/Behaviour.h"

namespace tdr {
  EMBED_SHADER(el_vertex_source, vertex, static)
  EMBED_SHADER(el_fragment_source, fragment, static)

  static thread_local std::optional<agl::Shader> elVertex, elFragment;

  static void initELShaders() {
    if (elVertex.has_value()) return;
    elVertex.emplace(vertexSource, vertexSourceSize - 1, GL_VERTEX_SHADER);
    elFragment.emplace(
        fragmentSource, fragmentSourceSize - 1, GL_FRAGMENT_SHADER);
  }

  void EnemyList::updatePositions() { enemies.update(); }

  void EnemyList::clear() noexcept { enemies.clear(); }

  size_t EnemyList::nEnemies() const noexcept { return enemies.count(); }

  EnemyHandle EnemyList::createEnemyB1(
      PV position, PV velocity, ShotID id, int32_t health) {
    const EnemyGraphic& graph = shotsheet.getRectByID(id);
    EnemyHandle h = enemies.spawn(
        position, graph.collisionRadiusToPlayer, graph.collisionRadiusToShot,
        health);
    h.setVelocity(velocity).setTexcoords(graph.texcoords);
    return h;
  }

  EnemyHandle EnemyList::createBoss(
      PV position, kfp::s16_16 collisionRadiusToPlayer,
      kfp::s16_16 collisionRadiusToShot) {
    EnemyHandle h = enemies.spawn(
        position, collisionRadiusToPlayer, collisionRadiusToShot, INT32_MAX);
    h.setBacksheet(true);
    return h;
  }

  void EnemyListView::setUp() {
    assert(p != nullptr);
    initELShaders();
    program.attach(*elVertex);
    program.attach(*elFragment);
    program.link();
    hasInitialisedProgram = true;
    vao.setActive();
    // Vertex data
    vbo.feedData(
        sizeof(agl::rectangleVertices), (void*) agl::rectangleVertices,
        GL_DYNAMIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*) 0);
    // InstanceData
    update();
    instanceVBO.setActive();
    glEnableVertexAttribArray(1);
    glVertexAttribIPointer(
        1, 2, GL_INT, sizeof(EnemyRenderInfo),
        (GLvoid*) (offsetof(EnemyRenderInfo, position)));
    glVertexAttribDivisor(1, 1);
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(
        2, 2, GL_INT, sizeof(EnemyRenderInfo),
        (GLvoid*) (offsetof(EnemyRenderInfo, dimensions)));
    glVertexAttribDivisor(2, 1);
    glEnableVertexAttribArray(3);
    glVertexAttribIPointer(
        3, 4, GL_SHORT, sizeof(EnemyRenderInfo),
        (GLvoid*) (offsetof(EnemyRenderInfo, texcoords)));
    glVertexAttribDivisor(3, 1);
    glEnableVertexAttribArray(4);
    glVertexAttribIPointer(
        4, 1, GL_INT, sizeof(EnemyRenderInfo),
        (GLvoid*) (offsetof(EnemyRenderInfo, rotation)));
    glVertexAttribDivisor(4, 1);
    glEnableVertexAttribArray(5);
    glVertexAttribIPointer(
        5, 1, GL_BYTE, sizeof(EnemyRenderInfo),
        (GLvoid*) (offsetof(EnemyRenderInfo, takesBackSprite)));
    glVertexAttribDivisor(5, 1);
#pragma GCC diagnostic pop
    agl::resetVBO();
    agl::resetVAO();
  }

  void EnemyListView::setUniforms() {
    vao.setActive();
    program.use();
    frontTex.bindTo(0);
    program.uniform1i("tex", 0);
    if (backTex.getID() != 0) {
      backTex.bindTo(1);
      program.uniform1i("backtex", 1);
    }
    if (hasSetUniforms) return;
    program.uniform2f("texDimensions", frontTex.getDimensionsF());
    program.uniform2f("screenDimensions", p->getDimensions());
    hasSetUniforms = true;
  }

  void EnemyListView::update() {
    spurt();
    instanceVBO.feedData(
        offsets.back() * sizeof(EnemyRenderInfo), nullptr, GL_STREAM_DRAW);
    for (size_t i = 0; i < rinfo.size(); ++i) {
      instanceVBO.feedSubdata(
          offsets[i] * sizeof(EnemyRenderInfo),
          (offsets[i + 1] - offsets[i]) * sizeof(EnemyRenderInfo),
          rinfo[i].data());
    }
  }

  void EnemyListView::spurt() {
    // Spurts the render-related data from the `bullets` colony
    // to the `rinfo` vector. This vector will then be used by
    // the render code to draw the bullets.
    for (auto& ri : rinfo) ri.clear();
    el->enemies.spurt(rinfo.data());
    // Update offsets vector
    offsets.resize(rinfo.size() + 1);
    offsets[0] = 0;
    for (size_t i = 0; i < rinfo.size(); ++i)
      offsets[1 + i] = offsets[i] + rinfo[i].size();
  }

  void EnemyListView::render() {
    if (!hasInitialisedProgram) setUp();
    vao.setActive();
    update();
    p->getFBOMS().setActive();
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    setUniforms();
    for (size_t i = 0; i < rinfo.size(); ++i) {
      agl::blendModes[i].use();
      // XXX: 4.2-ism here, will have to fix later
      glDrawArraysInstancedBaseInstance(
          GL_TRIANGLE_STRIP, 0, 4, offsets[i + 1] - offsets[i], offsets[i]);
    }
    agl::resetVAO();
  }
}
