#include "TDR/enemy/BossScene.h"

namespace tdr {
  SceneResult BossScene::startNextAttack() noexcept {
    if (activeStep == notStarted) {
      activeStep = 0;
      activeAttack = 0;
    } else {
      ++activeAttack;
    }
    while (activeStep < steps.size() &&
           activeAttack >= steps[activeStep].attacks.size()) {
      ++activeStep;
      activeAttack = 0;
    }
    if (activeStep >= steps.size()) return SceneResult::end;
    // Reset life and bomb counts
    st.nMiss = 0;
    st.nBomb = 0;
    // Set timer
    const BossAttack& at = *(currentAttack());
    st.timeRemaining = at.startTime;
    // Set boss health
    boss.setHealth(at.health);
    // Start the attack
    at.handler();
    return SceneResult::change;
  }
  SceneResult BossScene::tick() noexcept {
    if (activeStep != notStarted && activeStep >= steps.size())
      return SceneResult::end;
    if (activeStep == notStarted || st.timeRemaining == 0 ||
        boss.getHealth() <= 0) {
      // Time is up; proceed to next attack
      return startNextAttack();
    }
    --st.timeRemaining;
    return SceneResult::normal;
  }
  void BossScene::addBossAttack(size_t stepId, BossAttack&& at) {
    if (stepId >= steps.size()) steps.resize(stepId + 1);
    BossStep& step = steps[stepId];
    step.attacks.push_back(std::move(at));
  }
}
