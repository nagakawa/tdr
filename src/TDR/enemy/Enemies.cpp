#include "TDR/enemy/Enemies.h"

#include "TDR/enemy/Behaviour.h"
#include "TDR/enemy/EnemyHandle.h"

namespace tdr {
  EnemyHandle Enemies::spawn(
      PV position, kfp::s16_16 collisionRadiusToPlayer,
      kfp::s16_16 collisionRadiusToShot, int32_t health) noexcept {
    bool appended;
    auto gh = ga.allocate(appended);
    Physics p = {
        position,
        PV{0, 0},
        collisionRadiusToPlayer,
        collisionRadiusToShot,
        DamageRate{},
        health,
        true,
    };
    if (appended) {
      physicses.push_back(p);
      renders.emplace_back();
      behaviours.emplace_back();
    } else {
      physicses[gh.index] = p;
      behaviours[gh.index] = nullptr;
    }
    return EnemyHandle(this, gh);
  }
  void Enemies::spurt(std::vector<EnemyRenderInfo>* rinfo) const noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      if (isDeleted(i)) continue;
      const Physics& p = physicses[i];
      const Render& r = renders[i];
      auto& toPush = rinfo[0];
      toPush.emplace_back();
      EnemyRenderInfo& entry = toPush.back();
      entry.position = p.position;
      entry.rotation = 0;
      entry.dimensions = r.dimensions;
      entry.texcoords = r.texcoords;
      entry.takesBackSprite = r.takesBackSprite;
    }
  }
  void Enemies::update() noexcept {
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      Physics& p = physicses[i];
      p.position += p.velocity;
    }
    for (BulletIndex i = 0; i < physicses.size(); ++i) {
      if (isDeleted(i)) continue;
      Physics& p = physicses[i];
      EnemyBehaviour* beh = behaviours[i].get();
      if (p.health < 0) {
        if (beh != nullptr) {
          EnemyHandle h(this, GAH{ga.getGeneration(i), i});
          beh->onDeath(h);
        }
        if (p.autoDelete) deleteEnemy(i);
      }
      if (beh != nullptr) {
        EnemyHandle h(this, GAH{ga.getGeneration(i), i});
        beh->tick(h);
      }
    }
  }
  bool Enemies::collidesWithPlayer(BulletIndex i, const Circle& c) const
      noexcept {
    return physicses[i].hitboxToPlayer().intersects(c);
  }
  bool Enemies::collidesWithShot(BulletIndex i, const Circle& c) const
      noexcept {
    return physicses[i].hitboxToShot().intersects(c);
  }
  void Enemies::clear() noexcept { ga.clear(); }
}
