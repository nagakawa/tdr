#include "TDR/Player.h"

#include <kozet_fixed_point/kfp.h>

#include "TDR/Game.h"

namespace tdr {
  void PlayerState::evolve(const PlayerTraits& traits) noexcept {
    if (flag == PlayerStateFlag::hit && timeLeft == 0) {
      flag = PlayerStateFlag::down;
      timeLeft = traits.respawnTime;
    }
    if (flag == PlayerStateFlag::down && timeLeft == 0) {
      flag = PlayerStateFlag::normal;
      timeLeft = traits.respawnInvincibilityTime;
    }
    if (timeLeft != 0) --timeLeft;
  }

  PlayerTraits defaultPlayerTraits = {
      /* .unfocusedSpeed = */ 7,
      /* .focusedSpeed = */ 3,
      /* .startingBombs = */ 4,
      /* .startingLives = */ 2,
      /* .deathbombTime = */ 15,
      /* .respawnTime = */ 60,
      /* .respawnInvincibilityTime = */ 120,
      /* .hitRadius = */ "1.5"_s16_16,
      /* .grazeRadius = */ 20,
  };
}
