#version 330 core

layout (location = 0) in vec2 bounds;
layout (location = 1) in ivec2 position;
layout (location = 2) in ivec2 dimensions;
layout (location = 3) in ivec4 texcoords;
layout (location = 4) in int rotation;
out vec2 texCoord;
uniform vec2 texDimensions;
uniform vec2 screenDimensions;

const float fixedTurnsToRadians =
  2 * 3.14159265358979323 / 4294967296.0;

void main() {
	texCoord = vec2(
		mix(texcoords.x, texcoords.z, bounds.x),
		mix(texcoords.y, texcoords.w, bounds.y)) / texDimensions;
	float angle = rotation * fixedTurnsToRadians;
	mat2 rm = mat2(cos(angle), sin(angle), -sin(angle), cos(angle));
  vec2 nbounds = bounds * 2.0f - 1.0f;
	vec2 pos =
    position + rm * (nbounds * dimensions / 2);
  // Normalise to [0, 1]
  pos /= 65536;
  pos /= screenDimensions;
  // Normalise to [-1, 1]
  pos = pos * 2.0f - 1.0f;
  pos.y *= -1.0f;
	gl_Position = vec4(pos, 1.0f, 1.0f);
}
