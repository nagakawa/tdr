#version 330 core

in vec2 texCoord;
// GLSL doesn't allow varying bools, so we have to take it as an int instead.
flat in int takesBackSprite;
out vec4 colour;
uniform sampler2D tex;
uniform sampler2D backtex;

void main() {
  if (takesBackSprite != 0)
    colour = texture(backtex, texCoord);
  else
    colour = texture(tex, texCoord);
}
