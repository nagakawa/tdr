#version 330 core

layout(location = 0) in vec2 bounds;
layout(location = 1) in ivec2 position;
layout(location = 2) in ivec3 visualParams; // (angle, length, width)
layout(location = 3) in ivec4 shottc;
layout(location = 4) in int isLaser;
layout(location = 5) in uvec4 myBlend;
out vec2 texCoord;
out vec4 blend;
uniform vec2 texDimensions;
uniform vec2 screenDimensions;
uniform uint angleOffset;

const float fixedTurnsToRadians = 2 * 3.14159265358979323 / 4294967296.0;

mat2 angleToMat(float angle) {
  return mat2(cos(angle), sin(angle), -sin(angle), cos(angle));
}

void main() {
  vec2 tcBounds =
      angleToMat(angleOffset * fixedTurnsToRadians) * (bounds - 0.5) + 0.5;
  texCoord = vec2(
                 mix(shottc.x, shottc.z, tcBounds.x),
                 mix(shottc.y, shottc.w, tcBounds.y)) /
             texDimensions;
  float angle = visualParams.x * fixedTurnsToRadians;
  mat2 rm = angleToMat(angle);
  vec2 nbounds = bounds * 2.0f - 1.0f;
  vec2 pos = position + rm * (nbounds * visualParams.yz);
  // Normalise to [0, 1]
  pos /= 65536;
  pos /= screenDimensions;
  // Normalise to [-1, 1]
  pos = pos * 2.0f - 1.0f;
  pos.y *= -1.0f;
  gl_Position = vec4(pos, 1.0f, 1.0f);
  blend = vec4(myBlend) / 255.0;
}
