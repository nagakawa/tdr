#version 330 core

in vec2 texCoord;
in vec4 blend;
out vec4 colour;
uniform sampler2D tex;

void main() { colour = texture(tex, texCoord) * blend; }
