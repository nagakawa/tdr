#include "TDR/bullet/BulletList.h"

#include <assert.h>

#include <AGL/Shader.h>
#include <AGL/embedShader.h>

#include "TDR/Playfield.h"

namespace tdr {
  EMBED_SHADER(bl_vertex_source, vertex, static)
  EMBED_SHADER(bl_fragment_source, fragment, static)

  static thread_local std::optional<agl::Shader> blVertex, blFragment;

  static void initBLShaders() {
    if (blVertex.has_value()) return;
    blVertex.emplace(vertexSource, vertexSourceSize - 1, GL_VERTEX_SHADER);
    blFragment.emplace(
        fragmentSource, fragmentSourceSize - 1, GL_FRAGMENT_SHADER);
  }

  void BulletList::updatePositions(const agl::IRect16& bounds) {
    bullets.update(bounds);
  }

  BulletHandle BulletList::createShotA1(
      PV position, kfp::s16_16 speed, kfp::frac32 angle, ShotID id,
      uint8_t delay) {
    Bullets::Hitbox hb;
    hb.circle.c = position;
    hb.circle.r = 0;
    auto h = bullets.spawn(hb, false, id);
    h.setSpeed(speed)
        .setAngle(angle)
        .setVisualAngle(angle)
        .setGrazeFrequency(defaultBulletGrazeFrequency)
        .setDelay(delay);
    return h;
  }

  BulletHandle BulletList::createLooseLaser(
      PV position, kfp::s16_16 speed, kfp::frac32 angle, kfp::s16_16 length,
      kfp::s16_16 width, ShotID id, uint8_t delay) {
    Bullets::Hitbox hb;
    hb.circle.c = position;
    hb.circle.r = width / 2;
    // Compensate for additional length from round ends
    hb.len = (length - width) / 2;
    hb.refpoint = 1;
    auto h = bullets.spawn(hb, true, id);
    h.setSpeed(speed)
        .setAngle(angle)
        .setVisualAngle(angle)
        .setVisualWidth(width / 2)
        .setVisualLength(length / 2)
        .setGrazeFrequency(defaultLaserGrazeFrequency)
        .setSpellResistant(true)
        .setDelay(delay);
    return h;
  }

  void BulletListView::setUp() {
    assert(p != nullptr);
    initBLShaders();
    program.attach(*blVertex);
    program.attach(*blFragment);
    program.link();
    hasInitialisedProgram = true;
    vao.setActive();
    // Vertex data
    vbo.feedData(
        sizeof(agl::rectangleVertices), (void*) agl::rectangleVertices,
        GL_DYNAMIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*) 0);
    // InstanceData
    update();
    // The shader needs to know about the following fields:
    // (centre of bullet)
    // visualAngle visualWidth visualLength
    // texcoords
    // So, without further ado:
    instanceVBO.setActive();
    glEnableVertexAttribArray(1);
    // Would've loved to use GL_FIXED for this and be done with that,
    // but sadly, it's GL4.2+.
    glVertexAttribIPointer(
        1, 2, GL_INT, sizeof(BulletRenderInfo),
        (GLvoid*) (offsetof(BulletRenderInfo, position)));
    glVertexAttribDivisor(1, 1);
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(
        2, 3, GL_INT, sizeof(BulletRenderInfo),
        (GLvoid*) (offsetof(BulletRenderInfo, visualAngle)));
    glVertexAttribDivisor(2, 1);
    glEnableVertexAttribArray(3);
    glVertexAttribIPointer(
        3, 4, GL_SHORT, sizeof(BulletRenderInfo),
        (GLvoid*) (offsetof(BulletRenderInfo, texcoords)));
    glVertexAttribDivisor(3, 1);
    glEnableVertexAttribArray(4);
    glVertexAttribIPointer(
        4, 1, GL_INT, sizeof(BulletRenderInfo),
        (GLvoid*) (offsetof(BulletRenderInfo, isLaser)));
    glVertexAttribDivisor(4, 1);
    glEnableVertexAttribArray(5);
    glVertexAttribIPointer(
        5, 4, GL_UNSIGNED_BYTE, sizeof(BulletRenderInfo),
        (GLvoid*) (offsetof(BulletRenderInfo, blendColour)));
    glVertexAttribDivisor(5, 1);
#pragma GCC diagnostic pop
    agl::resetVBO();
    agl::resetVAO();
  }

  void BulletListView::setUniforms() {
    vao.setActive();
    program.use();
    t.bindTo(0);
    program.uniform1i("tex", 0);
    if (hasSetUniforms) return;
    program.uniform2f("texDimensions", t.getDimensionsF());
    program.uniform2f("screenDimensions", p->getDimensions());
    program.uniform1ui("angleOffset", angleOffset.underlying);
    hasSetUniforms = true;
  }

  void BulletListView::render() {
    if (!hasInitialisedProgram) setUp();
    vao.setActive();
    update();
    p->getFBOMS().setActive();
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    setUniforms();
    for (size_t i = 0; i < rinfo.size(); ++i) {
      agl::blendModes[i].use();
      // XXX: 4.2-ism here, will have to fix later
      glDrawArraysInstancedBaseInstance(
          GL_TRIANGLE_STRIP, 0, 4, offsets[i + 1] - offsets[i], offsets[i]);
    }
    agl::resetVAO();
  }

  void BulletListView::update() {
    // Set data area
#if AGL_PROFILER_TDR_GOODIES
    double t1 = glfwGetTime();
#endif
    spurt();
    instanceVBO.feedData(
        offsets.back() * sizeof(BulletRenderInfo), nullptr, GL_STREAM_DRAW);
    // Set individual chunks
    for (size_t i = 0; i < rinfo.size(); ++i) {
      instanceVBO.feedSubdata(
          offsets[i] * sizeof(BulletRenderInfo),
          (offsets[i + 1] - offsets[i]) * sizeof(BulletRenderInfo),
          rinfo[i].data());
    }
#if AGL_PROFILER_TDR_GOODIES
    double t2 = glfwGetTime();
    spurtTime = t2 - t1;
#endif
  }

  void BulletListView::spurt() {
    // Spurts the render-related data from the `bullets` colony
    // to the `rinfo` vector. This vector will then be used by
    // the render code to draw the bullets.
    for (auto& ri : rinfo) ri.clear();
    bl->bullets.spurt(rinfo.data(), delayCloud);
    // Update offsets vector
    offsets.resize(rinfo.size() + 1);
    offsets[0] = 0;
    for (size_t i = 0; i < rinfo.size(); ++i)
      offsets[1 + i] = offsets[i] + rinfo[i].size();
  }

  size_t BulletList::nBullets() const { return bullets.count(); }

  void BulletList::clear() { bullets.clear(); }
}
