#include "TDR/bullet/Bullets.h"

#include <iostream>

#include <FileUtil/likely_unlikely.h>
#include <kozet_fixed_point/kfp.h>
#include <kozet_fixed_point/kfp_extra.h>
#include <kozet_fixed_point/kfp_string.h>
#include <zekku/geometry.h>
#include <zekku/kfp_interop/timath.h>
#include <zekku/timath.h>

namespace tdr {
  Bullets::Render::Render(ShotID id, const Graphic& gr) :
      visualWidth(gr.visualRadius), visualLength(gr.visualRadius), shotId(id),
      bmIndex(gr.renderMode) {}

  static kfp::Fixed<int64_t, 32> distPS2(PV p, PV l0, PV l1) {
    // http://paulbourke.net/geometry/pointlineplane/
    using kfp::longMultiply;
    if (l0 == l1) {
      auto d = l0 - p;
      return kfp::hypot2(d.x, d.y);
    }
    auto len2 = kfp::hypot2(l1.x - l0.x, l1.y - l0.y);
    auto num = longMultiply(p.x - l0.x, l1.x - l0.x) +
               longMultiply(p.y - l0.y, l1.y - l0.y);
    auto u = (kfp::s16_16)(num / len2);
    if (u < 0) u = 0;
    if (u > 1) u = 1;
    auto l = l0 + (l1 - l0) * u;
    return kfp::hypot2(l.x - p.x, l.y - p.y);
  }
  static kfp::Fixed<int64_t, 32> distSS2(PV p0, PV p1, PV l0, PV l1) {
    kfp::Fixed<int64_t, 32> d1 = distPS2(p0, l0, l1), d2 = distPS2(p1, l0, l1),
                            d3 = distPS2(l0, p0, p1), d4 = distPS2(l1, p0, p1);
    return std::min({d1, d2, d3, d4});
  }

  bool Bullets::collidesWith(BulletIndex i, const Circle& c) const noexcept {
    using kfp::longMultiply;
    if (getDelayOf(i) > 0) return false;
    const auto& h = hitboxes[i];
    if ((bulletFlags[i] & BulletFlags::collides) == 0) return false;
    IF_LIKELY ((bulletFlags[i] & BulletFlags::isLaser) == 0 || h.len == 0) {
      return h.circle.intersects(c);
    }
    // Find the distance of the point ((centre of laser) - h.circle.c)
    // with the line segment (0 + t * s), -1 <= t <= 1
    PV p1 = c.c - getTrueCentreAssumingLaser(i);
    kfp::s2_30 ca, sa;
    kfp::sincos(h.visualAngle, ca, sa);
    PV s(h.len * ca, h.len * sa);
    auto dotprod = kfp::longMultiply(p1.x, s.x) + kfp::longMultiply(p1.y, s.y);
    kfp::s16_16 t = (kfp::s16_16)(dotprod / kfp::longMultiply(h.len, h.len));
    if (t < -1) t = -1;
    if (t > 1) t = 1;
    PV p2 = s * t;
    PV dp = p2 - p1;
    auto distsq = kfp::longMultiply(dp.x, dp.x) + kfp::longMultiply(dp.y, dp.y);
    auto thresh = kfp::longMultiply(c.r + h.circle.r, c.r + h.circle.r);
    return distsq < thresh;
  }

  bool Bullets::collidesWith(
      BulletIndex i, const zekku::AABB<kfp::s16_16>& box) const noexcept {
    using kfp::longMultiply;
    if (getDelayOf(i) > 0) return false;
    if ((bulletFlags[i] & BulletFlags::collides) == 0) return false;
    IF_LIKELY ((bulletFlags[i] & BulletFlags::isLaser) == 0) {
      return hitboxes[i].circle.intersects(box);
    }
    return !outOfBounds(i, box);
  }

  glm::tvec2<kfp::s16_16> Bullets::getTrueCentre(BulletIndex i) const noexcept {
    const Hitbox& hitbox = hitboxes[i];
    IF_LIKELY ((bulletFlags[i] & BulletFlags::isLaser) == 0)
      return hitbox.circle.c;
    return getTrueCentreAssumingLaser(i);
  }

  glm::tvec2<kfp::s16_16>
  Bullets::getTrueCentreAssumingLaser(BulletIndex i) const noexcept {
    const Hitbox& hitbox = hitboxes[i];
    kfp::s16_16 scaledRefpoint = -hitbox.len * hitbox.refpoint;
    kfp::s2_30 ca, sa;
    kfp::sincos(hitbox.visualAngle, ca, sa);
    glm::tvec2<kfp::s16_16> offset(scaledRefpoint * ca, scaledRefpoint * sa);
    return hitbox.circle.c + offset;
  }

  bool
  Bullets::outOfBounds(BulletIndex i, const zekku::AABB<kfp::s16_16>& box) const
      noexcept {
    const Hitbox& hitbox = hitboxes[i];
    if ((bulletFlags[i] & BulletFlags::isLaser) == 0) {
      IF_LIKELY (box.contains(hitbox.circle.c))
        return false;
      return !hitbox.circle.intersects(box);
    }
    // Find the distance between a line segment and an AABB:
    // https://groups.google.com/forum/#!topic/comp.graphics.algorithms/fW-nWhiHxK8
    using kfp::longMultiply;
    kfp::s2_30 ca, sa;
    kfp::sincos(hitbox.visualAngle, ca, sa);
    PV s(hitbox.len * ca, hitbox.len * sa);
    auto tc = getTrueCentreAssumingLaser(i);
    auto p0 = tc - s, p1 = tc + s;
    if (box.contains(p0) || box.contains(p1)) return false;
    auto thresh = kfp::longMultiply(hitbox.circle.r, hitbox.circle.r);
    auto nw = box.nwp(), ne = box.nep(), sw = box.swp(), se = box.sep();
    if (distSS2(nw, ne, p0, p1) < thresh) return false;
    if (distSS2(sw, se, p0, p1) < thresh) return false;
    if (distSS2(nw, sw, p0, p1) < thresh) return false;
    if (distSS2(ne, se, p0, p1) < thresh) return false;
    return true;
  }

  BulletHandle Bullets::spawn(Hitbox h, bool isLaser, ShotID id) noexcept {
    const Graphic& graph = shotsheet.getRectByID(id);
    if (h.circle.r == 0) h.circle.r = graph.collisionRadius;
    bool appended;
    auto gh = ga.allocate(appended);
    BulletFlags flags = BulletFlags::defo;
    if (isLaser) flags |= BulletFlags::isLaser;
    if (appended) {
      bulletFlags.push_back(flags);
      hitboxes.push_back(h);
      physicses.emplace_back();
      renders.emplace_back(id, graph);
      grazes.emplace_back();
      damages.push_back(65535);
      delays.push_back({0, 0});
    } else {
      bulletFlags[gh.index] = flags;
      hitboxes[gh.index] = h;
      damages[gh.index] = 65535;
      delays[gh.index] = {0, 0};
      renders[gh.index] = Render(id, graph);
    }
    return BulletHandle(this, gh);
  }

  void Bullets::refreshParametersWithoutCleaning(BulletIndex i) noexcept {
    // Refresh parameters for dirty bullets
    if (!ga.isLive(i) || (bulletFlags[i] & BulletFlags::dirty) == 0) return;
    Physics& p = physicses[i];
    if ((bulletFlags[i] & BulletFlags::useRadial) != 0) {
      kfp::s2_30 ac, as;
      kfp::sincos(p.angle, ac, as);
      p.velocity = PV(p.speed * ac, p.speed * as);
    } else {
      kfp::rectp(p.velocity.x, p.velocity.y, p.speed, p.angle);
    }
    if ((bulletFlags[i] & BulletFlags::detachVisualAndMovementAngles) != 0) {
      Hitbox& h = hitboxes[i];
      h.visualAngle = p.angle;
    }
  }

  void Bullets::refreshParameters(BulletIndex i) noexcept {
    refreshParametersWithoutCleaning(i);
    bulletFlags[i] &= ~BulletFlags::dirty;
  }

  void Bullets::refreshParameters() noexcept {
    // Refresh parameters for dirty bullets
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      refreshParametersWithoutCleaning(i);
    }
    // Loop through all entries, even those that are dead, because doing so
    // doesn't matter anyway and we want this to auto-vectorise.
    for (auto& flag : bulletFlags) { flag &= ~BulletFlags::dirty; }
  }

  void Bullets::refreshGraze() noexcept {
    // Loop through all entries, even those that are dead, because doing so
    // doesn't matter anyway and we want this to auto-vectorise.
    for (BulletIndex i = 0; i < grazes.size(); ++i) {
      Graze& g = grazes[i];
      if (g.timeToNextGraze != 0 && g.grazeFrequency != -1) --g.timeToNextGraze;
    }
  }

  void Bullets::update(const agl::IRect16& bounds) noexcept {
    refreshParameters();
    // Do actual physics
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      // Ignore the dead or delayed entries for now, since this is a pretty
      // intensive calculation.
      if (isDeleted(i) || getDelayOf(i) > 0) continue;
      Physics& p = physicses[i];
      if ((bulletFlags[i] & BulletFlags::useRadial) != 0) {
        p.angle += p.angularVelocity;
        if (p.angularVelocity != 0) { refreshParametersWithoutCleaning(i); }
      } else {
        p.velocity += p.acceleration;
        if (p.acceleration.x != 0 && p.acceleration.y != 0)
          bulletFlags[i] |= BulletFlags::dirty;
      }
      if ((bulletFlags[i] & BulletFlags::detachVisualAndMovementAngles) != 0) {
        Hitbox& h = hitboxes[i];
        h.visualAngle = p.angle;
      }
      Hitbox& h = hitboxes[i];
      h.circle.c += p.velocity;
    }
    // Update delay
    for (BulletIndex i = 0; i < delays.size(); ++i) {
      if (getDelayOf(i) != 0) --delays[i].current;
    }
    refreshGraze();
    zekku::AABB<kfp::s16_16> brect = {
        {
            kfp::s16_16(bounds.left + bounds.right) / 2,
            kfp::s16_16(bounds.top + bounds.bottom) / 2,
        },
        {
            kfp::s16_16(bounds.right - bounds.left) / 2,
            kfp::s16_16(bounds.bottom - bounds.top) / 2,
        }};
    // Delete OOB bullets
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      if (isDeleted(i) || getDelayOf(i) > 0) continue;
      if ((bulletFlags[i] & BulletFlags::deleteWhenOutOfBounds) != 0 &&
          outOfBounds(i, brect)) {
        deleteBullet(i);
      }
    }
    // Update rendering
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      if (isDeleted(i) || getDelayOf(i) > 0) continue;
      Render& r = renders[i];
      const Graphic& gr = getGraphicOf(r);
      if ((bulletFlags[i] & BulletFlags::isLaser) == 0)
        r.currentAngleOffset += gr.intrinsicAngularVelocity;
      r.frame = (r.frame + 1) % gr.texcoords.period();
    }
  }
  void
  Bullets::pushItemList(BulletIndex i, std::vector<tdr::PV>& positions) const
      noexcept {
    if ((bulletFlags[i] & BulletFlags::shouldDropItem) == 0) return;
    const Hitbox& h = hitboxes[i];
    if ((bulletFlags[i] & BulletFlags::isLaser) == 0) {
      positions.push_back(h.circle.c);
    } else {
      kfp::s16_16 len = h.len;
      PV centre = getTrueCentreAssumingLaser(i);
      kfp::frac32 angle = h.visualAngle;
      auto [c, s] = kfp::sincos(angle);
      PV xy = centre - PV{len * c, len * s};
      PV advance =
          PV{laserErasureItemDistance * c, laserErasureItemDistance * s};
      for (kfp::s16_16 pos = -len; pos < len + 1;
           pos += laserErasureItemDistance) {
        positions.push_back(xy);
        xy += advance;
      }
    }
  }
  void Bullets::pushItemList(std::vector<tdr::PV>& positions) const noexcept {
    positions.reserve(hitboxes.size());
    for (size_t i = 0; i < hitboxes.size(); ++i) {
      if (!isDeleted(i)) pushItemList(i, positions);
    }
  }

  size_t Bullets::count() const noexcept { return ga.countExtant(); }

  void Bullets::clear() noexcept { ga.clear(); }

  void Bullets::spurt(
      std::vector<BulletRenderInfo>* rinfo, const Graphic& delayCloud) const
      noexcept {
    for (BulletIndex i = 0; i < bulletFlags.size(); ++i) {
      if (!ga.isLive(i)) continue;
      bool isDelay = getDelayOf(i) > 0;
      const Render& r = renders[i];
      auto& toPush =
          rinfo[(size_t)(isDelay ? delayCloud.renderMode : r.bmIndex)];
      toPush.emplace_back();
      BulletRenderInfo& entry = toPush.back();
      if (!isDelay) {
        entry.isLaser = (bulletFlags[i] & BulletFlags::isLaser) != 0;
        entry.position = getTrueCentre(i);
        entry.visualAngle = hitboxes[i].visualAngle + r.currentAngleOffset;
        entry.texcoords = getRectOf(r);
        entry.visualLength = r.visualLength;
        entry.visualWidth = r.visualWidth;
        glm::u8vec4 blend = r.blend;
        blend.a = blend.a * shotsheet.getRectByID(r.shotId).alpha / 255;
        entry.blendColour = r.blend;
      } else {
        entry.isLaser = false;
        entry.position = hitboxes[i].circle.c;
        entry.visualAngle = 0;
        entry.texcoords = delayCloud.texcoords.at(0);
        entry.visualLength = delayCloud.visualRadius;
        entry.visualWidth = delayCloud.visualRadius;
        uint8_t alpha = delayCloud.alpha * delays[i].current / delays[i].max;
        entry.blendColour = {getDelayColourOf(r), alpha};
      }
    }
  }

  agl::UIRect16 Bullets::getRectOf(const Render& r) const noexcept {
    ShotID id = r.shotId;
    const Graphic& gr = shotsheet.getRectByID(id);
    return gr.texcoords.at(r.frame);
  }

  glm::u8vec3 Bullets::getDelayColourOf(const Render& r) const noexcept {
    ShotID id = r.shotId;
    const Graphic& gr = shotsheet.getRectByID(id);
    return gr.delayColour;
  }

  const Graphic& Bullets::getGraphicOf(const Render& r) const noexcept {
    ShotID id = r.shotId;
    const Graphic& gr = shotsheet.getRectByID(id);
    return gr;
  }
};
