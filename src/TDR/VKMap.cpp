#include "TDR/VKMap.h"

#include <string.h>

#define GLEW_STATIC
#include <GLFW/glfw3.h>

namespace tdr {
  int defaultVKMap[] = {
      GLFW_KEY_LEFT, GLFW_KEY_RIGHT, GLFW_KEY_UP,         GLFW_KEY_DOWN,
      GLFW_KEY_Z,    GLFW_KEY_X,     GLFW_KEY_LEFT_SHIFT, GLFW_KEY_C,
  };
  VKMap getDefaultVKMap() {
    VKMap res(sizeof(defaultVKMap) / sizeof(defaultVKMap[0]));
    memcpy(res.data(), defaultVKMap, sizeof(defaultVKMap));
    return res;
  }
}
