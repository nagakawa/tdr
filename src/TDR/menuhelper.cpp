#include "TDR/menuhelper.h"

namespace tdr {
  int menuHelper(
      int option, int count, agl::KeyStatus up, agl::KeyStatus down,
      int16_t& ticks, const MenuOptions& opts) {
    int off = 0;
    if (up == agl::KeyStatus::enter) {
      --off;
      ticks = opts.initialDelay;
    }
    if (down == agl::KeyStatus::enter) {
      ++off;
      ticks = opts.initialDelay;
    }
    if (down == agl::KeyStatus::hold) {
      --ticks;
      if (ticks == 0) {
        ++off;
        ticks = opts.repeatDelay;
      }
    } else if (up == agl::KeyStatus::hold) {
      --ticks;
      if (ticks == 0) {
        --off;
        ticks = opts.repeatDelay;
      }
    }
    int newOption = (option + off) % count;
    if (newOption < 0) newOption += count;
    return newOption;
  }
}
