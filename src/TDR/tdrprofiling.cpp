#include "TDR/tdrprofiling.h"

#include <string.h>

#include <FileUtil/rwtype.h>

namespace tdr {
  static agl::ProfilerTrack defaultTracks[] = {
      {
          "Frame Time",
          agl::ProfilerTrackType::duration,
      },
      {
          "Tick Time",
          agl::ProfilerTrackType::duration,
      },
#if AGL_PROFILER_TDR_GOODIES
      {
          "Update Time: Game::update()",
          agl::ProfilerTrackType::duration,
      },
      {
          "Bullet Update Time: Bullets::updatePositions()",
          agl::ProfilerTrackType::duration,
      },
      {
          "Collision Check Time: Game::checkForCollision()",
          agl::ProfilerTrackType::duration,
      },
      {
          "Render Time: Game::render()",
          agl::ProfilerTrackType::duration,
      },
      {
          "Bullet Count",
          agl::ProfilerTrackType::count,
      },
      {
          "User Update Time",
          agl::ProfilerTrackType::duration,
      },
      {
          "BulletList Spurt Time",
          agl::ProfilerTrackType::duration,
      },
#endif
  };
  agl::ProfilerOptions defaultProfilerOptions = {
      "TDR application",
      sizeof(defaultTracks) / sizeof(defaultTracks[0]),
      defaultTracks,
  };
}
