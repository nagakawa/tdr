/*
  Helper file for FILE* / stream boilerplate: meant to be included multiple
  times.
  Define the following:
  * IHandle: input file type
  * OHandle: output file type
  * READ(fh, buf, n): macro to read bytes
  * WRITE(fh, buf, n): macro to write bytes
*/

void write16u(OHandle fh, uint16_t n) {
  char buf[2] = {
      (char) (n),
      (char) (n >> 8),
  };
  WRITE(fh, buf, 2);
}

void write32u(OHandle fh, uint32_t n) {
  char buf[8] = {
      (char) (n),
      (char) (n >> 8),
      (char) (n >> 16),
      (char) (n >> 24),
  };
  WRITE(fh, buf, 4);
}

void write64u(OHandle fh, uint64_t n) {
  char buf[8] = {
      (char) (n),       (char) (n >> 8),  (char) (n >> 16), (char) (n >> 24),
      (char) (n >> 32), (char) (n >> 40), (char) (n >> 48), (char) (n >> 56),
  };
  WRITE(fh, buf, 8);
}

void write16s(OHandle fh, int16_t n) { write16u(fh, (uint16_t) n); }

void write32s(OHandle fh, int32_t n) { write32u(fh, (uint32_t) n); }

void write64s(OHandle fh, int64_t n) { write64u(fh, (uint64_t) n); }

uint16_t read16u(IHandle fh) {
  char buf[2];
  READ(fh, buf, 2);
  return ((uint8_t) buf[0]) | ((uint8_t) buf[1]) << 8;
}

uint32_t read32u(IHandle fh) {
  char buf[4];
  READ(fh, buf, 4);
  return ((uint8_t) buf[0]) | ((uint8_t) buf[1]) << 8 |
          ((uint8_t) buf[2]) << 16 | ((uint8_t) buf[3]) << 24;
}

uint64_t read64u(IHandle fh) {
  char buf[8];
  READ(fh, buf, 8);
  return ((uint64_t)(uint8_t) buf[0]) | ((uint64_t)(uint8_t) buf[1]) << 8 |
          ((uint64_t)(uint8_t) buf[2]) << 16 |
          ((uint64_t)(uint8_t) buf[3]) << 24 |
          ((uint64_t)(uint8_t) buf[4]) << 32 |
          ((uint64_t)(uint8_t) buf[5]) << 40 |
          ((uint64_t)(uint8_t) buf[6]) << 48 |
          ((uint64_t)(uint8_t) buf[7]) << 56;
}

int16_t read16s(IHandle fh) { return (int16_t) read16u(fh); }

int32_t read32s(IHandle fh) { return (int32_t) read32u(fh); }

int64_t read64s(IHandle fh) { return (int64_t) read64u(fh); }
