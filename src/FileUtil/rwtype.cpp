#include "FileUtil/rwtype.h"

#include <iostream>

/*
  -O2 or higher allows these functions to be optimised into a single
  write call with no shifts
  Clang doesn't recognise the pragma, though
*/
#if defined(__GNUC__) && !defined(__clang__)
#  pragma GCC optimize(3)
#endif

namespace futil {
#define IHandle std::istream&
#define OHandle std::ostream&
#define READ(fh, buf, n) fh.read(buf, n)
#define WRITE(fh, buf, n) fh.write(buf, n)
#include "./rwtype_helper.inc"
#undef IHandle
#undef OHandle
#undef READ
#undef WRITE
#define IHandle FILE*
#define OHandle FILE*
#define READ(fh, buf, n) fread(buf, n, 1, fh)
#define WRITE(fh, buf, n) fwrite(buf, n, 1, fh)
#include "./rwtype_helper.inc"
}
