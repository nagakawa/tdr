#include "FileUtil/exe_location.h"

#include <assert.h>

#ifdef __linux
#  include <linux/limits.h>
#  include <sys/stat.h>
#  include <sys/types.h>
#  include <unistd.h>
#elif _WIN32
#  include <windows.h>
#else
#  error "Platform not supported"
#endif

namespace futil {
#ifdef __linux__
  // Linux: /proc/self/exe is a symlink to the exe
  std::optional<std::string> getExeLocation() {
    struct stat s;
    int err = lstat("/proc/self/exe", &s);
    if (err != 0) return std::nullopt;
    off_t size = s.st_size;
    if (size == 0) size = PATH_MAX;
    std::string path;
    path.resize(size);
    ssize_t readSize = readlink("/proc/self/exe", path.data(), size);
    if (readSize < 0 || readSize > size) return std::nullopt;
    path.resize(readSize);
    return path;
  }
#elif _WIN32
  // Windows: use GetModuleFilename function
  // Untested because I don't have Windows.
  // Also, this is a bit hackish -- it can choke on long paths.
  std::optional<std::string> getExeLocation() {
    char buffer[MAX_PATH];
    DWORD status = GetModuleFilename(nullptr, buffer, MAX_PATH);
    if (status == 0) return std::nullopt;
    return std::string(buffer);
  }
#endif
}
