#include "AGL/GLFWApplication.h"

#if AGL_PROFILER
#  include <stdlib.h>
#endif
#include <iostream>

#include <fmt/core.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "AGL/sound/Mixer.h"

using namespace agl;

#include "AGL/begincbackdecl.h"
// Thanks learnopengl.com!
void APIENTRY glDebugOutput(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar* message, const void* userParam) {
  // ignore non-significant error/warning codes
  if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;
  std::cout << "---------------\n";
  fmt::print("Debug message ({}): {}\n", id, message);
  switch (source) {
  case GL_DEBUG_SOURCE_API: std::cout << "Source: API"; break;
  case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
    std::cout << "Source: Window System";
    break;
  case GL_DEBUG_SOURCE_SHADER_COMPILER:
    std::cout << "Source: Shader Compiler";
    break;
  case GL_DEBUG_SOURCE_THIRD_PARTY: std::cout << "Source: Third Party"; break;
  case GL_DEBUG_SOURCE_APPLICATION: std::cout << "Source: Application"; break;
  case GL_DEBUG_SOURCE_OTHER: std::cout << "Source: Other"; break;
  }
  std::cout << '\n';
  switch (type) {
  case GL_DEBUG_TYPE_ERROR: std::cout << "Type: Error"; break;
  case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    std::cout << "Type: Deprecated Behaviour";
    break;
  case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    std::cout << "Type: Undefined Behaviour";
    break;
  case GL_DEBUG_TYPE_PORTABILITY: std::cout << "Type: Portability"; break;
  case GL_DEBUG_TYPE_PERFORMANCE: std::cout << "Type: Performance"; break;
  case GL_DEBUG_TYPE_MARKER: std::cout << "Type: Marker"; break;
  case GL_DEBUG_TYPE_PUSH_GROUP: std::cout << "Type: Push Group"; break;
  case GL_DEBUG_TYPE_POP_GROUP: std::cout << "Type: Pop Group"; break;
  case GL_DEBUG_TYPE_OTHER: std::cout << "Type: Other"; break;
  }
  std::cout << '\n';
  switch (severity) {
  case GL_DEBUG_SEVERITY_HIGH: std::cout << "Severity: high"; break;
  case GL_DEBUG_SEVERITY_MEDIUM: std::cout << "Severity: medium"; break;
  case GL_DEBUG_SEVERITY_LOW: std::cout << "Severity: low"; break;
  case GL_DEBUG_SEVERITY_NOTIFICATION:
    std::cout << "Severity: notification";
    break;
  }
  std::cout << '\n';
  std::cout << '\n';
}
#include "AGL/endcbackdecl.h"

namespace agl {

  GLFWApplication::GLFWApplication(const ApplicationOptions& opts) :
      rollingFPS(60), cumulDelta(0), currInRF(0), w(opts.width), h(opts.height),
      mfps(opts.maxFPS), mixer(std::make_unique<agl::Mixer>(opts.audioDevice)) {
    // Set actual width and height of window to whatever
    // you're working on if not set
    int actualWidth = (opts.actualWidth == 0) ? opts.width : opts.actualWidth;
    int actualHeight =
        (opts.actualHeight == 0) ? opts.height : opts.actualHeight;
    aw = actualWidth;
    ah = actualHeight;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, opts.glMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, opts.glMinor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 4);
    window = glfwCreateWindow(
        actualWidth, actualHeight, opts.title, nullptr, nullptr);
    if (window == nullptr) {
      glfwTerminate();
      std::cerr << "Failed to create GLFW window\n";
      exit(-1);
    }
    glfwMakeContextCurrent(window);
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
      std::cerr << "Failed to initialise GLEW\n";
      exit(-1);
    }
    glGetError();
    if (opts.debug) {
      glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
      GLint flags;
      glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
      if (flags & GL_CONTEXT_FLAG_DEBUG_BIT || opts.glMajor >= 5 ||
          (opts.glMajor == 4 && opts.glMinor >= 3)) {
        // initialise debug output
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(glDebugOutput, nullptr);
        glDebugMessageControl(
            GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
      } else {
        std::cout << "Warning: requested debug output but we can't do it\n";
      }
    }
    glViewport(0, 0, actualWidth, actualHeight);
    glEnable(GL_MULTISAMPLE);
    glfwSetKeyCallback(window, keyCallback);
    // glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window, mouseCallback);
    memset(keys, 0, sizeof(uint64_t) * 16);
    glfwSetWindowUserPointer(window, this);
    // setVSyncEnable(false);
#if AGL_PROFILER
    outfh = nullptr;
    char* profileVal = getenv("AGL_PROFILER");
    bool shouldProfile = (profileVal != nullptr) && (profileVal[0] != '\0') &&
                         (strcmp(profileVal, "0") != 0);
    if (shouldProfile) {
      char fname[] = "/tmp/aglprofile.XXXXXX.bin";
      int fd = mkstemps(fname, 4);
      if (fd < 0) {
        perror("Opening profile file failed");
      } else {
        outfh = fdopen(fd, "wb");
        if (outfh == nullptr) {
          perror("Opening profile file failed");
        } else {
          fmt::print("Profiling to {}\n", fname);
        }
      }
    }
    popts = opts.popts;
    trackValues.resize(popts->nTracks);
    if (outfh != nullptr) popts->writeToFile(outfh);
    std::cerr << "finished setting up profile\n";
#endif
  }

#ifdef __WIN32
#  include <windows.h>
  inline void wait(double t) { Sleep((int) (t * 1000)); }
#else
#  include <time.h>
  inline void wait(double t) {
    struct timespec rqtp, remtp;
    rqtp.tv_sec = (int) (t / 1000000000);
    rqtp.tv_nsec = (int) ((int64_t)(t * 1000000000) % 1000000000);
    while (true) {
      int stat = clock_nanosleep(CLOCK_MONOTONIC, 0, &rqtp, &remtp);
      if (stat == 0) break; // We're done
      rqtp = remtp;
    }
  }
#endif

  constexpr double waitingDistance = 0.5;
  constexpr double thresh = 0.001;

  void GLFWApplication::start() {
    currentTime = glfwGetTime();
    initialise();
    while (!glfwWindowShouldClose(window)) {
      stashCurrentKeys();
      GLdouble prevTime = currentTime;
      currentTime = glfwGetTime();
      // Keep at slightly more than maxFPS so FPS doesn't dip below as often
      double goal = prevTime + 0.9995 / mfps;
      while (currentTime < goal) {
        currentTime = glfwGetTime();
        double diff = goal - currentTime;
        if (diff >= thresh) wait(waitingDistance * diff);
      }
      delta = currentTime - prevTime;
      writeProfile(track_names::frameTime, delta);
      fps = 1.0 / delta;
      cumulDelta += delta;
      ++currInRF;
      if (cumulDelta >= fpsUpdatePeriod) {
        rollingFPS = currInRF / cumulDelta;
        currInRF = 0;
        cumulDelta = 0;
      }
      glfwPollEvents();
      readKeys();
      tick();
      GLdouble tickDelta = glfwGetTime() - currentTime;
      writeProfile(track_names::tickTime, tickDelta);
#if AGL_PROFILER
      if (outfh != nullptr) {
        fwrite(trackValues.data(), sizeof(double), trackValues.size(), outfh);
        std::fill(trackValues.begin(), trackValues.end(), NAN);
      }
#endif
      glfwSwapBuffers(window);
      glFlush();
    }
  }

  GLFWApplication::~GLFWApplication() {
#if AGL_PROFILER
    if (outfh != nullptr) fclose(outfh);
#endif
    glfwSetWindowShouldClose(underlying(), true);
  }

  Mixer& GLFWApplication::getMixer() {
    if (mixer == nullptr) { mixer = std::make_unique<Mixer>(); }
    return *mixer;
  }

  void GLFWApplication::initialise() { return; }

  void GLFWApplication::tick() { return; }

  void GLFWApplication::readKeys() { return; }

  bool GLFWApplication::testKey(int code) {
    return (keys[code >> 6] >> (code & 63)) & 1;
  }

  KeyStatus GLFWApplication::testKey2(int code) {
    bool now = (keys[code >> 6] >> (code & 63)) & 1;
    bool then = (keysPrev[code >> 6] >> (code & 63)) & 1;
    return KeyStatus(2 * then + now);
  }

  void GLFWApplication::setVSyncEnable(bool enable) {
    glfwSwapInterval(enable);
  }

  void GLFWApplication::setKey(int code) {
    keys[code >> 6] |= (1LL << (code & 63));
  }

  void GLFWApplication::resetKey(int code) {
    keys[code >> 6] &= ~(1LL << (code & 63));
  }

  void GLFWApplication::stashCurrentKeys() {
    memcpy(keysPrev, keys, 16 * sizeof(uint64_t));
  }

#include "AGL/begincbackdecl.h"

  void
  keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
    auto* currentApp = (GLFWApplication*) glfwGetWindowUserPointer(window);
    if (action == GLFW_PRESS)
      currentApp->setKey(key);
    else if (action == GLFW_RELEASE)
      currentApp->resetKey(key);
  }

  void GLFWApplication::onMouse(double xpos, double ypos) { return; }

  void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    auto* currentApp = (GLFWApplication*) glfwGetWindowUserPointer(window);
    currentApp->onMouse(xpos, ypos);
  }

#include "AGL/endcbackdecl.h"
}