#version 330 core

#pragma shader_stage(fragment)

in vec2 texCoord;
out vec4 colour;
uniform sampler2D tex;

void main() { colour = texture(tex, texCoord); }
