#version 330 core

#pragma shader_stage(vertex)

layout(location = 0) in vec2 pos;
layout(location = 1) in vec4 source;
layout(location = 2) in vec4 dest;
out vec2 texCoord;
uniform vec2 texDimensions;
uniform vec2 screenDimensions;

void main() {
  texCoord =
      vec2(mix(source.x, source.z, pos.x), mix(source.y, source.w, pos.y)) /
      texDimensions;
  vec2 position = vec2(mix(dest.x, dest.z, pos.x), mix(dest.y, dest.w, pos.y)) /
                  screenDimensions;
  gl_Position =
      vec4(position * vec2(2.0f, -2.0f) + vec2(-1.0f, 1.0f), 1.0f, 1.0f);
}
