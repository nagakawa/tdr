#version 330 core

#pragma shader_stage(vertex)

layout(location = 0) in vec2 quad;
layout(location = 1) in ivec4 bounds;
layout(location = 2) in vec2 pos;
layout(location = 3) in vec2 glyphSize;
layout(location = 4) in int runIndex;
out vec2 pxy;
out vec2 linexy;
out float netLineSkip;
uniform vec2 screenDimensions;
#if DROP_SHADOW
uniform vec2 dropShadowOffset;
#else
#endif
out vec2 tc;
uniform sampler2D tex;

struct TextRun {
  vec2 localOffset;
  vec2 lineDimensions;
  float netLineSkip;
};
uniform TextRun textRuns[128];

void main() {
  tc = (mix(bounds.xy, bounds.zw, quad) - 0.5) / textureSize(tex, 0);
  pxy = pos + quad * glyphSize;
#if DROP_SHADOW
  pxy += dropShadowOffset;
#endif
  vec2 localOffset = textRuns[runIndex].localOffset;
  gl_Position = vec4(2.0 * pxy / screenDimensions + localOffset, 1.0f, 1.0f);
  vec2 lineDimensions = textRuns[runIndex].lineDimensions;
  netLineSkip = textRuns[runIndex].netLineSkip;
  linexy = vec2(pxy.x / lineDimensions.x, -pxy.y / lineDimensions.y);
}
