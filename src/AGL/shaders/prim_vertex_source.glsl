#version 330 core

#pragma shader_stage(vertex)

layout(location = 0) in vec4 col;
layout(location = 1) in vec3 pos;
layout(location = 2) in vec2 tc;
out vec4 mycolour;
out vec2 texCoord;
uniform vec2 screenDimensions;

void main() {
  texCoord = tc;
  mycolour = col;
  vec2 p01 = pos.xy / screenDimensions;
  p01.y = 1.0 - p01.y;
  gl_Position = vec4(2.0 * p01 - 1.0, 1.0, 1.0);
}
