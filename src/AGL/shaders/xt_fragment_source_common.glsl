#version 330 core

#pragma shader_stage(fragment)

#if DROP_SHADOW
uniform vec4 shadowColour;
uniform float threshold;
#else
struct FillStyle {
  vec2 centre;
  vec2 gradDir;
  vec4 col1, col2;
  sampler2D t;
};

struct TextStyle {
  FillStyle outline, fill;
  float outlineThreshold, fillThreshold;
};

uniform TextStyle textStyle;
#endif

in vec2 tc;
in vec2 pxy;
in vec2 linexy;
in float netLineSkip;
uniform sampler2D tex;
out vec4 colour;


#if !DROP_SHADOW
vec4 getFillColour(FillStyle fs, vec2 linexy, vec2 pxy) {
  vec2 gradOffset = linexy - fs.centre;
  float gradIntensity = dot(gradOffset, fs.gradDir) / length(fs.gradDir);
  vec4 textcol = mix(fs.col1, fs.col2, clamp(0.5 + 0.5 * gradIntensity, 0, 1));
  return textcol * texture(fs.t, pxy / textureSize(fs.t, 0));
}
#endif

float median(float r, float g, float b) {
  return max(min(r, g), min(max(r, g), b));
}

// thx
// http://metalbyexample.com/rendering-text-in-metal-with-signed-distance-fields/
void main() {
  vec2 myLineXy = linexy;
  myLineXy.y = mod(myLineXy.y, netLineSkip);
  vec3 sample = texture(tex, tc).rgb;
  float dist = median(sample.r, sample.g, sample.b);
  float edgeWidth = 0.7 * length(vec2(dFdx(dist), dFdy(dist)));
#if DROP_SHADOW
  vec4 textColour = shadowColour;
  float opacity =
      smoothstep(threshold - edgeWidth, threshold + edgeWidth, dist);
  vec4 background = vec4(textColour.rgb, 0.0);
  colour = mix(background, textColour, opacity);
#else
  vec4 textColour = getFillColour(textStyle.fill, myLineXy, pxy);
  vec4 outlineColour = getFillColour(textStyle.outline, myLineXy, pxy);
  float interiorOpacity = smoothstep(
      textStyle.fillThreshold - edgeWidth, textStyle.fillThreshold + edgeWidth,
      dist);
  float outlineOpacity = smoothstep(
      textStyle.outlineThreshold - edgeWidth,
      textStyle.outlineThreshold + edgeWidth, dist);
  vec4 background = vec4(outlineColour.rgb, 0.0);
  vec4 oc = mix(background, outlineColour, outlineOpacity);
  colour = mix(oc, textColour, interiorOpacity);
#endif
  // colour = sigDist >= 0 ? textColour : background;
  // colour = vec4(myLineXy, 0.0, 1.0);
}
