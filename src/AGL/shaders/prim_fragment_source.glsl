#version 330 core

#pragma shader_stage(fragment)

in vec2 texCoord;
in vec4 mycolour;
out vec4 colour;
uniform sampler2D tex;

void main() { colour = mycolour * texture(tex, texCoord); }
