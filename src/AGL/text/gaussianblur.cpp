#include "AGL/text/gaussianblur.h"

#include <assert.h>
#include <math.h>

#include <algorithm>
#include <numeric>
#include <vector>

#include <fmt/core.h>

namespace agl {
  // We don't need to normalise, since we're already dividing by sum of weights
  static float gaussScaled(float x, float sigma2) {
    return expf(-x * x / (2 * sigma2));
  }

  static std::pair<float, std::vector<std::pair<float, float>>>
  getCoefficients(float scale, int radius) {
    float sigma2 = (float) (radius * radius);
    std::vector<float> weights;
    weights.reserve(radius);
    for (int i = 1; i <= radius; ++i) {
      weights.push_back(gaussScaled(i, sigma2));
    }
    float centralWeight = 1;
    float totalWeight =
        2 * std::accumulate(weights.begin(), weights.end(), 0.0f) +
        centralWeight;
    for (float& w : weights) w /= totalWeight;
    centralWeight /= totalWeight;
    std::vector<std::pair<float, float>> out;
    out.reserve((radius + 1) / 2);
    for (int i = 1; i < radius; i += 2) {
      float off1 = (float) i;
      float off2 = off1 + 1;
      float weight1 = weights[i - 1], weight2 = weights[i];
      float weight = weight1 + weight2;
      float off = (off1 * weight1 + off2 * weight2) / weight;
      out.emplace_back(scale * off, weight);
    }
    if (radius % 2 != 0) { out.emplace_back(scale * radius, weights.back()); }
    return std::pair(centralWeight, out);
  }

  static std::string_view fragmentTemplateBegin = R"(
#version 330 core

uniform sampler2D tex;
uniform vec2 texDimensions;
in vec2 texCoord;
out vec4 colour;

void main() {
  vec4 sum = vec4(0.0, 0.0, 0.0, 0.0);
)";
  static std::string_view fragmentTemplateEnd = R"(
  colour = sum;
}
)";

  static std::pair<std::string, std::string> createShaders(int radius) {
    int kernWidth = std::min(radius, 7);
    float scale = ((float) radius) / kernWidth;
    auto [centralWeight, coefficients] = getCoefficients(scale, kernWidth);
    std::string horiz(fragmentTemplateBegin), vert(fragmentTemplateBegin);
    horiz +=
        fmt::format("  sum += {} * texture(tex, texCoord);\n", centralWeight);
    vert +=
        fmt::format("  sum += {} * texture(tex, texCoord);\n", centralWeight);
    for (auto [offset, weight] : coefficients) {
      horiz += fmt::format(
          "  sum += {} * texture(tex, texCoord + vec2({}, 0.0) / "
          "texDimensions);\n",
          weight, offset);
      vert += fmt::format(
          "  sum += {} * texture(tex, texCoord + vec2(0.0, {}) / "
          "texDimensions);\n",
          weight, offset);
      horiz += fmt::format(
          "  sum += {} * texture(tex, texCoord - vec2({}, 0.0) / "
          "texDimensions);\n",
          weight, offset);
      vert += fmt::format(
          "  sum += {} * texture(tex, texCoord - vec2(0.0, {}) / "
          "texDimensions);\n",
          weight, offset);
    }
    horiz += fragmentTemplateEnd;
    vert += fragmentTemplateEnd;
    return std::pair(std::move(horiz), std::move(vert));
  }

  static thread_local std::vector<std::pair<int, std::pair<Shader, Shader>>>
      sourceCache;

  std::pair<Shader*, Shader*> getShaders(int radius) {
    assert(radius > 0);
    for (auto& [r, p] : sourceCache) {
      if (r == radius) { return std::pair(&p.first, &p.second); }
    }
    auto [h, v] = createShaders(radius);
    Shader hs(h, GL_FRAGMENT_SHADER), vs(v, GL_FRAGMENT_SHADER);
    sourceCache.push_back(
        std::pair(radius, std::pair(std::move(hs), std::move(vs))));
    auto& p = sourceCache.back().second;
    return std::pair(&p.first, &p.second);
  }
}
