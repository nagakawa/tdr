#include "AGL/text/XText.h"

#include <iostream>
#include <optional>

#include "AGL/embedShader.h"
#include "AGL/text/gaussianblur.h"

EMBED_SHADER(xt_vertex_source, vertex, static)
EMBED_SHADER(xt_fragment_source, fragment, static)
EMBED_SHADER(xt_shadow_vertex_source, svertex, static)
EMBED_SHADER(xt_shadow_fragment_source, sfragment, static)

namespace agl {
  static thread_local std::optional<Shader> xtextVertex, xtextFragment;
  static thread_local std::optional<Shader> xtextVertexShadow,
      xtextFragmentShadow;

  static void initXTextShaders() {
    if (xtextVertex.has_value()) return;
    xtextVertex.emplace(vertexSource, vertexSourceSize - 1, GL_VERTEX_SHADER);
    xtextFragment.emplace(
        fragmentSource, fragmentSourceSize - 1, GL_FRAGMENT_SHADER);
  }
  static void initXTextShadowShaders() {
    if (xtextVertexShadow.has_value()) return;
    xtextVertexShadow.emplace(
        svertexSource, svertexSourceSize - 1, GL_VERTEX_SHADER);
    xtextFragmentShadow.emplace(
        sfragmentSource, sfragmentSourceSize - 1, GL_FRAGMENT_SHADER);
  }

  void XText::setUp() {
    initXTextShaders();
    program.attach(*xtextVertex);
    program.attach(*xtextFragment);
    program.link();
    hasInitialisedProgram = true;
    vao.setActive();
    vbo.feedData(
        sizeof(rectangleVertices), (void*) rectangleVertices, GL_STATIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*) 0);
    instanceVBO.setActive();
    glEnableVertexAttribArray(1);
    glVertexAttribIPointer(
        1, 4, GL_SHORT, sizeof(RInfo), (GLvoid*) (offsetof(RInfo, bounds)));
    glVertexAttribDivisor(1, 1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        2, 2, GL_FLOAT, false, sizeof(RInfo), (GLvoid*) (offsetof(RInfo, pos)));
    glVertexAttribDivisor(2, 1);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(
        3, 2, GL_FLOAT, false, sizeof(RInfo),
        (GLvoid*) (offsetof(RInfo, glyphSize)));
    glVertexAttribDivisor(3, 1);
    glEnableVertexAttribArray(4);
    glVertexAttribIPointer(
        4, 1, GL_INT, sizeof(RInfo), (GLvoid*) (offsetof(RInfo, runIndex)));
    glVertexAttribDivisor(4, 1);
#pragma GCC diagnostic pop
    agl::resetVBO();
    agl::resetVAO();
  }
  void XText::spurt(std::vector<GlyphInfo>& gis, Element& e) {
    // Spurts the render-related data from the glyph list
    // to the `rinfo` vector. This vector will then be used by
    // the render code to draw the glyphs.
    float marginCorrected =
        font->margin() * e.layout.fontSize / font->getSize();
    for (const GlyphInfo& gi : gis) {
      uint32_t id = gi.index;
      Font::GlyphInfo& props = font->getInfo(id);
      // Where should we push?
      auto& toPush = rinfo[props.texid];
      toPush.emplace_back();
      RInfo& entry = toPush.back();
      entry.bounds = props.box;
      entry.pos = {gi.x / 64.0f - marginCorrected,
                   gi.y / 64.0f - marginCorrected};
      entry.glyphSize = {gi.w + 2 * marginCorrected,
                         gi.h + 2 * marginCorrected};
      entry.runIndex = &e - &elems[0];
    }
  }
  void XText::updateOffsets() {
    // Update offsets vector
    offsets.resize(rinfo.size() + 1);
    offsets[0] = 0;
    for (size_t i = 0; i < rinfo.size(); ++i)
      offsets[1 + i] = offsets[i] + rinfo[i].size();
  }
  void XText::update() {
    instanceVBO.feedData(
        offsets.back() * sizeof(RInfo), nullptr, GL_DYNAMIC_DRAW);
    // Set individual chunks
    for (size_t i = 0; i < rinfo.size(); ++i) {
      instanceVBO.feedSubdata(
          offsets[i] * sizeof(RInfo),
          (offsets[i + 1] - offsets[i]) * sizeof(RInfo), rinfo[i].data());
    }
  }

  void XText::setTextRunUniforms(ShaderProgram& p) {
    p.uniform2f(
        "screenDimensions", glm::vec2{(GLfloat) vp[2], (GLfloat) vp[3]});
    if (elems.size() > 128) {
      std::cerr << "Oh no, I don't know how to handle more than 128 elems!\n";
      abort();
    }
    char str[64] = "textRuns[";
    char* wrptr = str + 9;
    for (size_t i = 0; i < elems.size(); ++i) {
      const auto& e = elems[i];
      auto pos = positionAsNDC(e);
      int cw = sprintf(wrptr, "%zu].localOffset", i) - 12;
      float lineWidth = e.layout.maxWidth;
      float lineHeight = e.diagnostics.lineHeight;
      float netLineHeight = e.layout.lineSkip + lineHeight;
      p.uniform2f(str, pos);
      strcpy(wrptr + cw, ".lineDimensions");
      p.uniform2f(str, {lineWidth, lineHeight});
      strcpy(wrptr + cw, ".netLineSkip");
      p.uniform1f(str, netLineHeight);
    }
  }

  void XText::setUniforms1() {
    vao.setActive();
    program.use();
    if (hasSetUniforms && !isDirty) return;
    getViewportDimensions();
    setTextRunUniforms(program);
    hasSetUniforms = true;
  }

  void XText::setUniforms2(ShaderProgram& p, size_t page, bool useStyle) {
    p.use();
    Texture& t = font->texs[page];
    t.bind();
    t.bindTo(0);
    p.uniform1i("tex", 0);
    if (useStyle) {
      style.passToShader(p, "textStyle");
      style.prepareRender();
    }
  }

  void XText::render() {
    if (!hasInitialisedProgram) setUp();
    vao.setActive();
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    BlendMode::alpha.use();
    if (isDirty) {
      relayout();
      update();
    }
    if (isDirty || !shadowsDone) { renderShadows(); }
    setUniforms1();
    isDirty = false;
    if (hasShadow()) {
      assert(dropShadowState.has_value());
      dropShadowState->spr.render();
    }
    vao.setActive();
    for (size_t i = 0; i < rinfo.size(); ++i) {
      setUniforms2(program, i);
      // XXX: 4.2-ism here, will have to fix later
      glDrawArraysInstancedBaseInstance(
          GL_TRIANGLE_STRIP, 0, 4, offsets[i + 1] - offsets[i], offsets[i]);
    }
    agl::resetVAO();
  }
  void XText::relayout() {
    for (auto& ri : rinfo) ri.clear();
    rinfo.resize(font->texs.size());
    for (Element& e : elems) {
      std::vector<GlyphInfo> glyphs =
          layoutText(e.text.c_str(), *font, e.layout, e.diagnostics);
      spurt(glyphs, e);
    }
    updateOffsets();
    setUniforms1();
  }
  void XText::setText(std::string&& s) {
    if (elems.empty()) elems.emplace_back();
    elems[0].text = std::move(s);
    isDirty = true;
  }
  glm::vec2 XText::positionAsNDC(const Element& e) const {
    glm::vec2 screen((GLfloat) vp[2], (GLfloat) vp[3]);
    glm::vec2 res = 2.0f * (e.position / screen) - 1.0f;
    return res * glm::vec2(1.0f, -1.0f);
  }
  void XText::setPos(glm::vec2 p) {
    elems[0].position = p;
    isDirty = true;
  }
  size_t XText::addText(std::string&& s, const LayoutInfo& l, glm::vec2 p) {
    isDirty = true;
    size_t os = elems.size();
    elems.emplace_back(std::move(s), l, p);
    return os;
  }

  void XText::getViewportDimensions() {
    if (app == nullptr) {
      glGetIntegerv(GL_VIEWPORT, vp);
    } else {
      vp[2] = app->getWidth();
      vp[3] = app->getHeight();
    }
  }
  void XText::initShadows() {
    if (dropShadowState.has_value()) return;
    initXTextShadowShaders();
    getViewportDimensions();
    GLint width = vp[2], height = vp[3];
    auto [fbo1, tex1] = makeFBOForMe(width, height);
    auto [fbo2, tex2] = makeFBOForMe(width, height);
    Sprite2D s(tex1.weak());
    s.addSprite({
        {0, (float) height, (float) width, (float) 0},
        {0, 0, (float) width, (float) height},
    });
    ShaderProgram init;
    init.attach(*xtextVertexShadow);
    init.attach(*xtextFragmentShadow);
    init.link();
    int radius = (int) style.dropShadowStyle.dropShadowRadius;
    if (radius == 0) {
      dropShadowState = DropShadowState{
          std::move(fbo1), std::move(fbo2), std::move(tex1), std::move(tex2),
          std::move(s),    std::nullopt,    std::nullopt,    std::move(init),
      };
    } else {
      auto [horizontalBlur, verticalBlur] = getShaders(radius);
      Sprite2D hblur(tex1.weak(), horizontalBlur);
      hblur.addSprite({
          {0, 0, (float) width, (float) height},
          {0, 0, (float) width, (float) height},
      });
      Sprite2D vblur(tex2.weak(), verticalBlur);
      vblur.addSprite({
          {0, 0, (float) width, (float) height},
          {0, 0, (float) width, (float) height},
      });
      dropShadowState = DropShadowState{
          std::move(fbo1), std::move(fbo2),  std::move(tex1),  std::move(tex2),
          std::move(s),    std::move(hblur), std::move(vblur), std::move(init),
      };
    }
  }

  bool XText::hasShadow() const noexcept {
    return style.dropShadowStyle.isActive();
  }

  void XText::renderShadows() {
    // Render shadows to t1.
    if (!hasShadow()) return;
    initShadows();
    GLint oldFboId = getCurrentFboId();
    assert(dropShadowState.has_value());
    dropShadowState->b1.setActive();
    glm::vec4 shadowColour = style.dropShadowStyle.dropShadowColour;
    glClearColor(shadowColour[0], shadowColour[1], shadowColour[2], 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    vao.setActive();
    dropShadowState->init.use();
    setTextRunUniforms(dropShadowState->init);
    dropShadowState->init.uniform2f(
        "dropShadowOffset",
        style.dropShadowStyle.dropShadowOffset * glm::vec2{1, -1});
    dropShadowState->init.uniform4f("shadowColour", shadowColour);
    dropShadowState->init.uniform1f("threshold", style.outlineThreshold);
    for (size_t i = 0; i < rinfo.size(); ++i) {
      setUniforms2(dropShadowState->init, i, false);
      // XXX: 4.2-ism here, will have to fix later
      glDrawArraysInstancedBaseInstance(
          GL_TRIANGLE_STRIP, 0, 4, offsets[i + 1] - offsets[i], offsets[i]);
    }
    if (dropShadowState->hblur.has_value()) {
      dropShadowState->b2.setActive();
      glClear(GL_COLOR_BUFFER_BIT);
      dropShadowState->hblur->render();
      dropShadowState->b1.setActive();
      glClear(GL_COLOR_BUFFER_BIT);
      dropShadowState->vblur->render();
    }
    // done
    bindFbo(oldFboId);
    glViewport(vp[0], vp[1], vp[2], vp[3]);
    shadowsDone = true;
    program.use();
  }
}
