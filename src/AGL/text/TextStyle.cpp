#include "AGL/text/TextStyle.h"

#include <string.h>

#include <glm/gtc/type_ptr.hpp>

#include "AGL/ShaderProgram.h"

namespace agl {
  FillStyle::FillStyle() :
      centre(0.0f, 0.0f), gradDir(1.0f, 0.0f), col1(1.0f, 1.0f, 1.0f, 1.0f),
      col2(1.0f, 1.0f, 1.0f, 1.0f) {}
  FillStyle::FillStyle(const glm::vec4& colour) :
      centre(0.0f, 0.0f), gradDir(1.0f, 0.0f), col1(colour), col2(colour) {}
  void FillStyle::passToShader(
      ShaderProgram& sp, const char* vname, GLint tIndex) const {
    size_t len = strlen(vname);
    char* namebuf = new char[len + 16];
    strcpy(namebuf, vname);
    char* wrptr = namebuf + len;
    strcpy(wrptr, ".centre");
    sp.uniform2f(namebuf, centre);
    strcpy(wrptr, ".gradDir");
    sp.uniform2f(namebuf, gradDir);
    strcpy(wrptr, ".col1");
    sp.uniform4f(namebuf, col1);
    strcpy(wrptr, ".col2");
    sp.uniform4f(namebuf, col2);
    strcpy(wrptr, ".t");
    sp.uniform1i(namebuf, tIndex);
    delete[] namebuf;
  }
  void FillStyle::setColour(const glm::vec4 col) { col1 = col2 = col; }
  void FillStyle::setVerticalGradient() {
    centre = gradDir = glm::vec2(0.0f, 0.5f);
  }
  TextStyle::TextStyle() :
      outline(), fill(), outlineThreshold(0.5f), fillThreshold(0.5f) {}
  void TextStyle::passToShader(ShaderProgram& sp, const char* vname) const {
    size_t len = strlen(vname);
    char* namebuf = new char[len + 24];
    strcpy(namebuf, vname);
    char* wrptr = namebuf + len;
    strcpy(wrptr, ".outline");
    outline.passToShader(sp, namebuf, 1);
    strcpy(wrptr, ".fill");
    fill.passToShader(sp, namebuf, 2);
    strcpy(wrptr, ".outlineThreshold");
    sp.uniform1f(namebuf, outlineThreshold);
    strcpy(wrptr, ".fillThreshold");
    sp.uniform1f(namebuf, fillThreshold);
    delete[] namebuf;
  }
  void TextStyle::prepareRender() const {
    initWhiteTexture();
    (outline.t.getID() != 0 ? outline.t : whiteTexture.weak()).bindTo(1);
    (fill.t.getID() != 0 ? fill.t : whiteTexture.weak()).bindTo(2);
  }
}
