#include "AGL/sound/Sound.h"

#include <assert.h>
#include <string.h>

#include <type_traits>

#include <fmt/core.h>

#include "AGL/sound/Reader.h"

namespace agl {
  Sound::Sound(void* buffer, size_t length, unsigned channels) {
    Reader reader(buffer, length);
    OggVorbis_File vf;
    int stat =
        ov_open_callbacks((void*) &reader, &vf, nullptr, 0, readerCallbacks);
    if (stat < 0) throw "Invalid file.";
    initialiseFromOVFile(vf, channels);
  }

  Sound::Sound(const char* fname, unsigned channels) {
    OggVorbis_File vf;
    int stat = ov_fopen(fname, &vf);
    if (stat < 0) throw "Invalid file.";
    initialiseFromOVFile(vf, channels);
  }

  void Sound::initialiseFromOVFile(OggVorbis_File& vf, unsigned channels) {
    assert(channels <= 2);
    constexpr size_t BUFFER_SIZE = 4096;
    vorbis_info* info = ov_info(&vf, -1);
    int srcChannels = info->channels;
    if (channels == 0) channels = std::min(srcChannels, 2);
    int rate = info->rate;
    int currentSection = -1;
    SType* data = new SType[BUFFER_SIZE * srcChannels];
    size_t size = 0;
    while (true) {
      long read = ov_read(
          &vf, (char*) data, BUFFER_SIZE * srcChannels * sizeof(SType), 0,
          sizeof(SType), std::is_signed_v<SType>, &currentSection);
      long sampleCount = read / sizeof(SType) / srcChannels;
      assert(read % (sizeof(SType) * srcChannels) == 0);
      if (read == 0)
        break;
      else if (read < 0) {
        delete[] data;
        throw "There is an error in the stream.";
      }
      else {
        samples.resize(size + channels * sampleCount);
        if (channels == 1) {
          for (long i = 0; i < sampleCount; ++i) {
            int sum = 0;
            for (int j = 0; j < srcChannels; ++j) {
              sum += data[srcChannels * i + j];
            }
            samples[size + i] = sum / srcChannels;
          }
        } else if (channels == 2) {
          for (long i = 0; i < sampleCount; ++i) {
            int sum1 = 0, sum2 = 0;
            for (int j = 0; j < srcChannels; ++j) {
              SType val = data[srcChannels * i + j];
              sum1 += val * (srcChannels - j) / srcChannels;
              sum2 += val * j / srcChannels;
            }
            samples[size + 2 * i] = sum1 / srcChannels;
            samples[size + 2 * i + 1] = sum2 / srcChannels;
          }
        }
        size += channels * sampleCount;
      }
    }
    this->samplesPerSecond = rate;
    this->sampleCount = size / channels;
    this->channels = channels;
    delete[] data;
    ov_clear(&vf);
  }

  void Sound::resample(size_t newSampleRate) {
    if (newSampleRate == samplesPerSecond) return;
    float ratio = (float) samplesPerSecond / newSampleRate;
    size_t osi = 0;
    float interm = 0.0f;
    std::vector<SType> newSamples;
    newSamples.reserve((size_t) (samples.size() / ratio) + 1);
    if (channels == 1) {
      while (osi < sampleCount) {
        float curr = samples[osi], next = samples[osi + 1];
        float qty = (curr * interm + next * (1 - interm));
        newSamples.push_back(qty);
        interm += ratio;
        size_t adv = (size_t) interm;
        osi += adv;
        interm -= adv;
      }
    } else if (channels == 2) {
      while (osi < sampleCount) {
        for (size_t j = 0; j < 2; ++j) {
          float curr = samples[2 * osi + j], next = samples[2 * (osi + 1) + j];
          float qty = (curr * interm + next * (1 - interm));
          newSamples.push_back(qty);
        }
        interm += ratio;
        size_t adv = (size_t) interm;
        osi += adv;
        interm -= adv;
      }
    }
    samplesPerSecond = newSampleRate;
    samples = std::move(newSamples);
    sampleCount = samples.size() / channels;
  }
}
