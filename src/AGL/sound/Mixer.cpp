#include "AGL/sound/Mixer.h"

#include <assert.h>

#include <limits>

namespace agl {
  static void memAddScaled(
      float* out, const SType* in, size_t n, float scale1, float scale2) {
    assert(n % 2 == 0);
    for (size_t i = 0; i < n / 2; ++i) {
      out[2 * i] += in[2 * i] * scale1;
      out[2 * i + 1] += in[2 * i + 1] * scale2;
    }
  }

  static void memAddScaled2(
      float* out, const SType* in, size_t n, float scale1, float scale2) {
    for (size_t i = 0; i < n; ++i) {
      out[2 * i] += in[i] * scale1;
      out[2 * i + 1] += in[i] * scale2;
    }
  }

  std::pair<float, float> getPanScale(float pan) {
    return std::pair(1 - pan, 1 + pan);
  }

  size_t Mixer::addSound(Sound&& s) {
    s.resample(outputRate);
    size_t wantid = sounds.size();
    sounds.push_back(std::move(s));
    return wantid;
  }
  size_t Mixer::addSoundFromFile(const char* fname) {
    return addSound(Sound(fname));
  }
  size_t Mixer::addSound(std::string&& index, Sound&& s) {
    s.resample(outputRate);
    size_t wantid = sounds.size();
    auto p = soundIDsByName.insert(std::make_pair(index, wantid));
    if (!p.second) {
      sounds[p.first->second] = std::move(s);
      return p.first->second;
    } else {
      sounds.push_back(std::move(s));
      return wantid;
    }
  }

  size_t Mixer::addSoundFromFile(std::string&& index, const char* fname) {
    return addSound(std::move(index), Sound(fname));
  }

  int MixerContext::playSound(size_t index, const PlayOptions& options) {
    MixerEntry m = {index, 0, options};
    entries[handleCount] = m;
    return handleCount++;
  }

  int Mixer::playSound(size_t index, const PlayOptions& options) {
    return getCurrentContext().playSound(index, options);
  }

  int Mixer::playSound(const std::string& index, const PlayOptions& options) {
    auto it = soundIDsByName.find(index);
    if (it != soundIDsByName.end()) return playSound(it->second, options);
    return -1;
  }

  void MixerContext::advance(unsigned long n, float* output, const Mixer& m) {
    float globalScale =
        m.globalVolume / (2.0f * std::numeric_limits<SType>::max());
    for (unsigned long i = 0; i < 2 * n; ++i) output[i] = 0.0f;
    for (auto it = entries.begin(); it != entries.end();) {
      MixerEntry& me = it->second;
      const Sound& s = m.sounds[me.key];
      size_t loopStart = me.options.loopStart;
      size_t loopSize = me.options.loopEnd - loopStart;
      // Get the number of samples to play before either the end of the sound
      // or the loop end point
      size_t limit = me.options.loop ? me.options.loopEnd : s.sampleCount;
      size_t nSamples = std::min(n, limit - me.curr);
      float scale = me.options.volume * globalScale;
      auto [a, b] = getPanScale(me.options.pan);
      float scaleLeft = scale * a, scaleRight = scale * b;
      /*
        We copy by blocks instead of by one sample at a time in order to get
        better performance.
      */
      if (s.channels == 1) {
        // Mix in the first iteration
        memAddScaled2(
            output, s.samples.data() + me.curr, nSamples, scaleLeft,
            scaleRight);
        me.curr += nSamples;
        if (me.options.loop && nSamples < n) {
          // Mix in any loops
          size_t i = nSamples;
          while (nSamples + loopSize <= n) { // All loops but the last
            memAddScaled2(
                output + 2 * i, s.samples.data() + loopStart, loopSize,
                scaleLeft, scaleRight);
            nSamples += loopSize;
          }
          // Last loop
          memAddScaled2(
              output + 2 * i, s.samples.data() + loopStart, n - i, scaleLeft,
              scaleRight);
          me.curr = loopStart + (n - i);
        }
      } else if (s.channels == 2) {
        // Mix in the first iteration
        memAddScaled(
            output, s.samples.data() + 2 * me.curr, 2 * nSamples, scaleLeft,
            scaleRight);
        me.curr += nSamples;
        if (me.options.loop && nSamples < n) {
          // Mix in any loops
          size_t i = nSamples;
          while (nSamples + loopSize <= n) { // All loops but the last
            memAddScaled(
                output + 2 * i, s.samples.data() + 2 * loopStart, 2 * loopSize,
                scaleLeft, scaleRight);
            nSamples += loopSize;
          }
          // Last loop
          memAddScaled(
              output + 2 * i, s.samples.data() + 2 * loopStart, 2 * (n - i),
              scaleLeft, scaleRight);
          me.curr = loopStart + (n - i);
        }
      }
      if (me.curr >= s.sampleCount)
        it = entries.erase(it);
      else
        ++it;
    }
  }

  void Mixer::advance(unsigned long n, float* output) {
    getCurrentContext().advance(n, output, *this);
  }

  int mixerCallback(
      const void* input, void* output, unsigned long frameCount,
      const PaStreamCallbackTimeInfo* timeInfo,
      PaStreamCallbackFlags statusFlags, void* userData) {
    (void) input;
    (void) timeInfo;
    (void) statusFlags;
    Mixer* mixer = (Mixer*) userData;
    float* out = (float*) output;
    mixer->advance(frameCount, out);
    return paContinue;
  }

  void Mixer::clear() { currentContext->clear(); }

  void Mixer::stop() { Pa_StopStream(stream); }

  Mixer::Mixer(int audioDevice) {
    auto [it, succ] = contexts.try_emplace("");
    (void) succ;
    currentContext = &(it->second);
    PaError stat = Pa_Initialize();
    if (stat != paNoError) throw MixerException(stat);
    int outdev =
        (audioDevice == -1) ? Pa_GetDefaultOutputDevice() : audioDevice;
    if (Pa_GetDeviceInfo(outdev) == nullptr)
      outdev = Pa_GetDefaultOutputDevice();
    PaStreamParameters parameters = {
        /* .device = */ outdev,
        /* .channelCount = */ 2,
        /* .sampleFormat = */ paFloat32,
        /* .suggestedLatency = */ 0.01,
        /* .hostApiSpecificStreamInfo = */ nullptr,
    };
    outputRate = Pa_GetDeviceInfo(outdev)->defaultSampleRate;
    /* Open an audio I/O stream. */
    stat = Pa_OpenStream(
        &stream, nullptr, &parameters, outputRate, paFramesPerBufferUnspecified,
        paNoFlag, mixerCallback, (void*) this);
    if (stat != paNoError) throw MixerException(stat);
    stat = Pa_StartStream(stream);
    if (stat != paNoError) throw MixerException(stat);
  }

  bool Mixer::addContext(std::string&& name) {
    auto [it, succ] = contexts.try_emplace(std::move(name));
    return succ;
  }

  bool Mixer::switchContext(std::string&& name) {
    auto it = contexts.find(name);
    if (it == contexts.end()) return false;
    contextName = std::move(name);
    currentContext = &(it->second);
    return true;
  }

  bool Mixer::removeContext(const std::string& name) {
    if (name.empty()) return false;
    if (name == contextName) {
      contextName.clear();
      currentContext = &(contexts[contextName]);
    }
    bool erased = contexts.erase(name) != 0;
    return erased;
  }

  void Mixer::restartSound(int id) { getCurrentContext().restartSound(id); }

  void MixerContext::restartSound(int id) {
    MixerEntry& e = entries.at(id);
    e.curr = 0;
  }

  Mixer::~Mixer() {
    stop();
    Pa_Terminate();
  }
}
