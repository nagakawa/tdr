#include "AGL/BlendMode.h"

using namespace agl;

void agl::BlendMode::use() const {
  glBlendEquationSeparate(eqRGB, eqAlpha);
  glBlendFuncSeparate(srcFuncRGB, destFuncRGB, srcFuncAlpha, destFuncAlpha);
}

const BlendMode agl::BlendMode::alpha = {GL_FUNC_ADD,  GL_FUNC_ADD,
                                         GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,
                                         GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA};

const BlendMode agl::BlendMode::add = {GL_FUNC_ADD, GL_FUNC_ADD, GL_SRC_ALPHA,
                                       GL_ONE,      GL_ONE,      GL_ONE};

const BlendMode agl::BlendMode::subtract = {GL_FUNC_REVERSE_SUBTRACT,
                                            GL_FUNC_REVERSE_SUBTRACT,
                                            GL_SRC_ALPHA,
                                            GL_ONE,
                                            GL_ONE,
                                            GL_ONE};

const BlendMode agl::BlendMode::multiply = {
    GL_FUNC_ADD, GL_FUNC_ADD, GL_DST_COLOR,
    GL_ZERO,     GL_ONE,      GL_ONE_MINUS_SRC_ALPHA};

const BlendMode agl::BlendMode::screen = {
    GL_FUNC_ADD, GL_FUNC_ADD, GL_ONE_MINUS_DST_COLOR,
    GL_ONE,      GL_ONE,      GL_ONE_MINUS_SRC_ALPHA};

const BlendMode agl::BlendMode::shadow = {GL_FUNC_REVERSE_SUBTRACT,
                                          GL_FUNC_REVERSE_SUBTRACT,
                                          GL_DST_COLOR,
                                          GL_ONE,
                                          GL_ONE,
                                          GL_ONE};

const BlendMode agl::blendModes[] = {
    BlendMode::alpha,    BlendMode::add,    BlendMode::subtract,
    BlendMode::multiply, BlendMode::screen, BlendMode::shadow,
};
