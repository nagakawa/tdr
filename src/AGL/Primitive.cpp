#include "AGL/Primitive.h"

#include <optional>

#include "AGL/embedShader.h"

EMBED_SHADER(prim_vertex_source, primVertex, static)
EMBED_SHADER(prim_fragment_source, primFragment, static)

namespace agl {

  static thread_local std::optional<Shader> primitiveVertex, primitiveFragment;

  /*
    This function should be static but Clang warns about an unneeded
    declaration if it is.
  */
  /*static*/ void initPrimShaders() {
    if (primitiveVertex.has_value()) return;
    primitiveVertex.emplace(
        primVertexSource, primVertexSourceSize, GL_VERTEX_SHADER);
    primitiveFragment.emplace(
        primFragmentSource, primFragmentSourceSize, GL_FRAGMENT_SHADER);
  }

  template<bool useElems> void Primitive<useElems>::setUp() {
    initPrimShaders();
    program.attach(*primitiveVertex);
    program.attach(*primitiveFragment);
    program.link();
    hasInitialisedProgram = true;
    vao.setActive();
    vbo.setActive();
    if constexpr (useElems) this->ebo.setActive();
      // Don't feed data yet, but specify vertex attributes
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 4, GL_FLOAT, false, sizeof(Vertex),
        (void*) offsetof(Vertex, colour));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, 3, GL_FLOAT, false, sizeof(Vertex), (void*) offsetof(Vertex, pos));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        2, 2, GL_FLOAT, false, sizeof(Vertex),
        (void*) offsetof(Vertex, texcoords));
#pragma GCC diagnostic pop
    resetVAO();
  }

  template<bool useElems>
  void Primitive<useElems>::setTexture(WeakTexture tex) {
    if (texture.getID() == tex.getID() && hasSetUniforms) return;
    vao.setActive();
    program.use();
    tex.bindTo(0);
    texture = tex;
    program.uniform1i("tex", 0);
    program.uniform2f(
        "texDimensions", {(GLfloat) tex.getWidth(), (GLfloat) tex.getHeight()});
    if (!hasDeducedScreenDimensions) {
      hasDeducedScreenDimensions = true;
      if (app == nullptr) {
        glGetIntegerv(GL_VIEWPORT, vp);
      } else {
        vp[2] = app->getWidth();
        vp[3] = app->getHeight();
      }
    }
    program.uniform2f("screenDimensions", {(GLfloat) vp[2], (GLfloat) vp[3]});
    hasSetUniforms = true;
  }

  template<bool useElems> void Primitive<useElems>::update() {
    vbo.feedData(
        vertices.size() * sizeof(Vertex), vertices.data(), GL_DYNAMIC_DRAW);
    if constexpr (useElems) {
      this->ebo.feedData(
          this->elements.size() * sizeof(uint32_t), this->elements.data(),
          GL_DYNAMIC_DRAW);
    }
  }

  template<bool useElems> void Primitive<useElems>::render() {
    if (!hasInitialisedProgram) setUp();
    if (texture.getID() == 0) {
      initWhiteTexture();
      texture = whiteTexture.weak();
    }
    vao.setActive();
    update();
    program.use();
    setTexture(texture);
    texture.bindTo(0);
    glEnable(GL_BLEND);
    bm.use();
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    if constexpr (useElems) {
      glDrawElements(
          mode, this->elements.size(), GL_UNSIGNED_INT, this->elements.data());
    } else {
      glDrawArrays(mode, 0, vertices.size());
    }
  }
  template void Primitive<true>::setUp();
  template void Primitive<false>::setUp();
  template void Primitive<true>::setTexture(WeakTexture tex);
  template void Primitive<false>::setTexture(WeakTexture tex);
  template void Primitive<true>::update();
  template void Primitive<false>::update();
  template void Primitive<true>::render();
  template void Primitive<false>::render();
}
