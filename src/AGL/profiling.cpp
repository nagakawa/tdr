#include "AGL/profiling.h"

#include <string.h>

#include <FileUtil/rwtype.h>

namespace agl {
  static ProfilerTrack defaultTracks[] = {
      {
          "Frame Time",
          ProfilerTrackType::duration,
      },
      {
          "Tick Time",
          ProfilerTrackType::duration,
      },
  };
  ProfilerOptions defaultProfilerOptions = {
      "AGL application",
      sizeof(defaultTracks) / sizeof(defaultTracks[0]),
      defaultTracks,
  };
  void ProfilerOptions::writeToFile(FILE* fh) const {
    /* local */ {
      size_t nChars = strlen(applicationName);
      futil::write16u(fh, nChars);
      fwrite(applicationName, nChars, 1, fh);
    }
    futil::write16u(fh, nTracks);
    for (size_t i = 0; i < nTracks; ++i) {
      const ProfilerTrack& track = tracks[i];
      size_t nChars = strlen(track.name);
      futil::write16u(fh, nChars);
      fwrite(track.name, nChars, 1, fh);
      fputc((uint8_t) track.type, fh);
    }
  }
}
