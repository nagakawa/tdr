#include "AGL/Sprite2D.h"

#include <memory>
#include <optional>

#include "AGL/embedShader.h"

EMBED_SHADER(s2d_vertex_source, s2dVertex, static)
EMBED_SHADER(s2d_fragment_source, s2dFragment, static)

namespace agl {

  static thread_local std::optional<Shader> sprite2DVertex, sprite2DFragment;

  static void initS2DShaders() {
    if (sprite2DVertex.has_value()) return;
    sprite2DVertex.emplace(
        s2dVertexSource, s2dVertexSourceSize, GL_VERTEX_SHADER);
    sprite2DFragment.emplace(
        s2dFragmentSource, s2dFragmentSourceSize, GL_FRAGMENT_SHADER);
  }

  Sprite2D::Sprite2D(WeakTexture t, Shader* customFragment) :
      app(nullptr), texture(t), hasSetUniforms(false) {
    setUp(customFragment);
  }

  void Sprite2D::setUp(Shader* customFragment) {
    initS2DShaders();
    program.attach(*sprite2DVertex);
    if (customFragment == nullptr)
      program.attach(*sprite2DFragment);
    else
      program.attach(*customFragment);
    program.link(); // There you go!
    vao.setActive();
    // Vertex data
    vbo.feedData(
        sizeof(rectangleVertices), (void*) rectangleVertices, GL_DYNAMIC_DRAW);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*) 0);
    // Instance data
    instanceVBO.setActive();
    instanceVBO.feedData(
        sprites.size() * sizeof(Sprite2DInfo), sprites.data(), GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        1, 4, GL_FLOAT, GL_FALSE, sizeof(Sprite2DInfo),
        (GLvoid*) offsetof(Sprite2DInfo, source));
    glVertexAttribPointer(
        2, 4, GL_FLOAT, GL_FALSE, sizeof(Sprite2DInfo),
        (GLvoid*) offsetof(Sprite2DInfo, dest));
    resetVBO();
    glVertexAttribDivisor(1, 1);
    glVertexAttribDivisor(2, 1);
#pragma GCC diagnostic pop
    resetVAO();
  }

  void Sprite2D::render() {
    assert(texture.getID() != 0);
    vao.setActive();
    program.use();
    setTexture(texture);
    texture.bindTo(0);
    glEnable(GL_BLEND);
    BlendMode::alpha.use();
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, sprites.size());
    resetVAO();
  }

  void Sprite2D::setTexture(WeakTexture tex) {
    if (texture.getID() == tex.getID() && hasSetUniforms) return;
    vao.setActive();
    program.use();
    tex.bindTo(0);
    texture = tex;
    program.uniform1i("tex", 0);
    program.uniform2f(
        "texDimensions", {(GLfloat) tex.getWidth(), (GLfloat) tex.getHeight()});
    if (!hasDeducedScreenDimensions) {
      hasDeducedScreenDimensions = true;
      vp = inferViewport(app);
    }
    program.uniform2f("screenDimensions", vp);
    hasSetUniforms = true;
  }

  void Sprite2D::update() {
    instanceVBO.feedData(
        sprites.size() * sizeof(Sprite2DInfo), sprites.data(), GL_DYNAMIC_DRAW);
  }

  int Sprite2D::addSprite(Sprite2DInfo loc) {
    int size = sprites.size();
    sprites.push_back(loc);
    update();
    return size;
  }

  glm::vec2 inferViewport(const GLFWApplication* app) {
    if (app == nullptr) {
      GLint vp[4];
      glGetIntegerv(GL_VIEWPORT, vp);
      return {(GLfloat) vp[2], (GLfloat) vp[3]};
    } else {
      return {(GLfloat) app->getWidth(), app->getHeight()};
    }
  }
}
