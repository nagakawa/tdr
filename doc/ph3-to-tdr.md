## Approximate Danmakufu PH3-to-TDR translation guide

Mostly for my own reference to see what needs to be added. Huge WIP.

### General

This guide assumes you already know C++ to a reasonable degree.

Instead of floating-point numbers, TDR uses fixed-point numbers for working with gameplay stuff:

* `kfp::s16_16` (15.16 with sign) for positions, velocities and such
* `kfp::frac32` (0.32 without sign) for angles (in fractions of a turn)
* `kfp::s2_30` (1.30 with sign) for expressing sines and cosines of an angle

TDR tends not to make anything global, so you have to explicitly reference an object in most cases.

The following variables are used in the TDR equivalents:

* `game` is an instance of `tdr::Game` (probably a subclass)
* `app` is an instance of `agl::GLFWApplication`; `tdr::Game` has a pointer to one named `app`
* `mixer` is an instance of `agl::Mixer`; call `GLFWApplication::getMixer()` to get a reference to one
* `bl` is the enemy bullet list (`game.getBulletList()`)
* `el` is the enemy list (`game.getEnemyList()`)

### The list

Key:

* `none yet`: not supported yet
* `none yet?`: not supported yet but would be trivial to add
* `none`: I don't intend to add this

| ph3 function | TDR function |
|-|-|
| `yield` | `kcr::yield()` [1] |
| calling a task | `kcr::spawn` |
| `power` (`^`) | none yet |
| `log`, `log10` | none yet |
| `cos`, `sin` | `kfp::sincos` |
| `tan` | none yet |
| `acos`, `asin`, `atan` | none yet |
| `atan2` | `kfp::rectp` |
| `rand` | `game.rng.next(min, max)` |
| `InstallFont` | use `agl::Font` constructor |
| `GetModuleDirectory` | `futil::getExeLocation()` |
| `GetReplayFps` | none yet? |
| `SetCommonData` | none yet? [2] |
| `GetCommonData` | none yet? [2] |
| `LoadSound` | `mixer.addSoundFromFile(...)` |
| `PlayBGM` | none yet? |
| `PlaySE` | `mixer.playSound(...)` |
| `GetVirtualKeyState` | `game.testVirtualKey2(key)` |
| `AddVirtualKey` | none yet? |
| `AddReplayTargetVirtualKey` | implicit upon adding a virtual key |
| `GetKeyState` | `game.testKey2(key)` |
| (anything to do with mice) | none |
| `LoadTexture` | use `agl::Texture` constructor |
| `RemoveTexture` | cause the `agl::Texture` object to be destroyed |
| `GetTextureWidth` | `texture.getWidth()` |
| `GetTextureHeight` | `texture.getHeight()` |
| `SetStgFrame` | pass appropriate parameters to `tdr::Game` constructor |
| `GetScore` | `game.gp.cd.score` |
| `AddScore` | `game.gp.cd.score += score;` |
| `GetGraze` | `game.gp.cd.graze` |
| `AddGraze` | `game.gp.cd.graze += graze;` |
| `GetPoint` | none |
| `AddPoint` | none |
| `IsReplay` | `game.isReplay()` |
| `GetPlayerObjectID` | `game.gp` |
| `SetPlayerSpeed` | `game.gp.setPlayerTraits(...)` |
| `SetPlayerLife` | `game.gp.cd.lives = lives;` |
| `SetPlayerSpell` | `game.gp.cd.bombs = bombs;` |
| `SetPlayerPower` | none |
| `SetPlayerInvincibilityFrame` | `gp.setInvincibilityFrames(frames)` |
| `SetPlayerDownStateFrame` | `game.gp.setPlayerTraits(...)` |
| `SetPlayerRebirthFrame` | `game.gp.setPlayerTraits(...)` |
| `SetPlayerRebirthLossFrame` | none yet |
| `SetPlayerAutoItemCollectLine` | none yet |
| `SetForbidPlayerShot` | none yet |
| `SetForbidPlayerSpell` | none yet |
| `GetPlayerX` | `game.gp.cd.hitbox.x` |
| `GetPlayerY` | `game.gp.cd.hitbox.y` |
| `GetPlayerState` | `game.gp.getState().flag` |
| `GetPlayerSpeed` | none yet? |
| `GetPlayerLife` | `game.gp.cd.lives` |
| `GetPlayerSpell` | `game.gp.cd.bombs` |
| `GetPlayerPower` | none |
| `GetPlayerInvincibilityFrame` | none yet? [3] |
| `GetPlayerDownStateFrame` | none yet? [3] |
| `GetPlayerRebirthFrame` | none yet? [3] |
| `GetAngleToPlayer` | none |
| `IsPermitPlayerShot` | none yet? |
| `IsPermitPlayerSpell` | `game.gp.canBomb()` |
| `IsPlayerLastSpellWait` | none yet |
| `IsPlayerSpellActive` | `game.gp.getBombFrames() > 0` |
| `GetPlayerID` | none yet |
| `GetPlayerReplayName` | none yet |
| `GetEnemyBossSceneObjectID` | none yet |
| `GetEnemyBossObjectID` | none yet |
| `GetAllEnemyID` | iterate with `el.forEach(callback)` |
| `LoadEnemyShotData`, `ReloadEnemyShotData` | `game.setEnemyShotsheet` [4] |
| `DeleteShotAll(TYPE_ALL, TYPE_IMMEDIATE)` | `bl.clear()` |
| `DeleteShotAll` | none yet |
| `DeleteShotInCircle` | none yet |
| `CreateShotA1` | `bl.createShotA1(...)` |
| `CreateShotA2` | none yet? |
| `CreateShotOA1` | none |
| `CreateShotB1` | none yet? |
| `CreateShotB2` | none yet? |
| `CreateShotOB1` | none |
| `CreateLooseLaserA1` | `bl.createLooseLaser(...)` |
| `CreateStraightLaserA1` | none yet |
| `CreateCurveLaserA1` | none yet |
| `SetShotIntersectionCircle` | none |
| `SetShotIntersectionLine` | none |
| `GetShotIdInCircleA1` | none yet |
| `GetShotIdInCircleA2` | none yet |
| `GetShotCount` | `bl.nBullets()` |
| `SetShotAutoDeleteClip` | none yet |
| `GetShotDataInfoA1` | `bl.shotsheet.getRectByID(id)` |
| `StartShotScript` | none |
| (item functionality) | none yet |
| `StartSlow` | none yet |
| `StopSlow` | none yet |
| `Obj_Delete` | `.markForDeletion()` for bullets and enemies |
| `Obj_IsDeleted` | `.isMarkedForDeletion()` for bullets and enemies [5] |
| `Obj_SetVisible` | none yet (maybe setting alpha to 0?) |
| `Obj_IsVisible` | none yet |
| `Obj_SetRenderPriority` | none |
| `Obj_SetRenderPriorityI` | none |
| `Obj_GetRenderPriority` | none |
| `Obj_GetRenderPriorityI` | none |
| `Obj_GetValue`, `Obj_GetValueD`, `Obj_SetValue`, `Obj_DeleteValue`, `Obj_IsValueExists`, `Obj_GetType` | none |
| (render functions) | varies – each object has its own interface |
| `ObjPrim_Create`... | see `agl::Primitive` |
| `ObjSprite2D_SetSourceRect`... | see `agl::Sprite2D` |
| `ObjSpriteList2D_SetSourceRect`... | see `agl::Sprite2D` |
| `ObjSprite3D_SetSourceRect`... | none yet |
| `ObjMesh_Create`... | none yet |
| `ObjText_Create`... | see `agl::XText` |


[1] Call this only inside a task.  
[2] Use this only for anything that needs to be saved at the start of each stage so replays can start from any stage (e.g. power, some gauge value).  
[3] Current frames to next state is `game.gp.getState().timeLeft`.  
[4] In TDR, only one shotsheet can be loaded for enemy shots at a time.  
[5] Note that many other methods cannot be called on bullet and enemy handles that refer to deleted entities.
