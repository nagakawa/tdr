# TDR changelog

## 0.2 14VIII2019

### documentation

The API docs have received a facelift. Some undocumented functionality has also been documented.

### agl

* Added the ability to specify drop shadow properties in `TextStyle`.
* Shaders can now be loaded from the SPIR-V format as well as from GLSL source (if your OpenGL supports it).
* Added actual methods to `ShaderProgram` to set uniforms.
  * These (as well as the old macros) are now implemented in terms of the `glProgramUniform*` functions (instead of `glUniform*` previously). This might break on boxes that don't support OpenGL 4.1 (though note that tdr requires 4.2 anyway). If there's enough demand, then I might provide a fallback for earlier versions.
* Shader uniform locations are now cached.
* Added sound panning in `Mixer`.
* `TextStyle::t` is now a `WeakTexture` instead of a `Texture*`.
* Some `BlendMode` constants are now spelt differently: instead of `BM_ALPHA`, use `BlendMode::alpha`.
* Likewise, `BMIndex` is now an enum class, and constants are spelt differently: instead of `BMIDX_ALPHA`, use `BMIndex::alpha`.
* `ScenedGLFWApplication` now stores only thunks to scenes until they are entered by default.

### tdr

#### great refactor of 2019

*See [this issue][spoony-restart] and [this one][gr2019-issue] for some of the rationales for doing this.*

Core gameplay logic is separated from the filthy impure matters such as input and rendering. In particular:

* Earlier versions had one class to derive from called `tdr::Game`. In 0.2, you instead derive from two classes: `tdr::GameModel` and `tdr::GameView`.
  * Both of the base classes take four template parameters: `GM` and `GV` are the names of the derived classes, `SD` is the type of the shared data for the scene, and `Msg` is the type of the message to pass from `GM` to `GV` (see below).
* The game view has only const access to the model.
* Likewise, the model cannot directly access the state of the game view. If the model wants to signal something to the view, it must use the `notify()` method to pass a message to the view.
* The model knows only about the current stage. All stage handling is done by the view.
* Now a faulty `myreset()` method in the view will usually cause only cosmetic bugs.
* Also see [the docs][game-model-info].

Likewise, other classes (see [this issue][gr2019-issue] for a list) were separated into pure and impure parts.

[spoony-restart]: [https://gitlab.com/nagakawa/tdr/issues/15]
[gr2019-issue]: [https://gitlab.com/nagakawa/tdr/issues/28]
[game-model-info]: [https://nagakawa.gitlab.io/tdr/classtdr_1_1GameModel.html#details]

#### new features

* Added some [utilities][moveutil-docs] for automating movement of things.
* Added the ability to change global shotsheet orientation.
* Added the ability to set the blend colour of a bullet (as opposed to only its alpha).
* Added a method to be called at the start of a boss attack.
* Added the ability to set a render-wise angular velocity of a shot in a shotsheet.
* Added the ability to specify animated shots in shotsheets.
* Added the ability to change incoming shot and spell damage multipliers of enemies. Equivalent to Danmakufu's [`ObjEnemy_SetDamageRate`][dnh-setdamagerate].

[moveutil-docs]: https://nagakawa.gitlab.io/tdr/moveutil_8h.html
[dnh-setdamagerate]: https://sparen.github.io/ph3tutorials/docs_object.html#fxn_ObjEnemy_SetDamageRate

#### changes

* `Bullets` now owns the shotsheet (vs. `BulletList` previously).
* Some properties such as texcoords from shotsheet and delay colour can only be set per-graphic, instead of per-bullet as previously.
* Lasers now spawn multiple items when erased.
  * The distance between the items can be configured.
* The mixer is now cleared when resetting.

## 1/τ @25V2019

1/τ ≈ 0.15915494309189535

Overall, partial 0.2 feature set, but before the great refactor.

### agl

* Added weak textures (`agl::WeakTexture`): these are non-owning OpenGL texture handles; use them instead of `agl::Texture*`.
* Added the notion of a mixer context. From the current docs:
  > A mixer can have one or more contexts, which store data about which sounds are playing therein. Only the current context is advanced by the mixer.
* Added support for stereo sounds.
* `agl::Mixer` in general has been optimised as well.
* Fixed a gradient artefact with long descenders or ascenders in `agl::XText`.
* `agl::Font` now has proper move methods.

### tdr

#### new features

* Add support for spawning enemies.
* Yes, bosses too!
* Players can now shoot bullets to hurt said enemies.
* Players can also use bombs^H^H^H^H^Hspellcards.
* Add support for spawning and collecting items.
* Add `BulletHandle::getDamage()` and `BulletHandle::setDamage()`. (These are ignored for bullets shot by enemies.)
* Add a simple particle manager, good enough for making a bit of eye candy.

#### changes

* `BulletList::createShotA1` and `BulletList::createLooseLaser` now take `tdr::PV` (aka `glm::tvec2<kfp::s16_16>`) for positions instead of separate x/y values.
* `Game::myreset()` is called when the game is first started.
* `tdr::Shotsheet` is now a special case of `tdr::TShotsheet`, which takes some type as a template parameter.
* Delay clouds are rendered for delayed bullets.
* Delayed bullets are not checked for collision.
* Laser hitboxes are now capsules (previously rectangles).
* `tdr::Shotsheet` no longer owns its texture.
* Shot alpha can now be specified.
* Traits of a player can now be changed on the fly.

#### removed

* `tdr::Bullets` used to provide an iterator that would produce `tdr::BulletHandle`s; this feature was rarely used and was removed.

## 0.1.5 @13I2019

Overall, some bug fixes and optimisations.

### agl

* Use `clock_nanosleep` instead of `nanosleep` for sleeping the game loop on POSIX systems.
* Some dependencies are now fetched by Conan.

### tdr

* More bullet methods to `tdr::BulletHandle` that were previously lost in the SoAisation.
* Bullets are immediately deleted when 'marked for deletion' instead of being deferred until they have no more references.
  * This means that it is no longer allowed to use most methods on a handle to a deleted bullet.
* `BulletList::spurt()` is faster on debug than in 0.1.
* `tdr::Shotsheet` is now backed by a `std::vector` of `Graphic`s (previously a `std::unordered_map<size_t, Graphic>`).
* TDRTest can read some config from a JSON file.
* `tdr::Game` stores only the state of the current stage. See [this commit][stage-info] for more details.

[stage-info]: https://gitlab.com/nagakawa/tdr/commit/c2820187cc54d54259e4f073974db2433245216e

## 0.1 @27XII2018

First named version. Basic shot and player functionality, pausing, replays, ending the game.

If you're curious, the first commit for this project was on [17IV2016][first-commit], when this was a Visual Studio project, mostly because I used [LearnOpenGL.com][learn-opengl] as a tutorial. From that time, this project took a bumpy road of over two years, involving:

* making the project cross-platform
* switching from fixed-point representations for bullet coordinates to floating-point and back to fixed-point, and then moving the fixed-point arithmetic stuff to a separate library
* fixing a bunch of mistakes I made early in development
* changing what TDR stood for

I've probably learnt a lot about C++ from developing this project.

[first-commit]: https://gitlab.com/nagakawa/tdr/commit/3c4078a853228b0596318eb2867a21638f8d88c6
[learn-opengl]: https://learnopengl.com/
