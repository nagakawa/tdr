#!/usr/bin/env python3

# Usage: ./execute-glsl-recipes.py <recipe file>

import os.path
import subprocess
import sys


def main():
    recipe = sys.argv[1]
    shader_dir = os.path.dirname(recipe)
    with open(recipe) as fh:
        for line in fh:
            dest, source, *opts = line.split()
            source_path = "{}/{}.glsl".format(shader_dir, source)
            dest_path = "{}/{}.glsl".format(shader_dir, dest)
            with open(dest_path, 'w') as dest_fh:
                status = subprocess.run(
                    ["glslc", "-E", source_path, *opts], stdout=dest_fh)
                if status.returncode != 0:
                    print("Failed to generate {}".format(dest), file=sys.stderr)
                    sys.exit(1)


main()
