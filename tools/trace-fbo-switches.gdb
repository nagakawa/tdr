break agl::FBO::setActive
command
silent
p this->id
backtrace
continue
end

break agl::FBO::setActiveNoViewport
command
silent
p this->id
backtrace
continue
end

break agl::FBO::blitTo
command
silent
p this->id
p other.id
backtrace
continue
end

run
