'use strict';

// thanks https://www.mulinblog.com/a-color-palette-optimized-for-data-visualization/
const chartColors = [
  "#5DA5DA",
  "#FAA43A",
  "#60BD68",
  "#F17CB0",
  "#B276B2",
  "#DECF3F",
  "#F15854",
  "#B2912F",
  "#4D4D4D",
];

function iota(n) {
  let a = new Array(n);
  for (let i = 0; i < n; ++i) a[i] = i;
  return a;
}

function readString16(buf, view, i) {
  // Returns [str, newI].
  let len = view.getUint16(i, true);
  let sl = buf.slice(i + 2, i + 2 + len);
  let str = Utf8ArrayToStr(new Uint8Array(sl));
  return [str, i + 2 + len];
}

function readData(buf, chart) {
  // Return an object that can be stored to myChart.data.
  let view = new DataView(buf);
  let i = 0;
  // From the (sloppily written) src/AGL/profiling.cpp:
  let temp = readString16(buf, view, i);
  i = temp[1];
  let applicationName = temp[0];
  let nTracks = view.getUint16(i, true);
  i += 2;
  let tracks = [];
  for (let j = 0; j < nTracks; ++j) {
    temp = readString16(buf, view, i);
    i = temp[1];
    let trackName = temp[0];
    let trackType = view.getUint8(i);
    i += 1;
    let ds = {
      label: trackName,
      data: [],
      lineTension: 0,
      pointRadius: 0,
      borderColor: chartColors[j % chartColors.length],
      backgroundColor: chartColors[j % chartColors.length] + "20",
      fill: false,
      trackType: trackType,
      yAxisID: ["time", "mode", "count"][trackType],
    };
    tracks.push(ds);
  }
  // Finished reading metadata.
  let remaining = buf.byteLength - i;
  let array = new Float64Array(buf.slice(i, i + (remaining & ~7)));
  for (let j = 0; j < array.length / nTracks; ++j) {
    for (let k = 0; k < nTracks; ++k) {
      tracks[k].data.push(array[j * nTracks + k]);
    }
  }
  for (let track of tracks) {
    if (track.trackType == 0) {
      for (let j = 0; j < track.data.length; ++j) {
        track.data[j] *= 1000; // get ms instead
      }
    }
  }
  chart.data = {
    labels: iota(Math.floor(array.length / nTracks)),
    datasets: tracks,
  };
  chart.options.title = {
    display: true,
    text: "Profile data for " + applicationName,
    fontSize: 18,
  };
}

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'line',
  data: {},
  options: {
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Tick#",
        },
        ticks: {
          beginAtZero: true,
        }
      }],
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "Time (ms)",
          },
          ticks: {
            beginAtZero: true
          },
          id: "time",
        },
        {
          scaleLabel: {
            display: true,
            labelString: "Count",
          },
          ticks: {
            beginAtZero: true
          },
          id: "count",
        },
      ]
    },
    tooltips: {
      mode: 'index',
      intersect: false,
      callbacks: {
        title: function (tooltipItems, data) {
          var title = '';
          var labels = data.labels;
          var labelCount = labels ? labels.length : 0;
          if (tooltipItems.length > 0) {
            var item = tooltipItems[0];
            if (item.xLabel) {
              title = item.xLabel;
            } else if (labelCount > 0 && item.index < labelCount) {
              title = labels[item.index];
            }
          }
          return "Frame " + title;
        },
        label: function (tooltipItem, data) {
          let ds = data.datasets[tooltipItem.datasetIndex];
          console.log(ds);
          let label = ds.label || '';
          if (label) {
            label += ': ';
          }
          if (ds.trackType == 0) {
            label += Math.round(tooltipItem.yLabel * 100) / 100;
            label += ' ms';
          } else {
            label += tooltipItem.yLabel;
          }
          return label;
        }
      },
    }
  }
});

function handleFiles(files) {
  let file = files[0];
  let reader = new FileReader();
  reader.onload = function (ev) {
    readData(ev.target.result, myChart);
    myChart.update();
  };
  reader.readAsArrayBuffer(file);
}